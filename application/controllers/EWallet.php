<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//require APPPATH.'/libraries/REST_Controller.php';

class EWallet extends CI_Controller {
//class eWallet extends REST_Controller {
    public function __Construct()
    {
   	   parent::__Construct();
       // constructor code
	   
		$CI =& get_instance();
		$this->domain = $CI->config->item('domain');
		$this->baseurl = $CI->config->item('base_url');
		$this->show_debug = $CI->config->item('show_debug');
		$this->basepath = $CI->config->item('base_url').$CI->config->item('index_page');
		
		$this->load->helper('url');
		
		$this->load->library('curl');  
		$this->load->helper('form');
		
		$this->load->model('mem_ewallet/Mewallet');
		
    }
	
	
	public function index(){
		echo $this->security->get_csrf_token_name();
		echo '<br>'.$this->security->get_csrf_hash();
	}
	
	public function test_server(){
		$username = 'admin';  
		$password = 'cerebro1'; 
		$result=$this->Mewallet->getBalanceEwallet_member("","2019-04-02");
		var_dump($result);
		/*		
		$this->curl->create('http://172.20.121.37/sap_api/index.php/api/coba');  
		// Optional, delete this line if your API is open  
		$login=$this->curl->http_login($username, $password);
		if ($login){
			echo 'Success';
		}else{
			echo 'Fail';
		}
		*/
	}
	
	public function post_ewallet($type = NULL, $fromdate = NULL, $todate = NULL){

		// $username = 'SGH_VENDA1';//'admin';  
		// $password = 'Equine.1';//'cerebro1'; 
		
		// $username = 'sgh_sys01';  
		// $password = 'PKPBo490'; 
		
		$username = 'sgh_ict03';  
		$password = 'golive0518'; 
		
		//$url='http://sapdrsedad.sohogroup.com:8010/sap/bc/yfi_uhn_ewa?sap-client=140';	
		
		// STAGING
		$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=fiewallet';
		// PROD
		// $url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=300/inbound&module=fiewallet';

		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
		$loginCek=$this->curl->http_login($username, $password);

		if ($loginCek){		

			if(empty($fromdate)){

				$date = date('m-d-Y');
				$date1 = str_replace('-', '/', $date);
				$fromdate = date('Y-m-d',strtotime($date1 . "-1 days"));

			}
	
			if(empty($todate)){
				
				$todate = date('Y-m-d');				
			
			}
			
			//if ($this->uri->segment(3)=='mem'){
			if ($type=='mem'){

				$result=$this->Mewallet->getBalanceEwallet_member("","$fromdate","$todate");

			}else{			

				$result=$this->Mewallet->getBalanceEwalletDeposit("","$fromdate","$todate");
				
			}
			
			//var_dump($result); die();
			
			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				// echo $conArray;
				//Metode curl ci
				//$result=$this->curl->post($dtpost);
				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				//$result = $this->curl->execute();

				//var_dump($resultCurl[10]); die();
				
				if($resultCurl){
					echo 'Pengiriman Data .. '.count($result).' Data Telah Terkirim'; //$resultCurl;
					echo $resultCurl;
					if($resultCurl[10]=="S"){

						if ($type=='mem'){
							$this->Mewallet->updateBalanceEwallet_member("","$fromdate","$todate");
						}else{
							$this->Mewallet->updateBalanceEwalletDeposit("","$fromdate","$todate");
						}

						echo '<br> STATUS : BERHASIL';	

					}else{
						echo '<br> STATUS : GAGAL';	
					}
					//log_message('INFO', 'Test process : '.$resultCurl);
					$this->Mewallet->writeLogApi("POST Sinkronisasi API Stockist",'Mengirim : '.count($result).' Transaksi, data telah terproses, Result = '.$resultCurl);
				}else{
					echo 'Gagal ';
					//log_message('INFO', 'Test process : Gagal Proses');
					echo $resultCurl;
					$this->Mewallet->writeLogApi("POST Sinkronisasi API Stockist",'Gagal Mengirim : '.count($result).' Transaksi, gagal terproses, Result = '.$resultCurl);
				}

				header('Content-Type: application/json');
				echo json_encode($result, JSON_PRETTY_PRINT);


			}else{
				echo 'Hari Ini : '.count($result).' Transaksi, data tidak dapat di proses karena tidak ada transaksi';
				$this->Mewallet->writeLogApi("POST Sinkronisasi API Stockist",'Hari Ini : '.count($result).' Transaksi, data tidak dapat di proses karena tidak ada transaksi');
			}
						
		}else{
			echo 'Failed Connect To Server';
			$this->Mewallet->writeLogApi("Sinkronisasi API","Gagal Melakukan Otorisasi Koneksi Ke Server, Password atau Username salah");
		}
		
	}

	public function ajax_post_ewallet_mem(){
	
		// STAGING
		$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=fiewallet';
		// PROD
		// $url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=300/inbound&module=fiewallet';

		$this->curl->create($url);  
		// Optional, delete this line if your API is open  

		$username = 'sgh_ict03';  
		$password = 'golive0518'; 

		$loginCek=$this->curl->http_login($username, $password);
		if ($loginCek){	

			$data_arr = $this->input->post('data');
			//$data_arr = array("314258","314259","314264");

			$array_id = implode(',', $data_arr);					
			
			$result = $this->Mewallet->get_data_ewallet_mem_by_array_id($array_id);

			//var_dump($result); die();

			if (count($result)>0){

				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);

				//Metode curl ci
				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				
				if($resultCurl){

					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';

					if($resultCurl[29]=="S"){
						echo '<br> STATUS : BERHASIL';
						$this->Mewallet->update_data_ewallet_mem_by_array_id($array_id);		
					}else{
						echo '<br> STATUS : GAGAL';	
					}
					
					echo json_encode($varSuc);					
					
				}else{
					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo json_encode($varSuc);	
				}
				
				$this->Mewallet->writeLogApi("POST Sinkronisasi API Member",'Mengirim : '.count($result).' Transaksi, data telah terproses, Result = '.$resultCurl);


			}else{
				echo json_encode('gagal');
			}
		}else{
			echo json_encode('Failed Connect To API Server');	
		}
		
	}

	public function ajax_post_ewallet_stc(){
	
		// STAGING
		$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=fiewallet';
		// PROD
		// $url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=300/inbound&module=fiewallet';

		$this->curl->create($url);  
		// Optional, delete this line if your API is open  

		$username = 'sgh_ict03';  
		$password = 'golive0518'; 

		$loginCek=$this->curl->http_login($username, $password);
		if ($loginCek){	

			$data_arr = $this->input->post('data');
			//$data_arr = array("49093","49098","49100","49104","49104");

			$array_id = implode(',', $data_arr);					
			
			$result = $this->Mewallet->get_data_ewallet_stc_by_array_id($array_id);

			//var_dump($result); die();

			if (count($result)>0){

				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);

				//Metode curl ci
				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				
				if($resultCurl){

					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';

					//echo json_encode($varSuc);
					$this->Mewallet->update_data_ewallet_stc_by_array_id($array_id);	
					
					echo json_encode($varSuc);					
					
				}else{
					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo json_encode($varSuc);	
				}
				
				$this->Mewallet->writeLogApi("POST Sinkronisasi API Stockist",'Mengirim : '.count($result).' Transaksi, data telah terproses, Result = '.$resultCurl);

			}else{
				echo json_encode('gagal');
			}
		}else{
			echo json_encode('Failed Connect To API Server');	
		}
		
	}

	public function post_ewallet_csv(){
		/*
		$result=$this->Mewallet->getBalanceEwallet("","2010-04-20","2010-04-20");
		header('Content-Type: application/json');
		echo json_encode($result);
		*/
		/*
		$username = 'SGH_VENDA1';//'admin';  
		$password = 'Equine.1';//'cerebro1'; 
		*/
		
		// $username = 'sgh_sys01';  
		// $password = 'PKPBo490'; 
		
		$username = 'sgh_ict03';  
		$password = 'golive0518'; 
		
		//$url='http://sapdrsedad.sohogroup.com:8010/sap/bc/yfi_uhn_ewa?sap-client=140';	
		
		// STAGING
		$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=fiewallet';
		
		// PROD
		// $url='xhttp://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=300/inbound&module=fiewallet';
		
		//$url='http://172.20.121.37/sap_api/index.php/api/coba/insertTesth';
		//$url='http://localhost/sohomlm_api/api/ewallet/user_post';
//		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
//		$loginCek=$this->curl->http_login($username, $password);
//		if ($loginCek){		
			$date = date('m-d-Y');
			$date1 = str_replace('-', '/', $date);
			$tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));

			//echo $tomorrow; 
			//echo $this->uri->segment(3);
			
			if ($this->uri->segment(3)=='mem'){
				//$result=$this->Mewallet->getBalanceEwallet_member("","2009-07-14","2009-07-15");
				//$result=$this->Mewallet->getBalanceEwallet_member("",date("Y-m-d"),$tomorrow);
				
				//$result=$this->Mewallet->getBalanceEwallet_member("","2019-04-02");
				//$result=$this->Mewallet->getBalanceEwallet_member("","2019-05-01","2019-05-21");
				//$result=$this->Mewallet->getBalanceEwallet_member("","2019-05-22","2019-05-31");
				//$result=$this->Mewallet->getBalanceEwallet_member("","2019-06-01","2019-06-04");
				//$result=$this->Mewallet->getBalanceEwallet_member("","2019-06-05","2019-06-30");
				//$result=$this->Mewallet->getBalanceEwallet_member("","2019-07-01","2019-07-10");
				//$result=$this->Mewallet->getBalanceEwallet_member("","2019-07-11","2019-07-21");
				//$result=$this->Mewallet->getBalanceEwallet_member("","2019-07-22","2019-08-03");
				$result=$this->Mewallet->getBalanceEwallet_member("","2019-08-04","2019-08-19");
			}else{
				//$result=$this->Mewallet->getBalanceEwallet("","2010-04-20","2010-04-21");
				//$result=$this->Mewallet->getBalanceEwallet("",date("Y-m-d"),$tomorrow);
				//$result=$this->Mewallet->getBalanceEwallet("","2019-03-31","2019-04-01");
				//$result=$this->Mewallet->getBalanceEwallet("","2019-03-31","2019-04-01");
				
				//$result=$this->Mewallet->getBalanceEwallet("","2019-04-02");
				//$result=$this->Mewallet->getBalanceEwallet("","2019-05-01","2019-05-21");
				//$result=$this->Mewallet->getBalanceEwallet("","2019-05-22","2019-05-31");
				//$result=$this->Mewallet->getBalanceEwallet("","2019-06-01","2019-06-04");
				//$result=$this->Mewallet->getBalanceEwallet("","2019-06-05","2019-06-30");
				//$result=$this->Mewallet->getBalanceEwallet("","2019-07-01","2019-07-10");
				//$result=$this->Mewallet->getBalanceEwallet("","2019-07-11","2019-07-21");
				//$result=$this->Mewallet->getBalanceEwallet("","2019-07-22","2019-08-03");

				$result=$this->Mewallet->getBalanceEwallet("","2020-02-01","2020-02-05");
			}
			//var_dump($result);
			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				echo $conArray;
				//Metode curl ci
				//$result=$this->curl->post($dtpost);
//				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				//$result = $this->curl->execute();
/*				
				if($resultCurl){
					echo 'Pengiriman Data .. '.count($result).' Data Telah Terkirim'; //$resultCurl;
					echo $resultCurl;
					//log_message('INFO', 'Test process : '.$resultCurl);
					$this->Mewallet->writeLogApi("POST Sinkronisasi API Stockist",'Mengirim : '.count($result).' Transaksi, data telah terproses, Result = '.$resultCurl);
				}else{
					echo 'Gagal ';
					//log_message('INFO', 'Test process : Gagal Proses');
					echo $resultCurl;
					$this->Mewallet->writeLogApi("POST Sinkronisasi API Stockist",'Gagal Mengirim : '.count($result).' Transaksi, gagal terproses, Result = '.$resultCurl);
				}
*/

				header('Content-Type: application/json');
				echo json_encode($result, JSON_PRETTY_PRINT);

/*
			}else{
				echo 'Hari Ini : '.count($result).' Transaksi, data tidak dapat di proses karena tidak ada transaksi';
				$this->Mewallet->writeLogApi("POST Sinkronisasi API Stockist",'Hari Ini : '.count($result).' Transaksi, data tidak dapat di proses karena tidak ada transaksi');
			}
*/
			/*
			// Native PHP code for Curl
			$curl_handle = curl_init();
			curl_setopt($curl_handle, CURLOPT_URL, $url);
			curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl_handle, CURLOPT_POST, 1);
			curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $dtpost);
			 
			//Optional, delete this line if your API is open
			curl_setopt($curl_handle, CURLOPT_USERPWD, $username . ':' . $password);
			 
			$buffer = curl_exec($curl_handle);
			curl_close($curl_handle);
			
			/*
			$json = json_decode($buffer,true);
			//echo $url.' From Offline -- '.$buffer;
			if ($json){
				echo 'Success POST Data to : '.$url.'<br>'.$buffer;
			}else{
				echo 'Failed POST Data';
			}
			*/
			//echo $buffer;
			
						
		}else{
			echo 'Failed Connect To Server';
			$this->Mewallet->writeLogApi("Sinkronisasi API","Gagal Melakukan Otorisasi Koneksi Ke Server, Password atau Username salah");
		}
		
		
	}
	
	public function post_ewallet_all(){
		
		$resSync=array();
		
		$username = 'admin';  
		$password = 'cerebro1'; 
			
		$url='http://172.20.121.37/sap_api/index.php/api/coba/insertTesth';
		//$url='http://localhost/sohomlm_api/api/ewallet/user_post';
		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
		$loginCek=$this->curl->http_login($username, $password);
		if ($loginCek){		
			$date = date('m-d-Y');
			$date1 = str_replace('-', '/', $date);
			$tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));
			$varSuc='Informasi :';
			
			// Proses 1. Ambil data Member
			$result=$this->Mewallet->getBalanceEwallet_member("",date("Y-m-d"),$tomorrow);
			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				//$result = $this->curl->execute();
				if($resultCurl){
					$varSuc=$varSuc.' '.count($result).' Data Transaksi Member berhasil Terkirim.';
					//log_message('INFO', 'Test process : '.$resultCurl);
				}else{
					$varSuc=$varSuc.' Data Transaksi Member GAGAL Terkirim.';
					//log_message('INFO', 'Test process : Gagal Proses');
					
				}	
			}else{
				$varSuc=$varSuc.' Data MEMBER tidak dapat di proses karena tidak ada transaksi yang terjadi pada hari ini... ';
			}
			
			// Proses 2. Ambil data Stokiest
			$result=$this->Mewallet->getBalanceEwallet("",date("Y-m-d"),$tomorrow);
			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				//$result = $this->curl->execute();
				if($resultCurl){
					$varSuc=$varSuc.' '.count($result).' Data Transaksi Stockist berhasil Terkirim.';
					//log_message('INFO', 'Test process : '.$resultCurl);
					$resSync[]=$resultCurl;
				}else{
					$varSuc=$varSuc.' Data Transaksi Member STOCKIST Terkirim.';
					//log_message('INFO', 'Test process : Proses Syncronisasi Gagal');
					$cRow=array();
					$cRow['id']='1';
					$cRow['data'][]=$varSuc;
					$resSync[]=$cRow;
				}	
			}else{
				$varSuc=$varSuc.' Data Transaksi STOCKIST tidak dapat di proses karena tidak ada transaksi untuk stockist yang terjadi hari ini';
				$cRow=array();
				$cRow['id']='1';
				$cRow['data'][]=$varSuc;
				$resSync[]=$cRow;
			}
			
			$this->Mewallet->writeLogApi("POST Sinkronisasi API ",$varSuc);
			
			
			
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='Authorize Failed, Cannot Connect To Server';
			$resSync[]=$cRow;
			$this->Mewallet->writeLogApi("POST Sinkronisasi API ",'Authorize Failed, Cannot Connect To Server');
		}
		
		header('Content-Type: application/json');
		//echo json_encode($resSync);
		echo json_encode($resSync, JSON_PRETTY_PRINT);
	}
	
	public function stc(){
		/*
		$userName=$this->input->post("txtUserName");
		$periode1=$this->input->post("txtMulai");
		$periode2=$this->input->post("txtSelesai");
		
		if (empty($userName)){
			echo 'Username Kosong';
		}else if (empty($periode1)){
			
		}
		
		echo $this->input->post("txtUserName").' <br> '.$periode1.' - '.$periode2;
		*/
		
		//$result=$this->Mewallet->getBalanceEwallet("",$periode1,$periode2);
		//header('Content-Type: application/json');
		//echo json_encode($result);
		
		
		if (empty($this->input->post("txtUserName"))){
			echo 'Username Harus Di Isi';
		}else if (empty($this->input->post("txtPassword"))){
			echo 'Password Harus Di Isi';
		}else if (empty($this->input->post("txtMulai"))){
			echo 'Periode Awal Harus Di Isi';
		}else if (empty($this->input->post("txtSelesai"))){
			echo 'Periode Akhir Harus Di Isi';
		}else if (empty($this->input->post("txtURL"))){
			echo 'API Url Harus Di Isi';
		}else{
			$periode1=$this->input->post("txtMulai");
			$periode2=$this->input->post("txtSelesai");
			
			$this->curl->create($this->input->post("txtURL"));  
			// Optional, delete this line if your API is open  
			$loginCek=$this->curl->http_login($this->input->post("txtUserName"), $this->input->post("txtPassword"));
			if ($loginCek){		

				$result=$this->Mewallet->getBalanceEwallet("",$periode1,$periode2);
				header('Content-Type: application/json');
				//echo json_encode($result);
				echo json_encode($result, JSON_PRETTY_PRINT);
			
				$this->Mewallet->writeLogApi("View Sinkronisasi API Stockist","Berhasil menampilkan data Member pada ".date("Y-m-d H:m:s"));
			}else{
				echo 'Failed Connect To Server';
				$this->Mewallet->writeLogApi("Sinkronisasi API","Gagal Melakukan Otorisasi Koneksi Ke Server, Password atau Username salah");
			}
		}
		
		
		
	}
	
	public function mem(){
		/* Harus jeda 1 hari */
		/*
		$result=$this->Mewallet->getBalanceEwallet_member("","2009-07-14","2009-07-15");
		header('Content-Type: application/json');
		//echo json_encode($result);
		echo json_encode($result, JSON_PRETTY_PRINT); //Tampilannya enak sayang load halamannya lebih lama..
		*/
		
		if (empty($this->input->post("txtUserName"))){
			echo 'Username Harus Di Isi';
		}else if (empty($this->input->post("txtPassword"))){
			echo 'Password Harus Di Isi';
		}else if (empty($this->input->post("txtMulai"))){
			echo 'Periode Awal Harus Di Isi';
		}else if (empty($this->input->post("txtSelesai"))){
			echo 'Periode Akhir Harus Di Isi';
		}else if (empty($this->input->post("txtURL"))){
			echo 'API Url Harus Di Isi';
		}else{
			$periode1=$this->input->post("txtMulai");
			$periode2=$this->input->post("txtSelesai");
			
			//echo $periode1.' - '.$periode2;
			$this->curl->create($this->input->post("txtURL"));
			$loginCek=$this->curl->http_login($this->input->post("txtUserName"), $this->input->post("txtPassword"));
			if ($loginCek){		
				$result=$this->Mewallet->getBalanceEwallet_member("",$periode1,$periode2);
				header('Content-Type: application/json');
				//echo json_encode($result);
				echo json_encode($result, JSON_PRETTY_PRINT);
				
				$this->Mewallet->writeLogApi("View Sinkronisasi API Member","Berhasil menampilkan data Member pada ".date("Y-m-d H:m:s"));
			}else{
				echo 'Failed Connect To Server';
				$this->Mewallet->writeLogApi("Sinkronisasi API","Gagal Melakukan Otorisasi Koneksi Ke Server, Password atau Username salah");
			}
		}
		
	}
	
	
	
}
?>
