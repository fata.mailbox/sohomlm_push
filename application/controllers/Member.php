<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';

class Member extends CI_Controller {
    public function __Construct()
    {
   	   parent::__Construct();
        // constructor code
	    /*
		$CI =& get_instance();
		$this->domain = $CI->config->item('domain');
		$this->baseurl = $CI->config->item('base_url');
		$this->show_debug = $CI->config->item('show_debug');
		$this->basepath = $CI->config->item('base_url').$CI->config->item('index_page');
		$this->load->helper('url');
		$this->load->library('session');
		*/
		
		$this->load->model('mem_soho/MAuth');
		$this->load->library('curl');  
		
    }
	
	
	public function index(){
		$config['per_page'] = 20;
        $config['uri_segment'] = 3;
		
		$member=$this->MAuth->searchUser("",$config['per_page'],$this->uri->segment($config['uri_segment']));
		
		header('Content-Type: application/json');
		echo json_encode($member);
	}
	
	public function testajax(){
		/*
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST');  
		header('Access-Control-Allow-Headers: Content-Type');  
		*/
		//$datax=$this->input->post();
		//header('Content-Type: application/json');
		//echo json_encode($data);
		//print_r ($datax);
		
		
		header('Content-Type: application/json');
		echo 'Nilai Postnya : '.$_POST['txtURL'].' - '.$this->input->post("txtUserName");
		
		
	}
	
}
?>
