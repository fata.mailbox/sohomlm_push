<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//require APPPATH.'/libraries/REST_Controller.php';

class Fico extends CI_Controller {
//class eWallet extends REST_Controller {
    public function __Construct()
    {
   	   parent::__Construct();
       // constructor code
	   
		$CI =& get_instance();
		$this->domain = $CI->config->item('domain');
		$this->baseurl = $CI->config->item('base_url');
		$this->show_debug = $CI->config->item('show_debug');
		$this->basepath = $CI->config->item('base_url').$CI->config->item('index_page');
		
		$this->load->helper('url');
		
		$this->load->library('curl');  
		$this->load->helper('form');
		
		$this->load->model('fico/MFico');
		
    }
	
	
	public function index(){
		/*
		echo $this->security->get_csrf_token_name();
		echo '<br>'.$this->security->get_csrf_hash();
		*/
	}
	
	public function test_server(){
		$username = 'admin';  
		$password = 'cerebro1'; 
		
		$this->curl->create('http://172.20.121.37/sap_api/index.php/api/coba');  
		// Optional, delete this line if your API is open  
		$login=$this->curl->http_login($username, $password);
		if ($login){
			echo 'Success';
		}else{
			echo 'Fail';
		}
		
	}
	
	public function post_ewallet(){
		/*
		Untuk Kirim Data Secara partial URL :
		Member/STAFF	: http://localhost/sohomlm_push/fico/post_ewallet/mem 
		Stockiest 		: http://localhost/sohomlm_push/fico/post_ewallet/stc
		*/
		 
		$username = 'admin';  
		$password = 'cerebro1'; 
			
		$url='http://172.20.121.37/sap_api/index.php/api/coba/insertTesth';
		//$url='http://localhost/sohomlm_api/api/ewallet/user_post';
		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
		$loginCek=$this->curl->http_login($username, $password);
		if ($loginCek){
			
			$date = date('m-d-Y');
			$date1 = str_replace('-', '/', $date);
			$tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));
			$startdate=date("Y-m-d");
			if (!empty($this->input->post("txtMulai"))){
				$startdate=$this->input->post("txtMulai");
			}else if (!empty($this->input->post("txtSelesai"))){
				$tomorrow =$this->input->post("txtSelesai");
			}
			
			
			if ($this->uri->segment(3)=='mem'){
				//$result=$this->MFico->getBalanceEwallet_member("","2009-07-14","2009-07-15");
				$result=$this->MFico->getBalanceEwallet_member("",$startdate,$tomorrow);
			}else{
				//$result=$this->MFico->getBalanceEwallet("","2010-04-20","2010-04-21");
				$result=$this->MFico->getBalanceEwallet("",$startdate,$tomorrow);
				
			}
			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				//Metode curl ci
				//$result=$this->curl->post($dtpost);
				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				//$result = $this->curl->execute();
				if($resultCurl){
					echo 'Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim'; //$resultCurl;
					//log_message('INFO', 'Test process : '.$resultCurl);
					$data_log_api=array(
						'log_api_tgl' => date("Y-m-d H:i:s"),
						'log_api_ket' => 'Mengirim : '.count($result).' Transaksi, data Transaksi telah terproses',
						'log_api_kategori' => 'POST Sinkronisasi API Stockist Ke SAP', 
						'log_api_modul' => 'FICO', 
						'log_api_url' => $url, 
						'log_api_status' => '1', 
						'log_api_type' => 'PUSH' 
					);
					$this->MFico->writeLogApi($data_log_api);
				}else{
					echo 'Gagal ';
					//log_message('INFO', 'Test process : Gagal Proses');
					$data_log_api=array(
						'log_api_tgl' => date("Y-m-d H:i:s"),
						'log_api_ket' => 'Gagal Mengirim : '.count($result).' Transaksi. Suncronisasi Gagal',
						'log_api_kategori' => 'POST Sinkronisasi API Stockist Ke SAP', 
						'log_api_modul' => 'FICO', 
						'log_api_url' => $url, 
						'log_api_status' => '1', 
						'log_api_type' => 'PUSH' 
					);
					$this->MFico->writeLogApi($data_log_api);
				}
				
				
				
			}else{
				$data_log_api=array(
					'log_api_tgl' => date("Y-m-d H:i:s"),
					'log_api_ket' => 'Hari Ini : '.count($result).' Transaksi, data tidak dapat di proses karena tidak ada transaksi',
					'log_api_kategori' => 'POST Sinkronisasi API Stockist Ke SAP', 
					'log_api_modul' => 'FICO', 
					'log_api_url' => $url, 
					'log_api_status' => '0', 
					'log_api_type' => 'PUSH' 
				);
				$this->MFico->writeLogApi($data_log_api);
				echo 'Hari Ini : '.count($result).' Transaksi, data tidak dapat di proses karena tidak ada transaksi';
			}
			
			/*
			// Native PHP code for Curl
			$curl_handle = curl_init();
			curl_setopt($curl_handle, CURLOPT_URL, $url);
			curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl_handle, CURLOPT_POST, 1);
			curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $dtpost);
			 
			//Optional, delete this line if your API is open
			curl_setopt($curl_handle, CURLOPT_USERPWD, $username . ':' . $password);
			 
			$buffer = curl_exec($curl_handle);
			curl_close($curl_handle);
			
			/*
			$json = json_decode($buffer,true);
			//echo $url.' From Offline -- '.$buffer;
			if ($json){
				echo 'Success POST Data to : '.$url.'<br>'.$buffer;
			}else{
				echo 'Failed POST Data';
			}
			*/
			//echo $buffer;
			
						
		}else{
			$data_log_api=array(
				'log_api_tgl' => date("Y-m-d H:i:s"),
				'log_api_ket' => 'Gagal Melakukan Otorisasi Koneksi Ke Server, Password atau Username salah',
				'log_api_kategori' => 'POST Sinkronisasi API Stockist Ke SAP', 
				'log_api_modul' => 'FICO', 
				'log_api_url' => $url, 
				'log_api_status' => '0', 
				'log_api_type' => 'PUSH' 
			);
			$this->MFico->writeLogApi($data_log_api);
			echo 'Failed Connect To Server';
			
		}
		
		
	}
	
	public function post_ewallet_all(){
		/*
		Untuk Kirim Data Secara Keseluruhan Antara Staff, Member dan Stokist, URL:
		http://localhost/sohomlm_push/fico/post_ewallet_all
		*/
		
		$resSync=array();
		$username = 'admin';  
		$password = 'cerebro1'; 
			
		$url='http://172.20.121.37/sap_api/index.php/api/coba/insertTesth';
		//$url='http://localhost/sohomlm_api/api/ewallet/user_post';
		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
		$loginCek=$this->curl->http_login($username, $password);
		if ($loginCek){		
			$date = date('m-d-Y');
			$date1 = str_replace('-', '/', $date);
			$tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));
			$startdate=date("Y-m-d");
			if (!empty($this->input->post("txtMulai"))){
				$startdate=$this->input->post("txtMulai");
			}else if (!empty($this->input->post("txtSelesai"))){
				$tomorrow =$this->input->post("txtSelesai");
			}
			$varSuc='Informasi :';
			
			// Proses 1. Ambil data Member
			$result=$this->MFico->getBalanceEwallet_member("",$startdate,$tomorrow);
			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10));
				//Other Method For POST curl..
				//$this->curl->post($dtpost);
				//$result =  $this->curl->execute();
				//$result = $this->curl->post($dtpost);
				if($resultCurl){
					$varSuc=$varSuc.' '.count($result).' Data Transaksi Member berhasil Terkirim.';
					//log_message('INFO', 'Test process : '.$resultCurl);
				}else{
					$varSuc=$varSuc.' Data Transaksi Member GAGAL Terkirim.';
					//log_message('INFO', 'Test process : Gagal Proses');
					
				}	
			}else{
				$varSuc=$varSuc.' Data MEMBER tidak dapat di proses karena tidak ada transaksi yang terjadi pada hari ini... ';
			}
			
			// Proses 2. Ambil data Stokiest
			$result=$this->MFico->getBalanceEwallet("",$startdate,$tomorrow);
			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				//Other Method For POST curl..
				//$this->curl->post($dtpost);
				//$result = $this->curl->execute();
				//$result = $this->curl->post($dtpost);
				if($resultCurl){
					$varSuc=$varSuc.' '.count($result).' Data Transaksi Stockist berhasil Terkirim.';
					//log_message('INFO', 'Test process : '.$resultCurl);
					$resSync[]=$resultCurl;
				}else{
					$varSuc=$varSuc.' Data Transaksi Member STOCKIST Terkirim.';
					//log_message('INFO', 'Test process : Proses Syncronisasi Gagal');
					$cRow=array();
					$cRow['id']='1';
					$cRow['data'][]=$varSuc;
					$resSync[]=$cRow;
				}	
			}else{
				$varSuc=$varSuc.' Data Transaksi STOCKIST tidak dapat di proses karena tidak ada transaksi untuk stockist yang terjadi hari ini';
				$cRow=array();
				$cRow['id']='1';
				$cRow['data'][]=$varSuc;
				$resSync[]=$cRow;
			}
			
			$data_log_api=array(
				'log_api_tgl' => date("Y-m-d H:i:s"),
				'log_api_ket' => $varSuc,
				'log_api_kategori' => 'POST Sinkronisasi API Ke SAP', 
				'log_api_modul' => 'FICO', 
				'log_api_url' => $url, 
				'log_api_status' => $cRow['id'], 
				'log_api_type' => 'PUSH' 
			);
			$this->MFico->writeLogApi($data_log_api);
			
			
			
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='Authorize Failed, Cannot Connect To Server';
			$resSync[]=$cRow;
		}
		
		header('Content-Type: application/json');
		//echo json_encode($resSync);
		echo json_encode($resSync, JSON_PRETTY_PRINT);
	}
	
	
	/*  ini cuma Untuk Test POST Saja.. ga usah di publish
	public function stc(){
		if (empty($this->input->post("txtUserName"))){
			echo 'Username';
		}else if (empty($this->input->post("txtPassword"))){
			echo 'Password';
		}else if (empty($this->input->post("txtMulai"))){
			echo 'Mulai';
		}else if (empty($this->input->post("txtSelesai"))){
			echo 'Selesai';
		}else{
			$periode1=$this->input->post("txtMulai");
			$periode2=$this->input->post("txtSelesai");
			
			//echo $periode1.' - '.$periode2;
			
			$result=$this->MFico->getBalanceEwallet("",$periode1,$periode2);
			header('Content-Type: application/json');
			//echo json_encode($result);
			echo json_encode($result, JSON_PRETTY_PRINT);
		}
	}
	
	public function mem(){
		if (empty($this->input->post("txtUserName"))){
			echo 'Username';
		}else if (empty($this->input->post("txtPassword"))){
			echo 'Password';
		}else if (empty($this->input->post("txtMulai"))){
			echo 'Mulai';
		}else if (empty($this->input->post("txtSelesai"))){
			echo 'Selesai';
		}else{
			$periode1=$this->input->post("txtMulai");
			$periode2=$this->input->post("txtSelesai");
			$result=$this->MFico->getBalanceEwallet_member("",$periode1,$periode2);
			header('Content-Type: application/json');
			echo json_encode($result, JSON_PRETTY_PRINT);
		}
		
	}
	End Of Part Test */
	
	
}
?>
