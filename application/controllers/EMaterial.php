<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//require APPPATH.'/libraries/REST_Controller.php';
    

class EMaterial extends MY_Controller {
//class eWallet extends REST_Controller {
    public function __Construct()
	{
   	   parent::__Construct();
       // constructor code
		$CI =& get_instance();
		$this->domain = $CI->config->item('domain');
		$this->baseurl = $CI->config->item('base_url');
		$this->show_debug = $CI->config->item('show_debug');
		$this->basepath = $CI->config->item('base_url').$CI->config->item('index_page');
		$this->load->helper('url');
		$this->load->library('curl');  
		$this->load->helper('form');
		$this->load->model('mmaterial/Mmaterial');
		$this->load->model('log/Mapi_log');
    }
	
	
	public function index(){
		/*
		echo $this->security->get_csrf_token_name();
		echo '<br>'.$this->security->get_csrf_hash();
		*/
	}
	

	public function get_data_ncm($fromdate = NULL, $todate = NULL){

		// if(empty($fromdate)){
		// 	$fromdate = date('Y-m-d');
		// }

		// if(empty($todate)){

		// 	$date = date('m-d-Y');
		// 	$date1 = str_replace('-', '/', $date);
		// 	$todate = date('Y-m-d',strtotime($date1 . "+1 days"));
		
		// }

		if(empty($fromdate)){

			$date = date('m-d-Y');
			$date1 = str_replace('-', '/', $date);
			$fromdate = date('Y-m-d',strtotime($date1 . "-1 days"));

		}

		if(empty($todate)){
			
			$todate = date('Y-m-d');				
		
		}
		
		// $result=$this->Mmaterial->getNcm("2019-08-30","2019-08-31");
		$result = $this->Mmaterial->getNcm($fromdate, $todate); //-- untuk live
		
		return $result;
		//echo json_encode($result);

	}
	
	public function get_data_adj_rusak($fromdate = NULL, $todate = NULL){

		// if(empty($fromdate)){
		// 	$fromdate = date('Y-m-d');
		// }

		// if(empty($todate)){

		// 	$date = date('m-d-Y');
		// 	$date1 = str_replace('-', '/', $date);
		// 	$todate = date('Y-m-d',strtotime($date1 . "+1 days"));
		
		// }

		if(empty($fromdate)){

			$date = date('m-d-Y');
			$date1 = str_replace('-', '/', $date);
			$fromdate = date('Y-m-d',strtotime($date1 . "-1 days"));

		}

		if(empty($todate)){
			
			$todate = date('Y-m-d');				
		
		}
		
		$result=$this->Mmaterial->getAdjRusak($fromdate, $todate);

		return $result;
		//echo json_encode($result);

	}
	
	public function get_data_adj_move($fromdate = NULL, $todate = NULL){

		// if(empty($fromdate)){
		// 	$fromdate = date('Y-m-d');
		// }

		// if(empty($todate)){

		// 	$date = date('m-d-Y');
		// 	$date1 = str_replace('-', '/', $date);
		// 	$todate = date('Y-m-d',strtotime($date1 . "+1 days"));
		
		// }

		if(empty($fromdate)){

			$date = date('m-d-Y');
			$date1 = str_replace('-', '/', $date);
			$fromdate = date('Y-m-d',strtotime($date1 . "-1 days"));

		}

		if(empty($todate)){
			
			$todate = date('Y-m-d');				
		
		}
		
		$result=$this->Mmaterial->getAdjMove($fromdate, $todate);

		//var_dump($result); die();
		return $result;
		//echo json_encode($result);

	}

	public function get_data_sloc($fromdate = NULL, $todate = NULL){

		// if(empty($fromdate)){
		// 	$fromdate = date('Y-m-d');
		// }

		// if(empty($todate)){

		// 	$date = date('m-d-Y');
		// 	$date1 = str_replace('-', '/', $date);
		// 	$todate = date('Y-m-d',strtotime($date1 . "+1 days"));
		
		// }

		if(empty($fromdate)){

			$date = date('m-d-Y');
			$date1 = str_replace('-', '/', $date);
			$fromdate = date('Y-m-d',strtotime($date1 . "-1 days"));

		}

		if(empty($todate)){
			
			$todate = date('Y-m-d');				
		
		}
		
		$result=$this->Mmaterial->getSloc($fromdate, $todate);

		//var_dump($result); die();
		return $result;
		//echo json_encode($result);

	}
	
	public function get_data_whs_from($fromdate = NULL, $todate = NULL){

		// if(empty($fromdate)){
		// 	$fromdate = date('Y-m-d');
		// }

		// if(empty($todate)){

		// 	$date = date('m-d-Y');
		// 	$date1 = str_replace('-', '/', $date);
		// 	$todate = date('Y-m-d',strtotime($date1 . "+1 days"));
		
		// }

		if(empty($fromdate)){

			$date = date('m-d-Y');
			$date1 = str_replace('-', '/', $date);
			$fromdate = date('Y-m-d',strtotime($date1 . "-1 days"));

		}

		if(empty($todate)){
			
			$todate = date('Y-m-d');				
		
		}
		
		$result=$this->Mmaterial->getMvWhsFrom($fromdate, $todate);

		//var_dump($result); die();
		return $result;
		//echo json_encode($result);

	}

	public function get_data_whs_to($fromdate = NULL, $todate = NULL){

		// if(empty($fromdate)){
		// 	$fromdate = date('Y-m-d');
		// }

		// if(empty($todate)){

		// 	$date = date('m-d-Y');
		// 	$date1 = str_replace('-', '/', $date);
		// 	$todate = date('Y-m-d',strtotime($date1 . "+1 days"));
		
		// }
		
		if(empty($fromdate)){

			$date = date('m-d-Y');
			$date1 = str_replace('-', '/', $date);
			$fromdate = date('Y-m-d',strtotime($date1 . "-1 days"));

		}

		if(empty($todate)){
			
			$todate = date('Y-m-d');				
		
		}
		
		$result=$this->Mmaterial->getMvWhsTo($fromdate, $todate);

		//var_dump($result); die();
		return $result;
		//echo json_encode($result);

	}
	

	public function post_ncm($fromdate = NULL, $todate = NULL){
	
		//dev
		$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=materialmovement&mvtype=201';
		//prod
		// $url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=300/inbound&module=materialmovement&mvtype=201';

		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
		$loginCek=$this->curl->http_login($this->username, $this->password);

		if ($loginCek){	

			// $fromdate = $this->input->post('fromdate');
			// $todate = $this->input->post('todate');

			// $fromdate = '2019-08-30';
			// $todate = '2019-08-31';

			if(empty($fromdate)){

				$date = date('m-d-Y');
				$date1 = str_replace('-', '/', $date);
				$fromdate = date('Y-m-d',strtotime($date1 . "-1 days"));

			}
	
			if(empty($todate)){
				
				$todate = date('Y-m-d');				
			
			}

			$result = $this->get_data_ncm($fromdate, $todate);				
			
			if (count($result)>0){

				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				//print_r($dtpost); die();
				//Metode curl ci
				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				
				$datass = str_replace('"response":','',$resultCurl);
				$datasss = json_decode( $datass, true );
				
				foreach($datasss as $data){
					var_dump($data);
				}
				
				//print_r((str_replace('"response":','',$resultCurl)));
				
				die();

				// echo "<pre>";
				// print_r($resultCurl);
				// echo "</pre>";
			
				//var_dump($resultCurl[29]); die();

				if($resultCurl){

					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';
					//log_message('INFO', 'Test process : '.$resultCurl);
					// echo '<br>'.$varSuc;
					if($resultCurl[16]=="S"){
						echo '<br> STATUS : BERHASIL';
						$this->Mmaterial->update_data_ncm($fromdate,$todate);		
					}else{
						echo '<br> STATUS : GAGAL';	
					}
					//echo '<br> '.$resultCurl;		
					echo $resultCurl;			
					
				}else{
					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo '<br> '.$resultCurl;
					//log_message('INFO', 'Test process : Gagal Proses');
				}
				
				$data_log_api=array(

					'log_api_tgl' => date("Y-m-d H:i:s"),
					'log_api_ket' => $varSuc,
					'log_api_kategori' => 'POST Sinkronisasi API Material Movement NRC/NCM From Unihealth To Soho SAP', 
					'log_api_modul' => 'MM',
					'log_api_url' => $url,
					'log_api_status' => $varSta,
					'log_api_type' => 'PUSH' 
				
				);
				
				$this->Mapi_log->writeLogApi($data_log_api);
				header('Content-Type: application/json');
				echo json_encode($result, JSON_PRETTY_PRINT);

			}else{
				echo 'Data Tidak Tersedia';
			}
		}else{
			echo 'Failed Connect To API Server';	
		}
		
	}
	

	public function post_adj_rusak($fromdate = NULL, $todate = NULL){
		
		//dev
		$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=materialmovement&mvtype=551';
		//prod
		//$url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=300/inbound&module=materialmovement&mvtype=551';

		
		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
		$loginCek=$this->curl->http_login($this->username, $this->password);
		if ($loginCek){	

			// $fromdate = '2020-02-01';
			// $todate = '2020-02-29';
			
			if(empty($fromdate)){

				$date = date('m-d-Y');
				$date1 = str_replace('-', '/', $date);
				$fromdate = date('Y-m-d',strtotime($date1 . "-1 days"));

			}
	
			if(empty($todate)){
				
				$todate = date('Y-m-d');				
			
			}

			$result = $this->get_data_adj_rusak($fromdate, $todate);

			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				//$dtpost = array($conArray);
			
				//Metode curl ci
				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				
				if($resultCurl){

					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';
					//log_message('INFO', 'Test process : '.$resultCurl);
					echo '<br>'.$varSuc;
					if($resultCurl[29]=="S"){
						echo '<br> STATUS : BERHASIL';
						$this->Mmaterial->update_data_adj_rusak($fromdate,$todate);		
					}else{
						echo '<br> STATUS : GAGAL';	
					}
					
				}else{
					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo 'Gagal ';
					//log_message('INFO', 'Test process : Gagal Proses');
				}
				
				$data_log_api=array(
					'log_api_tgl' => date("Y-m-d H:i:s"),
					'log_api_ket' => $varSuc,
					'log_api_kategori' => 'POST Sinkronisasi API Material Adjustment Scraping From Unihealth To SOHO SAP', 
					'log_api_modul' => 'MM', 
					'log_api_url' => $url, 
					'log_api_status' => $varSta, 
					'log_api_type' => 'PUSH' 
				);
				$this->Mapi_log->writeLogApi($data_log_api);
				
				header('Content-Type: application/json');
				echo json_encode($result, JSON_PRETTY_PRINT);
				
			}else{
				echo 'Data Tidak tersedia';
			}
		}else{
			echo 'Failed Connect To API Server';	
		}
		
	}

	public function post_adj_move($fromdate = NULL, $todate = NULL){

		//dev
		$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=materialmovement&mvtype=501';
		//prod
		//$url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=300/inbound&module=materialmovement&mvtype=501';
		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
		$loginCek=$this->curl->http_login($this->username, $this->password);
		if ($loginCek){	

			if(empty($fromdate)){

				$date = date('m-d-Y');
				$date1 = str_replace('-', '/', $date);
				$fromdate = date('Y-m-d',strtotime($date1 . "-1 days"));

			}
	
			if(empty($todate)){
				
				$todate = date('Y-m-d');				
			
			}

			$result = $this->get_data_adj_move($fromdate, $todate);

			//$result=$this->Mmaterial->getAdjMove(date("Y-m-d"),$tomorrow); -- untuk live
			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				//$dtpost = array($conArray);
				//Metode curl ci
				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 

				// -- Methode Lain untuk POST Data Via CURL
				//$result=$this->curl->post($dtpost);
				//$result = $this->curl->execute();
				if($resultCurl){
					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';
					//log_message('INFO', 'Test process : '.$resultCurl);
					echo '<br>'.$varSuc;
					if($resultCurl[29]=="S"){
						echo '<br> STATUS : BERHASIL';
						$this->Mmaterial->update_data_adj_move($fromdate,$todate);		
					}else{
						echo '<br> STATUS : GAGAL';	
					}
					
				}else{
					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo 'Gagal ';
					//log_message('INFO', 'Test process : Gagal Proses');
					
				}
				
				$data_log_api=array(
					'log_api_tgl' => date("Y-m-d H:i:s"),
					'log_api_ket' => $varSuc,
					'log_api_kategori' => 'POST Sinkronisasi API Material Movement Adjustment GR Without PO From Unihealth To Soho SAP', 
					'log_api_modul' => 'MM', 
					'log_api_url' => $url, 
					'log_api_status' => $varSta, 
					'log_api_type' => 'PUSH' 
				);
				$this->Mapi_log->writeLogApi($data_log_api);
				header('Content-Type: application/json');
				echo json_encode($result, JSON_PRETTY_PRINT);
				
			}else{
				echo 'Data Tidak tersedia';
			}
		}else{
			echo 'Failed Connect To API Server';	
		}
		
	}
	

	public function post_sloc($fromdate = NULL, $todate = NULL){

		//dev
		$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=materialmovement&mvtype=311';
		//prod
		// $url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=300/inbound&module=materialmovement&mvtype=311';

		$this->curl->create($url);  

		// Optional, delete this line if your API is open  
		$loginCek=$this->curl->http_login($this->username, $this->password);
		if ($loginCek){	

			// $fromdate = '2019-08-30';
			// $todate = '2019-12-31';

			if(empty($fromdate)){

				$date = date('m-d-Y');
				$date1 = str_replace('-', '/', $date);
				$fromdate = date('Y-m-d',strtotime($date1 . "-1 days"));

			}
	
			if(empty($todate)){
				
				$todate = date('Y-m-d');				
			
			}

			$result = $this->get_data_sloc($fromdate, $todate);

			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				//$dtpost = array($conArray);
				//Metode curl ci
				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				
				if($resultCurl){
					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';
					//log_message('INFO', 'Test process : '.$resultCurl);
					echo '<br>'.$varSuc;
					if($resultCurl[29]=="S"){
						echo '<br> STATUS : BERHASIL';
						$this->Mmaterial->update_data_sloc($fromdate,$todate);		
					}else{
						echo '<br> STATUS : GAGAL';	
					}
					
					
				}else{
					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo 'Gagal ';
					//log_message('INFO', 'Test process : Gagal Proses');
					
				}
				
				$data_log_api=array(
					'log_api_tgl' => date("Y-m-d H:i:s"),
					'log_api_ket' => $varSuc,
					'log_api_kategori' => 'POST Sinkronisasi API Material Movement Sloc To Sloc Unihealth To SOHO SAP', 
					'log_api_modul' => 'MM', 
					'log_api_url' => $url, 
					'log_api_status' => $varSta, 
					'log_api_type' => 'PUSH' 
				);
				$this->Mapi_log->writeLogApi($data_log_api);
				
				header('Content-Type: application/json');
				echo json_encode($result, JSON_PRETTY_PRINT);
				
			}else{
				echo 'Data Tidak tersedia';
			}
		}else{
			echo 'Failed Connect To API Server';	
		}
		
	}
	//C:\Program Files (x86)\GnuWin32\bin\wget.exe -q -O nul http://localhost/sohomlm_push/ematerial/get_whs_to/
	//"C:\Program Files (x86)\GnuWin32\bin\wget.exe -q -O nul http://localhost/sohomlm_push/ematerial/get_whs_to/ "
	
	public function post_whs_from($fromdate = NULL, $todate = NULL){
		
		//dev
		$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=materialmovement&mvtype=303';

		//prod
		// $url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=300/inbound&module=materialmovement&mvtype=303';

		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
		
		$loginCek=$this->curl->http_login($this->username, $this->password);
		if ($loginCek){	

			// $fromdate = $this->input->post('fromdate');
			// $todate = $this->input->post('todate');

			// $fromdate = '2019-08-30';
			// $todate = '2019-08-31';

			if(empty($fromdate)){

				$date = date('m-d-Y');
				$date1 = str_replace('-', '/', $date);
				$fromdate = date('Y-m-d',strtotime($date1 . "-1 days"));

			}
	
			if(empty($todate)){
				
				$todate = date('Y-m-d');				
			
			}

			$result = $this->get_data_whs_from($fromdate, $todate);
			//$result=$this->Mmaterial->getMvWhsFrom(date("Y-m-d"),$tomorrow); -- untuk live
			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				//$dtpost = array($conArray);
				//Metode curl ci
				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				
				if($resultCurl){
					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';
					//log_message('INFO', 'Test process : '.$resultCurl);
					echo '<br>'.$varSuc;
					if($resultCurl[29]=="S"){
						echo '<br> STATUS : BERHASIL';
						$this->Mmaterial->update_data_whs_from($fromdate,$todate);		
					}else{
						echo '<br> STATUS : GAGAL';	
					}
					
				}else{
					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo 'Gagal ';
					//log_message('INFO', 'Test process : Gagal Proses');
					
				}
				
				$data_log_api=array(
					'log_api_tgl' => date("Y-m-d H:i:s"),
					'log_api_ket' => $varSuc,
					'log_api_kategori' => 'POST Sinkronisasi API Material Movement whs to whs (Pengirim) From Unihealth To SOHO SAP', 
					'log_api_modul' => 'MM', 
					'log_api_url' => $url, 
					'log_api_status' => $varSta, 
					'log_api_type' => 'PUSH' 
				);
				$this->Mapi_log->writeLogApi($data_log_api);
				
				header('Content-Type: application/json');
				echo json_encode($result, JSON_PRETTY_PRINT);
				
			}else{
				echo 'Data Tidak tersedia';
			}
		}else{
			echo 'Failed Connect To API Server';	
		}
		
	}
	
	public function post_whs_to($fromdate = NULL, $todate = NULL){
		
		//dev
		$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=materialmovement&mvtype=305';

		//prod
		// $url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=300/inbound&module=materialmovement&mvtype=305';


		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
		$loginCek=$this->curl->http_login($this->username, $this->password);
		if ($loginCek){	

			// $fromdate = $this->input->post('fromdate');
			// $todate = $this->input->post('todate');

			// $fromdate = '2019-08-30';
			// $todate = '2019-08-31';

			if(empty($fromdate)){

				$date = date('m-d-Y');
				$date1 = str_replace('-', '/', $date);
				$fromdate = date('Y-m-d',strtotime($date1 . "-1 days"));

			}
	
			if(empty($todate)){
				
				$todate = date('Y-m-d');				
			
			}

			$result = $this->get_data_whs_to($fromdate, $todate);

			if (count($result)>0){
				$varSuc='';
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				//$dtpost = array($conArray);
				//Metode curl ci
				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				// -- Methode Lain untuk POST Data Via CURL
				//$result=$this->curl->post($dtpost);
				//$result = $this->curl->execute();
				if($resultCurl){
					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';
					//log_message('INFO', 'Test process : '.$resultCurl);
					echo '<br>'.$varSuc;
					if($resultCurl[29]=="S"){
						echo '<br> STATUS : BERHASIL';
						$this->Mmaterial->update_data_whs_to($fromdate,$todate);		
					}else{
						echo '<br> STATUS : GAGAL';	
					}
					
				}else{
					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo 'Gagal ';
					//log_message('INFO', 'Test process : Gagal Proses');
					
				}
				
				$data_log_api=array(
					'log_api_tgl' => date("Y-m-d H:i:s"),
					'log_api_ket' => $varSuc,
					'log_api_kategori' => 'POST Sinkronisasi API Material Movement whs to whs (Penerima) From Unihealth To SOHO SAP', 
					'log_api_modul' => 'MM', 
					'log_api_url' => $url, 
					'log_api_status' => $varSta, 
					'log_api_type' => 'PUSH' 
				);	
				$this->Mapi_log->writeLogApi($data_log_api);
			
				header('Content-Type: application/json');
				echo json_encode($result, JSON_PRETTY_PRINT);
			
				
			}else{
				echo 'Data Tidak tersedia';
			}
		}else{
			echo 'Failed Connect To API Server';	
		}
		
	}

	public function ajax_post_ncm(){
	
		//dev
		$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=materialmovement&mvtype=201';
		//prod
		// $url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=300/inbound&module=materialmovement&mvtype=201';

		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
		$loginCek=$this->curl->http_login($this->username, $this->password);
		if ($loginCek){	

			$data_arr = $this->input->post('data');
			
			//$data_arr = array("159698", "160029", "160030", "160045", "160046", "160050", "160051", "160058", "160059", "160106");

			$array_id = implode(',', $data_arr);			
			
			$result = $this->Mmaterial->get_data_ncm_by_array_id($array_id);

			if (count($result)>0){

				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);

				//Metode curl ci
				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				
				if($resultCurl){

					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';

					if($resultCurl[29]=="S"){
						echo '<br> STATUS : BERHASIL';
						$this->Mmaterial->update_data_ncm_by_array_id($array_id);		
					}else{
						echo '<br> STATUS : GAGAL';	
					}
					
					echo json_encode($varSuc);					
					
				}else{
					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo json_encode($varSuc);	
				}
				
				$data_log_api=array(

					'log_api_tgl' => date("Y-m-d H:i:s"),
					'log_api_ket' => $varSuc,
					'log_api_kategori' => 'Resend POST Sinkronisasi API Material Movement NRC/NCM From Unihealth To Soho SAP', 
					'log_api_modul' => 'MM',
					'log_api_url' => $url,
					'log_api_status' => $varSta,
					'log_api_type' => 'PUSH' 
				
				);
				
				$this->Mapi_log->writeLogApi($data_log_api);

			}else{
				echo json_encode('gagal');
			}
		}else{
			echo json_encode('Failed Connect To API Server');	
		}
		
	}

	public function ajax_post_adj_rusak(){
		
		//dev
		$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=materialmovement&mvtype=551';
		//prod
		//$url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=300/inbound&module=materialmovement&mvtype=551';

		
		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
		$loginCek=$this->curl->http_login($this->username, $this->password);
		if ($loginCek){	

			$data_arr = $this->input->post('data');

			$array_id = implode(',', $data_arr);
			
			$result = $this->Mmaterial->get_data_adj_rusak_by_array_id($array_id);

			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				//$dtpost = array($conArray);
			
				//Metode curl ci
				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				
				if($resultCurl){

					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';
					
					if($resultCurl[29]=="S"){
						echo '<br> STATUS : BERHASIL';
						$this->Mmaterial->update_data_adj_rusak_by_array_id($array_id);		
					}else{
						echo '<br> STATUS : GAGAL';	
					}

					echo json_encode($resultCurl);	
					
				}else{
					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo json_encode($varSuc);	
				}
				
				$data_log_api=array(
					'log_api_tgl' => date("Y-m-d H:i:s"),
					'log_api_ket' => $varSuc,
					'log_api_kategori' => 'Resend POST Sinkronisasi API Material Adjustment Scraping From Unihealth To SOHO SAP', 
					'log_api_modul' => 'MM', 
					'log_api_url' => $url, 
					'log_api_status' => $varSta, 
					'log_api_type' => 'PUSH' 
				);
				$this->Mapi_log->writeLogApi($data_log_api);
				
			}else{
				echo 'gagal';
			}
		}else{
			echo 'Failed Connect To API Server';	
		}
		
	}

	public function ajax_post_adj_move(){

		//dev
		$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=materialmovement&mvtype=501';
		//prod
		//$url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=300/inbound&module=materialmovement&mvtype=501';
		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
		$loginCek=$this->curl->http_login($this->username, $this->password);
		if ($loginCek){	

			$data_arr = $this->input->post('data');

			$array_id = implode(',', $data_arr);
			
			$result = $this->Mmaterial->get_data_adj_move_by_array_id($array_id);

			//$result=$this->Mmaterial->getAdjMove(date("Y-m-d"),$tomorrow); -- untuk live
			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				//$dtpost = array($conArray);
				//Metode curl ci
				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				// -- Methode Lain untuk POST Data Via CURL
				//$result=$this->curl->post($dtpost);
				//$result = $this->curl->execute();
				if($resultCurl){
					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';
					
					if($resultCurl[29]=="S"){
						echo '<br> STATUS : BERHASIL';
						$this->Mmaterial->update_data_adj_move_by_array_id($array_id);		
					}else{
						echo '<br> STATUS : GAGAL';	
					}

					echo json_encode($resultCurl);	
					
					
				}else{
					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo json_encode($varSuc);	
					
				}
				
				$data_log_api=array(
					'log_api_tgl' => date("Y-m-d H:i:s"),
					'log_api_ket' => $varSuc,
					'log_api_kategori' => 'Resend POST Sinkronisasi API Material Movement Adjustment GR Without PO From Unihealth To Soho SAP', 
					'log_api_modul' => 'MM', 
					'log_api_url' => $url, 
					'log_api_status' => $varSta, 
					'log_api_type' => 'PUSH' 
				);
				$this->Mapi_log->writeLogApi($data_log_api);
				
			}else{
				echo 'gagal';
			}
		}else{
			echo 'Failed Connect To API Server';	
		}
		
	}
	
	public function ajax_post_sloc(){

		//dev
		$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=materialmovement&mvtype=311';
		//prod
		// $url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=300/inbound&module=materialmovement&mvtype=311';

		$this->curl->create($url);  

		// Optional, delete this line if your API is open  
		$loginCek=$this->curl->http_login($this->username, $this->password);
		if ($loginCek){	

			$data_arr = $this->input->post('data');

			$array_id = implode(',', $data_arr);
			
			$result = $this->Mmaterial->get_data_sloc_by_array_id($array_id);

			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				//$dtpost = array($conArray);
				//Metode curl ci
				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				
				if($resultCurl){
					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';

					if($resultCurl[29]=="S"){
						echo '<br> STATUS : BERHASIL';
						$this->Mmaterial->update_data_sloc_by_array_id($array_id);		
					}else{
						echo '<br> STATUS : GAGAL';	
					}

					echo json_encode($resultCurl);	
					
					
				}else{
					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo json_encode($varSuc);	
					
				}
				
				$data_log_api=array(
					'log_api_tgl' => date("Y-m-d H:i:s"),
					'log_api_ket' => $varSuc,
					'log_api_kategori' => 'Resend POST Sinkronisasi API Material Movement Sloc To Sloc Unihealth To SOHO SAP', 
					'log_api_modul' => 'MM', 
					'log_api_url' => $url, 
					'log_api_status' => $varSta, 
					'log_api_type' => 'PUSH' 
				);

				$this->Mapi_log->writeLogApi($data_log_api);				

			}else{
				echo 'gagal';
			}
		}else{
			echo 'Failed Connect To API Server';	
		}
		
	}
	//C:\Program Files (x86)\GnuWin32\bin\wget.exe -q -O nul http://localhost/sohomlm_push/ematerial/get_whs_to/
	//"C:\Program Files (x86)\GnuWin32\bin\wget.exe -q -O nul http://localhost/sohomlm_push/ematerial/get_whs_to/ "
	
	public function ajax_post_whs_from(){
		
		//dev
		$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=materialmovement&mvtype=303';

		//prod
		// $url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=300/inbound&module=materialmovement&mvtype=303';

		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
		
		$loginCek=$this->curl->http_login($this->username, $this->password);
		if ($loginCek){	

			$data_arr = $this->input->post('data');

			$array_id = implode(',', $data_arr);
			
			$result = $this->Mmaterial->get_data_whs_from_by_array_id($array_id);			

			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				//$dtpost = array($conArray);
				//Metode curl ci
				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				
				if($resultCurl){
					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';

					if($resultCurl[29]=="S"){
						echo '<br> STATUS : BERHASIL';
						$this->Mmaterial->update_data_whs_from_by_array_id($array_id);		
					}else{
						echo '<br> STATUS : GAGAL';	
					}

					echo json_encode($resultCurl);	

				}else{
					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo json_encode($varSuc);	
					
				}
				
				$data_log_api=array(
					'log_api_tgl' => date("Y-m-d H:i:s"),
					'log_api_ket' => $varSuc,
					'log_api_kategori' => 'POST Sinkronisasi API Material Movement whs to whs (Pengirim) From Unihealth To SOHO SAP', 
					'log_api_modul' => 'MM', 
					'log_api_url' => $url, 
					'log_api_status' => $varSta, 
					'log_api_type' => 'PUSH' 
				);
				$this->Mapi_log->writeLogApi($data_log_api);
				
			}else{
				echo 'gagal';
			}
		}else{
			echo 'Failed Connect To API Server';	
		}
		
	}
	
	public function ajax_post_whs_to(){
		
		//dev
		$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=materialmovement&mvtype=305';

		//prod
		// $url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=300/inbound&module=materialmovement&mvtype=305';


		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
		$loginCek=$this->curl->http_login($this->username, $this->password);
		if ($loginCek){	

			$data_arr = $this->input->post('data');

			$array_id = implode(',', $data_arr);
			
			$result = $this->Mmaterial->get_data_whs_to_by_array_id($array_id);

			if (count($result)>0){
				$varSuc='';
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				//$dtpost = array($conArray);
				//Metode curl ci
				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				// -- Methode Lain untuk POST Data Via CURL
				//$result=$this->curl->post($dtpost);
				//$result = $this->curl->execute();
				if($resultCurl){
					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';

					if($resultCurl[29]=="S"){
						echo '<br> STATUS : BERHASIL';
						$this->Mmaterial->update_data_whs_to_by_array_id($array_id);		
					}else{
						echo '<br> STATUS : GAGAL';	
					}
				
					echo json_encode($resultCurl);	
					
				}else{

					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo json_encode($varSuc);	
					
				}
				
				$data_log_api=array(
					'log_api_tgl' => date("Y-m-d H:i:s"),
					'log_api_ket' => $varSuc,
					'log_api_kategori' => 'Resend POST Sinkronisasi API Material Movement whs to whs (Penerima) From Unihealth To SOHO SAP', 
					'log_api_modul' => 'MM', 
					'log_api_url' => $url, 
					'log_api_status' => $varSta, 
					'log_api_type' => 'PUSH' 
				);	

				$this->Mapi_log->writeLogApi($data_log_api);			
				
			}else{
				echo 'gagal';
			}
		}else{
			echo 'Failed Connect To API Server';	
		}
		
	}

}
?>
