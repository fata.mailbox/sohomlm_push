<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//require APPPATH.'/libraries/REST_Controller.php';
    

class eSales extends CI_Controller {
//class eWallet extends REST_Controller {
    public function __Construct()
	{
   	   parent::__Construct();
       // constructor code
	   
		$CI =& get_instance();
		$this->domain = $CI->config->item('domain');
		$this->baseurl = $CI->config->item('base_url');
		$this->show_debug = $CI->config->item('show_debug');
		$this->basepath = $CI->config->item('base_url').$CI->config->item('index_page');
		
		$this->load->helper('url');
		
		$this->load->library('curl');  
		$this->load->helper('form');
		
		$this->load->model('sales/Msales');
		$this->load->model('log/Mapi_log');
		
    }
	
	
	public function index(){
		/*
		echo $this->security->get_csrf_token_name();
		echo '<br>'.$this->security->get_csrf_hash();
		*/
	}
	

	public function get_so(){
		/*
		$username = 'SGH_VENDA1';//'admin';  
		$password = 'Equine.1';//'cerebro1'; 
		*/
		/*
		// dev
		$username = 'sgh_ict03';  
		$password = 'golive0518'; 
		*/
		
		// prod
		$username = 'sgh_sys01';  
		$password = 'PKPBo490'; 
		
		//$url='http://172.20.121.37/sohomlm_api/api/unihealth_api/materialGimmick';
		//$url='http://localhost/sohomlm_api/api/unihealth_api_test/insertTest';
		//dev
		//$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=sduhninv';
		
		//$url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=sduhninv';
		//prod
		$url='xhttp://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=300/inbound&module=sduhninv';

		
//		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
//		$loginCek=$this->curl->http_login($username, $password);
//		if ($loginCek){	
			
			$date = date('m-d-Y');
			$date1 = str_replace('-', '/', $date);
			$tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));
			$startdate=date("Y-m-d");
			
			if (!empty($this->input->post("txtMulai"))){
				$startdate=$this->input->post("txtMulai");
			}else if (!empty($this->input->post("txtSelesai"))){
				$tomorrow =$this->input->post("txtSelesai");
			}
			
			
			
			//$result=$this->Msales->getSo_data("2019-04-01","2019-04-30");
			//$result=$this->Msales->getSo_data("2019-05-01","2019-05-05");
			//$result=$this->Msales->getSo_data("2019-05-06","2019-05-31");
			//$result=$this->Msales->getSo_data("2019-05-01","2019-05-10");
			//$result=$this->Msales->getSo_data("2019-05-11","2019-05-31");
			$result=$this->Msales->getSo_data("2019-06-01","2019-06-30");
			//$result=$this->Msales->getSo_data($startdate,$tomorrow);
			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				//$dtpost = array($conArray);
				//Metode curl ci
//				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				// -- Methode Lain untuk POST Data Via CURL
				//$result=$this->curl->post($dtpost);
				//$result = $this->curl->execute();
/*
				if($resultCurl){
					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';
					echo $resultCurl;
					log_message('INFO', 'Send SO process : '.$resultCurl);
					echo '<br>'.$varSuc;
					
					
				}else{
					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo 'Gagal ';
					echo $resultCurl;
					log_message('INFO', 'Send SO process : Gagal Proses');
					
				}
*/
$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
$varSta='1';

				$data_log_api=array(
					'log_api_tgl' => date("Y-m-d H:i:s"),
					'log_api_ket' => $varSuc,
					'log_api_kategori' => 'POST Sinkronisasi API SO From Unihealth To Soho SAP', 
					'log_api_modul' => 'SD', 
					'log_api_url' => $url, 
					'log_api_status' => $varSta, 
					'log_api_type' => 'PUSH' 
				);
				$this->Mapi_log->writeLogApi($data_log_api);
				
				header('Content-Type: application/json');
				echo json_encode($result, JSON_PRETTY_PRINT);
				
			}else{
				echo 'Data Tidak Tersedia';
			}
/*
		}else{
			echo 'Failed Connect To API Server';	
		}
*/		
	}
	
	public function get_ro(){
		/*
		// dev
		$username = 'sgh_ict03';  
		$password = 'golive0518'; 
		*/
		
		// prod
		$username = 'sgh_sys01';  
		$password = 'PKPBo490'; 
		
		//$url='http://172.20.121.37/sohomlm_api/api/unihealth_api/materialGimmick';
		//$url='http://localhost/sohomlm_api/api/unihealth_api_test/insertTest';
		//dev
		//$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=sduhninv';
		
		//$url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=sduhninv';
		//prod
		$url='xhttp://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=300/inbound&module=sduhninv';

//		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
//		$loginCek=$this->curl->http_login($username, $password);
//		if ($loginCek){	
			$date = date('m-d-Y');
			$date1 = str_replace('-', '/', $date);
			$tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));
			$startdate=date("Y-m-d");
			
			if (!empty($this->input->post("txtMulai"))){
				$startdate=$this->input->post("txtMulai");
			}else if (!empty($this->input->post("txtSelesai"))){
				$tomorrow =$this->input->post("txtSelesai");
			}
			
			$result=$this->Msales->getRo_data("2019-06-21","2019-06-30");
			//$result=$this->Msales->getRo_data($startdate,$tomorrow);
			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				//$dtpost = array($conArray);
				//Metode curl ci
//				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				// -- Methode Lain untuk POST Data Via CURL
				//$result=$this->curl->post($dtpost);
				//$result = $this->curl->execute();
/*				
				if($resultCurl){
					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';
					echo $resultCurl;
					log_message('INFO', 'Send RO process : Gagal Proses');
					echo '<br>'.$varSuc;
					
					
				}else{
					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo 'Gagal ';
					log_message('INFO', 'Test process : Gagal Proses');
					echo '<br>'.$varSuc;
					
				}
*/
$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
$varSta='1';

				$data_log_api=array(
					'log_api_tgl' => date("Y-m-d H:i:s"),
					'log_api_ket' => $varSuc,
					'log_api_kategori' => 'POST Sinkronisasi API RO From Unihealth To Soho SAP', 
					'log_api_modul' => 'SD', 
					'log_api_url' => $url, 
					'log_api_status' => $varSta, 
					'log_api_type' => 'PUSH' 
				);
				$this->Mapi_log->writeLogApi($data_log_api);
				
				header('Content-Type: application/json');
				echo json_encode($result, JSON_PRETTY_PRINT);
				
			}else{
				echo 'Data Tidak Tersedia';
			}
/*			
		}else{
			echo 'Failed Connect To API Server';	
		}
*/		
	}
	

	public function get_sc_csv(){
		/*
		$username = 'sgh_ict03';  
		$password = 'golive0518'; 
		*/

		// prod
		$username = 'sgh_sys01';  
		$password = 'PKPBo490'; 

		//$url='http://172.20.121.37/sohomlm_api/api/unihealth_api/materialGimmick';
		//$url='http://localhost/sohomlm_api/api/unihealth_api_test/insertTest';
		//dev
		//$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=sduhninv';
		//$url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=sduhninv';
		//prod
		$url='xhttp://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=300/inbound&module=sduhninv';



//		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
//		$loginCek=$this->curl->http_login($username, $password);
//		if ($loginCek){	
			$date = date('m-d-Y');
			$date1 = str_replace('-', '/', $date);
			$tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));
			$startdate=date("Y-m-d");
			
			if (!empty($this->input->post("txtMulai"))){
				$startdate=$this->input->post("txtMulai");
			}else if (!empty($this->input->post("txtSelesai"))){
				$tomorrow =$this->input->post("txtSelesai");
			}
			
			//$result=$this->Msales->getSc_data("2019-04-01","2019-04-30");
			//$result=$this->Msales->getSc_data("2019-06-01","2019-06-30");
			$result=$this->Msales->getSc_data("2019-07-01","2019-07-31");
			//$result=$this->Msales->getSc_data($startdate,$tomorrow);
			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				//$dtpost = array($conArray);
				//Metode curl ci
//				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				// -- Methode Lain untuk POST Data Via CURL
				//$result=$this->curl->post($dtpost);
				//$result = $this->curl->execute();
/*
				if($resultCurl){
					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';
					echo $resultCurl;
					log_message('INFO', 'Send SC process : Gagal Proses');
					echo '<br>'.$varSuc;
					
				}else{
					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo 'Gagal ';
					log_message('INFO', 'Test process : Gagal Proses');
					echo '<br>'.$varSuc;
				}
*/
$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
$varSta='1';				
				$data_log_api=array(
					'log_api_tgl' => date("Y-m-d H:i:s"),
					'log_api_ket' => $varSuc,
					'log_api_kategori' => 'POST Sinkronisasi API SC From Unihealth To Soho SAP', 
					'log_api_modul' => 'SD', 
					'log_api_url' => $url, 
					'log_api_status' => $varSta, 
					'log_api_type' => 'PUSH' 
				);
				$this->Mapi_log->writeLogApi($data_log_api);
				
				header('Content-Type: application/json');
				echo json_encode($result, JSON_PRETTY_PRINT);
				
			}else{
				echo 'Data Tidak Tersedia';
			}
/*
		}else{
			echo 'Failed Connect To API Server';	
		}
*/		
	}

	public function get_sc_payment_csv(){
		/*
		$username = 'sgh_ict03';  
		$password = 'golive0518'; 
		*/
		// prod
		$username = 'sgh_sys01';  
		$password = 'PKPBo490'; 

		//$url='http://172.20.121.37/sohomlm_api/api/unihealth_api/materialGimmick';
		//$url='http://localhost/sohomlm_api/api/unihealth_api_test/insertTest';
		//dev
		/*
		$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=sduhninv';
		//$url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=sduhninv';
		*/
		//prod
		$url='xhttp://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=300/inbound&module=sduhninv';

//		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
//		$loginCek=$this->curl->http_login($username, $password);
//		if ($loginCek){	
			$date = date('m-d-Y');
			$date1 = str_replace('-', '/', $date);
			$tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));
			$startdate=date("Y-m-d");
			
			if (!empty($this->input->post("txtMulai"))){
				$startdate=$this->input->post("txtMulai");
			}else if (!empty($this->input->post("txtSelesai"))){
				$tomorrow =$this->input->post("txtSelesai");
			}
			
			//$result=$this->Msales->getScRo_data("2019-07-01","2019-07-10");
			//$result=$this->Msales->getScRo_data("2019-07-11","2019-07-20");
			$result=$this->Msales->getScRo_data("2019-07-21","2019-07-31");
			//$result=$this->Msales->getRo_data($startdate,$tomorrow);
			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				//$dtpost = array($conArray);
				//Metode curl ci
//				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				// -- Methode Lain untuk POST Data Via CURL
				//$result=$this->curl->post($dtpost);
				//$result = $this->curl->execute();
/*
				if($resultCurl){
					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';
					echo $resultCurl;
					log_message('INFO', 'Send RO from SC Payment process : Gagal Proses');
					echo '<br>'.$varSuc;
					
				}else{
					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo 'Gagal ';
					log_message('INFO', 'Test process : Gagal Proses');
					
				}
*/
$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
$varSta='1';				

				$data_log_api=array(
					'log_api_tgl' => date("Y-m-d H:i:s"),
					'log_api_ket' => $varSuc,
					'log_api_kategori' => 'POST Sinkronisasi API RO From SC Payment From Unihealth To Soho SAP', 
					'log_api_modul' => 'SD', 
					'log_api_url' => $url, 
					'log_api_status' => $varSta, 
					'log_api_type' => 'PUSH' 
				);
				$this->Mapi_log->writeLogApi($data_log_api);
				
				header('Content-Type: application/json');
				echo json_encode($result, JSON_PRETTY_PRINT);
				
			}else{
				echo 'Data Tidak Tersedia';
			}
/*
		}else{
			echo 'Failed Connect To API Server';	
		}
*/		
	}
	
	public function get_sc_retur_csv(){
		/*
		$username = 'sgh_ict03';  
		$password = 'golive0518'; 
		*/
		// prod
		$username = 'sgh_sys01';  
		$password = 'PKPBo490'; 

		//$url='http://172.20.121.37/sohomlm_api/api/unihealth_api/materialGimmick';
		//$url='http://localhost/sohomlm_api/api/unihealth_api_test/insertTest';
		/*
		//dev
		$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=sduhninv';
		//$url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=sduhninv';
		*/
		//prod
		$url='xhttp://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=300/inbound&module=sduhninv';


//		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
//		$loginCek=$this->curl->http_login($username, $password);
//		if ($loginCek){	
			$date = date('m-d-Y');
			$date1 = str_replace('-', '/', $date);
			$tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));
			$startdate=date("Y-m-d");
			
			if (!empty($this->input->post("txtMulai"))){
				$startdate=$this->input->post("txtMulai");
			}else if (!empty($this->input->post("txtSelesai"))){
				$tomorrow =$this->input->post("txtSelesai");
			}
			
			$result=$this->Msales->get_sc_retur_data("2019-07-01","2019-07-31");
			//$result=$this->Msales->get_sc_retur_data($startdate,$tomorrow);
			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				//$dtpost = array($conArray);
				//Metode curl ci
//				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				// -- Methode Lain untuk POST Data Via CURL
				//$result=$this->curl->post($dtpost);
				//$result = $this->curl->execute();
/*
				if($resultCurl){
					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';
					echo $resultCurl;
					log_message('INFO', 'Send SC Retur process : Gagal Proses');
					echo '<br>'.$varSuc;
					
				}else{
					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo 'Gagal ';
					log_message('INFO', 'Test process : Gagal Proses');
					
				}
*/
$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
$varSta='1';				

				$data_log_api=array(
					'log_api_tgl' => date("Y-m-d H:i:s"),
					'log_api_ket' => $varSuc,
					'log_api_kategori' => 'POST Sinkronisasi API SC Retur From Unihealth To Soho SAP', 
					'log_api_modul' => 'SD', 
					'log_api_url' => $url, 
					'log_api_status' => $varSta, 
					'log_api_type' => 'PUSH' 
				);
				$this->Mapi_log->writeLogApi($data_log_api);
				
				header('Content-Type: application/json');
				echo json_encode($result, JSON_PRETTY_PRINT);
				
			}else{
				echo 'Data Tidak Tersedia';
			}
/*
		}else{
			echo 'Failed Connect To API Server';	
		}
*/		
	}

	public function get_ro_retur_csv(){
		/*
		$username = 'sgh_ict03';  
		$password = 'golive0518'; 
		*/
		// prod
		$username = 'sgh_sys01';
		$password = 'PKPBo490'; 

		//$url='http://172.20.121.37/sohomlm_api/api/unihealth_api/materialGimmick';
		//$url='http://localhost/sohomlm_api/api/unihealth_api_test/insertTest';
		//dev
		//$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=sduhninv';
		//$url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=sduhninv';

		//prod
		$url='xhttp://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=300/inbound&module=sduhninv';


//		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
//		$loginCek=$this->curl->http_login($username, $password);
//		if ($loginCek){	
			$date = date('m-d-Y');
			$date1 = str_replace('-', '/', $date);
			$tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));
			$startdate=date("Y-m-d");
			
			if (!empty($this->input->post("txtMulai"))){
				$startdate=$this->input->post("txtMulai");
			}else if (!empty($this->input->post("txtSelesai"))){
				$tomorrow =$this->input->post("txtSelesai");
			}
			
			$result=$this->Msales->get_ro_retur_data("2019-07-01","2019-07-31");
			//$result=$this->Msales->get_sc_retur_data($startdate,$tomorrow);
			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				//$dtpost = array($conArray);
				//Metode curl ci
//				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				// -- Methode Lain untuk POST Data Via CURL
				//$result=$this->curl->post($dtpost);
				//$result = $this->curl->execute();
/*
				if($resultCurl){
					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';
					echo $resultCurl;
					log_message('INFO', 'Send RO Retur process : Gagal Proses');
					echo '<br>'.$varSuc;
					
				}else{
					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo 'Gagal ';
					log_message('INFO', 'Test process : Gagal Proses');
					
				}
*/
$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
$varSta='1';


				$data_log_api=array(
					'log_api_tgl' => date("Y-m-d H:i:s"),
					'log_api_ket' => $varSuc,
					'log_api_kategori' => 'POST Sinkronisasi API RO Retur From Unihealth To Soho SAP', 
					'log_api_modul' => 'SD', 
					'log_api_url' => $url, 
					'log_api_status' => $varSta, 
					'log_api_type' => 'PUSH' 
				);
				$this->Mapi_log->writeLogApi($data_log_api);
				
				header('Content-Type: application/json');
				echo json_encode($result, JSON_PRETTY_PRINT);
				
			}else{
				echo 'Data Tidak Tersedia';
			}
/*
		}else{
			echo 'Failed Connect To API Server';	
		}
*/	
	}


	public function get_sc_admin_csv(){
		/*
		$username = 'sgh_ict03';  
		$password = 'golive0518'; 
		*/

		// prod
		$username = 'sgh_sys01';  
		$password = 'PKPBo490'; 

		//$url='http://172.20.121.37/sohomlm_api/api/unihealth_api/materialGimmick';
		//$url='http://localhost/sohomlm_api/api/unihealth_api_test/insertTest';
		//dev
		//$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=sduhninv';
		//$url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=sduhninv';
		//prod
		$url='xhttp://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=300/inbound&module=sduhninv';



//		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
//		$loginCek=$this->curl->http_login($username, $password);
//		if ($loginCek){	
			$date = date('m-d-Y');
			$date1 = str_replace('-', '/', $date);
			$tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));
			$startdate=date("Y-m-d");
			
			if (!empty($this->input->post("txtMulai"))){
				$startdate=$this->input->post("txtMulai");
			}else if (!empty($this->input->post("txtSelesai"))){
				$tomorrow =$this->input->post("txtSelesai");
			}
			
			//$result=$this->Msales->getSc_data("2019-04-01","2019-04-30");
			//$result=$this->Msales->getSc_data("2019-06-01","2019-06-30");
			$result=$this->Msales->getScAdm_data("2019-07-01","2019-07-31");
			//$result=$this->Msales->getSc_data($startdate,$tomorrow);
			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				//$dtpost = array($conArray);
				//Metode curl ci
//				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				// -- Methode Lain untuk POST Data Via CURL
				//$result=$this->curl->post($dtpost);
				//$result = $this->curl->execute();
/*
				if($resultCurl){
					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';
					echo $resultCurl;
					log_message('INFO', 'Send SC process : Gagal Proses');
					echo '<br>'.$varSuc;
					
				}else{
					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo 'Gagal ';
					log_message('INFO', 'Test process : Gagal Proses');
					echo '<br>'.$varSuc;
				}
*/
$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
$varSta='1';				
				$data_log_api=array(
					'log_api_tgl' => date("Y-m-d H:i:s"),
					'log_api_ket' => $varSuc,
					'log_api_kategori' => 'POST Sinkronisasi API SC From Unihealth To Soho SAP', 
					'log_api_modul' => 'SD', 
					'log_api_url' => $url, 
					'log_api_status' => $varSta, 
					'log_api_type' => 'PUSH' 
				);
				$this->Mapi_log->writeLogApi($data_log_api);
				
				header('Content-Type: application/json');
				echo json_encode($result, JSON_PRETTY_PRINT);
				
			}else{
				echo 'Data Tidak Tersedia';
			}
/*
		}else{
			echo 'Failed Connect To API Server';	
		}
*/		
	}

	public function get_sc_adm_retur_csv(){
		/*
		$username = 'sgh_ict03';  
		$password = 'golive0518'; 
		*/
		// prod
		$username = 'sgh_sys01';  
		$password = 'PKPBo490'; 

		//$url='http://172.20.121.37/sohomlm_api/api/unihealth_api/materialGimmick';
		//$url='http://localhost/sohomlm_api/api/unihealth_api_test/insertTest';
		/*
		//dev
		$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=sduhninv';
		//$url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=sduhninv';
		*/
		//prod
		$url='xhttp://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=300/inbound&module=sduhninv';


//		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
//		$loginCek=$this->curl->http_login($username, $password);
//		if ($loginCek){	
			$date = date('m-d-Y');
			$date1 = str_replace('-', '/', $date);
			$tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));
			$startdate=date("Y-m-d");
			
			if (!empty($this->input->post("txtMulai"))){
				$startdate=$this->input->post("txtMulai");
			}else if (!empty($this->input->post("txtSelesai"))){
				$tomorrow =$this->input->post("txtSelesai");
			}
			
			$result=$this->Msales->get_sc_adm_retur_data("2019-07-01","2019-07-31");
			//$result=$this->Msales->get_sc_retur_data($startdate,$tomorrow);
			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				//$dtpost = array($conArray);
				//Metode curl ci
//				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				// -- Methode Lain untuk POST Data Via CURL
				//$result=$this->curl->post($dtpost);
				//$result = $this->curl->execute();
/*
				if($resultCurl){
					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';
					echo $resultCurl;
					log_message('INFO', 'Send SC Retur process : Gagal Proses');
					echo '<br>'.$varSuc;
					
				}else{
					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo 'Gagal ';
					log_message('INFO', 'Test process : Gagal Proses');
					
				}
*/
$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
$varSta='1';
				
				$data_log_api=array(
					'log_api_tgl' => date("Y-m-d H:i:s"),
					'log_api_ket' => $varSuc,
					'log_api_kategori' => 'POST Sinkronisasi API SC Retur From Unihealth To Soho SAP', 
					'log_api_modul' => 'SD', 
					'log_api_url' => $url, 
					'log_api_status' => $varSta, 
					'log_api_type' => 'PUSH' 
				);
				$this->Mapi_log->writeLogApi($data_log_api);
				
				header('Content-Type: application/json');
				echo json_encode($result, JSON_PRETTY_PRINT);
				
			}else{
				echo 'Data Tidak Tersedia';
			}
/*			
		}else{
			echo 'Failed Connect To API Server';	
		}
*/		
	}


	public function get_sc(){
		/*
		$username = 'sgh_ict03';  
		$password = 'golive0518'; 
		*/

		// prod
		$username = 'sgh_sys01';  
		$password = 'PKPBo490'; 

		//$url='http://172.20.121.37/sohomlm_api/api/unihealth_api/materialGimmick';
		//$url='http://localhost/sohomlm_api/api/unihealth_api_test/insertTest';
		//dev
		//$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=sduhninv';
		//$url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=sduhninv';
		//prod
		$url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=300/inbound&module=sduhninv';



		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
		$loginCek=$this->curl->http_login($username, $password);
		if ($loginCek){	
			$date = date('m-d-Y');
			$date1 = str_replace('-', '/', $date);
			$tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));
			$startdate=date("Y-m-d");
			
			if (!empty($this->input->post("txtMulai"))){
				$startdate=$this->input->post("txtMulai");
			}else if (!empty($this->input->post("txtSelesai"))){
				$tomorrow =$this->input->post("txtSelesai");
			}
			
			//$result=$this->Msales->getSc_data("2019-04-01","2019-04-30");
			//$result=$this->Msales->getSc_data("2019-06-01","2019-06-30");
			$result=$this->Msales->getSc_data("2019-07-01","2019-07-31");
			//$result=$this->Msales->getSc_data($startdate,$tomorrow);
			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				//$dtpost = array($conArray);
				//Metode curl ci
				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				// -- Methode Lain untuk POST Data Via CURL
				//$result=$this->curl->post($dtpost);
				//$result = $this->curl->execute();
				if($resultCurl){
					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';
					echo $resultCurl;
					log_message('INFO', 'Send SC process : Gagal Proses');
					echo '<br>'.$varSuc;
					
				}else{
					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo 'Gagal ';
					log_message('INFO', 'Test process : Gagal Proses');
					echo '<br>'.$varSuc;
				}
				
				$data_log_api=array(
					'log_api_tgl' => date("Y-m-d H:i:s"),
					'log_api_ket' => $varSuc,
					'log_api_kategori' => 'POST Sinkronisasi API SC From Unihealth To Soho SAP', 
					'log_api_modul' => 'SD', 
					'log_api_url' => $url, 
					'log_api_status' => $varSta, 
					'log_api_type' => 'PUSH' 
				);
				$this->Mapi_log->writeLogApi($data_log_api);
				
				header('Content-Type: application/json');
				echo json_encode($result, JSON_PRETTY_PRINT);
				
			}else{
				echo 'Data Tidak Tersedia';
			}
		}else{
			echo 'Failed Connect To API Server';	
		}
		
	}
	
	public function get_sc_payment(){
		$username = 'sgh_ict03';  
		$password = 'golive0518'; 
		//$url='http://172.20.121.37/sohomlm_api/api/unihealth_api/materialGimmick';
		//$url='http://localhost/sohomlm_api/api/unihealth_api_test/insertTest';
		$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=sduhninv';
		//$url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=sduhninv';

		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
		$loginCek=$this->curl->http_login($username, $password);
		if ($loginCek){	
			$date = date('m-d-Y');
			$date1 = str_replace('-', '/', $date);
			$tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));
			$startdate=date("Y-m-d");
			
			if (!empty($this->input->post("txtMulai"))){
				$startdate=$this->input->post("txtMulai");
			}else if (!empty($this->input->post("txtSelesai"))){
				$tomorrow =$this->input->post("txtSelesai");
			}
			
			$result=$this->Msales->getScRo_data("2019-04-01","2019-04-30");
			//$result=$this->Msales->getRo_data($startdate,$tomorrow);
			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				//$dtpost = array($conArray);
				//Metode curl ci
				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				// -- Methode Lain untuk POST Data Via CURL
				//$result=$this->curl->post($dtpost);
				//$result = $this->curl->execute();
				if($resultCurl){
					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';
					echo $resultCurl;
					log_message('INFO', 'Send RO from SC Payment process : Gagal Proses');
					echo '<br>'.$varSuc;
					
				}else{
					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo 'Gagal ';
					log_message('INFO', 'Test process : Gagal Proses');
					
				}
				
				$data_log_api=array(
					'log_api_tgl' => date("Y-m-d H:i:s"),
					'log_api_ket' => $varSuc,
					'log_api_kategori' => 'POST Sinkronisasi API RO From SC Payment From Unihealth To Soho SAP', 
					'log_api_modul' => 'SD', 
					'log_api_url' => $url, 
					'log_api_status' => $varSta, 
					'log_api_type' => 'PUSH' 
				);
				$this->Mapi_log->writeLogApi($data_log_api);
				
				header('Content-Type: application/json');
				echo json_encode($result, JSON_PRETTY_PRINT);
				
			}else{
				echo 'Data Tidak Tersedia';
			}
		}else{
			echo 'Failed Connect To API Server';	
		}
		
	}
	
	public function get_sc_retur(){
		$username = 'sgh_ict03';  
		$password = 'golive0518'; 
		//$url='http://172.20.121.37/sohomlm_api/api/unihealth_api/materialGimmick';
		//$url='http://localhost/sohomlm_api/api/unihealth_api_test/insertTest';
		$url='http://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=sduhninv';
		//$url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=sduhninv';

		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
		$loginCek=$this->curl->http_login($username, $password);
		if ($loginCek){	
			$date = date('m-d-Y');
			$date1 = str_replace('-', '/', $date);
			$tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));
			$startdate=date("Y-m-d");
			
			if (!empty($this->input->post("txtMulai"))){
				$startdate=$this->input->post("txtMulai");
			}else if (!empty($this->input->post("txtSelesai"))){
				$tomorrow =$this->input->post("txtSelesai");
			}
			
			$result=$this->Msales->get_sc_retur_data("2019-04-01","2019-04-30");
			//$result=$this->Msales->get_sc_retur_data($startdate,$tomorrow);
			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				//$dtpost = array($conArray);
				//Metode curl ci
				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				// -- Methode Lain untuk POST Data Via CURL
				//$result=$this->curl->post($dtpost);
				//$result = $this->curl->execute();
				if($resultCurl){
					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';
					echo $resultCurl;
					log_message('INFO', 'Send SC Retur process : Gagal Proses');
					
					
				}else{
					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo 'Gagal ';
					log_message('INFO', 'Test process : Gagal Proses');
					
				}
				
				$data_log_api=array(
					'log_api_tgl' => date("Y-m-d H:i:s"),
					'log_api_ket' => $varSuc,
					'log_api_kategori' => 'POST Sinkronisasi API SC Retur From Unihealth To Soho SAP', 
					'log_api_modul' => 'SD', 
					'log_api_url' => $url, 
					'log_api_status' => $varSta, 
					'log_api_type' => 'PUSH' 
				);
				$this->Mapi_log->writeLogApi($data_log_api);
				
				header('Content-Type: application/json');
				echo json_encode($result, JSON_PRETTY_PRINT);
				
			}else{
				echo 'Data Tidak Tersedia';
			}
		}else{
			echo 'Failed Connect To API Server';	
		}
		
	}

	public function get_ro_retur(){
		$username = 'sgh_ict03';  
		$password = 'golive0518'; 
		//$url='http://172.20.121.37/sohomlm_api/api/unihealth_api/materialGimmick';
		//$url='http://localhost/sohomlm_api/api/unihealth_api_test/insertTest';
		$url='xhttp://uhnsap.webapi.dev.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=sduhninv';
		//$url='http://uhnsap.webapi.sohoglobalhealth.com/sap/bc/yrest_service?sap-client=120/inbound&module=sduhninv';

//		$this->curl->create($url);  
		// Optional, delete this line if your API is open  
//		$loginCek=$this->curl->http_login($username, $password);
//		if ($loginCek){	
			$date = date('m-d-Y');
			$date1 = str_replace('-', '/', $date);
			$tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));
			$startdate=date("Y-m-d");
			
			if (!empty($this->input->post("txtMulai"))){
				$startdate=$this->input->post("txtMulai");
			}else if (!empty($this->input->post("txtSelesai"))){
				$tomorrow =$this->input->post("txtSelesai");
			}
			
			$result=$this->Msales->get_ro_retur_data("2019-06-01","2019-06-30");
			//$result=$this->Msales->get_sc_retur_data($startdate,$tomorrow);
			if (count($result)>0){
				$conArray = json_encode($result);
				$dtpost = array('items'=>$conArray);
				//$dtpost = array($conArray);
				//Metode curl ci
//				$resultCurl=$this->curl->simple_post($url, $dtpost, array(CURLOPT_BUFFERSIZE => 10)); 
				// -- Methode Lain untuk POST Data Via CURL
				//$result=$this->curl->post($dtpost);
				//$result = $this->curl->execute();
/*
				if($resultCurl){
					$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
					$varSta='1';
					echo $resultCurl;
					log_message('INFO', 'Send RO Retur process : Gagal Proses');
					
					
				}else{
					$varSuc='Sinkronisasi Gagal';
					$varSta='0';
					echo 'Gagal ';
					log_message('INFO', 'Test process : Gagal Proses');
					
				}
*/
$varSuc='Sinkronisasi Berhasil, '.count($result).' Data Telah Terkirim';
$varSta='1';


				$data_log_api=array(
					'log_api_tgl' => date("Y-m-d H:i:s"),
					'log_api_ket' => $varSuc,
					'log_api_kategori' => 'POST Sinkronisasi API RO Retur From Unihealth To Soho SAP', 
					'log_api_modul' => 'SD', 
					'log_api_url' => $url, 
					'log_api_status' => $varSta, 
					'log_api_type' => 'PUSH' 
				);
				$this->Mapi_log->writeLogApi($data_log_api);
				
				header('Content-Type: application/json');
				echo json_encode($result, JSON_PRETTY_PRINT);
				
			}else{
				echo 'Data Tidak Tersedia';
			}
/*
		}else{
			echo 'Failed Connect To API Server';	
		}
*/		
	}


}
?>
