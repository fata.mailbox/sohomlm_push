<?php 
class MAuth extends CI_Model{
    function __construct(){
        parent::__construct();
    
	}
   
    /*
    |--------------------------------------------------------------------------
    | jSon API Member List
    |--------------------------------------------------------------------------
    |
    | @created 2019-02-20
    | @author Budi Prayudi 
	| @email : budi@nawadata.com, it.unihealth2@gmail.com
    |     
	*/
	
    public function searchUser($keywords=0,$num,$offset){
		$result = array();
		$cSql="SELECT * FROM account LIMIT 0,50";
		$query=$this->db->query($cSql);
		if ($query){	
            foreach($query->result_array() as $row){
               $cRow=array();
				$cRow['member_id']=$row['member_id'];
				$cRow['member_nama']=$row['name'];
				$result[] = $cRow;
            }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$result[]=$cRow;
		}
		
		//return json_encode($result);
		return $result;
		
	}
	
	public function searchItem($keywords=0,$num,$offset){
		
		$result = array();
		$cSql="SELECT * FROM item ";
		$query=$this->db->query($cSql);
		if ($query){	
            foreach($query->result_array() as $row){
                $cRow=array();
				$cRow['item_id']=$row['id'];
				$cRow['item_nama']=$row['name'];
				$cRow['item_whs']=$row['warehouse_id'];
				$result[] = $cRow;
            }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$result[]=$cRow;
		}
		
		return json_encode($result);
		
    }
    
}
?>