<?php
class Msales extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | API view balance ewallet
    |--------------------------------------------------------------------------
    |
    | @author Budi Prayudi
    | @created 2019-03-06
    |
    */
	
    public function getSo_data($fromdate,$todate){
		$data=array();

		$cSql = "
			SELECT
			a.so_id
			, a.id
			, 'ZU02' AS U_AUART
			, b.tgl AS U_SODAT
			, b.member_id AS U_KUNNR
			, CASE WHEN b.member_id = 'STAFF' THEN 'U2' ELSE 'U1' END AS U_CTYPE
			, CASE WHEN b.delivery_addr > 0 AND k.timur = 0 THEN 'WEST' 
			       WHEN b.delivery_addr > 0 AND k.timur = 1 THEN 'EAST'
			       WHEN b.delivery_addr = 0 AND b.statuspu = '1' AND k2.timur = 0 THEN 'WEST'
			       WHEN b.delivery_addr = 0 AND b.statuspu = '1' AND k2.timur = 1 THEN 'EAST'
			       WHEN b.delivery_addr = 0 AND b.statuspu = '0' THEN 'WEST'
			       END AS U_PL_WE
			, ifnull(w2.sap_code,w.sap_code) AS VKBUR
			, IFNULL(w.sap_code,'1601') AS WERKS
			, CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS MATNR
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.name ELSE i.name END AS ARKTX
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.material_group ELSE i.material_group END AS MATKL
			, 'BATCH' AS CHARG
			, CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*a.qty ELSE a.qty END AS KWMENG
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.hpp ELSE i.hpp END AS COGS
			, CASE WHEN rp.item_id IS NOT NULL AND b.delivery_addr > 0 AND k.timur = 0 AND i2.price > 0 THEN i2.price 
			       WHEN rp.item_id IS NOT NULL AND b.delivery_addr > 0 AND k.timur = 0 AND i2.price = 0 THEN rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.delivery_addr > 0 AND k.timur = 1 AND i2.price2 > 0  THEN i2.price2 
			       WHEN rp.item_id IS NOT NULL AND b.delivery_addr > 0 AND k.timur = 1 AND i2.price2 = 0  THEN rp.item_price 
			       WHEN rp.item_id IS NOT NULL AND b.delivery_addr = 0 AND b.statuspu = '1' AND k2.timur = 0  AND i2.price > 0  THEN i2.price 
			       WHEN rp.item_id IS NOT NULL AND b.delivery_addr = 0 AND b.statuspu = '1' AND k2.timur = 0  AND i2.price = 0  THEN rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.delivery_addr = 0 AND b.statuspu = '1' AND k2.timur = 1  AND i2.price2 > 0  THEN i2.price2 
			       WHEN rp.item_id IS NOT NULL AND b.delivery_addr = 0 AND b.statuspu = '1' AND k2.timur = 1  AND i2.price2 = 0  THEN rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.delivery_addr = 0 AND b.statuspu = '0' AND i2.price > 0  THEN i2.price 
			       WHEN rp.item_id IS NOT NULL AND b.delivery_addr = 0 AND b.statuspu = '0' AND i2.price = 0  THEN rp.item_price
			       WHEN rp.item_id IS NULL AND b.delivery_addr > 0 AND k.timur = 0 THEN i.price 
			       WHEN rp.item_id IS NULL AND b.delivery_addr > 0 AND k.timur = 1 THEN i.price2 
			       WHEN rp.item_id IS NULL AND b.delivery_addr = 0 AND b.statuspu = '1' AND k2.timur = 0 THEN i.price 
			       WHEN rp.item_id IS NULL AND b.delivery_addr = 0 AND b.statuspu = '1' AND k2.timur = 1 THEN i.price2 
			       WHEN rp.item_id IS NULL AND b.delivery_addr = 0 AND b.statuspu = '0' THEN i.price 
			       END AS ZUHN
			, CASE WHEN rp.item_id IS NOT NULL AND b.delivery_addr > 0 AND k.timur = 0 AND i2.price > rp.item_price THEN i2.price - rp.item_price 
			       WHEN rp.item_id IS NOT NULL AND b.delivery_addr > 0 AND k.timur = 1 AND i2.price2 > rp.item_price THEN i2.price2 - rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.delivery_addr = 0 AND b.statuspu = '1' AND k2.timur = 0 AND i2.price > rp.item_price THEN i2.price - rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.delivery_addr = 0 AND b.statuspu = '1' AND k2.timur = 1 AND i2.price2 > rp.item_price THEN i2.price2 - rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.delivery_addr = 0 AND b.statuspu = '0' AND i2.price > rp.item_price THEN i2.price - rp.item_price
			       WHEN rp.item_id IS NULL AND b.delivery_addr > 0 AND k.timur = 0 AND a.harga < i.price THEN i.price - a.harga
			       WHEN rp.item_id IS NULL AND b.delivery_addr > 0 AND k.timur = 1 AND a.harga < i.price2  THEN i.price2 - a.harga 
			       WHEN rp.item_id IS NULL AND b.delivery_addr = 0 AND b.statuspu = '1' AND k2.timur = 0 AND a.harga < i.price  THEN i.price - a.harga
			       WHEN rp.item_id IS NULL AND b.delivery_addr = 0 AND b.statuspu = '1' AND k2.timur = 1 AND a.harga < i.price2  THEN i.price2 - a.harga
			       WHEN rp.item_id IS NULL AND b.delivery_addr = 0 AND b.statuspu = '0' AND a.harga < i.price  THEN i.price - a.harga
			       WHEN rp.item_id IS NULL AND b.delivery_addr = 0 AND a.harga = i.price  THEN 0
			       ELSE 0 END AS ZUA1
			, 0 AS ZUA2
			, 0 AS ZUA4
			, '' AS U_VCDNO
			, '' AS U_FGOOD
			, 0 AS PV
			, 0 AS BV
			, CASE WHEN rp.item_id IS NOT NULL THEN a.item_id ELSE '' END AS YYBANDCD
			, '' AS AUGRU
			, '' AS U_REF_CNCL_INV
			, CASE WHEN rp.item_id IS NOT NULL AND i2.material_group = 'UGIMMICK' THEN 'G' 
			       WHEN rp.item_id IS NULL AND i.material_group = 'UGIMMICK' THEN 'G'
				   ELSE '' END AS ITEM_TYPE
			FROM so_d a
			LEFT JOIN reff_package rp ON a.item_id = rp.package_id AND a.harga = rp.package_price
			LEFT JOIN so b ON b.id = a.so_id 
			LEFT JOIN item i ON i.id = a.item_id  
			LEFT JOIN item i2 ON i2.id = rp.item_id  
			LEFT JOIN member m ON m.id = b.member_id   
			LEFT JOIN warehouse w ON w.id = a.warehouse_id 
			LEFT JOIN warehouse w2 ON w2.id = b.warehouse_id 
			LEFT JOIN member_delivery md ON b.delivery_addr = md.id
			LEFT JOIN kota k ON k.id = md.kota
			LEFT JOIN kota k2 ON k2.id = m.kota_id
			WHERE b.tgl BETWEEN '".$fromdate."' AND '".$todate."' AND b.stockiest_id='0'
			ORDER BY b.id, a.id
			";
		$query=$this->db->query($cSql);
		$curr_so_id =0;
		$thefreeItem = TRUE;
		$xx = 0;
		if ($query){	
			 foreach($query->result_array() as $row){
				if($curr_so_id == 0){
					$curr_so_id =$row['so_id'];	
				}
				
				if($curr_so_id!=$row['so_id']){
					$curr_so_id =$row['so_id'];
					$thefreeItem = TRUE;
					$xx = 0;
				}
				$cRow=array();
				$varTax='Y';
				$varU_AUART=$row['U_AUART'];//'ZU02';
				$varCancelSo='';
				$varCogs=$row['COGS'];
				$varHna=$row['ZUHN'];
				//$varNetValue=$row['totalsales'];
				//$varPPN=$row['totalppn'];
				$varUsGroup=$row['U_CTYPE'];
				$varCancelSoRemark='';
				$varCancelAuart = '';
				$varCancelRef = '';
				//if($row['ITEM_TYPE']=='UGIMMICK')$item_type = 'G'; else $item_type='';
				$item_type = $row['ITEM_TYPE'];
				
				/* Cek Cancel OR RETUR */
				$sqlCek="SELECT a.*, b.remark FROM canceled_so a 
				LEFT JOIN canceled_so_remark b ON b.so_id_canceled = a.so_id_canceled 
				WHERE so_id_execute = ".$row['so_id'];
				$qrCek=$this->db->query($sqlCek);
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						$varCancelSo='ZU2';
						$varU_AUART='ZU06';
						//$varCancelSo=$rCek[''];
						$varCancelSoRemark=$rCek['remark'];
						$row['KWMENG'] = $row['KWMENG']*-1;
						$varCancelAuart = 'ZU02';
						$varCancelRef = $rCek['so_id_canceled'];
					}
				}
				
				/* sk aug */
				if ($row['YYBANDCD'] == 'SK0089' AND $varU_AUART != 'ZU06' AND $row['MATNR'] == 'BY0026' ){
					$row['WERKS'] = '1601';
				}
				if ($row['YYBANDCD'] == 'SK0090' AND $varU_AUART != 'ZU06' AND $row['MATNR'] == 'BY0025' ){
					$row['WERKS'] = '1601';
				}
				if ($row['YYBANDCD'] == 'SK0090' AND $varU_AUART != 'ZU06' AND $row['MATNR'] == 'BY0026' ){
					$row['WERKS'] = '1601';
				}
				if ($row['YYBANDCD'] == 'SK0090' AND $varU_AUART != 'ZU06' AND $row['MATNR'] == 'BY0027' ){
					$row['WERKS'] = '1601';
				}
				if ($row['YYBANDCD'] == 'SK0090' AND $varU_AUART != 'ZU06' AND $row['MATNR'] == 'BY0028' ){
					$row['WERKS'] = '1601';
				}


				/* Cek HNA */
				$sqlCek="SELECT * FROM item WHERE id='".$row['MATNR']."'";
				$qrCek=$this->db->query($sqlCek);
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						$varHna=$rCek['price'];
					}
				}
				
				
				/* get E-Wallet */
				$sqlCek="SELECT * FROM ewallet_mem_d a WHERE a.noref = ".$row['so_id']." and description LIKE 'SO%' and member_id = '".$row['U_KUNNR']."'";
				//echo $sqlCek;
				$qrCek=$this->db->query($sqlCek);
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						$varEWalletHdr=$rCek['ewallet_mem_id'];
						$varEWalletItm=$rCek['id'];
					}
				}

				$xx = $xx+1;
				// echo "<b>".$xx." ".$curr_so_id."</b><br>";

				$cRow['U_AUART']=$varU_AUART;
				$cRow['U_UHINV']=$row['so_id'];
				$cRow['U_POSNR']=$xx;
				$cRow['U_SUBNR']=$row['id'];
				$cRow['U_SODAT']=substr($row['U_SODAT'],8,2).".".substr($row['U_SODAT'],5,2).".".substr($row['U_SODAT'],0,4);
				$cRow['U_KUNNR']=$row['U_KUNNR'];
				$cRow['U_CTYPE']=$row['U_CTYPE'];//$varUsGroup;
				$cRow['U_PL_WE']=$row['U_PL_WE'];//"305";//barat timur
				$cRow['U_TAXCLASS']= "Y";
				$cRow['VKBUR']=$row['VKBUR'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ARKTX']=$row['ARKTX'];
				$cRow['MATKL']=$row['MATKL']; //SAP akan mengambil data dari material master SAP
				$cRow['CHARG']=$row['CHARG']; //SAP akan mengambil batch secara FIFO
				$cRow['KWMENG']=$row['KWMENG'];
				$cRow['COGS']=$varCogs; //COGS Nilai Sebelum PPN cond type ZUCS -- ini bisa bikin kisruh.. wkwkwkwk
				$cRow['ZUHN']=$row['ZUHN'];//$varHna; //HNA Nilai Sebelum PPN cond type ZUH1 -- ini bisa bikin pusing.. hahahhaa
				$cRow['ZUA1']=$row['ZUA1']*$row['KWMENG']; //Disc Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
				$cRow['ZUA2']=$row['ZUA2']; //Disc Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bisa bikin ribut.. hihihi
				$cRow['ZUA4']=$row['ZUA4']; //Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC -- jangan2 bisa bikin resign.. huhuhu
				$cRow['U_VCDNO']=$row['U_VCDNO']; 
				$cRow['U_FGOOD']=$row['U_FGOOD']; //ZUF1 Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC
				//$cRow['ZUH2']=$row['ZUH2']; //Net Value Nilai Sebelum PPN cond type ZUNT -- ini juga bisa bikin kisruh.. wkwkwkwk
				//$cRow['ZPPN']=$varPPN; //PPN Nilai Sebelum PPN cond type ZUTX -- ini yg bikin pusing.. hahahhaa
				$cRow['PV']=$row['PV'];  //PV Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
				$cRow['BV']=$row['BV'];  //BV Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bikin ribut.. hihihi
				$cRow['YYBANDCD']=$row['YYBANDCD']; //Banded Material..
				$cRow['EWALLET_HDR']=$varEWalletHdr; //Banded Material..
				$cRow['EWALLET_ITM']=$varEWalletItm; //Banded Material..
				$cRow['ITEM_TYPE']='';//$item_type; //Inserted date..
				$cRow['WAERS']='IDR'; //Inserted date.
				$cRow['AUGRU']=$varCancelSo;
				$cRow['U_REF_CNCL_INV']= $varCancelRef;//$row['hremark'].' '.$varCancelSoRemark;
				$cRow['U_REF_CNCL_AUART']= $varCancelAuart;//$row['hremark'].' '.$varCancelSoRemark;
				$data[] = $cRow;
				
				
				/* Cek Free Item SO Klw Ada */
				if($thefreeItem){
					$free_itemSql="
					SELECT a.*, 
						c.id as nc_d_id,
						c.item_id as free_item_id,
						i.`name` as Free_Item_Name,
						c.qty as GetFree,		
						b.warehouse_id,
						w.sap_code,
						a.periode,
						i.material_group,
						c.hpp,
						i.price, i.pv, i.bv, i.material_group
					FROM ref_trx_nc a 
					LEFT JOIN ncm b ON b.id = a.nc_id 
					LEFT JOIN ncm_d c ON c.ncm_id = b.id 
					LEFT JOIN item i ON i.id = c.item_id   
					LEFT JOIN warehouse w ON w.id = b.warehouse_id   
					WHERE a.trx_id='".$row['so_id']."'
					AND a.trx = 'SO'
					AND c.id NOT IN (
							'164709'
							,'164710'
							,'164711'
							,'164712'
							,'164714'
							,'164715'
							,'164716'
							,'164717'
							,'164806'
							,'164807'
							,'164808'
							,'164809'
							,'164817'
							,'164972'
							,'164973'
							,'164974'
							,'164975'
							,'164977'
							,'165125'
							,'165126'
							,'165127'
							,'165128'
							,'165137'
							,'165138'
							,'165139'
							,'165140'
							,'165222'
							,'165223'
							,'165224'
							,'165225'
							,'165232'
							,'165313'
							,'165314'
							,'165315'
							,'165316'
							,'165317'
							,'165318'
							,'165405'
							,'165408'
							,'165649'
							,'165692'
					)
					";
					
					$qrFreeItem=$this->db->query($free_itemSql);
					if ($qrFreeItem){
						foreach($qrFreeItem->result_array() as $rowFree){
							$xx++;
							$fRow=array();
							if($rowFree['material_group']=='UGIMMICK')$item_type = 'G'; else $item_type='';
							
							$fRow['U_AUART']=$varU_AUART;
							$fRow['U_UHINV']=$row['so_id'];
							$fRow['U_POSNR']=$xx;
							$fRow['U_SUBNR']=$rowFree['nc_d_id'];
							$fRow['U_SODAT']=substr($rowFree['periode'],8,2).".".substr($rowFree['periode'],5,2).".".substr($rowFree['periode'],0,4);
							$fRow['U_KUNNR']=$row['U_KUNNR'];
							$fRow['U_CTYPE']=$row['U_CTYPE'];;
							$fRow['U_PL_WE']="WEST";//barat timur
							$fRow['U_TAXCLASS']= $varTax;
							$fRow['VKBUR']=$row['VKBUR'];
							$fRow['WERKS']=$rowFree['sap_code'];
							$fRow['MATNR']=$rowFree['free_item_id']; // Item Gratisnya
							$fRow['ARKTX']=$rowFree['Free_Item_Name']; // Nama Item Gratisnya..
							$fRow['MATKL']=$rowFree['material_group']; //SAP akan mengambil data dari material master SAP
							$fRow['CHARG']="BATCH"; //SAP akan mengambil batch secara FIFO
							$fRow['KWMENG']=$rowFree['GetFree']; // Dpt Gratisnya
							$fRow['COGS']=$rowFree['hpp']; //COGS Nilai Sebelum PPN cond type ZUCS -- ini bisa bikin kisruh.. wkwkwkwk
							$fRow['ZUHN']=$rowFree['price']; //HNA Nilai Sebelum PPN cond type ZUH1 -- ini bisa bikin pusing.. hahahhaa
							$fRow['ZUA1']=$rowFree['price']*$rowFree['GetFree']; //Disc Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
							$fRow['ZUA2']="0"; //Disc Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bisa bikin ribut.. hihihi
							$fRow['ZUA4']="0"; //Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC -- jangan2 bisa bikin resign.. huhuhu
							$fRow['U_VCDNO']=""; 
							$fRow['U_FGOOD']="ZUF1"; //ZUF1 Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC
							$fRow['PV']=$rowFree['pv'];;  //PV Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
							$fRow['BV']=$rowFree['bv'];;  //BV Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bikin ribut.. hihihi
							$fRow['YYBANDCD']=""; //Banded Material..
							$fRow['EWALLET_HDR']=$varEWalletHdr; //Banded Material..
							$fRow['EWALLET_ITM']=$varEWalletItm; //Banded Material..
							$fRow['ITEM_TYPE']='';//$item_type;
							
							$fRow['AUGRU']=$varCancelSo;
							//$fRow['U_REF_CNCL_INV']= 'Free Item '.$row['so_id'].' - '.$row['hremark'].' '.$varCancelSoRemark;
							$fRow['U_REF_CNCL_INV']= $varCancelRef;//$row['hremark'].' '.$varCancelSoRemark;
							$fRow['U_REF_CNCL_AUART']= $varCancelAuart;//$row['hremark'].' '.$varCancelSoRemark;
							$fRow['WAERS']='IDR';
							
							$data[] = $fRow;
						}
						
					}
					$thefreeItem = FALSE;
				}
				/** End Of Free Item RO **/
				
				//$xx++;
				
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return $data;


	}
	
	
	public function getRo_data($fromdate,$todate){
		$data=array();

		$cSql = "
			SELECT
			a.ro_id
			, a.id
			, 'ZU01' AS U_AUART
			, b.date AS U_SODAT
			, b.member_id AS U_KUNNR
			, 'U0' AS U_CTYPE
			, CASE WHEN b.deliv_addr > 0 AND k.timur = 0 THEN 'WEST' 
			       WHEN b.deliv_addr > 0 AND k.timur = 1 THEN 'EAST'
			       WHEN b.deliv_addr = 0 AND b.delivery = '1' AND k2.timur = 0 THEN 'WEST'
			       WHEN b.deliv_addr = 0 AND b.delivery = '1' AND k2.timur = 1 THEN 'EAST'
			       WHEN b.deliv_addr = 0 AND b.delivery = '0' THEN 'WEST'
			       END AS U_PL_WE
			, IFNULL(w2.sap_code,w.sap_code) AS VKBUR
			, IFNULL(w.sap_code,'1601') AS WERKS
			, CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS MATNR
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.name ELSE i.name END AS ARKTX
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.material_group ELSE i.material_group END AS MATKL
			, 'BATCH' AS CHARG
			, CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*a.qty ELSE a.qty END AS KWMENG
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.hpp ELSE i.hpp END AS COGS
			, CASE WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 0 AND i2.price > 0 THEN i2.price 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 0 AND i2.price = 0 THEN rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 1 AND i2.price > 0  THEN i2.price2 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 1 AND i2.price = 0  THEN rp.item_price 
			       -- WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND i2.price > 0  THEN i2.price 
			       -- WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND i2.price = 0  THEN rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 0 AND i2.price > 0  THEN i2.price 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 0 AND i2.price = 0  THEN rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 1 AND i2.price2 > 0  THEN i2.price2 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 1 AND i2.price2 = 0  THEN rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.delivery = 0 AND i2.price > 0  THEN i2.price 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.delivery = 0 AND i2.price = 0  THEN rp.item_price
			       WHEN rp.item_id IS NULL AND b.deliv_addr > 0 AND k.timur = 0 THEN i.price 
			       WHEN rp.item_id IS NULL AND b.deliv_addr > 0 AND k.timur = 1 THEN i.price2 
			       WHEN rp.item_id IS NULL AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 0 THEN i.price 
			       WHEN rp.item_id IS NULL AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 1 THEN i.price2
			       WHEN rp.item_id IS NULL AND b.deliv_addr = 0 AND b.delivery = 0 THEN i.price
			       END AS ZUHN
			, CASE WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 0 AND i2.price > 0 THEN i2.price - rp.item_price 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 0 AND i2.price = 0 THEN 0
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 1 AND i2.price2 > 0  THEN i2.price2 - rp.item_price 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 1 AND i2.price2 = 0  THEN 0 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 0  AND i2.price > 0  THEN i2.price - rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 0  AND i2.price = 0  THEN 0
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 1  AND i2.price2 > 0  THEN i2.price2 - rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 1  AND i2.price2 = 0  THEN 0
			      	WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.delivery = 0 AND i2.price > 0  THEN i2.price - rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.delivery = 0 AND i2.price = 0  THEN 0
			    	WHEN rp.item_id IS NULL AND a.item_id = 'NT0062' AND a.harga_ = 139000 AND b.deliv_addr > 0 AND k.timur = 0 THEN 10000 
			       WHEN rp.item_id IS NULL AND a.item_id = 'NT0062' AND a.harga_ = 152900 AND b.deliv_addr > 0 AND k.timur = 1 THEN 11000
			       WHEN rp.item_id IS NULL AND a.item_id = 'NT0062' AND a.harga_ = 139000 AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 0 THEN 10000 
			       WHEN rp.item_id IS NULL AND a.item_id = 'NT0062' AND a.harga_ = 152900 AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 1 THEN 11000
			       WHEN rp.item_id IS NULL AND a.item_id = 'NT0062' AND a.harga_ = 139000 AND b.deliv_addr = 0 AND b.delivery = 0 THEN 10000
			       WHEN rp.item_id IS NULL AND a.item_id = 'NT0063' AND a.harga_ = 139000 AND b.deliv_addr > 0 AND k.timur = 0 THEN 10000 
			       WHEN rp.item_id IS NULL AND a.item_id = 'NT0063' AND a.harga_ = 152900 AND b.deliv_addr > 0 AND k.timur = 1 THEN 11000
			       WHEN rp.item_id IS NULL AND a.item_id = 'NT0063' AND a.harga_ = 139000 AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 0 THEN 10000 
			       WHEN rp.item_id IS NULL AND a.item_id = 'NT0063' AND a.harga_ = 152900 AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 1 THEN 11000
			       WHEN rp.item_id IS NULL AND a.item_id = 'NT0063' AND a.harga_ = 139000 AND b.deliv_addr = 0 AND b.delivery = 0 THEN 10000
			       -- eof tambahan
			       ELSE 0
			       END AS ZUA1
			-- , CASE WHEN rp.item_id IS NOT NULL THEN a.harga_-rp.item_price ELSE 0 END AS ZUA1
			, 0 AS ZUA2
			, 0 AS ZUA4
			, '' AS U_VCDNO
			, '' AS U_FGOOD
			, 0 AS PV
			, 0 AS BV
			, CASE WHEN rp.item_id IS NOT NULL THEN a.item_id ELSE '' END AS YYBANDCD
			, '' AS AUGRU
			, '' AS U_REF_CNCL_INV
			, CASE WHEN rp.item_id IS NOT NULL AND i2.material_group = 'UGIMMICK' THEN 'G' 
			       WHEN rp.item_id IS NULL AND i.material_group = 'UGIMMICK' THEN 'G'
				   ELSE '' END AS ITEM_TYPE
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.material_group ELSE i.material_group END AS item_type
			, dt.pinjaman_id
			, a.harga_ - a.harga AS diskonrp
			FROM ro_d a
			LEFT JOIN reff_package rp ON a.item_id = rp.package_id AND a.harga_ = rp.package_price
			LEFT JOIN ro b ON b.id = a.ro_id 
			LEFT JOIN item i ON i.id = a.item_id  
			LEFT JOIN item i2 ON i2.id = rp.item_id  
			LEFT JOIN member m ON m.id = b.member_id   
			LEFT JOIN warehouse w ON w.id = a.warehouse_id 
			LEFT JOIN warehouse w2 ON w2.id = b.warehouse_id 
			LEFT JOIN member_delivery md ON b.deliv_addr = md.id
			LEFT JOIN kota k ON k.id = md.kota
			LEFT JOIN kota k2 ON k2.id = m.kota_id
			LEFT JOIN (
				SELECT ro_id, pinjaman_id
				FROM pinjaman_payment pp 
				WHERE pp.pinjaman_payment_tgl BETWEEN '".$fromdate."' AND '".$todate."'
				GROUP BY ro_id
			) AS dt ON dt.ro_id = a.ro_id
			WHERE b.date BETWEEN '".$fromdate."' AND '".$todate."'
			HAVING dt.pinjaman_id IS NULL
			order by b.id, a.id
			";
		$query=$this->db->query($cSql);
		
		$curr_ro_id =0;
		$thefreeItem = TRUE;
		$xx = 0;
		
		if ($query){	
			 foreach($query->result_array() as $row){
				if($curr_ro_id == 0){
					$curr_ro_id =$row['ro_id'];	
				}
				
				if($curr_ro_id!=$row['ro_id']){
					$curr_ro_id =$row['ro_id'];
					$thefreeItem = TRUE;
					$xx = 0;
				}
				$cRow=array();
				
				$varTax='Y';
				$varU_AUART=$row['U_AUART'];//'ZU02';
				$varCancelSo='';
				$varCogs=$row['COGS'];
				$varHna=$row['ZUHN'];
				//$varNetValue=$row['totalsales'];
				//$varPPN=$row['totalppn'];
				$varUsGroup=$row['U_CTYPE'];
				$varCancelSoRemark='';
				$varCancelAuart = '';
				$varCancelRef = '';
				$varZUA2 = 0;
				//if($row['ITEM_TYPE']=='UGIMMICK')$item_type = 'G'; else $item_type='';
				$item_type = $row['ITEM_TYPE'];
				
				/* Cek Cancel OR RETUR */
				$sqlCek="SELECT a.*, b.remark FROM cancel_ro a 
				LEFT JOIN cancel_ro_remark b ON b.ro_id_canceled = a.ro_id_canceled 
				WHERE a.ro_id_execute = ".$row['ro_id'];
				$qrCek=$this->db->query($sqlCek);
				$rCek = array();
				if ($qrCek){
					foreach($qrCek->result_array() as $rCek){
						$varCancelSo='ZU2';
						$varU_AUART='ZU06';
						//$varCancelSo=$rCek[''];
						$varCancelSoRemark=$rCek['remark'];
						$row['KWMENG'] = $row['KWMENG']*-1;
						$varCancelAuart = 'ZU01';
						$varCancelRef = $rCek['ro_id_canceled'];
					}
				}
				/* sk aug */
				if ($row['YYBANDCD'] == 'SK0089' AND $varU_AUART != 'ZU06' AND $row['MATNR'] == 'BY0026' ){
					$row['WERKS'] = '1601';
				}
				if ($row['YYBANDCD'] == 'SK0090' AND $varU_AUART != 'ZU06' AND $row['MATNR'] == 'BY0025' ){
					$row['WERKS'] = '1601';
				}
				if ($row['YYBANDCD'] == 'SK0090' AND $varU_AUART != 'ZU06' AND $row['MATNR'] == 'BY0026' ){
					$row['WERKS'] = '1601';
				}
				if ($row['YYBANDCD'] == 'SK0090' AND $varU_AUART != 'ZU06' AND $row['MATNR'] == 'BY0027' ){
					$row['WERKS'] = '1601';
				}
				if ($row['YYBANDCD'] == 'SK0090' AND $varU_AUART != 'ZU06' AND $row['MATNR'] == 'BY0028' ){
					$row['WERKS'] = '1601';
				}
				
				/* get E-Wallet */
				$sqlCek="SELECT * FROM ewallet_stc_d a WHERE a.noref = ".$row['ro_id']." and description = 'Request Order' and member_id = '".$row['U_KUNNR']."'";
				$qrCek=$this->db->query($sqlCek);
				$rCek = array();
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						$varEWalletHdr=$rCek['ewallet_stc_id'];
						$varEWalletItm=$rCek['id'];
					}
				}
				
				/* Cek HJP atau COGS */
				/*
				$sqlCek="SELECT * FROM hjp_history WHERE item_id='".$row['item_id']."' ORDER BY id DESC LIMIT 1";
				$qrCek=$this->db->query($sqlCek);
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						$varCogs=$rCek['hpp'];
					}
				}
				*/
				
				/*cek diskon*/
				if($row['YYBANDCD']!=''){
					$sqlCekItemqty="SELECT COUNT(*) as jmlitem FROM manufaktur WHERE manufaktur_id = '".$row['YYBANDCD']."'";
					$qrCekItemqty=$this->db->query($sqlCekItemqty);
					$rCekItemqty = array();
					if ($qrCekItemqty){	
						foreach($qrCekItemqty->result_array() as $rCekItemqty){
							$Itemqty=$rCekItemqty['jmlitem'];
						}
						$row['diskonrp'] = $row['diskonrp']/$Itemqty;
						// cek item qty detail
						$sqlCekItemqtyDet="SELECT qty FROM manufaktur WHERE manufaktur_id = '".$row['YYBANDCD']."' AND item_id = '".$row['MATNR']."'";
						$qrCekItemqtyDet=$this->db->query($sqlCekItemqtyDet);
						$rCekItemqtyDet = array();
						if ($qrCekItemqtyDet){	
							foreach($qrCekItemqtyDet->result_array() as $rCekItemqtyDet){
								$Itemqtydet=$rCekItemqtyDet['qty'];
							}
							$row['diskonrp'] = $row['diskonrp']/$Itemqtydet;
						}
					}
				$qrCek=$this->db->query($sqlCek);
				}else{
					$varZUA2 = $row['diskonrp'];
				}
				
				$xx = $xx + 1;
				echo "<b>".$xx." ".$curr_ro_id."</b><br>";
				
				$cRow['U_AUART']=$varU_AUART;
				$cRow['U_UHINV']=$row['ro_id'];
				$cRow['U_POSNR']=$xx;
				$cRow['U_SUBNR']=$row['id'];
				$cRow['U_SODAT']=substr($row['U_SODAT'],8,2).".".substr($row['U_SODAT'],5,2).".".substr($row['U_SODAT'],0,4);
				$cRow['U_KUNNR']=$row['U_KUNNR'];
				$cRow['U_CTYPE']=$row['U_CTYPE'];//$varUsGroup;
				$cRow['U_PL_WE']=$row['U_PL_WE'];//"305";//barat timur
				$cRow['U_TAXCLASS']= "Y";
				$cRow['VKBUR']=$row['VKBUR'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ARKTX']=$row['ARKTX'];
				$cRow['MATKL']=$row['MATKL']; //SAP akan mengambil data dari material master SAP
				$cRow['CHARG']=$row['CHARG']; //SAP akan mengambil batch secara FIFO
				$cRow['KWMENG']=$row['KWMENG'];
				$cRow['COGS']=$varCogs; //COGS Nilai Sebelum PPN cond type ZUCS -- ini bisa bikin kisruh.. wkwkwkwk
				$cRow['ZUHN']=$row['ZUHN'];//$varHna; //HNA Nilai Sebelum PPN cond type ZUH1 -- ini bisa bikin pusing.. hahahhaa
				$cRow['ZUA1']=$row['ZUA1']*$row['KWMENG']; //Disc Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
				$cRow['ZUA2']=round($row['diskonrp'])*$row['KWMENG'];// $row['ZUA2']; //Disc Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bisa bikin ribut.. hihihi
				$cRow['ZUA4']=$row['ZUA4']; //Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC -- jangan2 bisa bikin resign.. huhuhu
				$cRow['U_VCDNO']=$row['U_VCDNO']; 
				$cRow['U_FGOOD']=$row['U_FGOOD']; //ZUF1 Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC
				//$cRow['ZUH2']=$row['ZUH2']; //Net Value Nilai Sebelum PPN cond type ZUNT -- ini juga bisa bikin kisruh.. wkwkwkwk
				//$cRow['ZPPN']=$varPPN; //PPN Nilai Sebelum PPN cond type ZUTX -- ini yg bikin pusing.. hahahhaa
				$cRow['PV']=$row['PV'];  //PV Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
				$cRow['BV']=$row['BV'];  //BV Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bikin ribut.. hihihi
				$cRow['YYBANDCD']=$row['YYBANDCD']; //Banded Material..
				$cRow['EWALLET_HDR']=$varEWalletHdr; //Banded Material..
				$cRow['EWALLET_ITM']=$varEWalletItm; //Banded Material..
				$cRow['ITEM_TYPE']=$item_type; //Inserted date..
				$cRow['WAERS']='IDR'; //Inserted date..
				//$cRow['ERDAT']=""; //Inserted date..
				//$cRow['ERZET']=""; //Inserted time..
				/*
				ZU01 = Return
				ZU02 = Cancel billing
				*/
				$cRow['AUGRU']=$varCancelSo;
				$cRow['U_REF_CNCL_INV']= $varCancelRef;//$row['hremark'].' '.$varCancelSoRemark;
				$cRow['U_REF_CNCL_AUART']= $varCancelAuart;//$row['hremark'].' '.$varCancelSoRemark;
				
				$data[] = $cRow;
				
				/** Start Free Item RO **/
				/* Cek Free Item SO Klw Ada */
				if($thefreeItem){
					$free_itemSql="
						SELECT a.*, 
							c.id as nc_d_id,
							c.item_id as free_item_id,
							i.`name` as Free_Item_Name,
							c.qty as GetFree,		
							b.warehouse_id,
							w.sap_code,
							a.periode,
							i.material_group,
							c.hpp,
							i.price, i.pv, i.bv, i.material_group
						FROM ref_trx_nc a 
						LEFT JOIN ncm b ON b.id = a.nc_id 
						LEFT JOIN ncm_d c ON c.ncm_id = b.id 
						LEFT JOIN item i ON i.id = c.item_id   
						LEFT JOIN warehouse w ON w.id = b.warehouse_id   
						WHERE a.trx_id='".$row['ro_id']."'
						AND a.trx = 'RO'
						and c.id NOT IN (
							'165835'
							,'165838'
							,'165839'
							,'165846'
							,'165847'
							,'165848'
							,'165849'
							,'165851'
							,'165852'
							,'165853'
							,'165854'
							,'165855'
							,'165862'
							,'165863'
							,'165864'
							,'165865'
							,'165867'
							,'165868'
							,'165869'
							,'165870'
							,'165871'
							,'165872'
							,'165873'
							,'165874'
							,'165875'
							,'165876'
							,'165877'
							,'165878'
							,'165879'
							,'165880'
							,'165881'
							,'165882'
							,'165883'
							,'165884'
							,'165889'
							,'165890'
							,'165891'
							,'165892'
							,'165893'
							,'165896'
							,'165897'
							,'165898'
							,'165899'
							,'165900'
							,'165902'
							,'165910'
							,'165911'
							,'165912'
							,'165913'
							,'165914'
							,'165925'
							,'165926'
							,'165927'
							,'165928'
							,'165929'
							,'165930'
							,'165936'
							,'165937'
							,'165938'
							,'165939'
							,'165940'
							,'165941'
							,'165942'
							,'165943'
							,'165944'
							,'165945'
							,'165946'
							,'165947'
							,'165948'
							,'165949'
							,'165950'
							,'165952'
							,'165958'
							,'165959'
							,'165960'
							,'165961'
							,'165963'
							,'165965'
							,'165966'
							,'165967'
							,'165968'
							,'165969'
							,'165986'
							,'165987'
							,'165988'
							,'165989'
							,'165992'
							,'165993'
							,'165994'
							,'165995'
							,'165996'
							,'165997'
							,'165998'
							,'165999'
							,'166012'
							,'166013'
							,'166014'
							,'166015'
							,'166016'
							,'166017'
							,'166018'
							,'166019'
							,'166026'
							,'166029'
							,'166030'
							,'166031'
							,'166032'
							,'166036'
							,'166037'
							,'166038'
							,'166039'
							,'166040'
							,'166047'
							,'166049'
							,'166086'
							,'166087'
							,'166094'
							,'166098'
							,'166111'
							,'166112'
							,'166113'
							,'166114'
							,'166115'
							,'166116'
							,'166117'
							,'166118'
							,'166119'
							,'166130'
							,'166131'
							,'166132'
							,'166133'
							,'166134'
							,'166135'
							,'166137'
							,'166138'
							,'166139'
							,'166140'
							,'166143'
							,'166144'
							,'166145'
							,'166146'
							,'166147'
							,'166148'
							,'166149'
							,'166150'
							,'166155'
							,'166156'
							,'166157'
							,'166158'
							,'166159'
							,'166160'
							,'166168'
							,'166171'
							,'166172'
							,'166173'
							,'166177'
							,'166178'
							,'166179'
							,'166180'
							,'166181'
							,'166191'
							,'166192'
							,'166193'
							,'166194'
							,'166195'
							,'166198'
							,'166203'
							,'166204'
							,'166205'
							,'166206'

					)
					";
					
					$qrFreeItem=$this->db->query($free_itemSql);
					if ($qrFreeItem){
						foreach($qrFreeItem->result_array() as $rowFree){
							$xx++;
							$fRow=array();
							if($rowFree['material_group']=='UGIMMICK')$item_type = 'G'; else $item_type='';
							
							$fRow['U_AUART']=$varU_AUART;
							$fRow['U_UHINV']=$row['ro_id'];
							$fRow['U_POSNR']=$xx;
							$fRow['U_SUBNR']=$rowFree['nc_d_id'];
							$fRow['U_SODAT']=substr($rowFree['periode'],8,2).".".substr($rowFree['periode'],5,2).".".substr($rowFree['periode'],0,4);
							$fRow['U_KUNNR']=$row['U_KUNNR'];
							$fRow['U_CTYPE']=$row['U_CTYPE'];
							$fRow['U_PL_WE']="WEST";//barat timur
							$fRow['U_TAXCLASS']= $varTax;
							$fRow['VKBUR']=$row['VKBUR'];
							$fRow['WERKS']=$rowFree['sap_code'];
							$fRow['MATNR']=$rowFree['free_item_id']; // Item Gratisnya
							$fRow['ARKTX']=$rowFree['Free_Item_Name']; // Nama Item Gratisnya..
							$fRow['MATKL']=$rowFree['material_group']; //SAP akan mengambil data dari material master SAP
							$fRow['CHARG']="BATCH"; //SAP akan mengambil batch secara FIFO
							$fRow['KWMENG']=$rowFree['GetFree']; // Dpt Gratisnya
							$fRow['COGS']=$rowFree['hpp']; //COGS Nilai Sebelum PPN cond type ZUCS -- ini bisa bikin kisruh.. wkwkwkwk
							$fRow['ZUHN']=$rowFree['price']; //HNA Nilai Sebelum PPN cond type ZUH1 -- ini bisa bikin pusing.. hahahhaa
							$fRow['ZUA1']=$rowFree['price']*$rowFree['GetFree']; //Disc Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe	
							$fRow['ZUA2']="0"; //Disc Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bisa bikin ribut.. hihihi
							$fRow['ZUA4']="0"; //Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC -- jangan2 bisa bikin resign.. huhuhu
							$fRow['U_VCDNO']=""; 
							$fRow['U_FGOOD']="ZUF1"; //ZUF1 Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC
							$fRow['PV']=$rowFree['pv'];;  //PV Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
							$fRow['BV']=$rowFree['bv'];;  //BV Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bikin ribut.. hihihi
							$fRow['YYBANDCD']=""; //Banded Material..
							$fRow['EWALLET_HDR']=$varEWalletHdr; //Banded Material..
							$fRow['EWALLET_ITM']=$varEWalletItm; //Banded Material..
							$fRow['ITEM_TYPE']=$item_type;
								
							$fRow['AUGRU']=$varCancelSo;
							//$fRow['U_REF_CNCL_INV']= 'Free Item '.$row['so_id'].' - '.$row['hremark'].' '.$varCancelSoRemark;
							$fRow['U_REF_CNCL_INV']= $varCancelRef;//$row['hremark'].' '.$varCancelSoRemark;
							$fRow['U_REF_CNCL_AUART']= $varCancelAuart;//$row['hremark'].' '.$varCancelSoRemark;
							$fRow['WAERS']='IDR';
								
							$data[] = $fRow;
						}
						$thefreeItem = FALSE;
					}

				}
				/** End Of Free Item RO **/
				//$xx++;
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return $data;


	}
	
	public function getScRo_data($fromdate,$todate){
		$data=array();
		/*
		$cSql="SELECT
			a.*, YEAR(b.date) as thn,
			i.`name` as item_nm,
			i.satuan,
			b.member_id as member_id,
			m.nama as member_nama,
			m.npwp,
			b.warehouse_id as whs_id,
			w.sap_code as warehouse_id,
			b.totalharga as totalharga,
			b.totalbv as totalbv,
			b.totalpv as totalpv,
			b.date as hdate,
			b.created as hcreated,
			b.createdby as hcreatedby,
			DATE_FORMAT(b.date, '%d.%m.%Y') AS tgl_sap,
			b.remark as hremark 
			FROM
				(ro_d a)
			INNER JOIN ro b ON b.id = a.ro_id 
			INNER JOIN item i ON i.id = a.item_id  
			INNER JOIN member m ON m.id = b.member_id   
			INNER JOIN warehouse w ON w.id = b.warehouse_id   
			WHERE b.date BETWEEN '".$fromdate."' AND '".$todate."' ";
		*/
		$cSql = "
			SELECT
			a.ro_id
			, a.id
			, 'ZU05' AS U_AUART
			, b.date AS U_SODAT
			, b.member_id AS U_KUNNR
			, 'U0' AS U_CTYPE
			, CASE WHEN b.deliv_addr > 0 AND k.timur = 0 THEN 'WEST' 
			       WHEN b.deliv_addr > 0 AND k.timur = 1 THEN 'EAST'
			       WHEN b.deliv_addr = 0 AND b.delivery = '1' AND k2.timur = 0 THEN 'WEST'
			       WHEN b.deliv_addr = 0 AND b.delivery = '1' AND k2.timur = 1 THEN 'EAST'
			       WHEN b.deliv_addr = 0 AND b.delivery = '0' THEN 'WEST'
			       END AS U_PL_WE
			, IFNULL(w2.sap_code,w.sap_code) AS VKBUR
			, IFNULL(w.sap_code,'1601') AS WERKS
			, CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS MATNR
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.name ELSE i.name END AS ARKTX
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.material_group ELSE i.material_group END AS MATKL
			, 'BATCH' AS CHARG
			, CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*a.qty ELSE a.qty END AS KWMENG
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.hpp ELSE i.hpp END AS COGS
			/*
			, CASE WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 0 AND i2.price > 0 THEN i2.price 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 0 AND i2.price = 0 THEN rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 1 AND i2.price > 0  THEN i2.price2 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 1 AND i2.price = 0  THEN rp.item_price 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND i2.price > 0  THEN i2.price 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND i2.price = 0  THEN rp.item_price
			       WHEN rp.item_id IS NULL AND b.deliv_addr > 0 AND k.timur = 0 THEN i.price 
			       WHEN rp.item_id IS NULL AND b.deliv_addr > 0 AND k.timur = 1 THEN i.price2 
			       WHEN rp.item_id IS NULL AND b.deliv_addr = 0 THEN i.price 
			       END AS ZUHN
			, CASE WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 0 AND i2.price > 0 THEN i2.price - rp.item_price 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 0 AND i2.price = 0 THEN 0
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 1 AND i2.price > 0  THEN i2.price2 - rp.item_price 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 1 AND i2.price = 0  THEN 0 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND i2.price > 0  THEN i2.price - rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND i2.price = 0  THEN 0
			       ELSE 0
			       END AS ZUA1
			*/
			, CASE WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 0 AND i2.price > 0 THEN i2.price 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 0 AND i2.price = 0 THEN rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 1 AND i2.price > 0  THEN i2.price2 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 1 AND i2.price = 0  THEN rp.item_price 
			       -- WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND i2.price > 0  THEN i2.price 
			       -- WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND i2.price = 0  THEN rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 0 AND i2.price > 0  THEN i2.price 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 0 AND i2.price = 0  THEN rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 1 AND i2.price2 > 0  THEN i2.price2 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 1 AND i2.price2 = 0  THEN rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.delivery = 0 AND i2.price > 0  THEN i2.price 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.delivery = 0 AND i2.price = 0  THEN rp.item_price
			       WHEN rp.item_id IS NULL AND b.deliv_addr > 0 AND k.timur = 0 THEN i.price 
			       WHEN rp.item_id IS NULL AND b.deliv_addr > 0 AND k.timur = 1 THEN i.price2 
			       WHEN rp.item_id IS NULL AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 0 THEN i.price 
			       WHEN rp.item_id IS NULL AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 1 THEN i.price2
			       WHEN rp.item_id IS NULL AND b.deliv_addr = 0 AND b.delivery = 0 THEN i.price
			       END AS ZUHN
			, CASE WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 0 AND i2.price > 0 THEN i2.price - rp.item_price 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 0 AND i2.price = 0 THEN 0
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 1 AND i2.price2 > 0  THEN i2.price2 - rp.item_price 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 1 AND i2.price2 = 0  THEN 0 
			       -- WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND i2.price > 0  THEN i2.price - rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 0  AND i2.price > 0  THEN i2.price - rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 0  AND i2.price = 0  THEN 0
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 1  AND i2.price2 > 0  THEN i2.price2 - rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 1  AND i2.price2 = 0  THEN 0
			       -- WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND i2.price = 0  THEN 0
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.delivery = 0 AND i2.price > 0  THEN i2.price - rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.delivery = 0 AND i2.price = 0  THEN 0
			       -- tambahan
			       WHEN rp.item_id IS NULL AND a.item_id = 'NT0062' AND a.harga_ = 139000 AND b.deliv_addr > 0 AND k.timur = 0 THEN 10000 
			       WHEN rp.item_id IS NULL AND a.item_id = 'NT0062' AND a.harga_ = 152900 AND b.deliv_addr > 0 AND k.timur = 1 THEN 11000
			       WHEN rp.item_id IS NULL AND a.item_id = 'NT0062' AND a.harga_ = 139000 AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 0 THEN 10000 
			       WHEN rp.item_id IS NULL AND a.item_id = 'NT0062' AND a.harga_ = 152900 AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 1 THEN 11000
			       WHEN rp.item_id IS NULL AND a.item_id = 'NT0062' AND a.harga_ = 139000 AND b.deliv_addr = 0 AND b.delivery = 0 THEN 10000
			       WHEN rp.item_id IS NULL AND a.item_id = 'NT0063' AND a.harga_ = 139000 AND b.deliv_addr > 0 AND k.timur = 0 THEN 10000 
			       WHEN rp.item_id IS NULL AND a.item_id = 'NT0063' AND a.harga_ = 152900 AND b.deliv_addr > 0 AND k.timur = 1 THEN 11000
			       WHEN rp.item_id IS NULL AND a.item_id = 'NT0063' AND a.harga_ = 139000 AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 0 THEN 10000 
			       WHEN rp.item_id IS NULL AND a.item_id = 'NT0063' AND a.harga_ = 152900 AND b.deliv_addr = 0 AND b.delivery = 1 AND k2.timur = 1 THEN 11000
			       WHEN rp.item_id IS NULL AND a.item_id = 'NT0063' AND a.harga_ = 139000 AND b.deliv_addr = 0 AND b.delivery = 0 THEN 10000
			       -- eof tambahan
			       ELSE 0
			       END AS ZUA1
			-- , CASE WHEN rp.item_id IS NOT NULL THEN a.harga_-rp.item_price ELSE 0 END AS ZUA1
			, 0 AS ZUA2
			, 0 AS ZUA4
			, '' AS U_VCDNO
			, '' AS U_FGOOD
			, 0 AS PV
			, 0 AS BV
			, CASE WHEN rp.item_id IS NOT NULL THEN a.item_id ELSE '' END AS YYBANDCD
			, '' AS AUGRU
			, '' AS U_REF_CNCL_INV
			, CASE WHEN rp.item_id IS NOT NULL AND i2.material_group = 'UGIMMICK' THEN 'G' 
			       WHEN rp.item_id IS NULL AND i.material_group = 'UGIMMICK' THEN 'G'
				   ELSE '' END AS ITEM_TYPE
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.material_group ELSE i.material_group END AS item_type
			, dt.pinjaman_id
			, a.harga_ - a.harga AS diskonrp
			FROM ro_d a
			LEFT JOIN reff_package rp ON a.item_id = rp.package_id AND a.harga_ = rp.package_price
			LEFT JOIN ro b ON b.id = a.ro_id 
			LEFT JOIN item i ON i.id = a.item_id  
			LEFT JOIN item i2 ON i2.id = rp.item_id  
			LEFT JOIN member m ON m.id = b.member_id   
			LEFT JOIN warehouse w ON w.id = a.warehouse_id 
			LEFT JOIN warehouse w2 ON w2.id = b.warehouse_id 
			LEFT JOIN member_delivery md ON b.deliv_addr = md.id
			LEFT JOIN kota k ON k.id = md.kota
			LEFT JOIN kota k2 ON k2.id = m.kota_id
			LEFT JOIN (
				SELECT ro_id, pinjaman_id
				FROM pinjaman_payment pp 
				WHERE pp.pinjaman_payment_tgl BETWEEN '".$fromdate."' AND '".$todate."'
				GROUP BY ro_id
			) AS dt ON dt.ro_id = a.ro_id
			WHERE b.date BETWEEN '".$fromdate."' AND '".$todate."'
			-- and a.ro_id in(52872,52913,52923,52925,52969,53033,53034,53036,53040,53074,53095,53137,53143,53145,53151,53170,53178,53194,53240,53358)
			-- and a.ro_id in(52565,52726,52758,52798,52847)
			HAVING dt.pinjaman_id IS NOT NULL
			order by b.id, a.id
			";
		$query=$this->db->query($cSql);
		
		$curr_ro_id =0;
		$thefreeItem = TRUE;
		$xx = 0;
		
		if ($query){	
			 foreach($query->result_array() as $row){
				if($curr_ro_id == 0){
					$curr_ro_id =$row['ro_id'];	
				}
				
				if($curr_ro_id!=$row['ro_id']){
					$curr_ro_id =$row['ro_id'];
					$thefreeItem = TRUE;
					$xx = 0;
				}
				$cRow=array();
				
				$varTax='Y';
				$varU_AUART=$row['U_AUART'];//'ZU02';
				$varCancelSo='';
				$varCogs=$row['COGS'];
				$varHna=$row['ZUHN'];
				//$varNetValue=$row['totalsales'];
				//$varPPN=$row['totalppn'];
				$varUsGroup=$row['U_CTYPE'];
				$varCancelSoRemark='';
				$varCancelAuart = '';
				$varCancelRef = '';
				$varZUA2 = 0;
				//if($row['ITEM_TYPE']=='UGIMMICK')$item_type = 'G'; else $item_type='';
				$item_type = $row['ITEM_TYPE'];
				
				/* Cek Cancel OR RETUR */
				$sqlCek="SELECT a.*, b.remark FROM cancel_ro a 
				LEFT JOIN cancel_ro_remark b ON b.ro_id_canceled = a.ro_id_canceled 
				WHERE a.ro_id_execute = ".$row['ro_id'];
				$qrCek=$this->db->query($sqlCek);
				$rCek = array();
				if ($qrCek){
					foreach($qrCek->result_array() as $rCek){
						$varCancelSo='ZU2';
						$varU_AUART='ZU06';
						//$varCancelSo=$rCek[''];
						$varCancelSoRemark=$rCek['remark'];
						$row['KWMENG'] = $row['KWMENG']*-1;
						$varCancelAuart = 'ZU01';
						$varCancelRef = $rCek['ro_id_canceled'];
					}
				}

				/* sk aug */
				if ($row['YYBANDCD'] == 'SK0089' AND $varU_AUART != 'ZU06' AND $row['MATNR'] == 'BY0026' ){
					$row['WERKS'] = '1601';
				}
				if ($row['YYBANDCD'] == 'SK0090' AND $varU_AUART != 'ZU06' AND $row['MATNR'] == 'BY0025' ){
					$row['WERKS'] = '1601';
				}
				if ($row['YYBANDCD'] == 'SK0090' AND $varU_AUART != 'ZU06' AND $row['MATNR'] == 'BY0026' ){
					$row['WERKS'] = '1601';
				}
				if ($row['YYBANDCD'] == 'SK0090' AND $varU_AUART != 'ZU06' AND $row['MATNR'] == 'BY0027' ){
					$row['WERKS'] = '1601';
				}
				if ($row['YYBANDCD'] == 'SK0090' AND $varU_AUART != 'ZU06' AND $row['MATNR'] == 'BY0028' ){
					$row['WERKS'] = '1601';
				}

				/* Cek HNA */
				/*
				$sqlCek="SELECT * FROM item_price_hna WHERE item_id='".$row['item_id']."'";
				$qrCek=$this->db->query($sqlCek);
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						$varHna=$rCek['hna_customer'];;
					}
				}
				*/
				/* get E-Wallet */
				$sqlCek="SELECT * FROM ewallet_stc_d a WHERE a.noref = ".$row['ro_id']." and description = 'RO From SC Payment' and member_id = '".$row['U_KUNNR']."'";
				$qrCek=$this->db->query($sqlCek);
				$rCek = array();
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						$varEWalletHdr=$rCek['ewallet_stc_id'];
						$varEWalletItm=$rCek['id'];
					}
				}
				
				/* Cek HJP atau COGS */
				/*
				$sqlCek="SELECT * FROM hjp_history WHERE item_id='".$row['item_id']."' ORDER BY id DESC LIMIT 1";
				$qrCek=$this->db->query($sqlCek);
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						$varCogs=$rCek['hpp'];
					}
				}
				*/
				
				/*cek diskon*/
				if($row['YYBANDCD']!=''){
					$sqlCekItemqty="SELECT COUNT(*) as jmlitem FROM manufaktur WHERE manufaktur_id = '".$row['YYBANDCD']."'";
					$qrCekItemqty=$this->db->query($sqlCekItemqty);
					$rCekItemqty = array();
					if ($qrCekItemqty){	
						foreach($qrCekItemqty->result_array() as $rCekItemqty){
							$Itemqty=$rCekItemqty['jmlitem'];
						}
						$row['diskonrp'] = $row['diskonrp']/$Itemqty;
						// cek item qty detail
						$sqlCekItemqtyDet="SELECT qty FROM manufaktur WHERE manufaktur_id = '".$row['YYBANDCD']."' AND item_id = '".$row['MATNR']."'";
						$qrCekItemqtyDet=$this->db->query($sqlCekItemqtyDet);
						$rCekItemqtyDet = array();
						if ($qrCekItemqtyDet){	
							foreach($qrCekItemqtyDet->result_array() as $rCekItemqtyDet){
								$Itemqtydet=$rCekItemqtyDet['qty'];
							}
							$row['diskonrp'] = $row['diskonrp']/$Itemqtydet;
						}
					}
				$qrCek=$this->db->query($sqlCek);
				}else{
					$varZUA2 = $row['diskonrp'];
				}
				
				$xx = $xx + 1;
				echo "<b>".$xx." ".$curr_ro_id."</b><br>";
				
				$cRow['U_AUART']=$varU_AUART;
				$cRow['U_UHINV']=$row['ro_id'];
				$cRow['U_POSNR']=$xx;
				$cRow['U_SUBNR']=$row['id'];
				$cRow['U_SODAT']=substr($row['U_SODAT'],8,2).".".substr($row['U_SODAT'],5,2).".".substr($row['U_SODAT'],0,4);
				$cRow['U_KUNNR']=$row['U_KUNNR'];
				$cRow['U_CTYPE']=$row['U_CTYPE'];//$varUsGroup;
				$cRow['U_PL_WE']=$row['U_PL_WE'];//"305";//barat timur
				$cRow['U_TAXCLASS']= "Y";
				$cRow['VKBUR']=$row['VKBUR'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ARKTX']=$row['ARKTX'];
				$cRow['MATKL']=$row['MATKL']; //SAP akan mengambil data dari material master SAP
				$cRow['CHARG']=$row['CHARG']; //SAP akan mengambil batch secara FIFO
				$cRow['KWMENG']=$row['KWMENG'];
				$cRow['COGS']=$varCogs; //COGS Nilai Sebelum PPN cond type ZUCS -- ini bisa bikin kisruh.. wkwkwkwk
				$cRow['ZUHN']=$row['ZUHN'];//$varHna; //HNA Nilai Sebelum PPN cond type ZUH1 -- ini bisa bikin pusing.. hahahhaa
				$cRow['ZUA1']=$row['ZUA1']*$row['KWMENG']; //Disc Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
				$cRow['ZUA2']=round($row['diskonrp'])*$row['KWMENG'];// $row['ZUA2']; //Disc Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bisa bikin ribut.. hihihi
				$cRow['ZUA4']=$row['ZUA4']; //Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC -- jangan2 bisa bikin resign.. huhuhu
				$cRow['U_VCDNO']=$row['U_VCDNO']; 
				$cRow['U_FGOOD']=$row['U_FGOOD']; //ZUF1 Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC
				//$cRow['ZUH2']=$row['ZUH2']; //Net Value Nilai Sebelum PPN cond type ZUNT -- ini juga bisa bikin kisruh.. wkwkwkwk
				//$cRow['ZPPN']=$varPPN; //PPN Nilai Sebelum PPN cond type ZUTX -- ini yg bikin pusing.. hahahhaa
				$cRow['PV']=$row['PV'];  //PV Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
				$cRow['BV']=$row['BV'];  //BV Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bikin ribut.. hihihi
				$cRow['YYBANDCD']=$row['YYBANDCD']; //Banded Material..
				$cRow['EWALLET_HDR']=$varEWalletHdr; //Banded Material..
				$cRow['EWALLET_ITM']=$varEWalletItm; //Banded Material..
				$cRow['ITEM_TYPE']=$item_type; //Inserted date..
				$cRow['WAERS']='IDR'; //Inserted date..
				//$cRow['ERDAT']=""; //Inserted date..
				//$cRow['ERZET']=""; //Inserted time..
				/*
				ZU01 = Return
				ZU02 = Cancel billing
				*/
				$cRow['AUGRU']=$varCancelSo;
				$cRow['U_REF_CNCL_INV']= $varCancelRef;//$row['hremark'].' '.$varCancelSoRemark;
				$cRow['U_REF_CNCL_AUART']= $varCancelAuart;//$row['hremark'].' '.$varCancelSoRemark;
				
				$data[] = $cRow;
				
				/** Start Free Item RO **/
				/* Cek Free Item SO Klw Ada */
				if($thefreeItem){
					$free_itemSql="
						SELECT a.*, 
							c.id as nc_d_id,
							c.item_id as free_item_id,
							i.`name` as Free_Item_Name,
							c.qty as GetFree,		
							b.warehouse_id,
							w.sap_code,
							a.periode,
							i.material_group,
							c.hpp,
							i.price, i.pv, i.bv, i.material_group
						FROM ref_trx_nc a 
						LEFT JOIN ncm b ON b.id = a.nc_id 
						LEFT JOIN ncm_d c ON c.ncm_id = b.id 
						LEFT JOIN item i ON i.id = c.item_id   
						LEFT JOIN warehouse w ON w.id = b.warehouse_id   
						WHERE a.trx_id='".$row['ro_id']."'
						AND a.trx = 'RO'
						AND c.ncm_id NOT IN (
							/*
							'164449'
							,'165047'
							,'165048'
							,'165049'
							,'165050'
							,'165676'
							,'165677'
							,'165678'
							,'165679'
							*/
							'165954'
							,'165955'
							,'165956'
							,'165957'
							,'166010'

						)
					";
					
					$qrFreeItem=$this->db->query($free_itemSql);
					if ($qrFreeItem){
						foreach($qrFreeItem->result_array() as $rowFree){
							$xx++;
							$fRow=array();
							if($rowFree['material_group']=='UGIMMICK')$item_type = 'G'; else $item_type='';
							
							$fRow['U_AUART']=$varU_AUART;
							$fRow['U_UHINV']=$row['ro_id'];
							$fRow['U_POSNR']=$xx;
							$fRow['U_SUBNR']=$rowFree['nc_d_id'];
							$fRow['U_SODAT']=substr($rowFree['periode'],8,2).".".substr($rowFree['periode'],5,2).".".substr($rowFree['periode'],0,4);
							$fRow['U_KUNNR']=$row['U_KUNNR'];
							$fRow['U_CTYPE']=$row['U_CTYPE'];
							$fRow['U_PL_WE']="WEST";//barat timur
							$fRow['U_TAXCLASS']= $varTax;
							$fRow['VKBUR']=$row['VKBUR'];
							$fRow['WERKS']=$rowFree['sap_code'];
							$fRow['MATNR']=$rowFree['free_item_id']; // Item Gratisnya
							$fRow['ARKTX']=$rowFree['Free_Item_Name']; // Nama Item Gratisnya..
							$fRow['MATKL']=$rowFree['material_group']; //SAP akan mengambil data dari material master SAP
							$fRow['CHARG']="BATCH"; //SAP akan mengambil batch secara FIFO
							$fRow['KWMENG']=$rowFree['GetFree']; // Dpt Gratisnya
							$fRow['COGS']=$rowFree['hpp']; //COGS Nilai Sebelum PPN cond type ZUCS -- ini bisa bikin kisruh.. wkwkwkwk
							$fRow['ZUHN']=$rowFree['price']; //HNA Nilai Sebelum PPN cond type ZUH1 -- ini bisa bikin pusing.. hahahhaa
							$fRow['ZUA1']=$rowFree['price']*$rowFree['GetFree']; //Disc Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe	
							$fRow['ZUA2']="0"; //Disc Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bisa bikin ribut.. hihihi
							$fRow['ZUA4']="0"; //Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC -- jangan2 bisa bikin resign.. huhuhu
							$fRow['U_VCDNO']=""; 
							$fRow['U_FGOOD']="ZUF1"; //ZUF1 Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC
							$fRow['PV']=$rowFree['pv'];;  //PV Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
							$fRow['BV']=$rowFree['bv'];;  //BV Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bikin ribut.. hihihi
							$fRow['YYBANDCD']=""; //Banded Material..
							$fRow['EWALLET_HDR']=$varEWalletHdr; //Banded Material..
							$fRow['EWALLET_ITM']=$varEWalletItm; //Banded Material..
							$fRow['ITEM_TYPE']=$item_type;
								
							$fRow['AUGRU']=$varCancelSo;
							//$fRow['U_REF_CNCL_INV']= 'Free Item '.$row['so_id'].' - '.$row['hremark'].' '.$varCancelSoRemark;
							$fRow['U_REF_CNCL_INV']= $varCancelRef;//$row['hremark'].' '.$varCancelSoRemark;
							$fRow['U_REF_CNCL_AUART']= $varCancelAuart;//$row['hremark'].' '.$varCancelSoRemark;
							$fRow['WAERS']='IDR';
								
							$data[] = $fRow;
						}
						$thefreeItem = FALSE;
					}

				}
				/** End Of Free Item RO **/
				//$xx++;
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return $data;


	}
	
	public function getSc_data($fromdate,$todate){
		$data=array();
		$cSql="
			SELECT
			a.pinjaman_id
			, a.id
			, 'ZU03' AS U_AUART
			, b.tgl AS U_SODAT
			, b.stockiest_id AS U_KUNNR
			, 'U0' AS U_CTYPE
			, CASE WHEN b.deliv_addr > 0 AND k.timur = 0 THEN 'WEST' 
			       WHEN b.deliv_addr > 0 AND k.timur = 1 THEN 'EAST'
			       WHEN b.deliv_addr = 0 AND b.pu = '1' AND k2.timur = 0 THEN 'WEST'
			       WHEN b.deliv_addr = 0 AND b.pu = '1' AND k2.timur = 1 THEN 'EAST'
			       WHEN b.deliv_addr = 0 AND b.pu = '0' THEN 'WEST'
			       END AS U_PL_WE
			, IFNULL(w2.sap_code,w.sap_code) AS VKBUR
			, IFNULL(w.sap_code,'1601') AS WERKS
			, CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS MATNR
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.name ELSE i.name END AS ARKTX
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.material_group ELSE i.material_group END AS MATKL
			, 'BATCH' AS CHARG
			, CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*a.qty ELSE a.qty END AS KWMENG
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.hpp ELSE i.hpp END AS COGS
			, CASE WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 0 AND i2.price > 0 THEN i2.price 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 0 AND i2.price = 0 THEN rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 1 AND i2.price > 0  THEN i2.price2 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 1 AND i2.price = 0  THEN rp.item_price 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.pu = 1 AND k2.timur = 0 AND i2.price > 0  THEN i2.price 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.pu = 1 AND k2.timur = 0 AND i2.price = 0  THEN rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.pu = 1 AND k2.timur = 1 AND i2.price2 > 0  THEN i2.price2 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.pu = 1 AND k2.timur = 1 AND i2.price2 = 0  THEN rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.pu = 0 AND i2.price > 0  THEN i2.price 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.pu = 0 AND i2.price = 0  THEN rp.item_price
			       WHEN rp.item_id IS NULL AND b.deliv_addr > 0 AND k.timur = 0 THEN i.price 
			       WHEN rp.item_id IS NULL AND b.deliv_addr > 0 AND k.timur = 1 THEN i.price2 
			       WHEN rp.item_id IS NULL AND b.deliv_addr = 0 AND b.pu = 1 AND k2.timur = 0 THEN i.price 
			       WHEN rp.item_id IS NULL AND b.deliv_addr = 0 AND b.pu = 1 AND k2.timur = 1 THEN i.price2
			       WHEN rp.item_id IS NULL AND b.deliv_addr = 0 AND b.pu = 0 THEN i.price
			       END AS ZUHN
			, CASE WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 0 AND i2.price > 0 THEN i2.price - rp.item_price 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 0 AND i2.price = 0 THEN 0
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 1 AND i2.price2 > 0  THEN i2.price2 - rp.item_price 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr > 0 AND k.timur = 1 AND i2.price2 = 0  THEN 0 
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.pu = 1 AND k2.timur = 0  AND i2.price > 0  THEN i2.price - rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.pu = 1 AND k2.timur = 0  AND i2.price = 0  THEN 0
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.pu = 1 AND k2.timur = 1  AND i2.price2 > 0  THEN i2.price2 - rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.pu = 1 AND k2.timur = 1  AND i2.price2 = 0  THEN 0
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.pu = 0 AND i2.price > 0  THEN i2.price - rp.item_price
			       WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND b.pu = 0 AND i2.price = 0  THEN 0
			       ELSE 0
			       END AS ZUA1
			, 0 AS ZUA2
			, 0 AS ZUA4
			, '' AS U_VCDNO
			, '' AS U_FGOOD
			, 0 AS PV
			, 0 AS BV
			, CASE WHEN rp.item_id IS NOT NULL THEN a.item_id ELSE '' END AS YYBANDCD
			, '' AS AUGRU
			, '' AS U_REF_CNCL_INV
			, CASE WHEN rp.item_id IS NOT NULL AND i2.material_group = 'UGIMMICK' THEN 'G' 
			       WHEN rp.item_id IS NULL AND i.material_group = 'UGIMMICK' THEN 'G'
				   ELSE '' END AS ITEM_TYPE
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.material_group ELSE i.material_group END AS item_type
			FROM pinjaman_d a
			LEFT JOIN reff_package rp ON a.item_id = rp.package_id AND a.harga = rp.package_price
			LEFT JOIN pinjaman b ON b.id = a.pinjaman_id
			LEFT JOIN item i ON i.id = a.item_id  
			LEFT JOIN item i2 ON i2.id = rp.item_id  
			LEFT JOIN member m ON m.id = b.stockiest_id   
			LEFT JOIN warehouse w ON w.id = a.warehouse_id 
			LEFT JOIN warehouse w2 ON w2.id = b.warehouse_id 
			LEFT JOIN member_delivery md ON b.deliv_addr = md.id
			LEFT JOIN kota k ON k.id = md.kota
			LEFT JOIN kota k2 ON k2.id = m.kota_id
			WHERE b.tgl BETWEEN '".$fromdate."' AND '".$todate."'
		";
		
		$curr_pinjaman_id ='';
		$thefreeItem = TRUE;
		$xx = 1;

		
		$query=$this->db->query($cSql);
		if ($query){	
			 foreach($query->result_array() as $row){
				if($curr_pinjaman_id == ''){
					$curr_pinjaman_id =$row['pinjaman_id'];	
				}
				
				if($curr_pinjaman_id!=$row['pinjaman_id']){
					$curr_pinjaman_id =$row['pinjaman_id'];
					$thefreeItem = TRUE;
					$xx = 1;
				}
				
				$cRow=array();
				
				$varTax='Y';
				$varU_AUART=$row['U_AUART'];//'ZU02';
				$varCancelSo='';
				$varCogs=$row['COGS'];
				$varHna=$row['ZUHN'];
				$varUsGroup=$row['U_CTYPE'];
				$varCancelScRemark='';
				$varCancelAuart = '';
				$varCancelRef = '';
				if($row['item_type']=='UGIMMICK')$item_type = 'G'; else $item_type='';
				
				/* sk aug */
				if ($row['YYBANDCD'] == 'SK0089' AND $varU_AUART != 'ZU06' AND $row['MATNR'] == 'BY0026' ){
					$row['WERKS'] = '1601';
				}
				if ($row['YYBANDCD'] == 'SK0090' AND $varU_AUART != 'ZU06' AND $row['MATNR'] == 'BY0025' ){
					$row['WERKS'] = '1601';
				}
				if ($row['YYBANDCD'] == 'SK0090' AND $varU_AUART != 'ZU06' AND $row['MATNR'] == 'BY0026' ){
					$row['WERKS'] = '1601';
				}
				if ($row['YYBANDCD'] == 'SK0090' AND $varU_AUART != 'ZU06' AND $row['MATNR'] == 'BY0027' ){
					$row['WERKS'] = '1601';
				}
				if ($row['YYBANDCD'] == 'SK0090' AND $varU_AUART != 'ZU06' AND $row['MATNR'] == 'BY0028' ){
					$row['WERKS'] = '1601';
				}
				
				$cRow['U_AUART']=$varU_AUART;
				$cRow['U_UHINV']=$row['pinjaman_id'];
				$cRow['U_POSNR']=$xx;
				$cRow['U_SUBNR']=$row['id'];
				$cRow['U_SODAT']=substr($row['U_SODAT'],8,2).".".substr($row['U_SODAT'],5,2).".".substr($row['U_SODAT'],0,4);
				$cRow['U_KUNNR']=$row['U_KUNNR'];
				$cRow['U_CTYPE']=$row['U_CTYPE'];//$varUsGroup;
				$cRow['U_PL_WE']=$row['U_PL_WE'];//"305";//barat timur
				$cRow['U_TAXCLASS']= "Y";
				$cRow['VKBUR']=$row['VKBUR'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ARKTX']=$row['ARKTX'];
				$cRow['MATKL']=$row['MATKL']; //SAP akan mengambil data dari material master SAP
				$cRow['CHARG']=$row['CHARG']; //SAP akan mengambil batch secara FIFO
				$cRow['KWMENG']=$row['KWMENG'];
				$cRow['COGS']=$varCogs; //COGS Nilai Sebelum PPN cond type ZUCS -- ini bisa bikin kisruh.. wkwkwkwk
				$cRow['ZUHN']=$row['ZUHN'];//$varHna; //HNA Nilai Sebelum PPN cond type ZUH1 -- ini bisa bikin pusing.. hahahhaa
				$cRow['ZUA1']=$row['ZUA1']*$row['KWMENG']; //Disc Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
				$cRow['ZUA2']=$row['ZUA2']; //Disc Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bisa bikin ribut.. hihihi
				$cRow['ZUA4']=$row['ZUA4']; //Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC -- jangan2 bisa bikin resign.. huhuhu
				$cRow['U_VCDNO']=$row['U_VCDNO']; 
				$cRow['U_FGOOD']=$row['U_FGOOD']; //ZUF1 Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC
				$cRow['PV']=$row['PV'];  //PV Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
				$cRow['BV']=$row['BV'];  //BV Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bikin ribut.. hihihi
				$cRow['YYBANDCD']=$row['YYBANDCD']; //Banded Material..
				$cRow['EWALLET_HDR']='';//$varEWalletHdr; //Banded Material..
				$cRow['EWALLET_ITM']='';//$varEWalletItm; //Banded Material..
				$cRow['ITEM_TYPE']='';//$item_type; //Inserted date..
				$cRow['WAERS']='IDR'; //Inserted date..
				//$cRow['ERDAT']=""; //Inserted date..
				//$cRow['ERZET']=""; //Inserted time..
				/*
				ZU01 = Return
				ZU02 = Cancel billing
				*/
				$cRow['AUGRU']=$varCancelSo;
				$cRow['U_REF_CNCL_INV']= $varCancelRef;//$row['hremark'].' '.$varCancelSoRemark;
				$cRow['U_REF_CNCL_AUART']= $varCancelAuart;//$row['hremark'].' '.$varCancelSoRemark;
				
				$data[] = $cRow;
				
				
				$free_itemSql="
						SELECT a.*, 
							a.pinjaman_nc_id AS nc_d_id,
							a.item_id AS free_item_id,
							i.`name` AS Free_Item_Name,
							a.qty AS GetFree,		
							a.warehouse_id,
							w.sap_code,
							a.pinjaman_nc_tgl AS periode,
							i.material_group,
							i.hpp,
							i.price, i.pv, i.bv, i.material_group
						FROM pinjaman_nc a 
						LEFT JOIN pinjaman_d b ON b.id = a.pinjaman_d_id
						LEFT JOIN item i ON i.id = a.item_id   
						LEFT JOIN warehouse w ON w.id = a.warehouse_id   
						LEFT JOIN promo d ON d.id = a.promo_id 
						WHERE a.pinjaman_id='".$row['pinjaman_id']."'
						and a.pinjaman_nc_id not in (
							'1467'
							,'1515'
							,'1516'
							,'1517'
							,'1518'
							,'1561'
						)
				
				";
				
				$qrFreeItem=$this->db->query($free_itemSql);
				if ($qrFreeItem){	
					foreach($qrFreeItem->result_array() as $rowFree){
							$xx++;
							$fRow=array();
							if($rowFree['material_group']=='UGIMMICK')$item_type = 'G'; else $item_type='';
							
							$fRow['U_AUART']=$varU_AUART;
							$fRow['U_UHINV']=$row['pinjaman_id'];
							$fRow['U_POSNR']=$xx;
							$fRow['U_SUBNR']=$rowFree['nc_d_id'];
							$fRow['U_SODAT']=substr($rowFree['periode'],8,2).".".substr($rowFree['periode'],5,2).".".substr($rowFree['periode'],0,4);
							//$fRow['U_SODAT']='01.07.2019';	
							$fRow['U_KUNNR']=$row['U_KUNNR'];
							$fRow['U_CTYPE']=$row['U_CTYPE'];;
							$fRow['U_PL_WE']="WEST";//barat timur
							$fRow['U_TAXCLASS']= $varTax;
							$fRow['VKBUR']=$row['VKBUR'];
							$fRow['WERKS']=$rowFree['sap_code'];
							$fRow['MATNR']=$rowFree['free_item_id']; // Item Gratisnya
							$fRow['ARKTX']=$rowFree['Free_Item_Name']; // Nama Item Gratisnya..
							$fRow['MATKL']=$rowFree['material_group']; //SAP akan mengambil data dari material master SAP
							$fRow['CHARG']="BATCH"; //SAP akan mengambil batch secara FIFO
							$fRow['KWMENG']=$rowFree['GetFree']; // Dpt Gratisnya
							$fRow['COGS']=$rowFree['hpp']; //COGS Nilai Sebelum PPN cond type ZUCS -- ini bisa bikin kisruh.. wkwkwkwk
							$fRow['ZUHN']=$rowFree['price']; //HNA Nilai Sebelum PPN cond type ZUH1 -- ini bisa bikin pusing.. hahahhaa
							$fRow['ZUA1']=$rowFree['price']*$rowFree['GetFree']; //Disc Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe	
							$fRow['ZUA2']="0"; //Disc Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bisa bikin ribut.. hihihi
							$fRow['ZUA4']="0"; //Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC -- jangan2 bisa bikin resign.. huhuhu
							$fRow['U_VCDNO']=""; 
							$fRow['U_FGOOD']="ZUF1"; //ZUF1 Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC
							$fRow['PV']=$rowFree['pv'];;  //PV Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
							$fRow['BV']=$rowFree['bv'];;  //BV Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bikin ribut.. hihihi
							$fRow['YYBANDCD']=""; //Banded Material..
							$fRow['EWALLET_HDR']='';//$varEWalletHdr; //Banded Material..
							$fRow['EWALLET_ITM']='';//$varEWalletItm; //Banded Material..
							$fRow['ITEM_TYPE']=$item_type;
								
							$fRow['AUGRU']=$varCancelSo;
							//$fRow['U_REF_CNCL_INV']= 'Free Item '.$row['so_id'].' - '.$row['hremark'].' '.$varCancelSoRemark;
							$fRow['U_REF_CNCL_INV']= $varCancelRef;//$row['hremark'].' '.$varCancelSoRemark;
							$fRow['U_REF_CNCL_AUART']= $varCancelAuart;//$row['hremark'].' '.$varCancelSoRemark;
							$fRow['WAERS']='IDR';
							$data[] = $fRow;
					}
					$thefreeItem = FALSE;
				}
				/** End Of Free Item RO **/
				$xx++;
				
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return $data;


	}
	
	public function getScAdm_data($fromdate,$todate){
		$data=array();
		/*
		$cSql="SELECT
			a.*, YEAR(b.tgl) as thn,
			i.`name` as item_nm,
			i.satuan,
			b.stockiest_id as stockiest_id,
			b.totalharga as totalharga,
			b.totalpv as totalpv,
			b.warehouse_id as whs_id,
			w.sap_code as warehouse_id,
			b.tgl as hdate,
			b.created as hcreated,
			b.createdby as hcreatedby,
			DATE_FORMAT(b.tgl, '%d.%m.%Y') AS tgl_sap,
			b.remark as hremark 
			FROM
				(pinjaman_d a)
			INNER JOIN pinjaman b ON b.id = a.pinjaman_id 
			INNER JOIN item i ON i.id = a.item_id  
			INNER JOIN warehouse w ON w.id = b.warehouse_id   
			WHERE b.tgl BETWEEN '".$fromdate."' AND '".$todate."' ";
		*/
		$cSql="
			SELECT
			a.pinjaman_admin_id
			, a.id
			, 'ZU03' AS U_AUART
			, b.tgl AS U_SODAT
			, b.admin_id AS U_KUNNR
			, 'U0' AS U_CTYPE
			, 'WEST'AS U_PL_WE
			, w.sap_code AS VKBUR
			, IFNULL(w.sap_code,'1601') AS WERKS
			, CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS MATNR
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.name ELSE i.name END AS ARKTX
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.material_group ELSE i.material_group END AS MATKL
			, 'BATCH' AS CHARG
			, CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*a.qty ELSE a.qty END AS KWMENG
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.hpp ELSE i.hpp END AS COGS
			, CASE WHEN rp.item_id IS NOT NULL AND i2.price > 0 THEN i2.price 
			       WHEN rp.item_id IS NOT NULL AND i2.price = 0 THEN rp.item_price
			       WHEN rp.item_id IS NULL THEN i.price
			       END AS ZUHN
			, CASE WHEN rp.item_id IS NOT NULL AND i2.price > 0 THEN i2.price - rp.item_price 
			       WHEN rp.item_id IS NOT NULL AND i2.price = 0 THEN 0
			       ELSE 0
			       END AS ZUA1
			-- , CASE WHEN rp.item_id IS NOT NULL THEN a.harga-rp.item_price ELSE 0 END AS ZUA1
			, 0 AS ZUA2
			, 0 AS ZUA4
			, '' AS U_VCDNO
			, '' AS U_FGOOD
			, 0 AS PV
			, 0 AS BV
			, CASE WHEN rp.item_id IS NOT NULL THEN a.item_id ELSE '' END AS YYBANDCD
			, '' AS AUGRU
			, '' AS U_REF_CNCL_INV
			, CASE WHEN rp.item_id IS NOT NULL AND i2.material_group = 'UGIMMICK' THEN 'G' 
			       WHEN rp.item_id IS NULL AND i.material_group = 'UGIMMICK' THEN 'G'
				   ELSE '' END AS ITEM_TYPE
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.material_group ELSE i.material_group END AS item_type
			FROM pinjaman_admin_d a
			LEFT JOIN reff_package rp ON a.item_id = rp.package_id AND a.harga = rp.package_price
			LEFT JOIN pinjaman_admin b ON b.id = a.pinjaman_admin_id
			LEFT JOIN item i ON i.id = a.item_id  
			LEFT JOIN item i2 ON i2.id = rp.item_id  
			LEFT JOIN admins m ON m.id = b.admin_id   
			LEFT JOIN warehouse w ON w.id = b.warehouse_id 
			-- LEFT JOIN warehouse w2 ON w2.id = b.warehouse_id 
			-- LEFT JOIN member_delivery md ON b.deliv_addr = md.id
			-- LEFT JOIN kota k ON k.id = md.kota
			-- LEFT JOIN kota k2 ON k2.id = m.kota_id
			WHERE b.tgl BETWEEN '".$fromdate."' AND '".$todate."'	
			-- WHERE b.tgl BETWEEN '2019-07-01' AND '2019-07-31'	
			-- AND b.id IN(9261,9242,9207,9204)		
			-- AND b.id IN(9421)
			-- WHERE b.tgl BETWEEN '2019-04-01' AND '2019-04-30'
		";
		
		$curr_pinjaman_id ='';
		$thefreeItem = TRUE;
		$xx = 1;

		
		$query=$this->db->query($cSql);
		if ($query){	
			 foreach($query->result_array() as $row){
				if($curr_pinjaman_id == ''){
					$curr_pinjaman_id =$row['pinjaman_admin_id'];	
				}
				
				if($curr_pinjaman_id!=$row['pinjaman_admin_id']){
					$curr_pinjaman_id =$row['pinjaman_admin_id'];
					$thefreeItem = TRUE;
					$xx = 1;
				}
				
				$cRow=array();
				/* 
				$varTax='Y';
				$varU_AUART='ZU03';
				$varCancelSo='';
				$varCogs='';
				$varHna='';
				$varCancelScRemark='';
				
				$varDiskon='';
				*/
				
				$varTax='Y';
				$varU_AUART=$row['U_AUART'];//'ZU02';
				$varCancelSo='';
				$varCogs=$row['COGS'];
				$varHna=$row['ZUHN'];
				//$varNetValue=$row['totalsales'];
				//$varPPN=$row['totalppn'];
				$varUsGroup=$row['U_CTYPE'];
				$varCancelScRemark='';
				$varCancelAuart = '';
				$varCancelRef = '';
				if($row['item_type']=='UGIMMICK')$item_type = 'G'; else $item_type='';
				
				/* Cek HNA */
				/*
				$sqlCek="SELECT * FROM item_price_hna WHERE item_id='".$row['item_id']."'";
				$qrCek=$this->db->query($sqlCek);
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						$varHna=$rCek['hna_customer'];;
					}
				}
				*/
				
				/* Cek HJP atau COGS */
				/*
				$sqlCek="SELECT * FROM hjp_history WHERE item_id='".$row['item_id']."' ORDER BY id DESC LIMIT 1";
				$qrCek=$this->db->query($sqlCek);
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						$varCogs=$rCek['hpp'];
					}
				}
				*/
				
				$cRow['U_AUART']=$varU_AUART;
				$cRow['U_UHINV']=$row['pinjaman_admin_id'];
				$cRow['U_POSNR']=$xx;
				$cRow['U_SUBNR']=$row['id'];
				$cRow['U_SODAT']=substr($row['U_SODAT'],8,2).".".substr($row['U_SODAT'],5,2).".".substr($row['U_SODAT'],0,4);
				//$cRow['U_SODAT']='01.07.2019';
				$cRow['U_KUNNR']=$row['U_KUNNR'];
				$cRow['U_CTYPE']=$row['U_CTYPE'];//$varUsGroup;
				$cRow['U_PL_WE']=$row['U_PL_WE'];//"305";//barat timur
				$cRow['U_TAXCLASS']= "Y";
				$cRow['VKBUR']=$row['VKBUR'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ARKTX']=$row['ARKTX'];
				$cRow['MATKL']=$row['MATKL']; //SAP akan mengambil data dari material master SAP
				$cRow['CHARG']=$row['CHARG']; //SAP akan mengambil batch secara FIFO
				$cRow['KWMENG']=$row['KWMENG'];
				$cRow['COGS']=$varCogs; //COGS Nilai Sebelum PPN cond type ZUCS -- ini bisa bikin kisruh.. wkwkwkwk
				$cRow['ZUHN']=$row['ZUHN'];//$varHna; //HNA Nilai Sebelum PPN cond type ZUH1 -- ini bisa bikin pusing.. hahahhaa
				$cRow['ZUA1']=$row['ZUA1']*$row['KWMENG']; //Disc Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
				$cRow['ZUA2']=$row['ZUA2']; //Disc Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bisa bikin ribut.. hihihi
				$cRow['ZUA4']=$row['ZUA4']; //Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC -- jangan2 bisa bikin resign.. huhuhu
				$cRow['U_VCDNO']=$row['U_VCDNO']; 
				$cRow['U_FGOOD']=$row['U_FGOOD']; //ZUF1 Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC
				//$cRow['ZUH2']=$row['ZUH2']; //Net Value Nilai Sebelum PPN cond type ZUNT -- ini juga bisa bikin kisruh.. wkwkwkwk
				//$cRow['ZPPN']=$varPPN; //PPN Nilai Sebelum PPN cond type ZUTX -- ini yg bikin pusing.. hahahhaa
				$cRow['PV']=$row['PV'];  //PV Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
				$cRow['BV']=$row['BV'];  //BV Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bikin ribut.. hihihi
				$cRow['YYBANDCD']=$row['YYBANDCD']; //Banded Material..
				$cRow['EWALLET_HDR']='';//$varEWalletHdr; //Banded Material..
				$cRow['EWALLET_ITM']='';//$varEWalletItm; //Banded Material..
				$cRow['ITEM_TYPE']='';//$item_type; //Inserted date..
				$cRow['WAERS']='IDR'; //Inserted date..
				//$cRow['ERDAT']=""; //Inserted date..
				//$cRow['ERZET']=""; //Inserted time..
				/*
				ZU01 = Return
				ZU02 = Cancel billing
				*/
				$cRow['AUGRU']=$varCancelSo;
				$cRow['U_REF_CNCL_INV']= $varCancelRef;//$row['hremark'].' '.$varCancelSoRemark;
				$cRow['U_REF_CNCL_AUART']= $varCancelAuart;//$row['hremark'].' '.$varCancelSoRemark;
				
				$data[] = $cRow;
				
				
				$xx++;
				
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return $data;


	}

	public function test_json(){
		$data=array();
		
		/*
		U_AUART
		BSTKD
		BSTDK
		U_KUNNR
		U_CTYPE
		U_PL_WE
		U_TAXCLASS
		VKBUR
		WERKS
		MATNR
		ARKTX
		MATKL-
		CHARG
		KWMENG
		KWERT
		KWERT
		KWERT
		KWERT
		KWERT
		U_VCDNO
		U_FGOOD
		KWERT
		KWERT
		KWERT
		KWERT
		
		YYBANDCD
		AUGRU
		YREF
		*/
		
		$cRow=array();
		$cRow['U_AUART']="ZU01";
		$cRow['U_UHINV']="4901906158";
		$cRow['U_SODAT']="2018";
		$cRow['U_KUNNR']="whs to whs";
		$cRow['U_CTYPE']="1";
		$cRow['U_PL_WE']="305";
		$cRow['U_TAXCLASS']= "S";
		$cRow['VKBUR']="1602";
		$cRow['WERKS']="UHN1";
		$cRow['MATNR']="BYI0017";
		$cRow['ARKTX']="Astaderm Set";
		$cRow['MATKL']="2";
		$cRow['CHARG']="CBFYC";
		$cRow['KWMENG']="30.06.2020";
		$cRow['KWERT']="10000";
		$cRow['KWERT']="10000";
		$cRow['KWERT']="10000";
		$cRow['KWERT']="10000";
		$cRow['YYBANDCD']=" -   ";
		$cRow['AUGRU']="22.11.2018";
		$cRow['YREF']= "SIP_WHS13";
		$data[] = $cRow;
		
		return $data;
	}
 	
	
	public function get_sc_retur_data($fromdate,$todate){
		$data=array();
		$cSql = "
			SELECT
			a.retur_pinjaman_id
			, a.id
			, 'ZU04' AS U_AUART
			, b.tgl AS U_SODAT
			, b.stockiest_id AS U_KUNNR
			, 'U0' AS U_CTYPE
			, CASE WHEN ap.deliv_addr > 0 AND k.timur = 0 THEN 'WEST' 
			       WHEN ap.deliv_addr > 0 AND k.timur = 1 THEN 'EAST'
			       WHEN ap.deliv_addr = 0 AND ap.pu = '1' AND k2.timur = 0 THEN 'WEST'
			       WHEN ap.deliv_addr = 0 AND ap.pu = '1' AND k2.timur = 1 THEN 'EAST'
			       WHEN ap.deliv_addr = 0 AND ap.pu = '0' THEN 'WEST'
			       END AS U_PL_WE
			, w.sap_code AS VKBUR
			, IFNULL(w.sap_code,'1601') AS WERKS
			, CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS MATNR
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.name ELSE i.name END AS ARKTX
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.material_group ELSE i.material_group END AS MATKL
			, 'BATCH' AS CHARG
			, CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*a.qty ELSE a.qty END AS KWMENG
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.hpp ELSE i.hpp END AS COGS
			, CASE WHEN rp.item_id IS NOT NULL AND ap.deliv_addr > 0 AND k.timur = 0 AND i2.price > 0 THEN i2.price 
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr > 0 AND k.timur = 0 AND i2.price = 0 THEN rp.item_price
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr > 0 AND k.timur = 1 AND i2.price > 0  THEN i2.price2 
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr > 0 AND k.timur = 1 AND i2.price = 0  THEN rp.item_price 
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr = 0 AND i2.price > 0  THEN i2.price 
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr = 0 AND i2.price = 0  THEN rp.item_price
			       WHEN rp.item_id IS NULL AND ap.deliv_addr > 0 AND k.timur = 0 THEN i.price 
			       WHEN rp.item_id IS NULL AND ap.deliv_addr > 0 AND k.timur = 1 THEN i.price2 
			       WHEN rp.item_id IS NULL AND ap.deliv_addr = 0 THEN i.price 
			       END AS ZUHN
			, CASE WHEN rp.item_id IS NOT NULL AND ap.deliv_addr > 0 AND k.timur = 0 AND i2.price > 0 THEN i2.price - rp.item_price
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr > 0 AND k.timur = 0 AND i2.price = 0 THEN 0
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr > 0 AND k.timur = 1 AND i2.price > 0  THEN i2.price2 - rp.item_price
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr > 0 AND k.timur = 1 AND i2.price = 0  THEN 0 
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr = 0 AND i2.price > 0  THEN i2.price - rp.item_price
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr = 0 AND i2.price = 0  THEN 0
			       END AS ZUA1
			-- , CASE WHEN rp.item_id IS NOT NULL THEN a.harga-rp.item_price ELSE 0 END AS ZUA1
			, 0 AS ZUA2
			, 0 AS ZUA4
			, '' AS U_VCDNO
			, '' AS U_FGOOD
			, 0 AS PV
			, 0 AS BV
			, CASE WHEN rp.item_id IS NOT NULL THEN a.item_id ELSE '' END AS YYBANDCD
			, '' AS AUGRU
			, '' AS U_REF_CNCL_INV
			, CASE WHEN rp.item_id IS NOT NULL AND i2.material_group = 'UGIMMICK' THEN 'G' 
			       WHEN rp.item_id IS NULL AND i.material_group = 'UGIMMICK' THEN 'G'
				   ELSE '' END AS ITEM_TYPE
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.material_group ELSE i.material_group END AS item_type
			, a.pinjaman_id
			, a.pinjaman_d_id
			FROM retur_pinjaman_d a
			LEFT JOIN reff_package rp ON a.item_id = rp.package_id AND a.harga = rp.package_price
			LEFT JOIN retur_pinjaman b ON b.id = a.retur_pinjaman_id 
			LEFT JOIN pinjaman ap ON ap.id = b.pinjaman_id
			LEFT JOIN item i ON i.id = a.item_id  
			LEFT JOIN item i2 ON i2.id = rp.item_id  
			LEFT JOIN member m ON m.id = b.stockiest_id   
			LEFT JOIN warehouse w ON w.id = b.warehouse_id 
			-- LEFT JOIN warehouse w2 ON w2.id = b.warehouse_id 
			LEFT JOIN member_delivery md ON ap.deliv_addr = md.id
			LEFT JOIN kota k ON k.id = md.kota
			LEFT JOIN kota k2 ON k2.id = m.kota_id
			WHERE b.tgl BETWEEN '".$fromdate."' AND '".$todate."'
			-- WHERE b.tgl BETWEEN '2019-04-01' AND '2019-04-30'
			";
		$query=$this->db->query($cSql);
		
		$curr_ro_id ='';
		$thefreeItem = TRUE;
		$xx = 1;
		
		if ($query){	
			 foreach($query->result_array() as $row){
				if($curr_ro_id == ''){
					$curr_ro_id =$row['retur_pinjaman_id'];	
				}
				
				if($curr_ro_id!=$row['retur_pinjaman_id']){
					$curr_ro_id =$row['retur_pinjaman_id'];
					$thefreeItem = TRUE;
					$xx = 1;
				}
				$cRow=array();
				
				$varTax='Y';
				$varU_AUART=$row['U_AUART'];//'ZU02';
				$varCancelSo='ZU1';
				$varCogs=$row['COGS'];
				$varHna=$row['ZUHN'];
				//$varNetValue=$row['totalsales'];
				//$varPPN=$row['totalppn'];
				$varUsGroup=$row['U_CTYPE'];
				$varCancelSoRemark='';
				$varCancelAuart = '';
				$varCancelRef = '';
				if($row['item_type']=='UGIMMICK')$item_type = 'G'; else $item_type='';
				
				/* Cek Cancel OR RETUR */
				/*
				$sqlCek="SELECT a.*, b.remark FROM cancel_ro a 
				LEFT JOIN cancel_ro_remark b ON b.ro_id_canceled = a.ro_id_canceled 
				WHERE a.ro_id_execute = ".$row['ro_id'];
				$qrCek=$this->db->query($sqlCek);
				$rCek = array();
				if ($qrCek){
					foreach($qrCek->result_array() as $rCek){
						$varCancelSo='ZU2';
						$varU_AUART='ZU06';
						//$varCancelSo=$rCek[''];
						$varCancelSoRemark=$rCek['remark'];
						$row['KWMENG'] = $row['KWMENG']*-1;
						$varCancelAuart = 'ZU01';
						$varCancelRef = $rCek['ro_id_canceled'];
					}
				}
				*/
				/* Cek HNA */
				/*
				$sqlCek="SELECT * FROM item_price_hna WHERE item_id='".$row['item_id']."'";
				$qrCek=$this->db->query($sqlCek);
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						$varHna=$rCek['hna_customer'];;
					}
				}
				*/
				/* get E-Wallet */
				/*
				$sqlCek="SELECT * FROM ewallet_stc_d a WHERE a.noref = ".$row['ro_id']." and description = 'RO From SC Payment' and member_id = '".$row['U_KUNNR']."'";
				//echo $sqlCek.'<br>';
				$qrCek = $this->db->query($sqlCek);
				$rCek = array();
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						$varEWalletHdr=$rCek['ewallet_stc_id'];
						$varEWalletItm=$rCek['id'];
					}
				}
				*/
				/* Cek HJP atau COGS */
				/*
				$sqlCek="SELECT * FROM hjp_history WHERE item_id='".$row['item_id']."' ORDER BY id DESC LIMIT 1";
				$qrCek=$this->db->query($sqlCek);
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						$varCogs=$rCek['hpp'];
					}
				}
				*/
				
				
				$cRow['U_AUART']=$varU_AUART;
				$cRow['U_UHINV']=$row['retur_pinjaman_id'];
				$cRow['U_POSNR']=$xx;
				$cRow['U_SUBNR']=$row['id'];
				$cRow['U_SODAT']=substr($row['U_SODAT'],8,2).".".substr($row['U_SODAT'],5,2).".".substr($row['U_SODAT'],0,4);
				$cRow['U_KUNNR']=$row['U_KUNNR'];
				$cRow['U_CTYPE']=$row['U_CTYPE'];//$varUsGroup;
				$cRow['U_PL_WE']=$row['U_PL_WE'];//"305";//barat timur
				$cRow['U_TAXCLASS']= "Y";
				$cRow['VKBUR']=$row['VKBUR'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ARKTX']=$row['ARKTX'];
				$cRow['MATKL']=$row['MATKL']; //SAP akan mengambil data dari material master SAP
				$cRow['CHARG']=$row['CHARG']; //SAP akan mengambil batch secara FIFO
				$cRow['KWMENG']=$row['KWMENG'];
				$cRow['COGS']=$varCogs; //COGS Nilai Sebelum PPN cond type ZUCS -- ini bisa bikin kisruh.. wkwkwkwk
				$cRow['ZUHN']=$row['ZUHN'];//$varHna; //HNA Nilai Sebelum PPN cond type ZUH1 -- ini bisa bikin pusing.. hahahhaa
				$cRow['ZUA1']=$row['ZUA1']*$row['KWMENG']; //Disc Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
				$cRow['ZUA2']=$row['ZUA2']; //Disc Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bisa bikin ribut.. hihihi
				$cRow['ZUA4']=$row['ZUA4']; //Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC -- jangan2 bisa bikin resign.. huhuhu
				$cRow['U_VCDNO']=$row['U_VCDNO']; 
				$cRow['U_FGOOD']=$row['U_FGOOD']; //ZUF1 Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC
				//$cRow['ZUH2']=$row['ZUH2']; //Net Value Nilai Sebelum PPN cond type ZUNT -- ini juga bisa bikin kisruh.. wkwkwkwk
				//$cRow['ZPPN']=$varPPN; //PPN Nilai Sebelum PPN cond type ZUTX -- ini yg bikin pusing.. hahahhaa
				$cRow['PV']=$row['PV'];  //PV Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
				$cRow['BV']=$row['BV'];  //BV Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bikin ribut.. hihihi
				$cRow['YYBANDCD']=$row['YYBANDCD']; //Banded Material..
				$cRow['EWALLET_HDR']='';//$varEWalletHdr; //Banded Material..
				$cRow['EWALLET_ITM']='';//$varEWalletItm; //Banded Material..
				$cRow['ITEM_TYPE']=$item_type; //Inserted date..
				$cRow['WAERS']='IDR'; //Inserted date..
				//$cRow['ERDAT']=""; //Inserted date..
				//$cRow['ERZET']=""; //Inserted time..
				/*
				ZU01 = Return
				ZU02 = Cancel billing
				*/
				$cRow['AUGRU']=$varCancelSo;
				$cRow['U_REF_CNCL_INV']= $row['pinjaman_id'];//$varCancelRef;//$row['hremark'].' '.$varCancelSoRemark;
				$cRow['U_REF_CNCL_AUART']= $varCancelAuart;//$row['hremark'].' '.$varCancelSoRemark;
				
				$data[] = $cRow;
				
				/** Start Free Item RO **/
				/* Cek Free Item SO Klw Ada */
				if($thefreeItem){
					$free_itemSql="
						SELECT a.*, 
							a.pinjaman_nc_id AS nc_d_id,
							a.item_id AS free_item_id,
							i.`name` AS Free_Item_Name,
							IFNULL((b.qty_retur/d.free_item_qty),0) AS GetFree,		
							a.warehouse_id,
							w.sap_code,
							a.pinjaman_nc_tgl AS periode,
							i.material_group,
							i.hpp,
							i.price, i.pv, i.bv, i.material_group
						FROM pinjaman_nc a 
						LEFT JOIN pinjaman_d b ON b.id = a.pinjaman_d_id
						LEFT JOIN item i ON i.id = a.item_id   
						LEFT JOIN warehouse w ON w.id = a.warehouse_id   
						LEFT JOIN promo d ON d.id = a.promo_id 
						WHERE a.pinjaman_d_id=".$row['pinjaman_d_id']."
					";
					
					$qrFreeItem=$this->db->query($free_itemSql);
					if ($qrFreeItem){
						foreach($qrFreeItem->result_array() as $rowFree){
							$xx++;
							$fRow=array();
							if($rowFree['material_group']=='UGIMMICK')$item_type = 'G'; else $item_type='';
							
							$fRow['U_AUART']=$varU_AUART;
							$fRow['U_UHINV']=$row['retur_pinjaman_id'];
							$fRow['U_POSNR']=$xx;
							$fRow['U_SUBNR']=$rowFree['nc_d_id'];
							$fRow['U_SODAT']=substr($rowFree['periode'],8,2).".".substr($rowFree['periode'],5,2).".".substr($rowFree['periode'],0,4);
							$fRow['U_KUNNR']=$row['U_KUNNR'];
							$fRow['U_CTYPE']=$row['U_CTYPE'];;
							$fRow['U_PL_WE']="WEST";//barat timur
							$fRow['U_TAXCLASS']= $varTax;
							$fRow['VKBUR']=$row['VKBUR'];
							$fRow['WERKS']=$rowFree['sap_code'];
							$fRow['MATNR']=$rowFree['free_item_id']; // Item Gratisnya
							$fRow['ARKTX']=$rowFree['Free_Item_Name']; // Nama Item Gratisnya..
							$fRow['MATKL']=$rowFree['material_group']; //SAP akan mengambil data dari material master SAP
							$fRow['CHARG']="BATCH"; //SAP akan mengambil batch secara FIFO
							$fRow['KWMENG']=$rowFree['GetFree']; // Dpt Gratisnya
							$fRow['COGS']=$rowFree['hpp']; //COGS Nilai Sebelum PPN cond type ZUCS -- ini bisa bikin kisruh.. wkwkwkwk
							$fRow['ZUHN']=$rowFree['price']; //HNA Nilai Sebelum PPN cond type ZUH1 -- ini bisa bikin pusing.. hahahhaa
							$fRow['ZUA1']=$rowFree['price']*$rowFree['GetFree']; //Disc Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe	
							$fRow['ZUA2']="0"; //Disc Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bisa bikin ribut.. hihihi
							$fRow['ZUA4']="0"; //Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC -- jangan2 bisa bikin resign.. huhuhu
							$fRow['U_VCDNO']=""; 
							$fRow['U_FGOOD']="ZUF1"; //ZUF1 Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC
							$fRow['PV']=$rowFree['pv'];;  //PV Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
							$fRow['BV']=$rowFree['bv'];;  //BV Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bikin ribut.. hihihi
							$fRow['YYBANDCD']=""; //Banded Material..
							$fRow['EWALLET_HDR']='';//$varEWalletHdr; //Banded Material..
							$fRow['EWALLET_ITM']='';//$varEWalletItm; //Banded Material..
							$fRow['ITEM_TYPE']=$item_type;
								
							$fRow['AUGRU']=$varCancelSo;
							//$fRow['U_REF_CNCL_INV']= 'Free Item '.$row['so_id'].' - '.$row['hremark'].' '.$varCancelSoRemark;
							$fRow['U_REF_CNCL_INV']= $row['pinjaman_id'];//$varCancelRef;//$row['hremark'].' '.$varCancelSoRemark;
							$fRow['U_REF_CNCL_AUART']= $varCancelAuart;//$row['hremark'].' '.$varCancelSoRemark;
							$fRow['WAERS']='IDR';
								
							$data[] = $fRow;
						}
						//$thefreeItem = FALSE;
					}

				}
				/** End Of Free Item RO **/
				$xx++;
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return $data;


	}
	
	public function get_ro_retur_data($fromdate,$todate){
		$data=array();
		$cSql = "
			SELECT
			a.retur_titipan_id
			, a.id
			, 'ZU07' AS U_AUART
			, b.tgl AS U_SODAT
			, b.stockiest_id AS U_KUNNR
			, 'U0' AS U_CTYPE
			, CASE WHEN ap.deliv_addr > 0 AND k.timur = 0 THEN 'WEST' 
			       WHEN ap.deliv_addr > 0 AND k.timur = 1 THEN 'EAST'
			       WHEN ap.deliv_addr = 0 AND ap.delivery = '1' AND k2.timur = 0 THEN 'WEST'
			       WHEN ap.deliv_addr = 0 AND ap.delivery = '1' AND k2.timur = 1 THEN 'EAST'
			       WHEN ap.deliv_addr = 0 AND ap.delivery = '0' THEN 'WEST'
			       END AS U_PL_WE
			, w.sap_code AS VKBUR
			, IFNULL(w.sap_code,'1601') AS WERKS
			, CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS MATNR
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.name ELSE i.name END AS ARKTX
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.material_group ELSE i.material_group END AS MATKL
			, 'BATCH' AS CHARG
			, CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*a.qty ELSE a.qty END AS KWMENG
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.hpp ELSE i.hpp END AS COGS
			/*
			, CASE WHEN rp.item_id IS NOT NULL AND ap.deliv_addr > 0 AND k.timur = 0 AND i2.price > 0 THEN i2.price 
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr > 0 AND k.timur = 0 AND i2.price = 0 THEN rp.item_price
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr > 0 AND k.timur = 1 AND i2.price > 0  THEN i2.price2 
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr > 0 AND k.timur = 1 AND i2.price = 0  THEN rp.item_price 
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr = 0 AND i2.price > 0  THEN i2.price 
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr = 0 AND i2.price = 0  THEN rp.item_price
			       WHEN rp.item_id IS NULL AND ap.deliv_addr > 0 AND k.timur = 0 THEN i.price 
			       WHEN rp.item_id IS NULL AND ap.deliv_addr > 0 AND k.timur = 1 THEN i.price2 
			       WHEN rp.item_id IS NULL AND ap.deliv_addr = 0 THEN i.price 
			       END AS ZUHN
			, CASE WHEN rp.item_id IS NOT NULL THEN a.harga-rp.item_price ELSE 0 END AS ZUA1
			*/
			, CASE WHEN rp.item_id IS NOT NULL AND ap.deliv_addr > 0 AND k.timur = 0 AND i2.price > 0 THEN i2.price 
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr > 0 AND k.timur = 0 AND i2.price = 0 THEN rp.item_price
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr > 0 AND k.timur = 1 AND i2.price > 0  THEN i2.price2 
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr > 0 AND k.timur = 1 AND i2.price = 0  THEN rp.item_price 
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr = 0 AND ap.delivery = 1 AND k2.timur = 0 AND i2.price > 0  THEN i2.price 
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr = 0 AND ap.delivery = 1 AND k2.timur = 0 AND i2.price = 0  THEN rp.item_price
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr = 0 AND ap.delivery = 1 AND k2.timur = 1 AND i2.price2 > 0  THEN i2.price2 
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr = 0 AND ap.delivery = 1 AND k2.timur = 1 AND i2.price2 = 0  THEN rp.item_price
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr = 0 AND ap.delivery = 0 AND i2.price > 0  THEN i2.price 
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr = 0 AND ap.delivery = 0 AND i2.price = 0  THEN rp.item_price
			       WHEN rp.item_id IS NULL AND ap.deliv_addr > 0 AND k.timur = 0 THEN i.price 
			       WHEN rp.item_id IS NULL AND ap.deliv_addr > 0 AND k.timur = 1 THEN i.price2 
			       WHEN rp.item_id IS NULL AND ap.deliv_addr = 0 AND ap.delivery = 1 AND k2.timur = 0 THEN i.price 
			       WHEN rp.item_id IS NULL AND ap.deliv_addr = 0 AND ap.delivery = 1 AND k2.timur = 1 THEN i.price2
			       WHEN rp.item_id IS NULL AND ap.deliv_addr = 0 AND ap.delivery = 0 THEN i.price
			       END AS ZUHN
			, CASE WHEN rp.item_id IS NOT NULL AND ap.deliv_addr > 0 AND k.timur = 0 AND i2.price > 0 THEN i2.price - rp.item_price 
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr > 0 AND k.timur = 0 AND i2.price = 0 THEN 0
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr > 0 AND k.timur = 1 AND i2.price2 > 0  THEN i2.price2 - rp.item_price 
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr > 0 AND k.timur = 1 AND i2.price2 = 0  THEN 0 
			       -- WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND i2.price > 0  THEN i2.price - rp.item_price
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr = 0 AND ap.delivery = 1 AND k2.timur = 0  AND i2.price > 0  THEN i2.price - rp.item_price
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr = 0 AND ap.delivery = 1 AND k2.timur = 0  AND i2.price = 0  THEN 0
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr = 0 AND ap.delivery = 1 AND k2.timur = 1  AND i2.price2 > 0  THEN i2.price2 - rp.item_price
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr = 0 AND ap.delivery = 1 AND k2.timur = 1  AND i2.price2 = 0  THEN 0
			       -- WHEN rp.item_id IS NOT NULL AND b.deliv_addr = 0 AND i2.price = 0  THEN 0
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr = 0 AND ap.delivery = 0 AND i2.price > 0  THEN i2.price - rp.item_price
			       WHEN rp.item_id IS NOT NULL AND ap.deliv_addr = 0 AND ap.delivery = 0 AND i2.price = 0  THEN 0
			       ELSE 0
			       END AS ZUA1
			, 0 AS ZUA2
			, 0 AS ZUA4
			, '' AS U_VCDNO
			, '' AS U_FGOOD
			, 0 AS PV
			, 0 AS BV
			, CASE WHEN rp.item_id IS NOT NULL THEN a.item_id ELSE '' END AS YYBANDCD
			, '' AS AUGRU
			, '' AS U_REF_CNCL_INV
			, CASE WHEN rp.item_id IS NOT NULL AND i2.material_group = 'UGIMMICK' THEN 'G' 
			       WHEN rp.item_id IS NULL AND i.material_group = 'UGIMMICK' THEN 'G'
				   ELSE '' END AS ITEM_TYPE
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.material_group ELSE i.material_group END AS item_type
			-- , a.pinjaman_id
			-- , a.pinjaman_d_id
			, ap.id as ro_id
			FROM retur_titipan_d a
			LEFT JOIN reff_package rp ON a.item_id = rp.package_id AND a.harga = rp.package_price
			LEFT JOIN retur_titipan b ON b.id = a.retur_titipan_id 
			LEFT JOIN (
				SELECT ro.id, rod.item_id, ro.member_id, ro.date, ro.deliv_addr, ro.delivery
				FROM ro
				LEFT JOIN ro_d rod ON ro.id = rod.ro_id
				-- WHERE ro.date BETWEEN '2019-04-01' AND '2019-04-30'
				WHERE ro.date BETWEEN '".$fromdate."' AND '".$todate."'
				ORDER BY ro.id DESC, rod.item_id DESC
			) AS ap ON ap.member_id = b.stockiest_id AND ap.item_id = a.item_id AND ap.date <= b.tgl
			LEFT JOIN item i ON i.id = a.item_id  
			LEFT JOIN item i2 ON i2.id = rp.item_id  
			LEFT JOIN member m ON m.id = b.stockiest_id   
			LEFT JOIN warehouse w ON w.id = b.warehouse_id 
			-- LEFT JOIN warehouse w2 ON w2.id = b.warehouse_id 
			LEFT JOIN member_delivery md ON ap.deliv_addr = md.id
			LEFT JOIN kota k ON k.id = md.kota
			LEFT JOIN kota k2 ON k2.id = m.kota_id
			WHERE b.tgl BETWEEN '".$fromdate."' AND '".$todate."'
			-- WHERE b.tgl BETWEEN '2019-04-01' AND '2019-04-30'
			";
		//$cSql = "SELECT * FROM retur_sap";
		$query=$this->db->query($cSql);
		
		$curr_ro_id ='';
		$thefreeItem = TRUE;
		$xx = 0;
		
		if ($query){	
			 foreach($query->result_array() as $row){
				if($curr_ro_id == ''){
					$curr_ro_id =$row['retur_titipan_id'];	
				}
				
				if($curr_ro_id!=$row['retur_titipan_id']){
					$curr_ro_id =$row['retur_titipan_id'];
					$thefreeItem = TRUE;
					$xx = 0;
				}
				$cRow=array();
				
				$varTax='Y';
				$varU_AUART=$row['U_AUART'];//'ZU02';
				$varCancelSo='ZU1';
				$varCogs=$row['COGS'];
				$varHna=$row['ZUHN'];
				//$varNetValue=$row['totalsales'];
				//$varPPN=$row['totalppn'];
				$varUsGroup=$row['U_CTYPE'];
				$varCancelSoRemark='';
				$varCancelAuart = '';
				$varCancelRef = '';
				if($row['item_type']=='UGIMMICK')$item_type = 'G'; else $item_type='';
				
				/* Cek Cancel OR RETUR */
				/*
				$sqlCek="SELECT a.*, b.remark FROM cancel_ro a 
				LEFT JOIN cancel_ro_remark b ON b.ro_id_canceled = a.ro_id_canceled 
				WHERE a.ro_id_execute = ".$row['ro_id'];
				$qrCek=$this->db->query($sqlCek);
				$rCek = array();
				if ($qrCek){
					foreach($qrCek->result_array() as $rCek){
						$varCancelSo='ZU2';
						$varU_AUART='ZU06';
						//$varCancelSo=$rCek[''];
						$varCancelSoRemark=$rCek['remark'];
						$row['KWMENG'] = $row['KWMENG']*-1;
						$varCancelAuart = 'ZU01';
						$varCancelRef = $rCek['ro_id_canceled'];
					}
				}
				*/
				/* Cek HNA */
				/*
				$sqlCek="SELECT * FROM item_price_hna WHERE item_id='".$row['item_id']."'";
				$qrCek=$this->db->query($sqlCek);
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						$varHna=$rCek['hna_customer'];;
					}
				}
				*/
				/* get E-Wallet */
				/*
				$sqlCek="SELECT * FROM ewallet_stc_d a WHERE a.noref = ".$row['ro_id']." and description = 'RO From SC Payment' and member_id = '".$row['U_KUNNR']."'";
				//echo $sqlCek.'<br>';
				$qrCek = $this->db->query($sqlCek);
				$rCek = array();
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						$varEWalletHdr=$rCek['ewallet_stc_id'];
						$varEWalletItm=$rCek['id'];
					}
				}
				*/
				/* Cek HJP atau COGS */
				/*
				$sqlCek="SELECT * FROM hjp_history WHERE item_id='".$row['item_id']."' ORDER BY id DESC LIMIT 1";
				$qrCek=$this->db->query($sqlCek);
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						$varCogs=$rCek['hpp'];
					}
				}
				*/
				$varZUA2 = $row['diskonrp'];
				
				$xx = $xx + 1;
				echo "<b>".$xx." ".$curr_ro_id."</b><br>";
				
				$cRow['U_AUART']=$varU_AUART;
				$cRow['U_UHINV']=$row['retur_titipan_id'];
				$cRow['U_POSNR']=$xx;
				$cRow['U_SUBNR']=$row['id'];
				$cRow['U_SODAT']=substr($row['U_SODAT'],8,2).".".substr($row['U_SODAT'],5,2).".".substr($row['U_SODAT'],0,4);
				$cRow['U_KUNNR']=$row['U_KUNNR'];
				$cRow['U_CTYPE']=$row['U_CTYPE'];//$varUsGroup;
				$cRow['U_PL_WE']=$row['U_PL_WE'];//"305";//barat timur
				$cRow['U_TAXCLASS']= "Y";
				$cRow['VKBUR']=$row['VKBUR'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ARKTX']=$row['ARKTX'];
				$cRow['MATKL']=$row['MATKL']; //SAP akan mengambil data dari material master SAP
				$cRow['CHARG']=$row['CHARG']; //SAP akan mengambil batch secara FIFO
				$cRow['KWMENG']=$row['KWMENG'];
				$cRow['COGS']=$varCogs; //COGS Nilai Sebelum PPN cond type ZUCS -- ini bisa bikin kisruh.. wkwkwkwk
				$cRow['ZUHN']=$row['ZUHN'];//$varHna; //HNA Nilai Sebelum PPN cond type ZUH1 -- ini bisa bikin pusing.. hahahhaa
				$cRow['ZUA1']=$row['ZUA1']*$row['KWMENG']; //Disc Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
				$cRow['ZUA2']= $varZUA2;//$row['ZUA2']; //Disc Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bisa bikin ribut.. hihihi
				$cRow['ZUA4']=$row['ZUA4']; //Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC -- jangan2 bisa bikin resign.. huhuhu
				$cRow['U_VCDNO']=$row['U_VCDNO']; 
				$cRow['U_FGOOD']=$row['U_FGOOD']; //ZUF1 Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC
				//$cRow['ZUH2']=$row['ZUH2']; //Net Value Nilai Sebelum PPN cond type ZUNT -- ini juga bisa bikin kisruh.. wkwkwkwk
				//$cRow['ZPPN']=$varPPN; //PPN Nilai Sebelum PPN cond type ZUTX -- ini yg bikin pusing.. hahahhaa
				$cRow['PV']=$row['PV'];  //PV Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
				$cRow['BV']=$row['BV'];  //BV Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bikin ribut.. hihihi
				$cRow['YYBANDCD']=$row['YYBANDCD']; //Banded Material..
				$cRow['EWALLET_HDR']='';//$varEWalletHdr; //Banded Material..
				$cRow['EWALLET_ITM']='';//$varEWalletItm; //Banded Material..
				$cRow['ITEM_TYPE']=$item_type; //Inserted date..
				$cRow['WAERS']='IDR'; //Inserted date..
				//$cRow['ERDAT']=""; //Inserted date..
				//$cRow['ERZET']=""; //Inserted time..
				/*
				ZU01 = Return
				ZU02 = Cancel billing
				*/
				$cRow['AUGRU']=$varCancelSo;
				$cRow['U_REF_CNCL_INV']= $row['ro_id'];//$varCancelRef;//$row['hremark'].' '.$varCancelSoRemark;
				$cRow['U_REF_CNCL_AUART']= $varCancelAuart;//$row['hremark'].' '.$varCancelSoRemark;
				
				$data[] = $cRow;
				
				/** Start Free Item RO **/
				/* Cek Free Item SO Klw Ada */
				/*
				if($thefreeItem){
					$free_itemSql="
						SELECT a.*, 
							a.pinjaman_nc_id AS nc_d_id,
							a.item_id AS free_item_id,
							i.`name` AS Free_Item_Name,
							IFNULL((b.qty_retur/d.free_item_qty),0) AS GetFree,		
							a.warehouse_id,
							w.sap_code,
							a.pinjaman_nc_tgl AS periode,
							i.material_group,
							i.hpp,
							i.price, i.pv, i.bv, i.material_group
						FROM pinjaman_nc a 
						LEFT JOIN pinjaman_d b ON b.id = a.pinjaman_d_id
						LEFT JOIN item i ON i.id = a.item_id   
						LEFT JOIN warehouse w ON w.id = a.warehouse_id   
						LEFT JOIN promo d ON d.id = a.promo_id 
						WHERE a.pinjaman_d_id=".$row['pinjaman_d_id']."
					";
					
					$qrFreeItem=$this->db->query($free_itemSql);
					if ($qrFreeItem){
						foreach($qrFreeItem->result_array() as $rowFree){
							$xx++;
							$fRow=array();
							if($rowFree['material_group']=='UGIMMICK')$item_type = 'G'; else $item_type='';
							
							$fRow['U_AUART']=$varU_AUART;
							$fRow['U_UHINV']=$row['retur_pinjaman_id'];
							$fRow['U_POSNR']=$xx;
							$fRow['U_SUBNR']=$rowFree['nc_d_id'];
							$fRow['U_SODAT']=substr($rowFree['periode'],8,2).".".substr($rowFree['periode'],5,2).".".substr($rowFree['periode'],0,4);
							$fRow['U_KUNNR']=$row['U_KUNNR'];
							$fRow['U_CTYPE']=$row['U_CTYPE'];;
							$fRow['U_PL_WE']="WEST";//barat timur
							$fRow['U_TAXCLASS']= $varTax;
							$fRow['VKBUR']=$row['VKBUR'];
							$fRow['WERKS']=$rowFree['sap_code'];
							$fRow['MATNR']=$rowFree['free_item_id']; // Item Gratisnya
							$fRow['ARKTX']=$rowFree['Free_Item_Name']; // Nama Item Gratisnya..
							$fRow['MATKL']=$rowFree['material_group']; //SAP akan mengambil data dari material master SAP
							$fRow['CHARG']="BATCH"; //SAP akan mengambil batch secara FIFO
							$fRow['KWMENG']=$rowFree['GetFree']; // Dpt Gratisnya
							$fRow['COGS']=$rowFree['hpp']; //COGS Nilai Sebelum PPN cond type ZUCS -- ini bisa bikin kisruh.. wkwkwkwk
							$fRow['ZUHN']=$rowFree['price']; //HNA Nilai Sebelum PPN cond type ZUH1 -- ini bisa bikin pusing.. hahahhaa
							$fRow['ZUA1']=$rowFree['price']*$rowFree['GetFree']; //Disc Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe	
							$fRow['ZUA2']="0"; //Disc Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bisa bikin ribut.. hihihi
							$fRow['ZUA4']="0"; //Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC -- jangan2 bisa bikin resign.. huhuhu
							$fRow['U_VCDNO']=""; 
							$fRow['U_FGOOD']="ZUF1"; //ZUF1 Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC
							$fRow['PV']=$rowFree['pv'];;  //PV Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
							$fRow['BV']=$rowFree['bv'];;  //BV Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bikin ribut.. hihihi
							$fRow['YYBANDCD']=""; //Banded Material..
							$fRow['EWALLET_HDR']='';//$varEWalletHdr; //Banded Material..
							$fRow['EWALLET_ITM']='';//$varEWalletItm; //Banded Material..
							$fRow['ITEM_TYPE']=$item_type;
								
							$fRow['AUGRU']=$varCancelSo;
							//$fRow['U_REF_CNCL_INV']= 'Free Item '.$row['so_id'].' - '.$row['hremark'].' '.$varCancelSoRemark;
							$fRow['U_REF_CNCL_INV']= $row['pinjaman_id'];//$varCancelRef;//$row['hremark'].' '.$varCancelSoRemark;
							$fRow['U_REF_CNCL_AUART']= $varCancelAuart;//$row['hremark'].' '.$varCancelSoRemark;
							$fRow['WAERS']='IDR';
								
							$data[] = $fRow;
						}
						//$thefreeItem = FALSE;
					}

				}
				*/
				/** End Of Free Item RO **/
				//$xx++;
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return $data;


	}
	
	public function get_sc_adm_retur_data($fromdate,$todate){
		$data=array();
		$cSql = "
			SELECT
			a.pinjaman_admin_retur_id
			, a.id
			, 'ZU04' AS U_AUART
			, b.tgl AS U_SODAT
			, b.admin_id AS U_KUNNR
			, 'U0' AS U_CTYPE
			, 'WEST'AS U_PL_WE
			, w.sap_code AS VKBUR
			, IFNULL(w.sap_code,'1601') AS WERKS
			, CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS MATNR
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.name ELSE i.name END AS ARKTX
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.material_group ELSE i.material_group END AS MATKL
			, 'BATCH' AS CHARG
			, CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*a.qty ELSE a.qty END AS KWMENG
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.hpp ELSE i.hpp END AS COGS
			, CASE WHEN rp.item_id IS NOT NULL AND i2.price > 0 THEN i2.price 
			       WHEN rp.item_id IS NOT NULL AND i2.price = 0 THEN rp.item_price
			       WHEN rp.item_id IS NULL THEN i.price 
			       END AS ZUHN
			, CASE WHEN rp.item_id IS NOT NULL AND i2.price > 0 THEN i2.price - rp.item_price
			       WHEN rp.item_id IS NOT NULL AND i2.price = 0 THEN 0
			       ELSE 0
			       END AS ZUA1
			-- , CASE WHEN rp.item_id IS NOT NULL THEN a.harga-rp.item_price ELSE 0 END AS ZUA1
			, 0 AS ZUA2
			, 0 AS ZUA4
			, '' AS U_VCDNO
			, '' AS U_FGOOD
			, 0 AS PV
			, 0 AS BV
			, CASE WHEN rp.item_id IS NOT NULL THEN a.item_id ELSE '' END AS YYBANDCD
			, '' AS AUGRU
			, '' AS U_REF_CNCL_INV
			, CASE WHEN rp.item_id IS NOT NULL AND i2.material_group = 'UGIMMICK' THEN 'G' 
			       WHEN rp.item_id IS NULL AND i.material_group = 'UGIMMICK' THEN 'G'
				   ELSE '' END AS ITEM_TYPE
			, CASE WHEN rp.item_id IS NOT NULL THEN i2.material_group ELSE i.material_group END AS item_type
			, NULL AS pinjaman_id
			, NULL AS pinjaman_d_id
			FROM pinjaman_admin_retur_d a
			LEFT JOIN reff_package rp ON a.item_id = rp.package_id AND a.harga = rp.package_price
			LEFT JOIN pinjaman_admin_retur b ON b.id = a.pinjaman_admin_retur_id 
			-- LEFT JOIN pinjaman ap ON ap.id = b.pinjaman_id
			LEFT JOIN item i ON i.id = a.item_id  
			LEFT JOIN item i2 ON i2.id = rp.item_id  
			LEFT JOIN admins m ON m.id = b.admin_id   
			-- LEFT JOIN member m ON m.id = b.stockiest_id   
			LEFT JOIN warehouse w ON w.id = b.warehouse_id 
			-- LEFT JOIN warehouse w2 ON w2.id = b.warehouse_id 
			-- LEFT JOIN member_delivery md ON ap.deliv_addr = md.id
			-- LEFT JOIN kota k ON k.id = md.kota
			-- LEFT JOIN kota k2 ON k2.id = m.kota_id
			WHERE b.tgl BETWEEN '".$fromdate."' AND '".$todate."'
			-- WHERE b.tgl BETWEEN '2019-07-01' AND '2019-07-31'
			";
		$query=$this->db->query($cSql);
		
		$curr_ro_id ='';
		$thefreeItem = TRUE;
		$xx = 1;
		
		if ($query){	
			 foreach($query->result_array() as $row){
				if($curr_ro_id == ''){
					$curr_ro_id =$row['pinjaman_admin_retur_id'];	
				}
				
				if($curr_ro_id!=$row['pinjaman_admin_retur_id']){
					$curr_ro_id =$row['pinjaman_admin_retur_id'];
					$thefreeItem = TRUE;
					$xx = 1;
				}
				$cRow=array();
				
				$varTax='Y';
				$varU_AUART=$row['U_AUART'];//'ZU02';
				$varCancelSo='ZU1';
				$varCogs=$row['COGS'];
				$varHna=$row['ZUHN'];
				//$varNetValue=$row['totalsales'];
				//$varPPN=$row['totalppn'];
				$varUsGroup=$row['U_CTYPE'];
				$varCancelSoRemark='';
				$varCancelAuart = '';
				$varCancelRef = '';
				if($row['item_type']=='UGIMMICK')$item_type = 'G'; else $item_type='';
				
				/* Cek Cancel OR RETUR */
				/*
				$sqlCek="SELECT a.*, b.remark FROM cancel_ro a 
				LEFT JOIN cancel_ro_remark b ON b.ro_id_canceled = a.ro_id_canceled 
				WHERE a.ro_id_execute = ".$row['ro_id'];
				$qrCek=$this->db->query($sqlCek);
				$rCek = array();
				if ($qrCek){
					foreach($qrCek->result_array() as $rCek){
						$varCancelSo='ZU2';
						$varU_AUART='ZU06';
						//$varCancelSo=$rCek[''];
						$varCancelSoRemark=$rCek['remark'];
						$row['KWMENG'] = $row['KWMENG']*-1;
						$varCancelAuart = 'ZU01';
						$varCancelRef = $rCek['ro_id_canceled'];
					}
				}
				*/
				/* Cek HNA */
				/*
				$sqlCek="SELECT * FROM item_price_hna WHERE item_id='".$row['item_id']."'";
				$qrCek=$this->db->query($sqlCek);
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						$varHna=$rCek['hna_customer'];;
					}
				}
				*/
				/* get E-Wallet */
				/*
				$sqlCek="SELECT * FROM ewallet_stc_d a WHERE a.noref = ".$row['ro_id']." and description = 'RO From SC Payment' and member_id = '".$row['U_KUNNR']."'";
				//echo $sqlCek.'<br>';
				$qrCek = $this->db->query($sqlCek);
				$rCek = array();
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						$varEWalletHdr=$rCek['ewallet_stc_id'];
						$varEWalletItm=$rCek['id'];
					}
				}
				*/
				/* Cek HJP atau COGS */
				/*
				$sqlCek="SELECT * FROM hjp_history WHERE item_id='".$row['item_id']."' ORDER BY id DESC LIMIT 1";
				$qrCek=$this->db->query($sqlCek);
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						$varCogs=$rCek['hpp'];
					}
				}
				*/
				
				
				$cRow['U_AUART']=$varU_AUART;
				$cRow['U_UHINV']=$row['pinjaman_admin_retur_id'];
				$cRow['U_POSNR']=$xx;
				$cRow['U_SUBNR']=$row['id'];
				$cRow['U_SODAT']=substr($row['U_SODAT'],8,2).".".substr($row['U_SODAT'],5,2).".".substr($row['U_SODAT'],0,4);
				$cRow['U_KUNNR']=$row['U_KUNNR'];
				$cRow['U_CTYPE']=$row['U_CTYPE'];//$varUsGroup;
				$cRow['U_PL_WE']=$row['U_PL_WE'];//"305";//barat timur
				$cRow['U_TAXCLASS']= "Y";
				$cRow['VKBUR']=$row['VKBUR'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ARKTX']=$row['ARKTX'];
				$cRow['MATKL']=$row['MATKL']; //SAP akan mengambil data dari material master SAP
				$cRow['CHARG']=$row['CHARG']; //SAP akan mengambil batch secara FIFO
				$cRow['KWMENG']=$row['KWMENG'];
				$cRow['COGS']=$varCogs; //COGS Nilai Sebelum PPN cond type ZUCS -- ini bisa bikin kisruh.. wkwkwkwk
				$cRow['ZUHN']=$row['ZUHN'];//$varHna; //HNA Nilai Sebelum PPN cond type ZUH1 -- ini bisa bikin pusing.. hahahhaa
				$cRow['ZUA1']=$row['ZUA1']*$row['KWMENG']; //Disc Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
				$cRow['ZUA2']=$row['ZUA2']; //Disc Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bisa bikin ribut.. hihihi
				$cRow['ZUA4']=$row['ZUA4']; //Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC -- jangan2 bisa bikin resign.. huhuhu
				$cRow['U_VCDNO']=$row['U_VCDNO']; 
				$cRow['U_FGOOD']=$row['U_FGOOD']; //ZUF1 Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC
				//$cRow['ZUH2']=$row['ZUH2']; //Net Value Nilai Sebelum PPN cond type ZUNT -- ini juga bisa bikin kisruh.. wkwkwkwk
				//$cRow['ZPPN']=$varPPN; //PPN Nilai Sebelum PPN cond type ZUTX -- ini yg bikin pusing.. hahahhaa
				$cRow['PV']=$row['PV'];  //PV Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
				$cRow['BV']=$row['BV'];  //BV Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bikin ribut.. hihihi
				$cRow['YYBANDCD']=$row['YYBANDCD']; //Banded Material..
				$cRow['EWALLET_HDR']='';//$varEWalletHdr; //Banded Material..
				$cRow['EWALLET_ITM']='';//$varEWalletItm; //Banded Material..
				$cRow['ITEM_TYPE']=$item_type; //Inserted date..
				$cRow['WAERS']='IDR'; //Inserted date..
				//$cRow['ERDAT']=""; //Inserted date..
				//$cRow['ERZET']=""; //Inserted time..
				/*
				ZU01 = Return
				ZU02 = Cancel billing
				*/
				$cRow['AUGRU']=$varCancelSo;
				$cRow['U_REF_CNCL_INV']= $row['pinjaman_id'];//$varCancelRef;//$row['hremark'].' '.$varCancelSoRemark;
				$cRow['U_REF_CNCL_AUART']= $varCancelAuart;//$row['hremark'].' '.$varCancelSoRemark;
				
				$data[] = $cRow;
				
				$xx++;
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return $data;


	}


}
?>