<?php
class Msales extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | API view balance ewallet
    |--------------------------------------------------------------------------
    |
    | @author Budi Prayudi
    | @created 2019-03-06
    |
    */
	
    public function getSo_data($fromdate,$todate){
		$data=array();
		$cSql="SELECT
			a.*, YEAR(b.tgl) as thn,
			i.`name` as item_nm,
			i.satuan,
			b.stockiest_id as stockiest_id,
			b.member_id as member_id,
			m.nama as member_nama,
			m.npwp,
			b.warehouse_id as whs_id,
			w.sap_code as warehouse_id,
			b.tgl as hdate,
			b.totalharga as totalharga,
			b.totalsales as totalsales,
			b.totalppn as totalppn,
			b.totalbv as totalbv,
			b.totalpv as totalpv,
			b.created as hcreated,
			b.createdby as hcreatedby,
			DATE_FORMAT(b.tgl, '%d.%m.%Y') AS tgl_sap,
			b.remark as hremark 
			FROM
				(so_d a)
			INNER JOIN so b ON b.id = a.so_id 
			INNER JOIN item i ON i.id = a.item_id  
			INNER JOIN member m ON m.id = b.member_id   
			INNER JOIN warehouse w ON w.id = b.warehouse_id   
			WHERE b.tgl BETWEEN '".$fromdate."' AND '".$todate."' AND b.stockiest_id='0'";
		$query=$this->db->query($cSql);
		if ($query){	
			 foreach($query->result_array() as $row){
				$cRow=array();
				$varTax='Y';
				$varVBELN='ZU02';
				$varCancelSo='';
				$varCogs='';
				$varHna='';
				$varNetValue=$row['totalsales'];
				$varPPN=$row['totalppn'];
				$varUsGroup='U1';
				if (empty($row['npwp'])){$varTax='N';}
				if ($row['member_id']=='STAFF'){$varUsGroup='U2';}
				
				/*
				SAP Code Variabel For KDGRP
				U0 - UHN STOCKIST
				U1 - UHN MEMBER
				U2 - UHN STAFF
				*/
				
				/* Cek Cancel OR RETUR */
				$sqlCek="SELECT a.*, b.remark FROM canceled_so a 
				LEFT JOIN canceled_so_remark b ON b.so_id_canceled = a.so_id_canceled ";
				$qrCek=$this->db->query($sqlCek);
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						if ($row['so_id']==$rCek['so_id_execute']){ $varCancelSo='ZU01';$varVBELN='ZU07'; }
						//$varCancelSo=$rCek[''];
						$varCancelSoRemark=$rCek['remark'];
					}
				}
				
				/* Cek HNA */
				$sqlCek="SELECT * FROM item_price_hna WHERE item_id='".$row['item_id']."'";
				$qrCek=$this->db->query($sqlCek);
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						$varHna=$rCek['hna_customer'];;
					}
				}
				
				/* Cek HJP atau COGS */
				$sqlCek="SELECT * FROM hjp_history WHERE item_id='".$row['item_id']."' ORDER BY id DESC LIMIT 1";
				$qrCek=$this->db->query($sqlCek);
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						$varCogs=$rCek['hpp'];
					}
				}

				$cRow['VBELN']=$varVBELN;
				$cRow['BSTKD']=$row['so_id'];
				$cRow['BSTDK']=$row['tgl_sap'];
				$cRow['BNAME']=$row['member_id'];
				$cRow['KDGRP']=$varUsGroup;
				$cRow['BSTZD']="305";//barat timur
				$cRow['TAXK1']= "N";
				$cRow['VKBUR']=$row['warehouse_id'];
				$cRow['WERKS']=$row['sap_code'];
				$cRow['MATNR']=$row['item_id'];
				$cRow['ARKTX']=$row['item_nm'];
				$cRow['MATKL']=""; //SAP akan mengambil data dari material master SAP
				$cRow['CHARG']=""; //SAP akan mengambil batch secara FIFO
				$cRow['KWMENG']=$row['qty'];
				$cRow['KWERT']=$varCogs; //COGS Nilai Sebelum PPN cond type ZUCS -- ini bisa bikin kisruh.. wkwkwkwk
				$cRow['KWERT']=$varHna; //HNA Nilai Sebelum PPN cond type ZUH1 -- ini bisa bikin pusing.. hahahhaa
				$cRow['KWERT']="0"; //Disc Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
				$cRow['KWERT']="0"; //Disc Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bisa bikin ribut.. hihihi
				$cRow['KWERT']="0"; //Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC -- jangan2 bisa bikin resign.. huhuhu
				$cRow['YVCODE']="0"; 
				$cRow['PSTYV']="0"; //ZUF1 Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC
				$cRow['KWERT']=$varNetValue; //Net Value Nilai Sebelum PPN cond type ZUNT -- ini juga bisa bikin kisruh.. wkwkwkwk
				$cRow['KWERT']=$varPPN; //PPN Nilai Sebelum PPN cond type ZUTX -- ini yg bikin pusing.. hahahhaa
				$cRow['KWERT']=$row['totalpv'];  //PV Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
				$cRow['KWERT']=$row['totalbv'];  //BV Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bikin ribut.. hihihi
				$cRow['YBEND']=""; //Banded Material..
				/*
				ZU01 = Return
				ZU02 = Cancel billing
				*/
				$cRow['AUGRU']=$varCancelSo;
				$cRow['YREF']= $row['hremark'].' '.$varCancelSoRemark;
				$data[] = $cRow;
				
				
				/* Cek Free Item SO Klw Ada */
				$free_itemSql="
				SELECT a.*, 
					c.id as nc_d_id,
					c.item_id as free_item_id,
					i.`name` as Free_Item_Name,
					c.qty as GetFree,		
					b.warehouse_id,
					w.sap_code 
				FROM ref_trx_nc a 
				INNER JOIN ncm b ON b.id = a.nc_id 
				INNER JOIN ncm_d c ON c.ncm_id = b.id 
				INNER JOIN item i ON i.id = c.item_id   
				INNER JOIN warehouse w ON w.id = b.warehouse_id   
				WHERE a.trx_id='".$row['ro_id']."'";
				
				$qrFreeItem=$this->db->query($free_itemSql);
				if ($qrFreeItem){
					foreach($qrFreeItem->result_array() as $rowFree){
						$fRow=array();
						
						$fRow['VBELN']=$varVBELN;
						$fRow['BSTKD']=$rowFree['nc_d_id'];
						$fRow['BSTDK']=$row['tgl_sap'];
						$fRow['BNAME']=$row['member_id'];
						$fRow['KDGRP']="U1";
						$fRow['BSTZD']="305";//barat timur
						$fRow['TAXK1']= $varTax;
						$fRow['VKBUR']=$row['warehouse_id'];
						$fRow['WERKS']=$row['sap_code'];
						$fRow['MATNR']=$rowFree['free_item_id']; // Item Gratisnya
						$fRow['ARKTX']=$rowFree['Free_Item_Name']; // Nama Item Gratisnya..
						$fRow['MATKL']=""; //SAP akan mengambil data dari material master SAP
						$fRow['CHARG']=""; //SAP akan mengambil batch secara FIFO
						$fRow['KWMENG']=$rowFree['GetFree']; // Dpt Gratisnya
						$fRow['KWERT']="0"; //COGS Nilai Sebelum PPN cond type ZUCS -- ini bisa bikin kisruh.. wkwkwkwk
						$fRow['KWERT']="0"; //HNA Nilai Sebelum PPN cond type ZUH1 -- ini bisa bikin pusing.. hahahhaa
						$fRow['KWERT']="0"; //Disc Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
						$fRow['KWERT']="0"; //Disc Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bisa bikin ribut.. hihihi
						$fRow['KWERT']="0"; //Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC -- jangan2 bisa bikin resign.. huhuhu
						$fRow['YVCODE']="0"; 
						$fRow['PSTYV']="0"; //ZUF1 Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC
						$fRow['KWERT']="0"; //Net Value Nilai Sebelum PPN cond type ZUNT -- ini juga bisa bikin kisruh.. wkwkwkwk
						$fRow['KWERT']="0"; //PPN Nilai Sebelum PPN cond type ZUTX -- ini yg bikin pusing.. hahahhaa
						$fRow['KWERT']="0";  //PV Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
						$fRow['KWERT']="0";  //BV Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bikin ribut.. hihihi
						$fRow['YBEND']=""; //Banded Material..
						
						$fRow['AUGRU']=$varCancelSo;
						$fRow['YREF']= 'Free Item '.$row['ro_id'].' - '.$row['hremark'].' '.$varCancelSoRemark;
						$data[] = $fRow;
					}
					
				}
				/** End Of Free Item RO **/
				
				
				
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return $data;


	}
	
	
	public function getRo_data($fromdate,$todate){
		$data=array();
		$cSql="SELECT
			a.*, YEAR(b.date) as thn,
			i.`name` as item_nm,
			i.satuan,
			b.member_id as member_id,
			m.nama as member_nama,
			m.npwp,
			b.warehouse_id as whs_id,
			w.sap_code as warehouse_id,
			b.totalharga as totalharga,
			b.totalbv as totalbv,
			b.totalpv as totalpv,
			b.date as hdate,
			b.created as hcreated,
			b.createdby as hcreatedby,
			DATE_FORMAT(b.date, '%d.%m.%Y') AS tgl_sap,
			b.remark as hremark 
			FROM
				(ro_d a)
			INNER JOIN ro b ON b.id = a.ro_id 
			INNER JOIN item i ON i.id = a.item_id  
			INNER JOIN member m ON m.id = b.member_id   
			INNER JOIN warehouse w ON w.id = b.warehouse_id   
			WHERE b.date BETWEEN '".$fromdate."' AND '".$todate."' ";
		$query=$this->db->query($cSql);
		if ($query){	
			 foreach($query->result_array() as $row){
				$cRow=array();
				
				$varTax='Y';
				$varVBELN='ZU01';
				$varCancelSo='';
				$varCogs='';
				$varHna='';
				$varCancelSoRemark='';
				
				/* Cek Cancel OR RETUR */
				$sqlCek="SELECT a.*, b.remark FROM canceled_so a 
				LEFT JOIN canceled_so_remark b ON b.so_id_canceled = a.so_id_canceled ";
				$qrCek=$this->db->query($sqlCek);
				if ($qrCek){
					/*					
					foreach($qrCek->result_array() as $rCek){
						
						if ($row['ro_id']==$rCek['so_id_execute']){ $varCancelSo='ZU01';$varVBELN='ZU07'; }
						//$varCancelSo=$rCek[''];
						$varCancelSoRemark=$rCek['remark'];
						
					}
					*/
				}
				
				/* Cek HNA */
				$sqlCek="SELECT * FROM item_price_hna WHERE item_id='".$row['item_id']."'";
				$qrCek=$this->db->query($sqlCek);
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						$varHna=$rCek['hna_customer'];;
					}
				}
				
				
				/* Cek HJP atau COGS */
				$sqlCek="SELECT * FROM hjp_history WHERE item_id='".$row['item_id']."' ORDER BY id DESC LIMIT 1";
				$qrCek=$this->db->query($sqlCek);
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						$varCogs=$rCek['hpp'];
					}
				}

				
				
				$cRow['VBELN']=$varVBELN;
				$cRow['BSTKD']=$row['ro_id'];
				$cRow['BSTDK']=$row['tgl_sap'];
				$cRow['BNAME']=$row['member_id'];
				$cRow['KDGRP']="U1";
				$cRow['BSTZD']="305";//barat timur
				$cRow['TAXK1']= $varTax;
				$cRow['VKBUR']=$row['warehouse_id'];
				$cRow['WERKS']=$row['warehouse_id'];
				$cRow['MATNR']=$row['item_id'];
				$cRow['ARKTX']=$row['item_nm'];
				$cRow['MATKL']=""; //SAP akan mengambil data dari material master SAP
				$cRow['CHARG']=""; //SAP akan mengambil batch secara FIFO
				$cRow['KWMENG']=$row['qty'];
				$cRow['KWERT']=$varCogs; //COGS Nilai Sebelum PPN cond type ZUCS -- ini bisa bikin kisruh.. wkwkwkwk
				$cRow['KWERT']=$varHna; //HNA Nilai Sebelum PPN cond type ZUH1 -- ini bisa bikin pusing.. hahahhaa
				$cRow['KWERT']="0"; //Disc Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
				$cRow['KWERT']="0"; //Disc Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bisa bikin ribut.. hihihi
				$cRow['KWERT']="0"; //Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC -- jangan2 bisa bikin resign.. huhuhu
				$cRow['YVCODE']="0"; 
				$cRow['PSTYV']="0"; //ZUF1 Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC
				$cRow['KWERT']=$row['totalharga']; //Net Value Nilai Sebelum PPN cond type ZUNT -- ini juga bisa bikin kisruh.. wkwkwkwk
				$cRow['KWERT']="0"; //PPN Nilai Sebelum PPN cond type ZUTX -- ini yg bikin pusing.. hahahhaa
				$cRow['KWERT']=$row['totalpv'];  //PV Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
				$cRow['KWERT']=$row['totalbv'];  //BV Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bikin ribut.. hihihi
				$cRow['YBEND']=""; //Banded Material..
				/*
				ZU01 = Return
				ZU02 = Cancel billing
				*/
				$cRow['AUGRU']=$varCancelSo;
				$cRow['YREF']= $row['hremark'].' '.$varCancelSoRemark;
				
				$data[] = $cRow;
				
				/** Start Free Item RO **/
				$free_itemSql="
				SELECT a.*, 
					c.id as nc_d_id,
					c.item_id as free_item_id,
					i.`name` as Free_Item_Name,
					c.qty as GetFree,		
					b.warehouse_id,
					w.sap_code 
				FROM ref_trx_nc a 
				INNER JOIN ncm b ON b.id = a.nc_id 
				INNER JOIN ncm_d c ON c.ncm_id = b.id 
				INNER JOIN item i ON i.id = c.item_id   
				INNER JOIN warehouse w ON w.id = b.warehouse_id   
				WHERE a.trx_id='".$row['ro_id']."'";
				
				$qrFreeItem=$this->db->query($free_itemSql);
				if ($qrFreeItem){
					foreach($qrFreeItem->result_array() as $rowFree){
						$fRow=array();
						
						$fRow['VBELN']=$varVBELN;
						$fRow['BSTKD']=$rowFree['nc_d_id'];
						$fRow['BSTDK']=$row['tgl_sap'];
						$fRow['BNAME']=$row['member_id'];
						$fRow['KDGRP']="U1";
						$fRow['BSTZD']="305";//barat timur
						$fRow['TAXK1']= $varTax;
						$fRow['VKBUR']=$row['warehouse_id'];
						$fRow['WERKS']=$row['warehouse_id'];
						$fRow['MATNR']=$rowFree['free_item_id']; // Item Gratisnya
						$fRow['ARKTX']=$rowFree['Free_Item_Name']; // Nama Item Gratisnya..
						$fRow['MATKL']=""; //SAP akan mengambil data dari material master SAP
						$fRow['CHARG']=""; //SAP akan mengambil batch secara FIFO
						$fRow['KWMENG']=$rowFree['GetFree']; // Dpt Gratisnya
						$fRow['KWERT']="0"; //COGS Nilai Sebelum PPN cond type ZUCS -- ini bisa bikin kisruh.. wkwkwkwk
						$fRow['KWERT']="0"; //HNA Nilai Sebelum PPN cond type ZUH1 -- ini bisa bikin pusing.. hahahhaa
						$fRow['KWERT']="0"; //Disc Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
						$fRow['KWERT']="0"; //Disc Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bisa bikin ribut.. hihihi
						$fRow['KWERT']="0"; //Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC -- jangan2 bisa bikin resign.. huhuhu
						$fRow['YVCODE']="0"; 
						$fRow['PSTYV']="0"; //ZUF1 Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC
						$fRow['KWERT']="0"; //Net Value Nilai Sebelum PPN cond type ZUNT -- ini juga bisa bikin kisruh.. wkwkwkwk
						$fRow['KWERT']="0"; //PPN Nilai Sebelum PPN cond type ZUTX -- ini yg bikin pusing.. hahahhaa
						$fRow['KWERT']="0";  //PV Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
						$fRow['KWERT']="0";  //BV Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bikin ribut.. hihihi
						$fRow['YBEND']=""; //Banded Material..
						
						$fRow['AUGRU']=$varCancelSo;
						$fRow['YREF']= 'Free Item '.$row['ro_id'].' - '.$row['hremark'].' '.$varCancelSoRemark;
						$data[] = $fRow;
					}
					
				}
				/** End Of Free Item RO **/
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return $data;


	}
	
	
	public function getSc_data($fromdate,$todate){
		$data=array();
		$cSql="SELECT
			a.*, YEAR(b.tgl) as thn,
			i.`name` as item_nm,
			i.satuan,
			b.stockiest_id as stockiest_id,
			b.totalharga as totalharga,
			b.totalpv as totalpv,
			b.warehouse_id as whs_id,
			w.sap_code as warehouse_id,
			b.tgl as hdate,
			b.created as hcreated,
			b.createdby as hcreatedby,
			DATE_FORMAT(b.tgl, '%d.%m.%Y') AS tgl_sap,
			b.remark as hremark 
			FROM
				(pinjaman_d a)
			INNER JOIN pinjaman b ON b.id = a.pinjaman_id 
			INNER JOIN item i ON i.id = a.item_id  
			INNER JOIN warehouse w ON w.id = b.warehouse_id   
			WHERE b.tgl BETWEEN '".$fromdate."' AND '".$todate."' ";
		$query=$this->db->query($cSql);
		if ($query){	
			 foreach($query->result_array() as $row){
				$cRow=array();
				 
				$varTax='Y';
				$varVBELN='ZU03';
				$varCancelSo='';
				$varCogs='';
				$varHna='';
				$varCancelScRemark='';
				
				$varDiskon='';
				
				/* Cek HNA */
				$sqlCek="SELECT * FROM item_price_hna WHERE item_id='".$row['item_id']."'";
				$qrCek=$this->db->query($sqlCek);
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						$varHna=$rCek['hna_customer'];;
					}
				}
				
				/* Cek HJP atau COGS */
				$sqlCek="SELECT * FROM hjp_history WHERE item_id='".$row['item_id']."' ORDER BY id DESC LIMIT 1";
				$qrCek=$this->db->query($sqlCek);
				if ($qrCek){	
					foreach($qrCek->result_array() as $rCek){
						$varCogs=$rCek['hpp'];
					}
				}
				
				
				$cRow['VBELN']=$varVBELN;
				$cRow['BSTKD']=$row['pinjaman_id'];
				$cRow['BSTDK']=$row['tgl_sap'];
				$cRow['BNAME']=$row['stockiest_id'];
				$cRow['KDGRP']="U0";
				$cRow['BSTZD']="305";//barat timur
				$cRow['TAXK1']= $varTax;
				$cRow['VKBUR']=$row['whs_id'];
				$cRow['WERKS']=$row['warehouse_id'];
				$cRow['MATNR']=$row['item_id'];
				$cRow['ARKTX']=$row['item_nm'];
				$cRow['MATKL']=""; //SAP akan mengambil data dari material master SAP
				$cRow['CHARG']=""; //SAP akan mengambil batch secara FIFO
				$cRow['KWMENG']=$row['qty'];
				$cRow['KWERT']=$varCogs; //COGS Nilai Sebelum PPN cond type ZUCS -- ini bisa bikin kisruh.. wkwkwkwk
				$cRow['KWERT']=$varHna; //HNA Nilai Sebelum PPN cond type ZUH1 -- ini bisa bikin pusing.. hahahhaa
				$cRow['KWERT']="0"; //Disc Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
				$cRow['KWERT']="0"; //Disc Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bisa bikin ribut.. hihihi
				$cRow['KWERT']="0"; //Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC -- jangan2 bisa bikin resign.. huhuhu
				$cRow['YVCODE']="0"; 
				$cRow['PSTYV']="0"; //ZUF1 Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC
				$cRow['KWERT']=$row['totalharga']; //Net Value Nilai Sebelum PPN cond type ZUNT -- ini juga bisa bikin kisruh.. wkwkwkwk
				$cRow['KWERT']="0"; //PPN Nilai Sebelum PPN cond type ZUTX -- ini yg bikin pusing.. hahahhaa
				$cRow['KWERT']=$row['totalpv'];  //PV Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
				$cRow['KWERT']="";  //BV Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bikin ribut.. hihihi
				$cRow['YBEND']=""; //Banded Material..
				/*
				ZU01 = Return
				ZU02 = Cancel billing
				*/
				$cRow['AUGRU']=$varCancelSo;
				$cRow['YREF']= $row['hremark'].' '.$varCancelScRemark;
				$data[] = $cRow;
				
				
				$free_itemSql="
				SELECT a.*, 
				b.item_id,
				c.`name` AS item_name,
				( SELECT c. NAME AS free_item_name FROM item c WHERE c.id = d.free_item_id ) AS Free_Item_Name, 
				( SELECT xw.sap_code AS wh_tran FROM warehouse xw WHERE xw.id = b.warehouse_id ) AS Wh_Tran, 
				c.warehouse_id as wh_item,
				w.sap_code as sap_code,
				d.item_qty,
				d.free_item_id,
				d.free_item_qty,
				a.qty as GetFree
				FROM pinjaman_nc a 
				INNER JOIN pinjaman_d b ON b.id = a.pinjaman_d_id
				INNER JOIN item c ON c.id = b.item_id
				INNER JOIN promo d ON d.id = a.promo_id 
				INNER JOIN warehouse w ON w.id = c.warehouse_id 
				WHERE a.pinjaman_id='".$row['pinjaman_id']."'";
				
				$qrFreeItem=$this->db->query($free_itemSql);
				if ($qrFreeItem){	
					foreach($qrFreeItem->result_array() as $rowFree){
						$fRow=array();
						
						$fRow['VBELN']=$varVBELN;
						$fRow['BSTKD']=$rowFree['pinjaman_id'];
						$fRow['BSTDK']=$row['tgl_sap'];
						$fRow['BNAME']=$row['stockiest_id'];
						$fRow['KDGRP']="U0";
						$fRow['BSTZD']="305";//barat timur
						$fRow['TAXK1']= $varTax;
						$fRow['VKBUR']=$rowFree['Wh_Tran'];
						$fRow['WERKS']=$rowFree['sap_code'];
						$fRow['MATNR']=$rowFree['free_item_id']; // Item Gratisnya
						$fRow['ARKTX']=$rowFree['Free_Item_Name']; // Nama Item Gratisnya..
						$fRow['MATKL']=""; //SAP akan mengambil data dari material master SAP
						$fRow['CHARG']=""; //SAP akan mengambil batch secara FIFO
						$fRow['KWMENG']=$rowFree['GetFree']; // Dpt Gratisnya
						$fRow['KWERT']="0"; //COGS Nilai Sebelum PPN cond type ZUCS -- ini bisa bikin kisruh.. wkwkwkwk
						$fRow['KWERT']="0"; //HNA Nilai Sebelum PPN cond type ZUH1 -- ini bisa bikin pusing.. hahahhaa
						$fRow['KWERT']="0"; //Disc Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
						$fRow['KWERT']="0"; //Disc Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bisa bikin ribut.. hihihi
						$fRow['KWERT']="0"; //Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC -- jangan2 bisa bikin resign.. huhuhu
						$fRow['YVCODE']="0"; 
						$fRow['PSTYV']="0"; //ZUF1 Disc Voucher Code (IDR) Nilai Sebelum PPN ZUVC
						$fRow['KWERT']=$row['totalharga']; //Net Value Nilai Sebelum PPN cond type ZUNT -- ini juga bisa bikin kisruh.. wkwkwkwk
						$fRow['KWERT']="0"; //PPN Nilai Sebelum PPN cond type ZUTX -- ini yg bikin pusing.. hahahhaa
						$fRow['KWERT']=$row['totalpv'];  //PV Promo (IDR) Nilai Sebelum PPN cond type ZUA1 -- ini jg bikin ribet.. hehehe
						$fRow['KWERT']="";  //BV Stockist Fee (IDR) Nilai Sebelum PPN ZUA2 -- ini jg bikin ribut.. hihihi
						$fRow['YBEND']=""; //Banded Material..
						
						$fRow['AUGRU']=$varCancelSo;
						$fRow['YREF']= 'Free Item From Promo id : '.$rowFree['promo_id'].' - '.$row['hremark'].' '.$varCancelScRemark;
						$data[] = $fRow;
					}
				}
				
				
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return $data;


	}
	

	public function test_json(){
		$data=array();
		
		/*
		VBELN
		BSTKD
		BSTDK
		BNAME
		KDGRP
		BSTZD
		TAXK1
		VKBUR
		WERKS
		MATNR
		ARKTX
		MATKL-
		CHARG
		KWMENG
		KWERT
		KWERT
		KWERT
		KWERT
		KWERT
		YVCODE
		PSTYV
		KWERT
		KWERT
		KWERT
		KWERT
		
		YBEND
		AUGRU
		YREF
		*/
		
		$cRow=array();
		$cRow['VBELN']="ZU01";
		$cRow['BSTKD']="4901906158";
		$cRow['BSTDK']="2018";
		$cRow['BNAME']="whs to whs";
		$cRow['KDGRP']="1";
		$cRow['BSTZD']="305";
		$cRow['TAXK1']= "S";
		$cRow['VKBUR']="1602";
		$cRow['WERKS']="UHN1";
		$cRow['MATNR']="BYI0017";
		$cRow['ARKTX']="Astaderm Set";
		$cRow['MATKL']="2";
		$cRow['CHARG']="CBFYC";
		$cRow['KWMENG']="30.06.2020";
		$cRow['KWERT']="10000";
		$cRow['KWERT']="10000";
		$cRow['KWERT']="10000";
		$cRow['KWERT']="10000";
		$cRow['YBEND']=" -   ";
		$cRow['AUGRU']="22.11.2018";
		$cRow['YREF']= "SIP_WHS13";
		$data[] = $cRow;
		
		return $data;
	}
 	
	
    
}
?>