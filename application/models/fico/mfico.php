<?php

class MFico extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | API view balance ewallet
    |--------------------------------------------------------------------------
    |
    | @author Budi Prayudi
    | @created 2019-02-20
    |
    */
    
    public function getBalanceEwallet($memberid,$fromdate,$todate){
		/* Untuk Stockiest */
        $data=array();
		
		$cSql="
		SELECT s.id, s.ewallet_stc_id AS doc_head, 
		m.nama AS name_cus, s.noref, s.description, s.created, 
		DATE_FORMAT(s.created, '%d.%m.%Y') AS tgl, s.member_id, 
		m.kota_id, acc.bank_id AS bank_id, k.warehouse_id, w.sap_code, 
		s.createdby, FORMAT(s.saldoawal, 0)AS fsaldoawal, FORMAT(s.kredit, 0)AS fkredit, 
		FORMAT(s.debet, 0)AS fdebet, FORMAT(s.saldoakhir, 0)AS fsaldoakhir
		FROM `ewallet_stc_d` `s` 
		LEFT JOIN `member` `m` ON `m`.`id` = `s`.`member_id`
		LEFT JOIN `account` `acc` ON `acc`.`id` = `m`.`account_id`
		LEFT JOIN `kota` `k` ON `k`.`id` = `m`.`kota_id`
		LEFT JOIN `warehouse` `w` ON `w`.`id` = `k`.`warehouse_id`
		WHERE `s`.`member_id` <> 'company'
		AND `s`.`created` BETWEEN '".$fromdate."' AND '".$todate."'  
		ORDER BY `s`.`id` ASC
		";
		
		
		$query=$this->db->query($cSql);
		if ($query){	
            foreach($query->result_array() as $row){
                $cat_cust='STOCKIST';
				$cat_doc='Error';
				
				$cekWithdrawal="SELECT withdrawal.remark, 
				withdrawal.remarkapp, withdrawal.flag,withdrawal.event_id  
				FROM withdrawal WHERE id='".$row['noref']."'
				AND status='approved' ";
				$qrWithdrawal=$this->db->query($cekWithdrawal);
				if ($qrWithdrawal){
					foreach($qrWithdrawal->result_array() as $rowWth){
						//$cat_cust=$cat_cust.''.$rowWth['remark'].'-'.$rowWth['remarkapp'].'-'.$rowWth['event_id'];
						//$row['description']=$row['description'].' '.$rowWth['remark'].'-'.$rowWth['remarkapp']; -- Kalau pakai deskripsi dari ewallet_d
						$row['description']=$rowWth['remark'].'-'.$rowWth['remarkapp'];
						//$row['name_cus']=$row['name_cus'].''.substr_count($row['description'], 'Top Up');
					}
				}else{
					$row['description']="Undefined";
				}
				
				// Test Logical Buat Menentukan Kategori.. berdasarkan kata yang menyerupai..
				if (strpos($row['description'], 'Top Up') !== false) { $cat_doc='A1'; }
				else if (strpos($row['description'], 'Transfer') !== false) { $cat_doc='A2'; }
				else if (strpos($row['description'], 'Withdrawal koreksi') !== false) { $cat_doc='A3'; }
				else if (strpos($row['description'], 'User request') !== false) { $cat_doc='A4'; }
				else if (strpos($row['description'], 'Ongkos Kirim') !== false) { $cat_doc='A5'; }
				else if (strpos($row['description'], 'Biaya Admin') !== false) { $cat_doc='A6'; }
				else if (strpos($row['description'], 'Bonus') !== false) { $cat_doc='A7'; }
				else if (strpos($row['description'], 'Withdrawal Bonus') !== false) { $cat_doc='A8'; }
				else if (strpos($row['description'], 'Withdrawal Other') !== false) { $cat_doc='A9'; }
				else if (strpos($row['description'], 'Lain-lain') !== false) { $cat_doc='A9'; }
				else if (strpos($row['description'], 'Withdrawal Lainnya') !== false) { $cat_doc='A9'; }
				else { $cat_doc='A2'; } 
				$pay_meth='Transfer Bank';
				
				$cre_deb='CREDIT';
				// DEBET Khusus Untuk A2 dan A3
				if (($cat_doc=='A2') OR ($cat_doc=='A3') ){
					$cre_deb='DEBIT';
				}
				
				$pph21=0;
				$web_dmbtr=0;
				if (empty($row['fdebet'])){
					$web_dmbtr= str_replace(",","",$row['fkredit']);
					if ($cat_doc='A7'){
						// Bonus Title 999=pph21 
						$bonPPH="SELECT bonus.periode, bonus.member_id, bonus.title, bonus.nominal FROM bonus WHERE bonus.title='999' AND bonus.periode 
						BETWEEN '".$fromdate."' AND '".$todate."' AND bonus.member_id='".$row['member_id']."'";
						$qrPph=$this->db->query($bonPPH);
						foreach($qrPph->result_array() as $rowPph){
							$pph21=$rowPph['nominal'];
						}
						//$pph21=10/100*$web_dmbtr; // Total dbmtr * 10%
					}
					
				}else{
					$pph21=0;
					$web_dmbtr= str_replace(",","",$row['fdebet']);
				}
				
				
				
				if ($cat_doc<>"Error"){
					$cRow=array();
					$cRow['doc_head']=$row['doc_head'];
					$cRow['doc_det']=$row['id'];
					$cRow['cat_cust']=$cat_cust;
					$cRow['doc_date']=$row['tgl'];
					$cRow['doc_post']=$row['tgl'];
					$cRow['kunnr']=$row['member_id'];
					$cRow['name_cus']=$row['name_cus'];
					$cRow['cat_doc']=$cat_doc;
					$cRow['WERKS']=$row['sap_code'];
					$cRow['bank1']=$row['bank_id'];
					$cRow['pay_meth']=$row['id'];
					$cRow['cre_deb']=$cre_deb;
					$cRow['pph21']=$pph21;
					$cRow['web_dmbtr']=$web_dmbtr;
					$cRow['remark']=$cat_doc.' '.$row['description'];
					$cRow['web_CPUDT']=date("d.m.Y");
					$cRow['web_CPUTM']=date("H:i:s");
					$data[] = $cRow;
				}
                
            }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return $data;
		
    }
	
	
	
    public function getBalanceEwallet_member($memberid,$fromdate,$todate){
		/* Untuk Member dan staff (karena satu tabel)*/
        $data=array();
		
		$cSql="
		SELECT s.id, s.ewallet_mem_id AS doc_head, 
		m.nama AS name_cus, s.noref, s.description, s.created, 
		DATE_FORMAT(s.created, '%d.%m.%Y') AS tgl, s.member_id, 
		m.kota_id, acc.bank_id AS bank_id, k.warehouse_id, w.sap_code, 
		s.createdby, FORMAT(s.saldoawal, 0)AS fsaldoawal, FORMAT(s.kredit, 0)AS fkredit, 
		FORMAT(s.debet, 0)AS fdebet, FORMAT(s.saldoakhir, 0)AS fsaldoakhir
		FROM `ewallet_mem_d` `s` 
		LEFT JOIN `member` `m` ON `m`.`id` = `s`.`member_id`
		LEFT JOIN `account` `acc` ON `acc`.`id` = `m`.`account_id`
		LEFT JOIN `kota` `k` ON `k`.`id` = `m`.`kota_id`
		LEFT JOIN `warehouse` `w` ON `w`.`id` = `k`.`warehouse_id`
		WHERE `s`.`member_id` <> 'company'
		AND `s`.`created` BETWEEN '".$fromdate."' AND '".$todate."'  
		ORDER BY `s`.`id` ASC
		";
		
		
		$query=$this->db->query($cSql);
		if ($query){	
            foreach($query->result_array() as $row){
                $cat_cust='MEMBER';
				//CEK Jika STAFF
				if ($row['member_id']=='STAFF'){
					$cat_cust='STAFF';
				}
				
				$cat_doc='Error';
				
				$cekWithdrawal="SELECT withdrawal.remark, 
				withdrawal.remarkapp, withdrawal.flag,withdrawal.event_id  
				FROM withdrawal WHERE id='".$row['noref']."'
				AND status='approved' ";
				$qrWithdrawal=$this->db->query($cekWithdrawal);
				if ($qrWithdrawal){
					foreach($qrWithdrawal->result_array() as $rowWth){
						//$cat_cust=$cat_cust.''.$rowWth['remark'].'-'.$rowWth['remarkapp'].'-'.$rowWth['event_id'];
						//$row['description']=$row['description'].' '.$rowWth['remark'].'-'.$rowWth['remarkapp']; -- Kalau pakai deskripsi dari ewallet_d
						$row['description']=$rowWth['remark'].'-'.$rowWth['remarkapp'];
						//$row['name_cus']=$row['name_cus'].''.substr_count($row['description'], 'Top Up');
					}
				}else{
					$cat_cust="Undefined";
				}
				
				
				// Test Logical Buat Menentukan Kategori.. 
				if (strpos($row['description'], 'Top Up') !== false) { $cat_doc='A1'; }
				else if (strpos($row['description'], 'Transfer') !== false) { $cat_doc='A2'; }
				else if (strpos($row['description'], 'Withdrawal koreksi') !== false) { $cat_doc='A3'; }
				else if (strpos($row['description'], 'User request') !== false) { $cat_doc='A4'; }
				else if (strpos($row['description'], 'Ongkos Kirim') !== false) { $cat_doc='A5'; }
				else if (strpos($row['description'], 'Biaya Admin') !== false) { $cat_doc='A6'; }
				else if (strpos($row['description'], 'Bonus') !== false) { $cat_doc='A7'; }
				else if (strpos($row['description'], 'Withdrawal Bonus') !== false) { $cat_doc='A8'; }
				else if (strpos($row['description'], 'Withdrawal Other') !== false) { $cat_doc='A9'; }
				else if (strpos($row['description'], 'Lain-lain') !== false) { $cat_doc='A9'; }
				else if (strpos($row['description'], 'Withdrawal Lainnya') !== false) { $cat_doc='A9'; }
				else { $cat_doc='A2'; } 
				$pay_meth='Transfer Bank';
				
				$cre_deb='CREDIT';
				// DEBET Khusus Untuk A2 dan A3
				if (($cat_doc=='A2') OR ($cat_doc=='A3') ){
					$cre_deb='DEBIT';
				}
				
				
				$pph21=0;
				$web_dmbtr=0;
				if (empty($row['fdebet'])){
					$web_dmbtr= str_replace(",","",$row['fkredit']);
					
					$web_dmbtr= str_replace(",","",$row['fkredit']);
					if ($cat_doc='A7'){
						// Bonus Title 999=pph21 
						$bonPPH="SELECT bonus.periode, bonus.member_id, bonus.title, bonus.nominal FROM bonus WHERE bonus.title='999' AND bonus.periode 
						BETWEEN '".$fromdate."' AND '".$todate."' AND bonus.member_id='".$row['member_id']."'";
						$qrPph=$this->db->query($bonPPH);
						foreach($qrPph->result_array() as $rowPph){
							$pph21=$rowPph['nominal'];
						}
						//$pph21=10/100*$web_dmbtr; // Total dbmtr * 10%
					}
					
				}else{
					$pph21=0;
					$web_dmbtr= str_replace(",","",$row['fdebet']);
				}
				
				
				if (strpos($row['description'], 'Top Up') !== false) { $cat_doc='A1'; }
				if (strpos($row['description'], 'Transfer') !== false) { $cat_doc='A2'; }
				if (strpos($row['description'], 'Withdrawal koreksi') !== false) { $cat_doc='A3'; }
				if (strpos($row['description'], 'User request') !== false) { $cat_doc='A4'; }
				if (strpos($row['description'], 'Ongkos Kirim') !== false) { $cat_doc='A5'; }
				if (strpos($row['description'], 'Biaya Admin') !== false) { $cat_doc='A6'; }
				if (strpos($row['description'], 'Bonus') !== false) { $cat_doc='A7'; }
				if (strpos($row['description'], 'Withdrawal bonus') !== false) { $cat_doc='A8'; }
				if (strpos($row['description'], 'Withdrawal') !== false) { $cat_doc='A9'; }

				if ($cat_doc<>"Error"){
					$cRow=array();
					$cRow['doc_head']=$row['doc_head'];
					$cRow['doc_det']=$row['id'];
					$cRow['cat_cust']=$cat_cust;
					$cRow['doc_date']=$row['tgl'];
					$cRow['doc_post']=$row['tgl'];
					$cRow['kunnr']=$row['member_id'];
					$cRow['name_cus']=$row['name_cus'];
					$cRow['cat_doc']=$cat_doc;
					$cRow['WERKS']=$row['sap_code'];
					$cRow['bank1']=$row['bank_id'];
					$cRow['pay_meth']=$row['id'];
					$cRow['cre_deb']=$cre_deb;
					$cRow['pph21']=$pph21;
					$cRow['web_dmbtr']=$web_dmbtr;
					$cRow['remark']=$cat_doc.' '.$row['description'];
					$cRow['web_CPUDT']=date("d.m.Y");
					$cRow['web_CPUTM']=date("H:i:s");
					$data[] = $cRow;
				}
                
            }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
				
		return $data;
	}
	
	
	public function writeLogApi($data_log_api){
		$this->db->insert('log_api',$data_log_api);
	}
	
	
	
    
}
?>