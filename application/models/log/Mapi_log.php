<?php

class Mapi_log extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | API view balance ewallet
    |--------------------------------------------------------------------------
    |
    | @author Budi Prayudi
    | @created 2019-02-20
    |
    */
    
	public function writeLogApi($data_log_api){
		/*
		Variabelnya  Pindah Ke Controller ajah.... biar bisa masukin data yang beda..
		$data_log_api=array(
			'log_api_tgl' => date("Y-m-d H:i:s"),
			'log_api_ket' => $deskripsi,
			'log_api_kategori' => $kategori, 
			'log_api_modul' => $modul, 
			'log_api_url' => $varUrl, 
			'log_api_status' => $varStatus 
		);
		*/
		$this->db->insert('log_api',$data_log_api);
	}
	
	
	public function getApi_log($fromdate,$todate){
		$data=array();
		$cSql=" SELECT * FROM log_api WHERE  log_api_tgl BETWEEN '".$fromdate."' AND '".$todate."' ORDER BY log_api_id DESC";
		
		$query=$this->db->query($cSql);
		if ($query){	
			$cRow=array();
			$cRow['api_id']=$row['log_api_id'];
			$cRow['api_tgl']=$row['log_api_tgl'];
			$cRow['api_ket']=$row['log_api_ket'];
			$cRow['api_kategori']=$row['log_api_kategori'];
			$cRow['api_modul']=$row['log_api_modul'];
			$cRow['api_url']=$row['log_api_url'];
			$cRow['api_status']=$row['log_api_status'];
			$cRow['api_log_type']=$row['log_api_type'];
			$data[] = $cRow;
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		return $data;
		
    }
	
	/* Syncronisasi SAP */
	public function writeLogSyncApi($dataSync){
		$this->db->insert('log_api',$dataSync);
	}
	
	public function getApiSync_log($fromdate,$todate){
		$data=array();
		$cSql=" SELECT * FROM log_api WHERE  log_api_sync_tgl BETWEEN '".$fromdate."' AND '".$todate."' ORDER BY log_api_sync_id DESC";
		
		$query=$this->db->query($cSql);
		if ($query){	
			$cRow=array();
			$cRow['log_api_sync_id']=$row['log_api_sync_id'];
			$cRow['log_api_sync_tgl']=$row['log_api_sync_tgl'];
			$cRow['log_api_sync_ket']=$row['log_api_sync_ket'];
			$cRow['log_api_sync_kategori']=$row['log_api_sync_kategori'];
			$cRow['log_api_sync_modul']=$row['log_api_sync_modul'];
			$cRow['log_api_sync_url']=$row['log_api_sync_url'];
			$cRow['log_api_sync_status']=$row['log_api_sync_status'];
			$cRow['log_api_sync_type']=$row['log_api_sync_type'];
			$data[] = $cRow;
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		return $data;
		
    }
	
	
	
	
    
}
?>
