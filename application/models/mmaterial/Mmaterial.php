<?php
class Mmaterial extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | API view balance ewallet
    |--------------------------------------------------------------------------
    |
    | @author Budi Prayudi
    | @created 2019-03-06
    |
    */
    
    public function getNcm($fromdate,$todate){
		$data=array();
		$cSql = "
				SELECT
				i.purchasing_group AS EKGRP
				,dt.ncm_id AS YMBLNR_UHN
				,dt.ncm_d_id AS YZEILE_UHN
				,dt.jam AS CPUTM_MKPF
				,YEAR(dt.date) AS YMJAHR_UHN
				,'201' AS BWART
				,CASE WHEN dt.warehouse_id = 2 THEN '1601' ELSE w.sap_code END AS WERKS
				,CASE WHEN dt.warehouse_id = 2 THEN 'UHN2' ELSE 'UHN1' END AS LGORT
				,dt.itemo_id AS MATNR
				,dt.qty AS ERFMG
				,'ST' AS ERFME
				,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN 'BATCH' ELSE 'BATCH' END AS CHARG
				,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN '31.12.9999' ELSE '31.12.9999' END AS VFDAT
				,'1601112' AS KOSTL -- cost center
				,1601 AS PRCTR
				,dt.date AS BUDAT_MKPF
				,dt.createdby AS USNAM_MKPF
				,'01' AS YRETURN_IND
				,'IDR' AS WAERS
				,CASE WHEN dt.hargao > 0 THEN dt.hargao ELSE 1 END AS DMBTR
				,'620209' AS SAKTO -- G/L Account
				FROM (
					SELECT 
					nc.id AS ncm_id
					,ncd.id AS ncm_d_id
					,nc.warehouse_id 
					,nc.date
					,DATE_FORMAT(nc.created,'%H%i%s') as jam
					,nc.createdby
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ncd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp ELSE ncd.hpp END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*ncd.qty ELSE ncd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp*rp.qty*ncd.qty ELSE ncd.qty*ncd.hpp END AS total
					FROM ncm_d ncd
					LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
					LEFT JOIN manufaktur rp ON ncd.item_id = rp.manufaktur_id
					WHERE nc.`date` BETWEEN '".$fromdate."' AND '".$todate."'
					AND (remark NOT LIKE 'Free Of RO%'
					AND remark NOT LIKE 'CFRO no.%' AND remark NOT LIKE 'CFSO no.%' AND remark NOT LIKE 'Free Of SO%')
				) AS dt 
				LEFT JOIN item i ON i.id = dt.itemo_id
				LEFT JOIN warehouse w ON w.id = dt.warehouse_id
		";

		$query=$this->db->query($cSql);

		if ($query){
				
			 $x =1;
			 $currid = 0;
			 foreach($query->result_array() as $row){
				$cRow=array();

				if($currid ==0) $currid  = $row['YMBLNR_UHN'];
				if($currid != $row['YMBLNR_UHN']){
					$currid  = $row['YMBLNR_UHN'];
					$x = 1;
				}
				
				$cRow['EKGRP']=$row['EKGRP'];
				$cRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$cRow['YZEILE_UHN']=$x;
				$cRow['YID_UHN']=$row['YZEILE_UHN'];
				$cRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$cRow['BWART']=$row['BWART'];
				$cRow['WERKS']=$row['WERKS'];;
				$cRow['LGORT']=$row['LGORT'];;
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ERFMG']=$row['ERFMG'];
				$cRow['ERFME']=$row['ERFME'];
				$cRow['CHARG']=$row['CHARG'];
				$cRow['VFDAT']=$row['VFDAT'];
				$cRow['KOSTL']=$row['KOSTL'];
				$cRow['PRCTR']=$row['PRCTR'];
				$cRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"d.m.Y");
				$cRow['CPUTM_MKPF'] =  	$row['CPUTM_MKPF'];
				$cRow['USNAM_MKPF'] =	$row['USNAM_MKPF'];
				$cRow['YRETURN_IND']=$row['YRETURN_IND'];
				$cRow['WAERS']=$row['WAERS'];
				$cRow['DMBTR']=$row['DMBTR'];
				$cRow['SAKTO']=$row['SAKTO'];

				$tRow['EKGRP']=$row['EKGRP'];
				$tRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$tRow['YZEILE_UHN']=$x;
				$tRow['YID_UHN']=$row['YZEILE_UHN'];
				$tRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$tRow['BWART']=$row['BWART'];
				$tRow['WERKS']=$row['WERKS'];
				$tRow['LGORT']=$row['LGORT'];
				$tRow['MATNR']=$row['MATNR'];
				$tRow['ERFMG']=$row['ERFMG'];
				$tRow['ERFME']=$row['ERFME'];
				$tRow['CHARG']=$row['CHARG'];
				$tRow['VFDAT']=$row['VFDAT'];
				$tRow['KOSTL']=$row['KOSTL'];
				$tRow['PRCTR']=$row['PRCTR'];
				$tRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"Y-m-d");
				$tRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$tRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$tRow['YRETURN_IND']=$row['YRETURN_IND'];
				$tRow['WAERS']=$row['WAERS'];
				$tRow['DMBTR']=$row['DMBTR'];
				$tRow['STATUS']='Failed Send Data';
				$tRow['SAKTO']=$row['SAKTO'];
				$tRow['ACT'] = 'NCM';

				$this->db->where('YID_UHN', $row['YZEILE_UHN']);
				$this->db->where('ACT','NCM');
				$q = $this->db->get('temp_materialncm');
			
				if ( $q->num_rows() > 0 ) 
				{
					
					$this->db->where('YID_UHN', $row['YZEILE_UHN']);
					$this->db->where('ACT','NCM');
					$this->db->delete('temp_materialncm');

					$this->db->insert('temp_materialncm', $tRow);

				} else {

					$this->db->insert('temp_materialncm', $tRow);

				}


				$x++;
			
				$data[] = $cRow;
			 }

		}else{

			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		
		}

		return $data;

	}

    public function update_data_ncm($fromdate,$todate){
		$data=array();
		$cSql = "
				SELECT
				i.purchasing_group AS EKGRP
				,dt.ncm_id AS YMBLNR_UHN
				,dt.ncm_d_id AS YZEILE_UHN
				,dt.jam AS CPUTM_MKPF
				,YEAR(dt.date) AS YMJAHR_UHN
				,'201' AS BWART
				,CASE WHEN dt.warehouse_id = 2 THEN '1601' ELSE w.sap_code END AS WERKS
				,CASE WHEN dt.warehouse_id = 2 THEN 'UHN2' ELSE 'UHN1' END AS LGORT
				,dt.itemo_id AS MATNR
				,dt.qty AS ERFMG
				,'ST' AS ERFME
				,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN 'BATCH' ELSE 'BATCH' END AS CHARG
				,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN '31.12.9999' ELSE '31.12.9999' END AS VFDAT
				,'1601112' AS KOSTL -- cost center
				,1601 AS PRCTR
				,dt.date AS BUDAT_MKPF
				,dt.createdby AS USNAM_MKPF
				,'01' AS YRETURN_IND
				,'IDR' AS WAERS
				,CASE WHEN dt.hargao > 0 THEN dt.hargao ELSE 1 END AS DMBTR
				,'620209' AS SAKTO -- G/L Account
				FROM (
					SELECT 
					nc.id AS ncm_id
					,ncd.id AS ncm_d_id
					,nc.warehouse_id 
					,nc.date
					,DATE_FORMAT(nc.created,'%H%i%s') as jam
					,nc.createdby
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ncd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp ELSE ncd.hpp END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*ncd.qty ELSE ncd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp*rp.qty*ncd.qty ELSE ncd.qty*ncd.hpp END AS total
					FROM ncm_d ncd
					LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
					LEFT JOIN manufaktur rp ON ncd.item_id = rp.manufaktur_id
					WHERE nc.`date` BETWEEN '".$fromdate."' AND '".$todate."'
					AND (remark NOT LIKE 'Free Of RO%'
					AND remark NOT LIKE 'CFRO no.%' AND remark NOT LIKE 'CFSO no.%')
				) AS dt 
				LEFT JOIN item i ON i.id = dt.itemo_id
				LEFT JOIN warehouse w ON w.id = dt.warehouse_id
		";

		$query=$this->db->query($cSql);

		if ($query){
				
			 $x =1;
			 $currid = 0;
			 foreach($query->result_array() as $row){
				$cRow=array();

				if($currid ==0) $currid  = $row['YMBLNR_UHN'];
				if($currid != $row['YMBLNR_UHN']){
					$currid  = $row['YMBLNR_UHN'];
					$x = 1;
				}
				
				$cRow['EKGRP']=$row['EKGRP'];
				$cRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$cRow['YZEILE_UHN']=$x;
				$cRow['YID_UHN']=$row['YZEILE_UHN'];
				$cRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$cRow['BWART']=$row['BWART'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['LGORT']=$row['LGORT'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ERFMG']=$row['ERFMG'];
				$cRow['ERFME']=$row['ERFME'];
				$cRow['CHARG']=$row['CHARG'];
				$cRow['VFDAT']=$row['VFDAT'];
				$cRow['KOSTL']=$row['KOSTL'];
				$cRow['PRCTR']=$row['PRCTR'];
				$cRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"d.m.Y");
				$cRow['CPUTM_MKPF'] =  	$row['CPUTM_MKPF'];
				$cRow['USNAM_MKPF'] =	$row['USNAM_MKPF'];
				$cRow['YRETURN_IND']=$row['YRETURN_IND'];
				$cRow['WAERS']=$row['WAERS'];
				$cRow['DMBTR']=$row['DMBTR'];
				$cRow['SAKTO']=$row['SAKTO'];

				$tRow['EKGRP']=$row['EKGRP'];
				$tRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$tRow['YZEILE_UHN']=$x;
				$tRow['YID_UHN']=$row['YZEILE_UHN'];
				$tRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$tRow['BWART']=$row['BWART'];
				$tRow['WERKS']=$row['WERKS'];
				$tRow['LGORT']=$row['LGORT'];
				$tRow['MATNR']=$row['MATNR'];
				$tRow['ERFMG']=$row['ERFMG'];
				$tRow['ERFME']=$row['ERFME'];
				$tRow['CHARG']=$row['CHARG'];
				$tRow['VFDAT']=$row['VFDAT'];
				$tRow['KOSTL']=$row['KOSTL'];
				$tRow['PRCTR']=$row['PRCTR'];
				$tRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"Y-m-d");
				$tRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$tRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$tRow['YRETURN_IND']=$row['YRETURN_IND'];
				$tRow['WAERS']=$row['WAERS'];
				$tRow['DMBTR']=$row['DMBTR'];
				$tRow['STATUS']='Failed Send Data';
				$tRow['SAKTO']=$row['SAKTO'];
				$tRow['ACT'] = 'NCM';

				$data=array(
					'STATUS' => 'Success Send Data'
				);
					
				$this->db->update('temp_materialncm',$data,array('YID_UHN'=>$row['YZEILE_UHN'], 'ACT'=>'NCM'));


				$x++;
			
				$data[] = $cRow;
			}

		}

		return true;

	}

	public function get_data_ncm_by_array_id($array_id){
		$data=array();
		$cSql = "
				SELECT
				i.purchasing_group AS EKGRP
				,dt.ncm_id AS YMBLNR_UHN
				,dt.ncm_d_id AS YZEILE_UHN
				,dt.jam AS CPUTM_MKPF
				,YEAR(dt.date) AS YMJAHR_UHN
				,'201' AS BWART
				,CASE WHEN dt.warehouse_id = 2 THEN '1601' ELSE w.sap_code END AS WERKS
				,CASE WHEN dt.warehouse_id = 2 THEN 'UHN2' ELSE 'UHN1' END AS LGORT
				,dt.itemo_id AS MATNR
				,dt.qty AS ERFMG
				,'ST' AS ERFME
				,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN 'BATCH' ELSE 'BATCH' END AS CHARG
				,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN '31.12.9999' ELSE '31.12.9999' END AS VFDAT
				,'1601112' AS KOSTL
				,1601 AS PRCTR
				,dt.date AS BUDAT_MKPF
				,dt.createdby AS USNAM_MKPF
				,'01' AS YRETURN_IND
				,'IDR' AS WAERS
				,CASE WHEN dt.hargao > 0 THEN dt.hargao ELSE 1 END AS DMBTR
				,'620209' AS SAKTO -- G/L Account
				FROM (
					SELECT 
					nc.id AS ncm_id
					,ncd.id AS ncm_d_id
					,nc.warehouse_id 
					,nc.date
					,DATE_FORMAT(nc.created,'%H%i%s') as jam
					,nc.createdby
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ncd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp ELSE ncd.hpp END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*ncd.qty ELSE ncd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp*rp.qty*ncd.qty ELSE ncd.qty*ncd.hpp END AS total
					FROM ncm_d ncd
					LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
					LEFT JOIN manufaktur rp ON ncd.item_id = rp.manufaktur_id
					WHERE nc.id IN (".$array_id.")
					AND (remark NOT LIKE 'Free Of RO%'
					AND remark NOT LIKE 'CFRO no.%' AND remark NOT LIKE 'CFSO no.%')
				) AS dt 
				LEFT JOIN item i ON i.id = dt.itemo_id
				LEFT JOIN warehouse w ON w.id = dt.warehouse_id
		";

		$query=$this->db->query($cSql);

		//print_r($this->db->last_query()); die();

		if ($query){
				
			 $x =1;
			 $currid = 0;
			 foreach($query->result_array() as $row){
				$cRow=array();

				if($currid ==0) $currid  = $row['YMBLNR_UHN'];
				if($currid != $row['YMBLNR_UHN']){
					$currid  = $row['YMBLNR_UHN'];
					$x = 1;
				}
				
				$cRow['EKGRP']=$row['EKGRP'];
				$cRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$cRow['YZEILE_UHN']=$x;
				$cRow['YID_UHN']=$row['YZEILE_UHN'];
				$cRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$cRow['BWART']=$row['BWART'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['LGORT']=$row['LGORT'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ERFMG']=$row['ERFMG'];
				$cRow['ERFME']=$row['ERFME'];
				$cRow['CHARG']=$row['CHARG'];
				$cRow['VFDAT']=$row['VFDAT'];
				$cRow['KOSTL']=$row['KOSTL'];
				$cRow['PRCTR']=$row['PRCTR'];
				$cRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"d.m.Y");
				$cRow['CPUTM_MKPF'] =  $row['CPUTM_MKPF'];
				$cRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$cRow['YRETURN_IND']=$row['YRETURN_IND'];
				$cRow['WAERS']=$row['WAERS'];
				$cRow['DMBTR']=$row['DMBTR'];
				$cRow['SAKTO']=$row['SAKTO'];
				$tRow['EKGRP']=$row['EKGRP'];
				$tRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$tRow['YZEILE_UHN']=$x;
				$tRow['YID_UHN']=$row['YZEILE_UHN'];
				$tRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$tRow['BWART']=$row['BWART'];
				$tRow['WERKS']=$row['WERKS'];
				$tRow['LGORT']=$row['LGORT'];
				$tRow['MATNR']=$row['MATNR'];
				$tRow['ERFMG']=$row['ERFMG'];
				$tRow['ERFME']=$row['ERFME'];
				$tRow['CHARG']=$row['CHARG'];
				$tRow['VFDAT']=$row['VFDAT'];
				$tRow['KOSTL']=$row['KOSTL'];
				$tRow['PRCTR']=$row['PRCTR'];
				$tRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"Y-m-d");
				$tRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$tRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$tRow['YRETURN_IND']=$row['YRETURN_IND'];
				$tRow['WAERS']=$row['WAERS'];
				$tRow['DMBTR']=$row['DMBTR'];
				$tRow['STATUS']='Failed Send Data';
				$tRow['SAKTO']=$row['SAKTO'];
				$tRow['ACT'] = 'NCM';

				$x++;
			
				$data[] = $cRow;
			 }

		}else{

			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		
		}

		return $data;

	}
	
	public function update_data_ncm_by_array_id($array_id){
		$data=array();
		$cSql = "
				SELECT
				i.purchasing_group AS EKGRP
				,dt.ncm_id AS YMBLNR_UHN
				,dt.ncm_d_id AS YZEILE_UHN
				,dt.jam AS CPUTM_MKPF
				,YEAR(dt.date) AS YMJAHR_UHN
				,'201' AS BWART
				,CASE WHEN dt.warehouse_id = 2 THEN '1601' ELSE w.sap_code END AS WERKS
				,CASE WHEN dt.warehouse_id = 2 THEN 'UHN2' ELSE 'UHN1' END AS LGORT
				,dt.itemo_id AS MATNR
				,dt.qty AS ERFMG
				,'ST' AS ERFME
				,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN 'BATCH' ELSE 'BATCH' END AS CHARG
				,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN '31.12.9999' ELSE '31.12.9999' END AS VFDAT
				,'1601112' AS KOSTL
				,1601 AS PRCTR
				,dt.date AS BUDAT_MKPF
				,dt.createdby AS USNAM_MKPF
				,'01' AS YRETURN_IND
				,'IDR' AS WAERS
				,CASE WHEN dt.hargao > 0 THEN dt.hargao ELSE 1 END AS DMBTR
				,'620209' AS SAKTO -- G/L Account
				FROM (
					SELECT 
					nc.id AS ncm_id
					,ncd.id AS ncm_d_id
					,nc.warehouse_id 
					,nc.date
					,DATE_FORMAT(nc.created,'%H%i%s') as jam
					,nc.createdby
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ncd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp ELSE ncd.hpp END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*ncd.qty ELSE ncd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp*rp.qty*ncd.qty ELSE ncd.qty*ncd.hpp END AS total
					FROM ncm_d ncd
					LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
					LEFT JOIN manufaktur rp ON ncd.item_id = rp.manufaktur_id
					WHERE nc.id IN (".$array_id.")
					AND (remark NOT LIKE 'Free Of RO%'
					AND remark NOT LIKE 'CFRO no.%' AND remark NOT LIKE 'CFSO no.%')
				) AS dt 
				LEFT JOIN item i ON i.id = dt.itemo_id
				LEFT JOIN warehouse w ON w.id = dt.warehouse_id
		";

		$query=$this->db->query($cSql);

		//print_r($this->db->last_query()); die();

		if ($query){
				
			 $x =1;
			 $currid = 0;
			 foreach($query->result_array() as $row){
				$cRow=array();

				if($currid ==0) $currid  = $row['YMBLNR_UHN'];
				if($currid != $row['YMBLNR_UHN']){
					$currid  = $row['YMBLNR_UHN'];
					$x = 1;
				}
				
				$cRow['EKGRP']=$row['EKGRP'];
				$cRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$cRow['YZEILE_UHN']=$x;
				$cRow['YID_UHN']=$row['YZEILE_UHN'];
				$cRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$cRow['BWART']=$row['BWART'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['LGORT']=$row['LGORT'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ERFMG']=$row['ERFMG'];
				$cRow['ERFME']=$row['ERFME'];
				$cRow['CHARG']=$row['CHARG'];
				$cRow['VFDAT']=$row['VFDAT'];
				$cRow['KOSTL']=$row['KOSTL'];
				$cRow['PRCTR']=$row['PRCTR'];
				$cRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"d.m.Y");
				$cRow['CPUTM_MKPF'] =  $row['CPUTM_MKPF'];
				$cRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$cRow['YRETURN_IND']=$row['YRETURN_IND'];
				$cRow['WAERS']=$row['WAERS'];
				$cRow['DMBTR']=$row['DMBTR'];
				$cRow['SAKTO']=$row['SAKTO'];
				$tRow['EKGRP']=$row['EKGRP'];
				$tRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$tRow['YZEILE_UHN']=$x;
				$tRow['YID_UHN']=$row['YZEILE_UHN'];
				$tRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$tRow['BWART']=$row['BWART'];
				$tRow['WERKS']=$row['WERKS'];
				$tRow['LGORT']=$row['LGORT'];
				$tRow['MATNR']=$row['MATNR'];
				$tRow['ERFMG']=$row['ERFMG'];
				$tRow['ERFME']=$row['ERFME'];
				$tRow['CHARG']=$row['CHARG'];
				$tRow['VFDAT']=$row['VFDAT'];
				$tRow['KOSTL']=$row['KOSTL'];
				$tRow['PRCTR']=$row['PRCTR'];
				$tRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"Y-m-d");
				$tRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$tRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$tRow['YRETURN_IND']=$row['YRETURN_IND'];
				$tRow['WAERS']=$row['WAERS'];
				$tRow['DMBTR']=$row['DMBTR'];
				$tRow['STATUS']='Failed Send Data';
				$tRow['SAKTO']=$row['SAKTO'];
				$tRow['ACT'] = 'NCM';

				$data=array(
					'STATUS' => 'Success Send Data'
				);
					
				$this->db->update('temp_materialncm',$data,array('YID_UHN'=>$row['YZEILE_UHN'], 'ACT'=>'NCM'));

				$x++;
			
				$data[] = $cRow;
			 }

		}else{

			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		
		}

		return true;

	}
	
	//7. Material Adjustment Scraping - inventory > adjustment rusak Tabel (adjustment_rusak)
	public function getAdjRusak($fromdate,$todate){
		$data=array();

		$cSql="
			SELECT
			i.purchasing_group AS EKGRP
			,dt.adjustment_id AS YMBLNR_UHN
			,dt.adjustment_d_id AS YZEILE_UHN
			,YEAR(dt.date) AS YMJAHR_UHN
			,'551' AS BWART
			,w.sap_code AS WERKS
			,CASE WHEN dt.warehouse_id = 2 THEN 'UHN2' ELSE 'UHN1' END AS LGORT
			,dt.itemo_id AS MATNR
			,dt.qty AS ERFMG
			,dt.jam AS CPUTM_MKPF
			,'PC' AS ERFME
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN 'BATCH' ELSE 'BATCH' END AS CHARG
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN '31.12.9999' ELSE '31.12.9999' END AS VFDAT
			,'1601112' AS KOSTL -- cost center
			,1601 AS PRCTR
			,'' AS GRUND
			-- ,CONCAT(DAY(dt.date),'.',MONTH(dt.date),'.',YEAR(dt.date)) AS BUDAT_MKPF
			,dt.date AS BUDAT_MKPF
			,dt.createdby AS USNAM_MKPF
			,'01' AS YRETURN_IND
			,'IDR' AS WAERS
			,dt.hargao AS DMBTR
			,'620209' AS SAKTO -- G/L Account
			FROM (
				SELECT 
				a.adjustment_id
				,a.id AS adjustment_d_id
				,b.warehouse_id 
				,b.date
				,b.createdby
				,DATE_FORMAT(b.created,'%H%i%s') as jam
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS itemo_id
				,CASE WHEN rp.item_id IS NOT NULL THEN a.hpp ELSE a.hpp END AS hargao
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*a.qty ELSE a.qty END AS qty
				,CASE WHEN rp.item_id IS NOT NULL THEN a.hpp*rp.qty*a.qty ELSE a.qty*a.hpp END AS total
				FROM adjustment_d a
				LEFT JOIN adjustment b ON a.adjustment_id = b.id
				LEFT JOIN manufaktur rp ON a.item_id = rp.manufaktur_id
				WHERE b.date BETWEEN '".$fromdate."' AND '".$todate."'
				AND b.flag = 'Minus'
			) AS dt 
			LEFT JOIN item i ON i.id = dt.itemo_id
			LEFT JOIN warehouse w ON w.id = dt.warehouse_id
		";
	//	echo $cSql;
		$query=$this->db->query($cSql);
		if ($query){
			 $x =1;
			 $currid = 0;
			 foreach($query->result_array() as $row){
				$cRow=array();
				
				if($currid ==0) $currid  = $row['YMBLNR_UHN'];
				if($currid != $row['YMBLNR_UHN']){
					$currid  = $row['YMBLNR_UHN'];
					$x = 1;
				}
				
				$cRow['EKGRP']=$row['EKGRP'];
				$cRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$cRow['YZEILE_UHN']=$x;
				$cRow['YID_UHN']=$row['YZEILE_UHN'];
				$cRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$cRow['BWART']=$row['BWART'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['LGORT']=$row['LGORT'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ERFMG']=$row['ERFMG'];
				$cRow['ERFME']=$row['ERFME'];
				$cRow['CHARG']=$row['CHARG'];
				$cRow['VFDAT']=$row['VFDAT'];
				$cRow['KOSTL']=$row['KOSTL'];
				$cRow['PRCTR']=$row['PRCTR'];
				$cRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"d.m.Y");
				$cRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$cRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$cRow['YRETURN_IND']=$row['YRETURN_IND'];
				$cRow['WAERS']=$row['WAERS'];
				$cRow['DMBTR']=$row['DMBTR'];
				$cRow['GRUND']=$row['GRUND'];
				$cRow['SAKTO']=$row['SAKTO'];
				
		
				$tRow['EKGRP']=$row['EKGRP'];
				$tRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$tRow['YZEILE_UHN']=$x;
				$tRow['YID_UHN']=$row['YZEILE_UHN'];
				$tRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$tRow['BWART']=$row['BWART'];
				$tRow['WERKS']=$row['WERKS'];
				$tRow['LGORT']=$row['LGORT'];
				$tRow['MATNR']=$row['MATNR'];
				$tRow['ERFMG']=$row['ERFMG'];
				$tRow['ERFME']=$row['ERFME'];
				$tRow['CHARG']=$row['CHARG'];
				$tRow['VFDAT']=$row['VFDAT'];
				$tRow['KOSTL']=$row['KOSTL'];
				$tRow['PRCTR']=$row['PRCTR'];
				$tRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"Y-m-d");
				$tRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$tRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$tRow['YRETURN_IND']=$row['YRETURN_IND'];
				$tRow['WAERS']=$row['WAERS'];
				$tRow['DMBTR']=$row['DMBTR'];
				$tRow['STATUS']='Failed Send Data';
				$tRow['SAKTO']=$row['SAKTO'];
				$tRow['ACT'] = 'ADJ RUSAK';

				$this->db->where('YID_UHN', $row['YZEILE_UHN']);
				$this->db->where('ACT','ADJ RUSAK');
				$q = $this->db->get('temp_materialncm');
			
				if ( $q->num_rows() > 0 ) 
				{
					
					$this->db->where('YID_UHN', $row['YZEILE_UHN']);
					$this->db->where('ACT','ADJ RUSAK');
					$this->db->delete('temp_materialncm');

					$this->db->insert('temp_materialncm', $tRow);

				} else {

					$this->db->insert('temp_materialncm', $tRow);

				}

				$x++;
				
				$data[] = $cRow;
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return $data;
	}
	
	public function update_data_adj_rusak($fromdate,$todate){
		$data=array();

		$cSql="
			SELECT
			i.purchasing_group AS EKGRP
			,dt.adjustment_id AS YMBLNR_UHN
			,dt.adjustment_d_id AS YZEILE_UHN
			,YEAR(dt.date) AS YMJAHR_UHN
			,'551' AS BWART
			,w.sap_code AS WERKS
			,CASE WHEN dt.warehouse_id = 2 THEN 'UHN2' ELSE 'UHN1' END AS LGORT
			,dt.itemo_id AS MATNR
			,dt.qty AS ERFMG
			,dt.jam AS CPUTM_MKPF
			,'PC' AS ERFME
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN 'BATCH' ELSE 'BATCH' END AS CHARG
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN '31.12.9999' ELSE '31.12.9999' END AS VFDAT
			,'1601112' AS KOSTL -- cost center
			,1601 AS PRCTR
			,'' AS GRUND
			-- ,CONCAT(DAY(dt.date),'.',MONTH(dt.date),'.',YEAR(dt.date)) AS BUDAT_MKPF
			,dt.date AS BUDAT_MKPF
			,dt.createdby AS USNAM_MKPF
			,'01' AS YRETURN_IND
			,'IDR' AS WAERS
			,dt.hargao AS DMBTR
			,'620209' AS SAKTO -- G/L Account
			FROM (
				SELECT 
				a.adjustment_id
				,a.id AS adjustment_d_id
				,b.warehouse_id 
				,b.date
				,b.createdby
				,DATE_FORMAT(b.created,'%H%i%s') as jam
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS itemo_id
				,CASE WHEN rp.item_id IS NOT NULL THEN a.hpp ELSE a.hpp END AS hargao
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*a.qty ELSE a.qty END AS qty
				,CASE WHEN rp.item_id IS NOT NULL THEN a.hpp*rp.qty*a.qty ELSE a.qty*a.hpp END AS total
				FROM adjustment_d a
				LEFT JOIN adjustment b ON a.adjustment_id = b.id
				LEFT JOIN manufaktur rp ON a.item_id = rp.manufaktur_id
				WHERE b.date BETWEEN '".$fromdate."' AND '".$todate."'
				AND b.flag = 'Minus'
			) AS dt 
			LEFT JOIN item i ON i.id = dt.itemo_id
			LEFT JOIN warehouse w ON w.id = dt.warehouse_id
		";
		//	echo $cSql;
		$query=$this->db->query($cSql);
		if ($query){
			 $x =1;
			 $currid = 0;
			 foreach($query->result_array() as $row){
				$cRow=array();
				
				if($currid ==0) $currid  = $row['YMBLNR_UHN'];
				if($currid != $row['YMBLNR_UHN']){
					$currid  = $row['YMBLNR_UHN'];
					$x = 1;
				}
				
				$cRow['EKGRP']=$row['EKGRP'];
				$cRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$cRow['YZEILE_UHN']=$x;
				$cRow['YID_UHN']=$row['YZEILE_UHN'];
				$cRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$cRow['BWART']=$row['BWART'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['LGORT']=$row['LGORT'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ERFMG']=$row['ERFMG'];
				$cRow['ERFME']=$row['ERFME'];
				$cRow['CHARG']=$row['CHARG'];
				$cRow['VFDAT']=$row['VFDAT'];
				$cRow['KOSTL']=$row['KOSTL'];
				$cRow['PRCTR']=$row['PRCTR'];
				$cRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"d.m.Y");
				$cRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$cRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$cRow['YRETURN_IND']=$row['YRETURN_IND'];
				$cRow['WAERS']=$row['WAERS'];
				$cRow['DMBTR']=$row['DMBTR'];
				$cRow['GRUND']=$row['GRUND'];
				$cRow['SAKTO']=$row['SAKTO'];
				
		
				$tRow['EKGRP']=$row['EKGRP'];
				$tRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$tRow['YZEILE_UHN']=$x;
				$tRow['YID_UHN']=$row['YZEILE_UHN'];
				$tRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$tRow['BWART']=$row['BWART'];
				$tRow['WERKS']=$row['WERKS'];
				$tRow['LGORT']=$row['LGORT'];
				$tRow['MATNR']=$row['MATNR'];
				$tRow['ERFMG']=$row['ERFMG'];
				$tRow['ERFME']=$row['ERFME'];
				$tRow['CHARG']=$row['CHARG'];
				$tRow['VFDAT']=$row['VFDAT'];
				$tRow['KOSTL']=$row['KOSTL'];
				$tRow['PRCTR']=$row['PRCTR'];
				$tRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"Y-m-d");
				$tRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$tRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$tRow['YRETURN_IND']=$row['YRETURN_IND'];
				$tRow['WAERS']=$row['WAERS'];
				$tRow['DMBTR']=$row['DMBTR'];
				$tRow['STATUS']='Failed Send Data';
				$tRow['SAKTO']=$row['SAKTO'];
				$tRow['ACT'] = 'ADJ RUSAK';

				$data=array(
					'STATUS' => 'Success Send Data'
				);
					
				$this->db->update('temp_materialncm',$data,array('YID_UHN'=>$row['YZEILE_UHN'], 'ACT'=>'ADJ RUSAK'));

				$x++;
				
				$data[] = $cRow;
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return true;
	}
	
	public function get_data_adj_rusak_by_array_id($array_id){
		$data=array();

		$cSql="
			SELECT
			i.purchasing_group AS EKGRP
			,dt.adjustment_id AS YMBLNR_UHN
			,dt.adjustment_d_id AS YZEILE_UHN
			,YEAR(dt.date) AS YMJAHR_UHN
			,'551' AS BWART
			,w.sap_code AS WERKS
			,CASE WHEN dt.warehouse_id = 2 THEN 'UHN2' ELSE 'UHN1' END AS LGORT
			,dt.itemo_id AS MATNR
			,dt.qty AS ERFMG
			,dt.jam AS CPUTM_MKPF
			,'PC' AS ERFME
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN 'BATCH' ELSE 'BATCH' END AS CHARG
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN '31.12.9999' ELSE '31.12.9999' END AS VFDAT
			,'1601112' AS KOSTL -- cost center
			,1601 AS PRCTR
			,'' AS GRUND
			-- ,CONCAT(DAY(dt.date),'.',MONTH(dt.date),'.',YEAR(dt.date)) AS BUDAT_MKPF
			,dt.date AS BUDAT_MKPF
			,dt.createdby AS USNAM_MKPF
			,'01' AS YRETURN_IND
			,'IDR' AS WAERS
			,dt.hargao AS DMBTR
			,'620209' AS SAKTO -- G/L Account
			FROM (
				SELECT 
				a.adjustment_id
				,a.id AS adjustment_d_id
				,b.warehouse_id 
				,b.date
				,b.createdby
				,DATE_FORMAT(b.created,'%H%i%s') as jam
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS itemo_id
				,CASE WHEN rp.item_id IS NOT NULL THEN a.hpp ELSE a.hpp END AS hargao
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*a.qty ELSE a.qty END AS qty
				,CASE WHEN rp.item_id IS NOT NULL THEN a.hpp*rp.qty*a.qty ELSE a.qty*a.hpp END AS total
				FROM adjustment_d a
				LEFT JOIN adjustment b ON a.adjustment_id = b.id
				LEFT JOIN manufaktur rp ON a.item_id = rp.manufaktur_id
				WHERE a.adjustment_id IN (".$array_id.")
				AND b.flag = 'Minus'
			) AS dt 
			LEFT JOIN item i ON i.id = dt.itemo_id
			LEFT JOIN warehouse w ON w.id = dt.warehouse_id
		";
	//	echo $cSql;
		$query=$this->db->query($cSql);
		if ($query){
			 $x =1;
			 $currid = 0;
			 foreach($query->result_array() as $row){
				$cRow=array();
				
				if($currid ==0) $currid  = $row['YMBLNR_UHN'];
				if($currid != $row['YMBLNR_UHN']){
					$currid  = $row['YMBLNR_UHN'];
					$x = 1;
				}
				
				$cRow['EKGRP']=$row['EKGRP'];
				$cRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$cRow['YZEILE_UHN']=$x;
				$cRow['YID_UHN']=$row['YZEILE_UHN'];
				$cRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$cRow['BWART']=$row['BWART'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['LGORT']=$row['LGORT'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ERFMG']=$row['ERFMG'];
				$cRow['ERFME']=$row['ERFME'];
				$cRow['CHARG']=$row['CHARG'];
				$cRow['VFDAT']=$row['VFDAT'];
				$cRow['KOSTL']=$row['KOSTL'];
				$cRow['PRCTR']=$row['PRCTR'];
				$cRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"d.m.Y");
				$cRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$cRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$cRow['YRETURN_IND']=$row['YRETURN_IND'];
				$cRow['WAERS']=$row['WAERS'];
				$cRow['DMBTR']=$row['DMBTR'];
				$cRow['GRUND']=$row['GRUND'];
				$cRow['SAKTO']=$row['SAKTO'];
				
		
				$tRow['EKGRP']=$row['EKGRP'];
				$tRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$tRow['YZEILE_UHN']=$x;
				$tRow['YID_UHN']=$row['YZEILE_UHN'];
				$tRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$tRow['BWART']=$row['BWART'];
				$tRow['WERKS']=$row['WERKS'];
				$tRow['LGORT']=$row['LGORT'];
				$tRow['MATNR']=$row['MATNR'];
				$tRow['ERFMG']=$row['ERFMG'];
				$tRow['ERFME']=$row['ERFME'];
				$tRow['CHARG']=$row['CHARG'];
				$tRow['VFDAT']=$row['VFDAT'];
				$tRow['KOSTL']=$row['KOSTL'];
				$tRow['PRCTR']=$row['PRCTR'];
				$tRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"Y-m-d");
				$tRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$tRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$tRow['YRETURN_IND']=$row['YRETURN_IND'];
				$tRow['WAERS']=$row['WAERS'];
				$tRow['DMBTR']=$row['DMBTR'];
				$tRow['STATUS']='Failed Send Data';
				$tRow['SAKTO']=$row['SAKTO'];
				$tRow['ACT'] = 'ADJ RUSAK';

				$x++;
				
				$data[] = $cRow;
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return $data;
	}
	
	public function update_data_adj_rusak_by_array_id($array_id){
		$data=array();

		$cSql="
			SELECT
			i.purchasing_group AS EKGRP
			,dt.adjustment_id AS YMBLNR_UHN
			,dt.adjustment_d_id AS YZEILE_UHN
			,YEAR(dt.date) AS YMJAHR_UHN
			,'551' AS BWART
			,w.sap_code AS WERKS
			,CASE WHEN dt.warehouse_id = 2 THEN 'UHN2' ELSE 'UHN1' END AS LGORT
			,dt.itemo_id AS MATNR
			,dt.qty AS ERFMG
			,dt.jam AS CPUTM_MKPF
			,'PC' AS ERFME
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN 'BATCH' ELSE 'BATCH' END AS CHARG
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN '31.12.9999' ELSE '31.12.9999' END AS VFDAT
			,'1601112' AS KOSTL -- cost center
			,1601 AS PRCTR
			,'' AS GRUND
			-- ,CONCAT(DAY(dt.date),'.',MONTH(dt.date),'.',YEAR(dt.date)) AS BUDAT_MKPF
			,dt.date AS BUDAT_MKPF
			,dt.createdby AS USNAM_MKPF
			,'01' AS YRETURN_IND
			,'IDR' AS WAERS
			,dt.hargao AS DMBTR
			,'620209' AS SAKTO -- G/L Account
			FROM (
				SELECT 
				a.adjustment_id
				,a.id AS adjustment_d_id
				,b.warehouse_id 
				,b.date
				,b.createdby
				,DATE_FORMAT(b.created,'%H%i%s') as jam
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS itemo_id
				,CASE WHEN rp.item_id IS NOT NULL THEN a.hpp ELSE a.hpp END AS hargao
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*a.qty ELSE a.qty END AS qty
				,CASE WHEN rp.item_id IS NOT NULL THEN a.hpp*rp.qty*a.qty ELSE a.qty*a.hpp END AS total
				FROM adjustment_d a
				LEFT JOIN adjustment b ON a.adjustment_id = b.id
				LEFT JOIN manufaktur rp ON a.item_id = rp.manufaktur_id
				WHERE a.adjustment_id IN (".$array_id.")
				AND b.flag = 'Minus'
			) AS dt 
			LEFT JOIN item i ON i.id = dt.itemo_id
			LEFT JOIN warehouse w ON w.id = dt.warehouse_id
		";
	//	echo $cSql;
		$query=$this->db->query($cSql);
		if ($query){
			 $x =1;
			 $currid = 0;
			 foreach($query->result_array() as $row){
				$cRow=array();
				
				if($currid ==0) $currid  = $row['YMBLNR_UHN'];
				if($currid != $row['YMBLNR_UHN']){
					$currid  = $row['YMBLNR_UHN'];
					$x = 1;
				}
				
				$cRow['EKGRP']=$row['EKGRP'];
				$cRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$cRow['YZEILE_UHN']=$x;
				$cRow['YID_UHN']=$row['YZEILE_UHN'];
				$cRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$cRow['BWART']=$row['BWART'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['LGORT']=$row['LGORT'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ERFMG']=$row['ERFMG'];
				$cRow['ERFME']=$row['ERFME'];
				$cRow['CHARG']=$row['CHARG'];
				$cRow['VFDAT']=$row['VFDAT'];
				$cRow['KOSTL']=$row['KOSTL'];
				$cRow['PRCTR']=$row['PRCTR'];
				$cRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"d.m.Y");
				$cRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$cRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$cRow['YRETURN_IND']=$row['YRETURN_IND'];
				$cRow['WAERS']=$row['WAERS'];
				$cRow['DMBTR']=$row['DMBTR'];
				$cRow['GRUND']=$row['GRUND'];
				$cRow['SAKTO']=$row['SAKTO'];
				
		
				$tRow['EKGRP']=$row['EKGRP'];
				$tRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$tRow['YZEILE_UHN']=$x;
				$tRow['YID_UHN']=$row['YZEILE_UHN'];
				$tRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$tRow['BWART']=$row['BWART'];
				$tRow['WERKS']=$row['WERKS'];
				$tRow['LGORT']=$row['LGORT'];
				$tRow['MATNR']=$row['MATNR'];
				$tRow['ERFMG']=$row['ERFMG'];
				$tRow['ERFME']=$row['ERFME'];
				$tRow['CHARG']=$row['CHARG'];
				$tRow['VFDAT']=$row['VFDAT'];
				$tRow['KOSTL']=$row['KOSTL'];
				$tRow['PRCTR']=$row['PRCTR'];
				$tRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"Y-m-d");
				$tRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$tRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$tRow['YRETURN_IND']=$row['YRETURN_IND'];
				$tRow['WAERS']=$row['WAERS'];
				$tRow['DMBTR']=$row['DMBTR'];
				$tRow['STATUS']='Failed Send Data';
				$tRow['SAKTO']=$row['SAKTO'];
				$tRow['ACT'] = 'ADJ RUSAK';

				$data=array(
					'STATUS' => 'Success Send Data'
				);
					
				$this->db->update('temp_materialncm',$data,array('YID_UHN'=>$row['YZEILE_UHN'], 'ACT'=>'ADJ RUSAK'));

				$x++;
				
				$data[] = $cRow;
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return true;
	}
	
	//8. Material Movement Adjustment GR Without PO - inventory > adjustment stock (Tabel adjustment ,adjusment_d)
	public function getAdjMove($fromdate,$todate){
		$data=array();

		$cSql="
			SELECT
			i.purchasing_group AS EKGRP
			,dt.adjustment_id AS YMBLNR_UHN
			,dt.adjustment_d_id AS YZEILE_UHN
			,YEAR(dt.date) AS YMJAHR_UHN
			,'501' AS BWART
			,w.sap_code AS WERKS
			,CASE WHEN dt.warehouse_id = 2 THEN 'UHN2' ELSE 'UHN1' END AS LGORT
			,dt.itemo_id AS MATNR
			,dt.qty AS ERFMG
			,dt.jam AS CPUTM_MKPF
			,'PC' AS ERFME
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN 'BATCH' ELSE 'BATCH' END AS CHARG
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN '31.12.9999' ELSE '31.12.9999' END AS VFDAT
			,'1601112' AS KOSTL -- cost center
			,1601 AS PRCTR
			-- ,CONCAT(DAY(dt.date),'.',MONTH(dt.date),'.',YEAR(dt.date)) AS BUDAT_MKPF
			,dt.date AS BUDAT_MKPF
			,dt.createdby AS USNAM_MKPF
			,'01' AS YRETURN_IND
			,'IDR' AS WAERS
			,dt.hargao AS DMBTR
			,'510199' AS SAKTO -- G/L Account
			,'' as GRUND
			FROM (
				SELECT 
				a.adjustment_id
				,a.id AS adjustment_d_id
				,b.warehouse_id 
				,b.date
				,b.createdby
				,DATE_FORMAT(b.created,'%H%i%s') as jam
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS itemo_id
				,CASE WHEN rp.item_id IS NOT NULL THEN a.hpp ELSE a.hpp END AS hargao
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*a.qty ELSE a.qty END AS qty
				,CASE WHEN rp.item_id IS NOT NULL THEN a.hpp*rp.qty*a.qty ELSE a.qty*a.hpp END AS total
				FROM adjustment_d a
				LEFT JOIN adjustment b ON a.adjustment_id = b.id
				LEFT JOIN manufaktur rp ON a.item_id = rp.manufaktur_id
				WHERE b.date BETWEEN '".$fromdate."' AND '".$todate."'
				AND b.flag = 'Plus'
			) AS dt 
			LEFT JOIN item i ON i.id = dt.itemo_id
			LEFT JOIN warehouse w ON w.id = dt.warehouse_id
		";
		$query=$this->db->query($cSql);
	//	echo $cSql;
		if ($query){	
			 $x =1;
			 $currid = 0;
			 foreach($query->result_array() as $row){
				$cRow=array();
				
				if($currid ==0) $currid  = $row['YMBLNR_UHN'];
				if($currid != $row['YMBLNR_UHN']){
					$currid  = $row['YMBLNR_UHN'];
					$x = 1;
				}
				
				$cRow['EKGRP']=$row['EKGRP'];
				$cRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$cRow['YZEILE_UHN']=$x;
				$cRow['YID_UHN']=$row['YZEILE_UHN'];
				$cRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$cRow['BWART']=$row['BWART'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['LGORT']=$row['LGORT'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ERFMG']=$row['ERFMG'];
				$cRow['ERFME']=$row['ERFME'];
				$cRow['CHARG']=$row['CHARG'];
				$cRow['VFDAT']=$row['VFDAT'];
				$cRow['KOSTL']=$row['KOSTL'];
				$cRow['PRCTR']=$row['PRCTR'];
				$cRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"d.m.Y");
				$cRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$cRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$cRow['YRETURN_IND']=$row['YRETURN_IND'];
				$cRow['WAERS']=$row['WAERS'];
				$cRow['DMBTR']=$row['DMBTR'];
				$cRow['SAKTO']=$row['SAKTO'];
				$cRow['GRUND']=$row['GRUND'];

				$tRow['EKGRP']=$row['EKGRP'];
				$tRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$tRow['YZEILE_UHN']=$x;
				$tRow['YID_UHN']=$row['YZEILE_UHN'];
				$tRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$tRow['BWART']=$row['BWART'];
				$tRow['WERKS']=$row['WERKS'];
				$tRow['LGORT']=$row['LGORT'];
				$tRow['MATNR']=$row['MATNR'];
				$tRow['ERFMG']=$row['ERFMG'];
				$tRow['ERFME']=$row['ERFME'];
				$tRow['CHARG']=$row['CHARG'];
				$tRow['VFDAT']=$row['VFDAT'];
				$tRow['KOSTL']=$row['KOSTL'];
				$tRow['PRCTR']=$row['PRCTR'];
				$tRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"Y-m-d");
				$tRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$tRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$tRow['YRETURN_IND']=$row['YRETURN_IND'];
				$tRow['WAERS']=$row['WAERS'];
				$tRow['DMBTR']=$row['DMBTR'];
				$tRow['STATUS']='Failed Send Data';
				$tRow['SAKTO']=$row['SAKTO'];
				$tRow['ACT'] = 'ADJ MOVE';

				$this->db->where('YID_UHN', $row['YZEILE_UHN']);
				$this->db->where('ACT','ADJ MOVE');
				$q = $this->db->get('temp_materialncm');
			
				if ( $q->num_rows() > 0 ) 
				{
					
					$this->db->where('YID_UHN', $row['YZEILE_UHN']);
					$this->db->where('ACT','ADJ MOVE');
					$this->db->delete('temp_materialncm');

					$this->db->insert('temp_materialncm', $tRow);

				} else {

					$this->db->insert('temp_materialncm', $tRow);

				}

				$x++;
				$data[] = $cRow;
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return $data;
	}
	
	public function update_data_adj_move($fromdate,$todate){
		$data=array();

		$cSql="
			SELECT
			i.purchasing_group AS EKGRP
			,dt.adjustment_id AS YMBLNR_UHN
			,dt.adjustment_d_id AS YZEILE_UHN
			,YEAR(dt.date) AS YMJAHR_UHN
			,'501' AS BWART
			,w.sap_code AS WERKS
			,CASE WHEN dt.warehouse_id = 2 THEN 'UHN2' ELSE 'UHN1' END AS LGORT
			,dt.itemo_id AS MATNR
			,dt.qty AS ERFMG
			,dt.jam AS CPUTM_MKPF
			,'PC' AS ERFME
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN 'BATCH' ELSE 'BATCH' END AS CHARG
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN '31.12.9999' ELSE '31.12.9999' END AS VFDAT
			,'1601112' AS KOSTL -- cost center
			,1601 AS PRCTR
			-- ,CONCAT(DAY(dt.date),'.',MONTH(dt.date),'.',YEAR(dt.date)) AS BUDAT_MKPF
			,dt.date AS BUDAT_MKPF
			,dt.createdby AS USNAM_MKPF
			,'01' AS YRETURN_IND
			,'IDR' AS WAERS
			,dt.hargao AS DMBTR
			,'510199' AS SAKTO -- G/L Account
			,'' as GRUND
			FROM (
				SELECT 
				a.adjustment_id
				,a.id AS adjustment_d_id
				,b.warehouse_id 
				,b.date
				,b.createdby
				,DATE_FORMAT(b.created,'%H%i%s') as jam
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS itemo_id
				,CASE WHEN rp.item_id IS NOT NULL THEN a.hpp ELSE a.hpp END AS hargao
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*a.qty ELSE a.qty END AS qty
				,CASE WHEN rp.item_id IS NOT NULL THEN a.hpp*rp.qty*a.qty ELSE a.qty*a.hpp END AS total
				FROM adjustment_d a
				LEFT JOIN adjustment b ON a.adjustment_id = b.id
				LEFT JOIN manufaktur rp ON a.item_id = rp.manufaktur_id
				WHERE b.date BETWEEN '".$fromdate."' AND '".$todate."'
				AND b.flag = 'Plus'
			) AS dt 
			LEFT JOIN item i ON i.id = dt.itemo_id
			LEFT JOIN warehouse w ON w.id = dt.warehouse_id
		";
		$query=$this->db->query($cSql);
	//	echo $cSql;
		if ($query){	
			 $x =1;
			 $currid = 0;
			 foreach($query->result_array() as $row){
				$cRow=array();
				
				if($currid ==0) $currid  = $row['YMBLNR_UHN'];
				if($currid != $row['YMBLNR_UHN']){
					$currid  = $row['YMBLNR_UHN'];
					$x = 1;
				}
				
				$cRow['EKGRP']=$row['EKGRP'];
				$cRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$cRow['YZEILE_UHN']=$x;
				$cRow['YID_UHN']=$row['YZEILE_UHN'];
				$cRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$cRow['BWART']=$row['BWART'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['LGORT']=$row['LGORT'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ERFMG']=$row['ERFMG'];
				$cRow['ERFME']=$row['ERFME'];
				$cRow['CHARG']=$row['CHARG'];
				$cRow['VFDAT']=$row['VFDAT'];
				$cRow['KOSTL']=$row['KOSTL'];
				$cRow['PRCTR']=$row['PRCTR'];
				$cRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"d.m.Y");
				$cRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$cRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$cRow['YRETURN_IND']=$row['YRETURN_IND'];
				$cRow['WAERS']=$row['WAERS'];
				$cRow['DMBTR']=$row['DMBTR'];
				$cRow['SAKTO']=$row['SAKTO'];
				$cRow['GRUND']=$row['GRUND'];

				$tRow['EKGRP']=$row['EKGRP'];
				$tRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$tRow['YZEILE_UHN']=$x;
				$tRow['YID_UHN']=$row['YZEILE_UHN'];
				$tRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$tRow['BWART']=$row['BWART'];
				$tRow['WERKS']=$row['WERKS'];
				$tRow['LGORT']=$row['LGORT'];
				$tRow['MATNR']=$row['MATNR'];
				$tRow['ERFMG']=$row['ERFMG'];
				$tRow['ERFME']=$row['ERFME'];
				$tRow['CHARG']=$row['CHARG'];
				$tRow['VFDAT']=$row['VFDAT'];
				$tRow['KOSTL']=$row['KOSTL'];
				$tRow['PRCTR']=$row['PRCTR'];
				$tRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"Y-m-d");
				$tRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$tRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$tRow['YRETURN_IND']=$row['YRETURN_IND'];
				$tRow['WAERS']=$row['WAERS'];
				$tRow['DMBTR']=$row['DMBTR'];
				$tRow['STATUS']='Failed Send Data';
				$tRow['SAKTO']=$row['SAKTO'];
				$tRow['ACT'] = 'ADJ MOVE';

				$data=array(
					'STATUS' => 'Success Send Data'
				);

				$this->db->update('temp_materialncm',$data,array('YID_UHN'=>$row['YZEILE_UHN'], 'ACT'=>'ADJ MOVE'));

				$x++;
				$data[] = $cRow;
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return true;
	}
	
	public function get_data_adj_move_by_array_id($array_id){
		$data=array();

		$cSql="
			SELECT
			i.purchasing_group AS EKGRP
			,dt.adjustment_id AS YMBLNR_UHN
			,dt.adjustment_d_id AS YZEILE_UHN
			,YEAR(dt.date) AS YMJAHR_UHN
			,'501' AS BWART
			,w.sap_code AS WERKS
			,CASE WHEN dt.warehouse_id = 2 THEN 'UHN2' ELSE 'UHN1' END AS LGORT
			,dt.itemo_id AS MATNR
			,dt.qty AS ERFMG
			,dt.jam AS CPUTM_MKPF
			,'PC' AS ERFME
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN 'BATCH' ELSE 'BATCH' END AS CHARG
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN '31.12.9999' ELSE '31.12.9999' END AS VFDAT
			,'1601112' AS KOSTL -- cost center
			,1601 AS PRCTR
			-- ,CONCAT(DAY(dt.date),'.',MONTH(dt.date),'.',YEAR(dt.date)) AS BUDAT_MKPF
			,dt.date AS BUDAT_MKPF
			,dt.createdby AS USNAM_MKPF
			,'01' AS YRETURN_IND
			,'IDR' AS WAERS
			,dt.hargao AS DMBTR
			,'510199' AS SAKTO -- G/L Account
			,'' as GRUND
			FROM (
				SELECT 
				a.adjustment_id
				,a.id AS adjustment_d_id
				,b.warehouse_id 
				,b.date
				,b.createdby
				,DATE_FORMAT(b.created,'%H%i%s') as jam
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS itemo_id
				,CASE WHEN rp.item_id IS NOT NULL THEN a.hpp ELSE a.hpp END AS hargao
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*a.qty ELSE a.qty END AS qty
				,CASE WHEN rp.item_id IS NOT NULL THEN a.hpp*rp.qty*a.qty ELSE a.qty*a.hpp END AS total
				FROM adjustment_d a
				LEFT JOIN adjustment b ON a.adjustment_id = b.id
				LEFT JOIN manufaktur rp ON a.item_id = rp.manufaktur_id
				WHERE a.adjustment_id IN (".$array_id.")
				AND b.flag = 'Plus'
			) AS dt 
			LEFT JOIN item i ON i.id = dt.itemo_id
			LEFT JOIN warehouse w ON w.id = dt.warehouse_id
		";
		$query=$this->db->query($cSql);
	//	echo $cSql;
		if ($query){	
			 $x =1;
			 $currid = 0;
			 foreach($query->result_array() as $row){
				$cRow=array();
				
				if($currid ==0) $currid  = $row['YMBLNR_UHN'];
				if($currid != $row['YMBLNR_UHN']){
					$currid  = $row['YMBLNR_UHN'];
					$x = 1;
				}
				
				$cRow['EKGRP']=$row['EKGRP'];
				$cRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$cRow['YZEILE_UHN']=$x;
				$cRow['YID_UHN']=$row['YZEILE_UHN'];
				$cRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$cRow['BWART']=$row['BWART'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['LGORT']=$row['LGORT'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ERFMG']=$row['ERFMG'];
				$cRow['ERFME']=$row['ERFME'];
				$cRow['CHARG']=$row['CHARG'];
				$cRow['VFDAT']=$row['VFDAT'];
				$cRow['KOSTL']=$row['KOSTL'];
				$cRow['PRCTR']=$row['PRCTR'];
				$cRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"d.m.Y");
				$cRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$cRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$cRow['YRETURN_IND']=$row['YRETURN_IND'];
				$cRow['WAERS']=$row['WAERS'];
				$cRow['DMBTR']=$row['DMBTR'];
				$cRow['SAKTO']=$row['SAKTO'];
				$cRow['GRUND']=$row['GRUND'];

				$tRow['EKGRP']=$row['EKGRP'];
				$tRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$tRow['YZEILE_UHN']=$x;
				$tRow['YID_UHN']=$row['YZEILE_UHN'];
				$tRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$tRow['BWART']=$row['BWART'];
				$tRow['WERKS']=$row['WERKS'];
				$tRow['LGORT']=$row['LGORT'];
				$tRow['MATNR']=$row['MATNR'];
				$tRow['ERFMG']=$row['ERFMG'];
				$tRow['ERFME']=$row['ERFME'];
				$tRow['CHARG']=$row['CHARG'];
				$tRow['VFDAT']=$row['VFDAT'];
				$tRow['KOSTL']=$row['KOSTL'];
				$tRow['PRCTR']=$row['PRCTR'];
				$tRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"Y-m-d");
				$tRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$tRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$tRow['YRETURN_IND']=$row['YRETURN_IND'];
				$tRow['WAERS']=$row['WAERS'];
				$tRow['DMBTR']=$row['DMBTR'];
				$tRow['STATUS']='Failed Send Data';
				$tRow['SAKTO']=$row['SAKTO'];
				$tRow['ACT'] = 'ADJ MOVE';

				$x++;
				$data[] = $cRow;
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return $data;
	}
	
	public function update_data_adj_move_by_array_id($array_id){
		$data=array();

		$cSql="
			SELECT
			i.purchasing_group AS EKGRP
			,dt.adjustment_id AS YMBLNR_UHN
			,dt.adjustment_d_id AS YZEILE_UHN
			,YEAR(dt.date) AS YMJAHR_UHN
			,'501' AS BWART
			,w.sap_code AS WERKS
			,CASE WHEN dt.warehouse_id = 2 THEN 'UHN2' ELSE 'UHN1' END AS LGORT
			,dt.itemo_id AS MATNR
			,dt.qty AS ERFMG
			,dt.jam AS CPUTM_MKPF
			,'PC' AS ERFME
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN 'BATCH' ELSE 'BATCH' END AS CHARG
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN '31.12.9999' ELSE '31.12.9999' END AS VFDAT
			,'1601112' AS KOSTL -- cost center
			,1601 AS PRCTR
			-- ,CONCAT(DAY(dt.date),'.',MONTH(dt.date),'.',YEAR(dt.date)) AS BUDAT_MKPF
			,dt.date AS BUDAT_MKPF
			,dt.createdby AS USNAM_MKPF
			,'01' AS YRETURN_IND
			,'IDR' AS WAERS
			,dt.hargao AS DMBTR
			,'510199' AS SAKTO -- G/L Account
			,'' as GRUND
			FROM (
				SELECT 
				a.adjustment_id
				,a.id AS adjustment_d_id
				,b.warehouse_id 
				,b.date
				,b.createdby
				,DATE_FORMAT(b.created,'%H%i%s') as jam
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS itemo_id
				,CASE WHEN rp.item_id IS NOT NULL THEN a.hpp ELSE a.hpp END AS hargao
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*a.qty ELSE a.qty END AS qty
				,CASE WHEN rp.item_id IS NOT NULL THEN a.hpp*rp.qty*a.qty ELSE a.qty*a.hpp END AS total
				FROM adjustment_d a
				LEFT JOIN adjustment b ON a.adjustment_id = b.id
				LEFT JOIN manufaktur rp ON a.item_id = rp.manufaktur_id
				WHERE a.adjustment_id IN (".$array_id.")
				AND b.flag = 'Plus'
			) AS dt 
			LEFT JOIN item i ON i.id = dt.itemo_id
			LEFT JOIN warehouse w ON w.id = dt.warehouse_id
		";
		$query=$this->db->query($cSql);
	//	echo $cSql;
		if ($query){	
			 $x =1;
			 $currid = 0;
			 foreach($query->result_array() as $row){
				$cRow=array();
				
				if($currid ==0) $currid  = $row['YMBLNR_UHN'];
				if($currid != $row['YMBLNR_UHN']){
					$currid  = $row['YMBLNR_UHN'];
					$x = 1;
				}
				
				$cRow['EKGRP']=$row['EKGRP'];
				$cRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$cRow['YZEILE_UHN']=$x;
				$cRow['YID_UHN']=$row['YZEILE_UHN'];
				$cRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$cRow['BWART']=$row['BWART'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['LGORT']=$row['LGORT'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ERFMG']=$row['ERFMG'];
				$cRow['ERFME']=$row['ERFME'];
				$cRow['CHARG']=$row['CHARG'];
				$cRow['VFDAT']=$row['VFDAT'];
				$cRow['KOSTL']=$row['KOSTL'];
				$cRow['PRCTR']=$row['PRCTR'];
				$cRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"d.m.Y");
				$cRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$cRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$cRow['YRETURN_IND']=$row['YRETURN_IND'];
				$cRow['WAERS']=$row['WAERS'];
				$cRow['DMBTR']=$row['DMBTR'];
				$cRow['SAKTO']=$row['SAKTO'];
				$cRow['GRUND']=$row['GRUND'];

				$tRow['EKGRP']=$row['EKGRP'];
				$tRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$tRow['YZEILE_UHN']=$x;
				$tRow['YID_UHN']=$row['YZEILE_UHN'];
				$tRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$tRow['BWART']=$row['BWART'];
				$tRow['WERKS']=$row['WERKS'];
				$tRow['LGORT']=$row['LGORT'];
				$tRow['MATNR']=$row['MATNR'];
				$tRow['ERFMG']=$row['ERFMG'];
				$tRow['ERFME']=$row['ERFME'];
				$tRow['CHARG']=$row['CHARG'];
				$tRow['VFDAT']=$row['VFDAT'];
				$tRow['KOSTL']=$row['KOSTL'];
				$tRow['PRCTR']=$row['PRCTR'];
				$tRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"Y-m-d");
				$tRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$tRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$tRow['YRETURN_IND']=$row['YRETURN_IND'];
				$tRow['WAERS']=$row['WAERS'];
				$tRow['DMBTR']=$row['DMBTR'];
				$tRow['STATUS']='Failed Send Data';
				$tRow['SAKTO']=$row['SAKTO'];
				$tRow['ACT'] = 'ADJ MOVE';

				$data=array(
					'STATUS' => 'Success Send Data'
				);
					
				$this->db->update('temp_materialncm',$data,array('YID_UHN'=>$row['YZEILE_UHN'], 'ACT'=>'ADJ MOVE'));

				$x++;
				$data[] = $cRow;
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return true;
	}
	
	//9.	Material movement Adjustment Blocked Material  - abaikan tapi keren juga
	//10.	Material movement Adjustment UnBlocked Material - abaikan juga tapi keren juga
	
	
	//11.	Material movement sloc to sloc - 1. Wh movement rusak , 2.wh trf = Untuk perpindahan ke wh 2 (PPG)
	public function getSloc($fromdate,$todate){
		$data=array();

		$cSql="
			SELECT
			i.purchasing_group AS EKGRP
			,dt.mutasi_id AS YMBLNR_UHN
			,dt.mutasi_d_id AS YZEILE_UHN
			,YEAR(dt.date) AS YMJAHR_UHN
			,'311' AS BWART
			,dt.warehouse_id1
			,CASE WHEN dt.warehouse_id1 = 2 THEN 1601 ELSE w.sap_code END AS WERKS
			,CASE WHEN dt.warehouse_id1 = 2 THEN 'UHN2' ELSE 'UHN1' END AS LGORT
			,dt.itemo_id AS MATNR
			,dt.qty AS ERFMG
			,dt.jam AS CPUTM_MKPF
			,'PC' AS ERFME
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN 'BATCH' ELSE 'BATCH' END AS CHARG
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN '31.12.9999' ELSE '31.12.9999' END AS VFDAT
			,'1601112' AS KOSTL -- cost center
			,1601 AS PRCTR
			,CASE WHEN dt.warehouse_id2 = 2 THEN 'UHN2' ELSE 'UHN1' END AS UMLGO
			-- ,CONCAT(DAY(dt.date),'.',MONTH(dt.date),'.',YEAR(dt.date)) AS BUDAT_MKPF
			,dt.date AS BUDAT_MKPF
			,dt.createdby AS USNAM_MKPF
			,'01' AS YRETURN_IND
			,'IDR' AS WAERS
			,dt.hargao AS DMBTR
			,'' AS SAKTO -- G/L Account
			,'' AS GRUND
			FROM (
				SELECT 
				a.mutasi_id
				,a.id AS mutasi_d_id
				,b.warehouse_id1
				,b.warehouse_id2 
				,b.date
				,DATE_FORMAT(b.created,'%H%i%s') as jam
				,b.createdby
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS itemo_id
				,CASE WHEN rp.item_id IS NOT NULL THEN a.harga ELSE a.harga END AS hargao
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*a.qty ELSE a.qty END AS qty
				,CASE WHEN rp.item_id IS NOT NULL THEN a.harga*rp.qty*a.qty ELSE a.qty*a.harga END AS total
				FROM mutasi_d a
				LEFT JOIN mutasi b ON a.mutasi_id = b.id
				LEFT JOIN manufaktur rp ON a.item_id = rp.manufaktur_id
				WHERE b.date BETWEEN '".$fromdate."' AND '".$todate."'
				AND (b.warehouse_id1 = 2 OR b.warehouse_id2 = 2)
				AND b.status = 'Y'
			) AS dt 
			LEFT JOIN item i ON i.id = dt.itemo_id
			LEFT JOIN warehouse w ON w.id = dt.warehouse_id1
			LEFT JOIN warehouse w2 ON w2.id = dt.warehouse_id2
			";
	
		$query=$this->db->query($cSql);
		if ($query){	
			 $x =1;
			 $currid = 0;
			 foreach($query->result_array() as $row){
				 // 11 sloc to sloc 
				$cRow=array();
				
				if($currid ==0) $currid  = $row['YMBLNR_UHN'];
				if($currid != $row['YMBLNR_UHN']){
					$currid  = $row['YMBLNR_UHN'];
					$x = 1;
				}
				
				$cRow['EKGRP']=$row['EKGRP'];
				$cRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$cRow['YZEILE_UHN']=$x;
				$cRow['YID_UHN']=$row['YZEILE_UHN'];
				$cRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$cRow['BWART']=$row['BWART'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['LGORT']=$row['LGORT'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ERFMG']=$row['ERFMG'];
				$cRow['ERFME']=$row['ERFME'];
				$cRow['CHARG']=$row['CHARG'];
				$cRow['VFDAT']=$row['VFDAT'];
				$cRow['KOSTL']=$row['KOSTL'];
				$cRow['PRCTR']=$row['PRCTR'];
				$cRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"d.m.Y");
				$cRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$cRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$cRow['YRETURN_IND']=$row['YRETURN_IND'];
				$cRow['WAERS']=$row['WAERS'];
				$cRow['DMBTR']=$row['DMBTR'];
				$cRow['SAKTO']=$row['SAKTO'];
				$cRow['GRUND']=$row['GRUND'];
				$cRow['UMLGO']=$row['UMLGO'];
				$tRow['EKGRP']=$row['EKGRP'];
				$tRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$tRow['YZEILE_UHN']=$x;
				$tRow['YID_UHN']=$row['YZEILE_UHN'];
				$tRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$tRow['BWART']=$row['BWART'];
				$tRow['WERKS']=$row['WERKS'];
				$tRow['LGORT']=$row['LGORT'];
				$tRow['MATNR']=$row['MATNR'];
				$tRow['ERFMG']=$row['ERFMG'];
				$tRow['ERFME']=$row['ERFME'];
				$tRow['CHARG']=$row['CHARG'];
				$tRow['VFDAT']=$row['VFDAT'];
				$tRow['KOSTL']=$row['KOSTL'];
				$tRow['PRCTR']=$row['PRCTR'];
				$tRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"Y-m-d");
				$tRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$tRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$tRow['YRETURN_IND']=$row['YRETURN_IND'];
				$tRow['WAERS']=$row['WAERS'];
				$tRow['DMBTR']=$row['DMBTR'];
				$tRow['STATUS']='Failed Send Data';
				$tRow['SAKTO']=$row['SAKTO'];
				$tRow['ACT'] = 'SLOC';

				$this->db->where('YID_UHN', $row['YZEILE_UHN']);
				$this->db->where('ACT','SLOC');
				$q = $this->db->get('temp_materialncm');
			
				if ( $q->num_rows() > 0 ) 
				{
					
					$this->db->where('YID_UHN', $row['YZEILE_UHN']);
					$this->db->where('ACT','SLOC');
					$this->db->delete('temp_materialncm');

					$this->db->insert('temp_materialncm', $tRow);

				} else {

					$this->db->insert('temp_materialncm', $tRow);

				}

				$x++;
				
				$data[] = $cRow;
			 }

		}else{

			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;

		}
		
		return $data;
	}
	
	public function update_data_sloc($fromdate,$todate){
		$data=array();

		$cSql="
			SELECT
			i.purchasing_group AS EKGRP
			,dt.mutasi_id AS YMBLNR_UHN
			,dt.mutasi_d_id AS YZEILE_UHN
			,YEAR(dt.date) AS YMJAHR_UHN
			,'311' AS BWART
			,dt.warehouse_id1
			,CASE WHEN dt.warehouse_id1 = 2 THEN 1601 ELSE w.sap_code END AS WERKS
			,CASE WHEN dt.warehouse_id1 = 2 THEN 'UHN2' ELSE 'UHN1' END AS LGORT
			,dt.itemo_id AS MATNR
			,dt.qty AS ERFMG
			,dt.jam AS CPUTM_MKPF
			,'PC' AS ERFME
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN 'BATCH' ELSE 'BATCH' END AS CHARG
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN '31.12.9999' ELSE '31.12.9999' END AS VFDAT
			,'1601112' AS KOSTL -- cost center
			,1601 AS PRCTR
			,CASE WHEN dt.warehouse_id2 = 2 THEN 'UHN2' ELSE 'UHN1' END AS UMLGO
			-- ,CONCAT(DAY(dt.date),'.',MONTH(dt.date),'.',YEAR(dt.date)) AS BUDAT_MKPF
			,dt.date AS BUDAT_MKPF
			,dt.createdby AS USNAM_MKPF
			,'01' AS YRETURN_IND
			,'IDR' AS WAERS
			,dt.hargao AS DMBTR
			,'' AS SAKTO -- G/L Account
			,'' AS GRUND
			FROM (
				SELECT 
				a.mutasi_id
				,a.id AS mutasi_d_id
				,b.warehouse_id1
				,b.warehouse_id2 
				,b.date
				,DATE_FORMAT(b.created,'%H%i%s') as jam
				,b.createdby
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS itemo_id
				,CASE WHEN rp.item_id IS NOT NULL THEN a.harga ELSE a.harga END AS hargao
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*a.qty ELSE a.qty END AS qty
				,CASE WHEN rp.item_id IS NOT NULL THEN a.harga*rp.qty*a.qty ELSE a.qty*a.harga END AS total
				FROM mutasi_d a
				LEFT JOIN mutasi b ON a.mutasi_id = b.id
				LEFT JOIN manufaktur rp ON a.item_id = rp.manufaktur_id
				WHERE b.date BETWEEN '".$fromdate."' AND '".$todate."'
				AND (b.warehouse_id1 = 2 OR b.warehouse_id2 = 2)
				AND b.status = 'Y'
			) AS dt 
			LEFT JOIN item i ON i.id = dt.itemo_id
			LEFT JOIN warehouse w ON w.id = dt.warehouse_id1
			LEFT JOIN warehouse w2 ON w2.id = dt.warehouse_id2
			";
	
		$query=$this->db->query($cSql);
		if ($query){	
			 $x =1;
			 $currid = 0;
			 foreach($query->result_array() as $row){
				 // 11 sloc to sloc 
				$cRow=array();
				
				if($currid ==0) $currid  = $row['YMBLNR_UHN'];
				if($currid != $row['YMBLNR_UHN']){
					$currid  = $row['YMBLNR_UHN'];
					$x = 1;
				}
				
				$cRow['EKGRP']=$row['EKGRP'];
				$cRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$cRow['YZEILE_UHN']=$x;
				$cRow['YID_UHN']=$row['YZEILE_UHN'];
				$cRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$cRow['BWART']=$row['BWART'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['LGORT']=$row['LGORT'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ERFMG']=$row['ERFMG'];
				$cRow['ERFME']=$row['ERFME'];
				$cRow['CHARG']=$row['CHARG'];
				$cRow['VFDAT']=$row['VFDAT'];
				$cRow['KOSTL']=$row['KOSTL'];
				$cRow['PRCTR']=$row['PRCTR'];
				$cRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"d.m.Y");
				$cRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$cRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$cRow['YRETURN_IND']=$row['YRETURN_IND'];
				$cRow['WAERS']=$row['WAERS'];
				$cRow['DMBTR']=$row['DMBTR'];
				$cRow['SAKTO']=$row['SAKTO'];
				$cRow['GRUND']=$row['GRUND'];
				$cRow['UMLGO']=$row['UMLGO'];
				$tRow['EKGRP']=$row['EKGRP'];
				$tRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$tRow['YZEILE_UHN']=$x;
				$tRow['YID_UHN']=$row['YZEILE_UHN'];
				$tRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$tRow['BWART']=$row['BWART'];
				$tRow['WERKS']=$row['WERKS'];
				$tRow['LGORT']=$row['LGORT'];
				$tRow['MATNR']=$row['MATNR'];
				$tRow['ERFMG']=$row['ERFMG'];
				$tRow['ERFME']=$row['ERFME'];
				$tRow['CHARG']=$row['CHARG'];
				$tRow['VFDAT']=$row['VFDAT'];
				$tRow['KOSTL']=$row['KOSTL'];
				$tRow['PRCTR']=$row['PRCTR'];
				$tRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"Y-m-d");
				$tRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$tRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$tRow['YRETURN_IND']=$row['YRETURN_IND'];
				$tRow['WAERS']=$row['WAERS'];
				$tRow['DMBTR']=$row['DMBTR'];
				$tRow['STATUS']='Failed Send Data';
				$tRow['SAKTO']=$row['SAKTO'];
				$tRow['ACT'] = 'SLOC';

				$data=array(
					'STATUS' => 'Success Send Data'
				);

				$this->db->update('temp_materialncm',$data,array('YID_UHN'=>$row['YZEILE_UHN'], 'ACT'=>'SLOC'));

				$x++;
				
				$data[] = $cRow;
			 }

		}else{

			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;

		}
		
		return true;
	}
	
	public function get_data_sloc_by_array_id($array_id){
		$data=array();
		
		$cSql="
			SELECT
			i.purchasing_group AS EKGRP
			,dt.mutasi_id AS YMBLNR_UHN
			,dt.mutasi_d_id AS YZEILE_UHN
			,YEAR(dt.date) AS YMJAHR_UHN
			,'311' AS BWART
			,dt.warehouse_id1
			,CASE WHEN dt.warehouse_id1 = 2 THEN 1601 ELSE w.sap_code END AS WERKS
			,CASE WHEN dt.warehouse_id1 = 2 THEN 'UHN2' ELSE 'UHN1' END AS LGORT
			,dt.itemo_id AS MATNR
			,dt.qty AS ERFMG
			,dt.jam AS CPUTM_MKPF
			,'PC' AS ERFME
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN 'BATCH' ELSE 'BATCH' END AS CHARG
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN '31.12.9999' ELSE '31.12.9999' END AS VFDAT
			,'1601112' AS KOSTL -- cost center
			,1601 AS PRCTR
			,CASE WHEN dt.warehouse_id2 = 2 THEN 'UHN2' ELSE 'UHN1' END AS UMLGO
			-- ,CONCAT(DAY(dt.date),'.',MONTH(dt.date),'.',YEAR(dt.date)) AS BUDAT_MKPF
			,dt.date AS BUDAT_MKPF
			,dt.createdby AS USNAM_MKPF
			,'01' AS YRETURN_IND
			,'IDR' AS WAERS
			,dt.hargao AS DMBTR
			,'' AS SAKTO -- G/L Account
			,'' AS GRUND
			FROM (
				SELECT 
				a.mutasi_id
				,a.id AS mutasi_d_id
				,b.warehouse_id1
				,b.warehouse_id2 
				,b.date
				,DATE_FORMAT(b.created,'%H%i%s') as jam
				,b.createdby
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS itemo_id
				,CASE WHEN rp.item_id IS NOT NULL THEN a.harga ELSE a.harga END AS hargao
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*a.qty ELSE a.qty END AS qty
				,CASE WHEN rp.item_id IS NOT NULL THEN a.harga*rp.qty*a.qty ELSE a.qty*a.harga END AS total
				FROM mutasi_d a
				LEFT JOIN mutasi b ON a.mutasi_id = b.id
				LEFT JOIN manufaktur rp ON a.item_id = rp.manufaktur_id
				WHERE a.mutasi_id IN (".$array_id.")
				AND (b.warehouse_id1 = 2 OR b.warehouse_id2 = 2)
				AND b.status = 'Y'
			) AS dt 
			LEFT JOIN item i ON i.id = dt.itemo_id
			LEFT JOIN warehouse w ON w.id = dt.warehouse_id1
			LEFT JOIN warehouse w2 ON w2.id = dt.warehouse_id2
			";
	
		$query=$this->db->query($cSql);
		if ($query){	
			 $x =1;
			 $currid = 0;
			 foreach($query->result_array() as $row){
				 // 11 sloc to sloc 
				$cRow=array();
				
				if($currid ==0) $currid  = $row['YMBLNR_UHN'];
				if($currid != $row['YMBLNR_UHN']){
					$currid  = $row['YMBLNR_UHN'];
					$x = 1;
				}
				
				$cRow['EKGRP']=$row['EKGRP'];
				$cRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$cRow['YZEILE_UHN']=$x;
				$cRow['YID_UHN']=$row['YZEILE_UHN'];
				$cRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$cRow['BWART']=$row['BWART'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['LGORT']=$row['LGORT'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ERFMG']=$row['ERFMG'];
				$cRow['ERFME']=$row['ERFME'];
				$cRow['CHARG']=$row['CHARG'];
				$cRow['VFDAT']=$row['VFDAT'];
				$cRow['KOSTL']=$row['KOSTL'];
				$cRow['PRCTR']=$row['PRCTR'];
				$cRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"d.m.Y");
				$cRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$cRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$cRow['YRETURN_IND']=$row['YRETURN_IND'];
				$cRow['WAERS']=$row['WAERS'];
				$cRow['DMBTR']=$row['DMBTR'];
				$cRow['SAKTO']=$row['SAKTO'];
				$cRow['GRUND']=$row['GRUND'];
				$cRow['UMLGO']=$row['UMLGO'];
				$tRow['EKGRP']=$row['EKGRP'];
				$tRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$tRow['YZEILE_UHN']=$x;
				$tRow['YID_UHN']=$row['YZEILE_UHN'];
				$tRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$tRow['BWART']=$row['BWART'];
				$tRow['WERKS']=$row['WERKS'];
				$tRow['LGORT']=$row['LGORT'];
				$tRow['MATNR']=$row['MATNR'];
				$tRow['ERFMG']=$row['ERFMG'];
				$tRow['ERFME']=$row['ERFME'];
				$tRow['CHARG']=$row['CHARG'];
				$tRow['VFDAT']=$row['VFDAT'];
				$tRow['KOSTL']=$row['KOSTL'];
				$tRow['PRCTR']=$row['PRCTR'];
				$tRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"Y-m-d");
				$tRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$tRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$tRow['YRETURN_IND']=$row['YRETURN_IND'];
				$tRow['WAERS']=$row['WAERS'];
				$tRow['DMBTR']=$row['DMBTR'];
				$tRow['STATUS']='Failed Send Data';
				$tRow['SAKTO']=$row['SAKTO'];
				$tRow['ACT'] = 'SLOC';

				$x++;

				$data[] = $cRow;
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return $data;
	}
	
	public function update_data_sloc_by_array_id($array_id){
		$data=array();
		
		$cSql="
			SELECT
			i.purchasing_group AS EKGRP
			,dt.mutasi_id AS YMBLNR_UHN
			,dt.mutasi_d_id AS YZEILE_UHN
			,YEAR(dt.date) AS YMJAHR_UHN
			,'311' AS BWART
			,dt.warehouse_id1
			,CASE WHEN dt.warehouse_id1 = 2 THEN 1601 ELSE w.sap_code END AS WERKS
			,CASE WHEN dt.warehouse_id1 = 2 THEN 'UHN2' ELSE 'UHN1' END AS LGORT
			,dt.itemo_id AS MATNR
			,dt.qty AS ERFMG
			,dt.jam AS CPUTM_MKPF
			,'PC' AS ERFME
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN 'BATCH' ELSE 'BATCH' END AS CHARG
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN '31.12.9999' ELSE '31.12.9999' END AS VFDAT
			,'1601112' AS KOSTL -- cost center
			,1601 AS PRCTR
			,CASE WHEN dt.warehouse_id2 = 2 THEN 'UHN2' ELSE 'UHN1' END AS UMLGO
			-- ,CONCAT(DAY(dt.date),'.',MONTH(dt.date),'.',YEAR(dt.date)) AS BUDAT_MKPF
			,dt.date AS BUDAT_MKPF
			,dt.createdby AS USNAM_MKPF
			,'01' AS YRETURN_IND
			,'IDR' AS WAERS
			,dt.hargao AS DMBTR
			,'' AS SAKTO -- G/L Account
			,'' AS GRUND
			FROM (
				SELECT 
				a.mutasi_id
				,a.id AS mutasi_d_id
				,b.warehouse_id1
				,b.warehouse_id2 
				,b.date
				,DATE_FORMAT(b.created,'%H%i%s') as jam
				,b.createdby
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS itemo_id
				,CASE WHEN rp.item_id IS NOT NULL THEN a.harga ELSE a.harga END AS hargao
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*a.qty ELSE a.qty END AS qty
				,CASE WHEN rp.item_id IS NOT NULL THEN a.harga*rp.qty*a.qty ELSE a.qty*a.harga END AS total
				FROM mutasi_d a
				LEFT JOIN mutasi b ON a.mutasi_id = b.id
				LEFT JOIN manufaktur rp ON a.item_id = rp.manufaktur_id
				WHERE a.mutasi_id IN (".$array_id.")
				AND (b.warehouse_id1 = 2 OR b.warehouse_id2 = 2)
				AND b.status = 'Y'
			) AS dt 
			LEFT JOIN item i ON i.id = dt.itemo_id
			LEFT JOIN warehouse w ON w.id = dt.warehouse_id1
			LEFT JOIN warehouse w2 ON w2.id = dt.warehouse_id2
			";
	
		$query=$this->db->query($cSql);
		if ($query){	
			 $x =1;
			 $currid = 0;
			 foreach($query->result_array() as $row){
				 // 11 sloc to sloc 
				$cRow=array();
				
				if($currid ==0) $currid  = $row['YMBLNR_UHN'];
				if($currid != $row['YMBLNR_UHN']){
					$currid  = $row['YMBLNR_UHN'];
					$x = 1;
				}
				
				$cRow['EKGRP']=$row['EKGRP'];
				$cRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$cRow['YZEILE_UHN']=$x;
				$cRow['YID_UHN']=$row['YZEILE_UHN'];
				$cRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$cRow['BWART']=$row['BWART'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['LGORT']=$row['LGORT'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ERFMG']=$row['ERFMG'];
				$cRow['ERFME']=$row['ERFME'];
				$cRow['CHARG']=$row['CHARG'];
				$cRow['VFDAT']=$row['VFDAT'];
				$cRow['KOSTL']=$row['KOSTL'];
				$cRow['PRCTR']=$row['PRCTR'];
				$cRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"d.m.Y");
				$cRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$cRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$cRow['YRETURN_IND']=$row['YRETURN_IND'];
				$cRow['WAERS']=$row['WAERS'];
				$cRow['DMBTR']=$row['DMBTR'];
				$cRow['SAKTO']=$row['SAKTO'];
				$cRow['GRUND']=$row['GRUND'];
				$cRow['UMLGO']=$row['UMLGO'];
				$tRow['EKGRP']=$row['EKGRP'];
				$tRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$tRow['YZEILE_UHN']=$x;
				$tRow['YID_UHN']=$row['YZEILE_UHN'];
				$tRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$tRow['BWART']=$row['BWART'];
				$tRow['WERKS']=$row['WERKS'];
				$tRow['LGORT']=$row['LGORT'];
				$tRow['MATNR']=$row['MATNR'];
				$tRow['ERFMG']=$row['ERFMG'];
				$tRow['ERFME']=$row['ERFME'];
				$tRow['CHARG']=$row['CHARG'];
				$tRow['VFDAT']=$row['VFDAT'];
				$tRow['KOSTL']=$row['KOSTL'];
				$tRow['PRCTR']=$row['PRCTR'];
				$tRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"Y-m-d");
				$tRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$tRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$tRow['YRETURN_IND']=$row['YRETURN_IND'];
				$tRow['WAERS']=$row['WAERS'];
				$tRow['DMBTR']=$row['DMBTR'];
				$tRow['STATUS']='Failed Send Data';
				$tRow['SAKTO']=$row['SAKTO'];
				$tRow['ACT'] = 'SLOC';

				$data=array(
					'STATUS' => 'Success Send Data'
				);
					
				$this->db->update('temp_materialncm',$data,array('YID_UHN'=>$row['YZEILE_UHN'], 'ACT'=>'SLOC'));

				$x++;

				$data[] = $cRow;
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return true;
	}
	
	//12.	Material movement whs to whs (pengirim) - inventory > Whs Stock Movement (tabel mutasi – mutasi_d)
	public function getMvWhsFrom($fromdate,$todate){
		$data=array();

		$cSql="
			SELECT
			i.purchasing_group AS EKGRP
			,dt.mutasi_id AS YMBLNR_UHN
			,dt.mutasi_d_id AS YZEILE_UHN
			,YEAR(dt.date) AS YMJAHR_UHN
			,'303' AS BWART
			,w.sap_code AS WERKS
			,CASE WHEN dt.warehouse_id1 = 2 THEN 'UHN2' ELSE 'UHN1' END AS LGORT
			,dt.itemo_id AS MATNR
			,dt.qty AS ERFMG
			,'PC' AS ERFME
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN 'BATCH' ELSE 'BATCH' END AS CHARG
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN '31.12.9999' ELSE '31.12.9999' END AS VFDAT
			,'1601112' AS KOSTL -- cost center
			,1601 AS PRCTR
			-- ,CONCAT(DAY(dt.date),'.',MONTH(dt.date),'.',YEAR(dt.date)) AS BUDAT_MKPF
			,dt.date AS BUDAT_MKPF
			,dt.jam AS CPUTM_MKPF
			,dt.createdby AS USNAM_MKPF
			,'01' AS YRETURN_IND
			,'IDR' AS WAERS
			,dt.hargao AS DMBTR
			-- ,'620209' AS SAKTO -- G/L Account
			,w2.sap_code AS UMWRK
			FROM (
				SELECT 
				a.mutasi_id
				,a.id AS mutasi_d_id
				,b.warehouse_id1 
				,b.warehouse_id2 
				,b.date
				,b.createdby
				,DATE_FORMAT(b.created,'%H%i%s') as jam
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS itemo_id
				,CASE WHEN rp.item_id IS NOT NULL THEN a.harga ELSE a.harga END AS hargao
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*a.qty ELSE a.qty END AS qty
				,CASE WHEN rp.item_id IS NOT NULL THEN a.harga*rp.qty*a.qty ELSE a.qty*a.harga END AS total
				FROM mutasi_d a
				LEFT JOIN mutasi b ON a.mutasi_id = b.id
				LEFT JOIN manufaktur rp ON a.item_id = rp.manufaktur_id
				WHERE b.date BETWEEN '".$fromdate."' AND '".$todate."'
				AND (warehouse_id1 != 2 AND warehouse_id2 != 2)
				AND b.status = 'Y'
			) AS dt 
			LEFT JOIN item i ON i.id = dt.itemo_id
			LEFT JOIN warehouse w ON w.id = dt.warehouse_id1
			LEFT JOIN warehouse w2 ON w2.id = dt.warehouse_id2
		";
		$query=$this->db->query($cSql);
		if ($query){	
			 $x =1;
			 $currid = 0;
			 foreach($query->result_array() as $row){
				 // 11 sloc to sloc 
				$cRow=array();
				
				if($currid ==0) $currid  = $row['YMBLNR_UHN'];
				if($currid != $row['YMBLNR_UHN']){
					$currid  = $row['YMBLNR_UHN'];
					$x = 1;
				}
				
				$cRow['EKGRP']=$row['EKGRP'];
				$cRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$cRow['YZEILE_UHN']=$x;
				$cRow['YID_UHN']=$row['YZEILE_UHN'];
				$cRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$cRow['BWART']=$row['BWART'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['LGORT']=$row['LGORT'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ERFMG']=$row['ERFMG'];
				$cRow['ERFME']=$row['ERFME'];
				$cRow['CHARG']=$row['CHARG'];
				$cRow['VFDAT']=$row['VFDAT'];
				//$cRow['KOSTL']=$row['KOSTL'];
				//$cRow['PRCTR']=$row['PRCTR'];
				$cRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"d.m.Y");
				$cRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$cRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$cRow['YRETURN_IND']=$row['YRETURN_IND'];
				$cRow['WAERS']=$row['WAERS'];
				$cRow['DMBTR']=$row['DMBTR'];
				$cRow['UMWRK']=$row['UMWRK'];
				//$cRow['SAKTO']=$row['SAKTO'];





				$tRow['EKGRP']=$row['EKGRP'];
				$tRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$tRow['YZEILE_UHN']=$x;
				$tRow['YID_UHN']=$row['YZEILE_UHN'];
				$tRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$tRow['BWART']=$row['BWART'];
				$tRow['WERKS']=$row['WERKS'];
				$tRow['LGORT']=$row['LGORT'];
				$tRow['MATNR']=$row['MATNR'];
				$tRow['ERFMG']=$row['ERFMG'];
				$tRow['ERFME']=$row['ERFME'];
				$tRow['CHARG']=$row['CHARG'];
				$tRow['VFDAT']=$row['VFDAT'];
				$tRow['KOSTL']=$row['KOSTL'];
				$tRow['PRCTR']=$row['PRCTR'];
				$tRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"Y-m-d");
				$tRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$tRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$tRow['YRETURN_IND']=$row['YRETURN_IND'];
				$tRow['WAERS']=$row['WAERS'];
				$tRow['DMBTR']=$row['DMBTR'];
				$tRow['STATUS']='Failed Send Data';
			
				$tRow['ACT'] = 'WHS From';

				$this->db->where('YID_UHN', $row['YZEILE_UHN']);
				$this->db->where('ACT','WHS From');
				$q = $this->db->get('temp_materialncm');
			
				if ( $q->num_rows() > 0 ) 
				{
					
					$this->db->where('YID_UHN', $row['YZEILE_UHN']);
					$this->db->where('ACT','WHS From');
					$this->db->delete('temp_materialncm');

					$this->db->insert('temp_materialncm', $tRow);

				} else {

					$this->db->insert('temp_materialncm', $tRow);

				}

				$x++;
				
				$data[] = $cRow;
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return $data;

	}

	public function update_data_whs_from($fromdate,$todate){
		$data=array();

		$cSql="
			SELECT
			i.purchasing_group AS EKGRP
			,dt.mutasi_id AS YMBLNR_UHN
			,dt.mutasi_d_id AS YZEILE_UHN
			,YEAR(dt.date) AS YMJAHR_UHN
			,'303' AS BWART
			,w.sap_code AS WERKS
			,CASE WHEN dt.warehouse_id1 = 2 THEN 'UHN2' ELSE 'UHN1' END AS LGORT
			,dt.itemo_id AS MATNR
			,dt.qty AS ERFMG
			,'PC' AS ERFME
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN 'BATCH' ELSE 'BATCH' END AS CHARG
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN '31.12.9999' ELSE '31.12.9999' END AS VFDAT
			,'1601112' AS KOSTL -- cost center
			,1601 AS PRCTR
			-- ,CONCAT(DAY(dt.date),'.',MONTH(dt.date),'.',YEAR(dt.date)) AS BUDAT_MKPF
			,dt.date AS BUDAT_MKPF
			,dt.jam AS CPUTM_MKPF
			,dt.createdby AS USNAM_MKPF
			,'01' AS YRETURN_IND
			,'IDR' AS WAERS
			,dt.hargao AS DMBTR
			-- ,'620209' AS SAKTO -- G/L Account
			,w2.sap_code AS UMWRK
			FROM (
				SELECT 
				a.mutasi_id
				,a.id AS mutasi_d_id
				,b.warehouse_id1 
				,b.warehouse_id2 
				,b.date
				,b.createdby
				,DATE_FORMAT(b.created,'%H%i%s') as jam
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS itemo_id
				,CASE WHEN rp.item_id IS NOT NULL THEN a.harga ELSE a.harga END AS hargao
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*a.qty ELSE a.qty END AS qty
				,CASE WHEN rp.item_id IS NOT NULL THEN a.harga*rp.qty*a.qty ELSE a.qty*a.harga END AS total
				FROM mutasi_d a
				LEFT JOIN mutasi b ON a.mutasi_id = b.id
				LEFT JOIN manufaktur rp ON a.item_id = rp.manufaktur_id
				WHERE b.date BETWEEN '".$fromdate."' AND '".$todate."'
				AND (warehouse_id1 != 2 AND warehouse_id2 != 2)
				AND b.status = 'Y'
			) AS dt 
			LEFT JOIN item i ON i.id = dt.itemo_id
			LEFT JOIN warehouse w ON w.id = dt.warehouse_id1
			LEFT JOIN warehouse w2 ON w2.id = dt.warehouse_id2
		";
		$query=$this->db->query($cSql);
		if ($query){	
			 $x =1;
			 $currid = 0;
			 foreach($query->result_array() as $row){
				 // 11 sloc to sloc 
				$cRow=array();
				
				if($currid ==0) $currid  = $row['YMBLNR_UHN'];
				if($currid != $row['YMBLNR_UHN']){
					$currid  = $row['YMBLNR_UHN'];
					$x = 1;
				}
				
				$cRow['EKGRP']=$row['EKGRP'];
				$cRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$cRow['YZEILE_UHN']=$x;
				$cRow['YID_UHN']=$row['YZEILE_UHN'];
				$cRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$cRow['BWART']=$row['BWART'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['LGORT']=$row['LGORT'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ERFMG']=$row['ERFMG'];
				$cRow['ERFME']=$row['ERFME'];
				$cRow['CHARG']=$row['CHARG'];
				$cRow['VFDAT']=$row['VFDAT'];
				//$cRow['KOSTL']=$row['KOSTL'];
				//$cRow['PRCTR']=$row['PRCTR'];
				$cRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"d.m.Y");
				$cRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$cRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$cRow['YRETURN_IND']=$row['YRETURN_IND'];
				$cRow['WAERS']=$row['WAERS'];
				$cRow['DMBTR']=$row['DMBTR'];
				$cRow['UMWRK']=$row['UMWRK'];
				//$cRow['SAKTO']=$row['SAKTO'];

				$tRow['EKGRP']=$row['EKGRP'];
				$tRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$tRow['YZEILE_UHN']=$x;
				$tRow['YID_UHN']=$row['YZEILE_UHN'];
				$tRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$tRow['BWART']=$row['BWART'];
				$tRow['WERKS']=$row['WERKS'];
				$tRow['LGORT']=$row['LGORT'];
				$tRow['MATNR']=$row['MATNR'];
				$tRow['ERFMG']=$row['ERFMG'];
				$tRow['ERFME']=$row['ERFME'];
				$tRow['CHARG']=$row['CHARG'];
				$tRow['VFDAT']=$row['VFDAT'];
				$tRow['KOSTL']=$row['KOSTL'];
				$tRow['PRCTR']=$row['PRCTR'];
				$tRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"Y-m-d");
				$tRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$tRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$tRow['YRETURN_IND']=$row['YRETURN_IND'];
				$tRow['WAERS']=$row['WAERS'];
				$tRow['DMBTR']=$row['DMBTR'];
				$tRow['STATUS']='Failed Send Data';
			
				$tRow['ACT'] = 'WHS From';

				$data=array(
					'STATUS' => 'Success Send Data'
				);

				$this->db->update('temp_materialncm',$data,array('YID_UHN'=>$row['YZEILE_UHN'], 'ACT'=>'WHS From'));

				$x++;
				
				$data[] = $cRow;
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return true;

	}

	public function get_data_whs_from_by_array_id($array_id){
		$data=array();

		$cSql="
			SELECT
			i.purchasing_group AS EKGRP
			,dt.mutasi_id AS YMBLNR_UHN
			,dt.mutasi_d_id AS YZEILE_UHN
			,YEAR(dt.date) AS YMJAHR_UHN
			,'303' AS BWART
			,w.sap_code AS WERKS
			,CASE WHEN dt.warehouse_id1 = 2 THEN 'UHN2' ELSE 'UHN1' END AS LGORT
			,dt.itemo_id AS MATNR
			,dt.qty AS ERFMG
			,'PC' AS ERFME
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN 'BATCH' ELSE 'BATCH' END AS CHARG
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN '31.12.9999' ELSE '31.12.9999' END AS VFDAT
			,'1601112' AS KOSTL -- cost center
			,1601 AS PRCTR
			-- ,CONCAT(DAY(dt.date),'.',MONTH(dt.date),'.',YEAR(dt.date)) AS BUDAT_MKPF
			,dt.date AS BUDAT_MKPF
			,dt.jam AS CPUTM_MKPF
			,dt.createdby AS USNAM_MKPF
			,'01' AS YRETURN_IND
			,'IDR' AS WAERS
			,dt.hargao AS DMBTR
			-- ,'620209' AS SAKTO -- G/L Account
			,w2.sap_code AS UMWRK
			FROM (
				SELECT 
				a.mutasi_id
				,a.id AS mutasi_d_id
				,b.warehouse_id1 
				,b.warehouse_id2 
				,b.date
				,b.createdby
				,DATE_FORMAT(b.created,'%H%i%s') as jam
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS itemo_id
				,CASE WHEN rp.item_id IS NOT NULL THEN a.harga ELSE a.harga END AS hargao
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*a.qty ELSE a.qty END AS qty
				,CASE WHEN rp.item_id IS NOT NULL THEN a.harga*rp.qty*a.qty ELSE a.qty*a.harga END AS total
				FROM mutasi_d a
				LEFT JOIN mutasi b ON a.mutasi_id = b.id
				LEFT JOIN manufaktur rp ON a.item_id = rp.manufaktur_id
				WHERE a.mutasi_id IN (".$array_id.")
				AND (warehouse_id1 != 2 AND warehouse_id2 != 2)
				AND b.status = 'Y'
			) AS dt 
			LEFT JOIN item i ON i.id = dt.itemo_id
			LEFT JOIN warehouse w ON w.id = dt.warehouse_id1
			LEFT JOIN warehouse w2 ON w2.id = dt.warehouse_id2
		";
		$query=$this->db->query($cSql);
		if ($query){	
			 $x =1;
			 $currid = 0;
			 foreach($query->result_array() as $row){
				 // 11 sloc to sloc 
				$cRow=array();
				
				if($currid ==0) $currid  = $row['YMBLNR_UHN'];
				if($currid != $row['YMBLNR_UHN']){
					$currid  = $row['YMBLNR_UHN'];
					$x = 1;
				}
				
				$cRow['EKGRP']=$row['EKGRP'];
				$cRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$cRow['YZEILE_UHN']=$x;
				$cRow['YID_UHN']=$row['YZEILE_UHN'];
				$cRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$cRow['BWART']=$row['BWART'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['LGORT']=$row['LGORT'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ERFMG']=$row['ERFMG'];
				$cRow['ERFME']=$row['ERFME'];
				$cRow['CHARG']=$row['CHARG'];
				$cRow['VFDAT']=$row['VFDAT'];
				//$cRow['KOSTL']=$row['KOSTL'];
				//$cRow['PRCTR']=$row['PRCTR'];
				$cRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"d.m.Y");
				$cRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$cRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$cRow['YRETURN_IND']=$row['YRETURN_IND'];
				$cRow['WAERS']=$row['WAERS'];
				$cRow['DMBTR']=$row['DMBTR'];
				$cRow['UMWRK']=$row['UMWRK'];
				//$cRow['SAKTO']=$row['SAKTO'];

				$tRow['EKGRP']=$row['EKGRP'];
				$tRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$tRow['YZEILE_UHN']=$x;
				$tRow['YID_UHN']=$row['YZEILE_UHN'];
				$tRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$tRow['BWART']=$row['BWART'];
				$tRow['WERKS']=$row['WERKS'];
				$tRow['LGORT']=$row['LGORT'];
				$tRow['MATNR']=$row['MATNR'];
				$tRow['ERFMG']=$row['ERFMG'];
				$tRow['ERFME']=$row['ERFME'];
				$tRow['CHARG']=$row['CHARG'];
				$tRow['VFDAT']=$row['VFDAT'];
				$tRow['KOSTL']=$row['KOSTL'];
				$tRow['PRCTR']=$row['PRCTR'];
				$tRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"Y-m-d");
				$tRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$tRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$tRow['YRETURN_IND']=$row['YRETURN_IND'];
				$tRow['WAERS']=$row['WAERS'];
				$tRow['DMBTR']=$row['DMBTR'];
				$tRow['STATUS']='Failed Send Data';
			
				$tRow['ACT'] = 'WHS From';

				//$this->db->insert('temp_materialncm', $tRow);

				$x++;
				
				$data[] = $cRow;
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return $data;

	}

	public function update_data_whs_from_by_array_id($array_id){
		$data=array();

		$cSql="
			SELECT
			i.purchasing_group AS EKGRP
			,dt.mutasi_id AS YMBLNR_UHN
			,dt.mutasi_d_id AS YZEILE_UHN
			,YEAR(dt.date) AS YMJAHR_UHN
			,'303' AS BWART
			,w.sap_code AS WERKS
			,CASE WHEN dt.warehouse_id1 = 2 THEN 'UHN2' ELSE 'UHN1' END AS LGORT
			,dt.itemo_id AS MATNR
			,dt.qty AS ERFMG
			,'PC' AS ERFME
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN 'BATCH' ELSE 'BATCH' END AS CHARG
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN '31.12.9999' ELSE '31.12.9999' END AS VFDAT
			,'1601112' AS KOSTL -- cost center
			,1601 AS PRCTR
			-- ,CONCAT(DAY(dt.date),'.',MONTH(dt.date),'.',YEAR(dt.date)) AS BUDAT_MKPF
			,dt.date AS BUDAT_MKPF
			,dt.jam AS CPUTM_MKPF
			,dt.createdby AS USNAM_MKPF
			,'01' AS YRETURN_IND
			,'IDR' AS WAERS
			,dt.hargao AS DMBTR
			-- ,'620209' AS SAKTO -- G/L Account
			,w2.sap_code AS UMWRK
			FROM (
				SELECT 
				a.mutasi_id
				,a.id AS mutasi_d_id
				,b.warehouse_id1 
				,b.warehouse_id2 
				,b.date
				,b.createdby
				,DATE_FORMAT(b.created,'%H%i%s') as jam
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS itemo_id
				,CASE WHEN rp.item_id IS NOT NULL THEN a.harga ELSE a.harga END AS hargao
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*a.qty ELSE a.qty END AS qty
				,CASE WHEN rp.item_id IS NOT NULL THEN a.harga*rp.qty*a.qty ELSE a.qty*a.harga END AS total
				FROM mutasi_d a
				LEFT JOIN mutasi b ON a.mutasi_id = b.id
				LEFT JOIN manufaktur rp ON a.item_id = rp.manufaktur_id
				WHERE a.mutasi_id IN (".$array_id.")
				AND (warehouse_id1 != 2 AND warehouse_id2 != 2)
				AND b.status = 'Y'
			) AS dt 
			LEFT JOIN item i ON i.id = dt.itemo_id
			LEFT JOIN warehouse w ON w.id = dt.warehouse_id1
			LEFT JOIN warehouse w2 ON w2.id = dt.warehouse_id2
		";
		$query=$this->db->query($cSql);
		if ($query){	
			 $x =1;
			 $currid = 0;
			 foreach($query->result_array() as $row){
				 // 11 sloc to sloc 
				$cRow=array();
				
				if($currid ==0) $currid  = $row['YMBLNR_UHN'];
				if($currid != $row['YMBLNR_UHN']){
					$currid  = $row['YMBLNR_UHN'];
					$x = 1;
				}
				
				$cRow['EKGRP']=$row['EKGRP'];
				$cRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$cRow['YZEILE_UHN']=$x;
				$cRow['YID_UHN']=$row['YZEILE_UHN'];
				$cRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$cRow['BWART']=$row['BWART'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['LGORT']=$row['LGORT'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ERFMG']=$row['ERFMG'];
				$cRow['ERFME']=$row['ERFME'];
				$cRow['CHARG']=$row['CHARG'];
				$cRow['VFDAT']=$row['VFDAT'];
				//$cRow['KOSTL']=$row['KOSTL'];
				//$cRow['PRCTR']=$row['PRCTR'];
				$cRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"d.m.Y");
				$cRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$cRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$cRow['YRETURN_IND']=$row['YRETURN_IND'];
				$cRow['WAERS']=$row['WAERS'];
				$cRow['DMBTR']=$row['DMBTR'];
				$cRow['UMWRK']=$row['UMWRK'];
				//$cRow['SAKTO']=$row['SAKTO'];

				$tRow['EKGRP']=$row['EKGRP'];
				$tRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$tRow['YZEILE_UHN']=$x;
				$tRow['YID_UHN']=$row['YZEILE_UHN'];
				$tRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$tRow['BWART']=$row['BWART'];
				$tRow['WERKS']=$row['WERKS'];
				$tRow['LGORT']=$row['LGORT'];
				$tRow['MATNR']=$row['MATNR'];
				$tRow['ERFMG']=$row['ERFMG'];
				$tRow['ERFME']=$row['ERFME'];
				$tRow['CHARG']=$row['CHARG'];
				$tRow['VFDAT']=$row['VFDAT'];
				$tRow['KOSTL']=$row['KOSTL'];
				$tRow['PRCTR']=$row['PRCTR'];
				$tRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"Y-m-d");
				$tRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$tRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$tRow['YRETURN_IND']=$row['YRETURN_IND'];
				$tRow['WAERS']=$row['WAERS'];
				$tRow['DMBTR']=$row['DMBTR'];
				$tRow['STATUS']='Failed Send Data';
			
				$tRow['ACT'] = 'WHS From';

				$data=array(
					'STATUS' => 'Success Send Data'
				);
					
				$this->db->update('temp_materialncm',$data,array('YID_UHN'=>$row['YZEILE_UHN'], 'ACT'=>'WHS From'));

				$x++;
				
				$data[] = $cRow;
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return true;

	}

	//13.	Material movement whs to whs (penerima) - inventory > Receive Whs Movement Stock (tabel mutasi – mutasi_d)
	public function getMvWhsTo($fromdate,$todate){
		$data=array();

		$cSql="
			SELECT
			i.purchasing_group AS EKGRP
			,dt.mutasi_id AS YMBLNR_UHN
			,dt.mutasi_d_id AS YZEILE_UHN
			,YEAR(dt.date) AS YMJAHR_UHN
			,'305' AS BWART
			,w.sap_code AS WERKS
			,CASE WHEN dt.warehouse_id2 = 2 THEN 'UHN2' ELSE 'UHN1' END AS LGORT
			,dt.itemo_id AS MATNR
			,dt.qty AS ERFMG
			,'PC' AS ERFME
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN 'BATCH' ELSE 'BATCH' END AS CHARG
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN '31.12.9999' ELSE '31.12.9999' END AS VFDAT
			,'1601112' AS KOSTL -- cost center
			,1601 AS PRCTR
			-- ,CONCAT(DAY(dt.date),'.',MONTH(dt.date),'.',YEAR(dt.date)) AS BUDAT_MKPF
			-- ,dt.date AS BUDAT_MKPF
			,dt.date_approved AS BUDAT_MKPF
			,dt.jam AS CPUTM_MKPF
			,dt.createdby AS USNAM_MKPF
			,'01' AS YRETURN_IND
			,'IDR' AS WAERS
			,dt.hargao AS DMBTR
			-- ,'620209' AS SAKTO -- G/L Account
			FROM (
				SELECT 
				a.mutasi_id
				,a.id AS mutasi_d_id
				,b.warehouse_id2 
				,b.date
				,b.approved AS date_approved
				,b.createdby
				,DATE_FORMAT(b.created,'%H%i%s') as jam
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS itemo_id
				,CASE WHEN rp.item_id IS NOT NULL THEN a.harga ELSE a.harga END AS hargao
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*a.qty ELSE a.qty END AS qty
				,CASE WHEN rp.item_id IS NOT NULL THEN a.harga*rp.qty*a.qty ELSE a.qty*a.harga END AS total
				FROM mutasi_d a
				LEFT JOIN mutasi b ON a.mutasi_id = b.id
				LEFT JOIN manufaktur rp ON a.item_id = rp.manufaktur_id
				-- WHERE b.date BETWEEN '".$fromdate."' AND '".$todate."'
				WHERE b.approved BETWEEN '".$fromdate."' AND '".$todate."'
				AND (warehouse_id1 != 2 AND warehouse_id2 != 2)
				AND b.status = 'Y'
				/*
				AND a.mutasi_id NOT IN (
					SELECT 
					a.mutasi_id
					FROM mutasi_d a
					LEFT JOIN mutasi b ON a.mutasi_id = b.id
					LEFT JOIN manufaktur rp ON a.item_id = rp.manufaktur_id
					WHERE b.date BETWEEN '".$fromdate."' AND '".$todate."'
					-- WHERE b.approved BETWEEN '2019-07-01' AND '2019-07-31'
					AND (warehouse_id1 != 2 AND warehouse_id2 != 2)
					AND b.status = 'Y'
					GROUP BY a.mutasi_id
				)
				*/
			) AS dt 
			LEFT JOIN item i ON i.id = dt.itemo_id
			LEFT JOIN warehouse w ON w.id = dt.warehouse_id2
			-- where dt.mutasi_id in (1476)
		";
		$query=$this->db->query($cSql);
		if ($query){	
			 $x =1;
			 $currid = 0;
			 foreach($query->result_array() as $row){
				 // 11 sloc to sloc 
				$cRow=array();
				
				if($currid ==0) $currid  = $row['YMBLNR_UHN'];
				if($currid != $row['YMBLNR_UHN']){
					$currid  = $row['YMBLNR_UHN'];
					$x = 1;
				}
				
				$cRow['EKGRP']=$row['EKGRP'];
				$cRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$cRow['YZEILE_UHN']=$x;
				$cRow['YID_UHN']=$row['YZEILE_UHN'];
				$cRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$cRow['BWART']=$row['BWART'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['LGORT']=$row['LGORT'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ERFMG']=$row['ERFMG'];
				$cRow['ERFME']=$row['ERFME'];
				$cRow['CHARG']=$row['CHARG'];
				$cRow['VFDAT']=$row['VFDAT'];
				$cRow['KOSTL']=$row['KOSTL'];
				$cRow['PRCTR']=$row['PRCTR'];
				$cRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"d.m.Y");
				$cRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$cRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$cRow['YRETURN_IND']=$row['YRETURN_IND'];
				$cRow['WAERS']=$row['WAERS'];
				$cRow['DMBTR']=$row['DMBTR'];
				$tRow['EKGRP']=$row['EKGRP'];
				$tRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$tRow['YZEILE_UHN']=$x;
				$tRow['YID_UHN']=$row['YZEILE_UHN'];
				$tRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$tRow['BWART']=$row['BWART'];
				$tRow['WERKS']=$row['WERKS'];
				$tRow['LGORT']=$row['LGORT'];
				$tRow['MATNR']=$row['MATNR'];
				$tRow['ERFMG']=$row['ERFMG'];
				$tRow['ERFME']=$row['ERFME'];
				$tRow['CHARG']=$row['CHARG'];
				$tRow['VFDAT']=$row['VFDAT'];
				$tRow['KOSTL']=$row['KOSTL'];
				$tRow['PRCTR']=$row['PRCTR'];
				$tRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"Y-m-d");
				$tRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$tRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$tRow['YRETURN_IND']=$row['YRETURN_IND'];
				$tRow['WAERS']=$row['WAERS'];
				$tRow['DMBTR']=$row['DMBTR'];
				$tRow['STATUS']='Failed Send Data';
			
				$tRow['ACT'] = 'WHS To';

				$this->db->where('YID_UHN', $row['YZEILE_UHN']);
				$this->db->where('ACT','WHS To');
				$q = $this->db->get('temp_materialncm');
			
				if ( $q->num_rows() > 0 ) 
				{
					
					$this->db->where('YID_UHN', $row['YZEILE_UHN']);
					$this->db->where('ACT','WHS To');
					$this->db->delete('temp_materialncm');

					$this->db->insert('temp_materialncm', $tRow);

				} else {

					$this->db->insert('temp_materialncm', $tRow);

				}

				$x++;
				$data[] = $cRow;
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return $data;

	}

	public function update_data_whs_to($fromdate,$todate){
		$data=array();

		$cSql="
			SELECT
			i.purchasing_group AS EKGRP
			,dt.mutasi_id AS YMBLNR_UHN
			,dt.mutasi_d_id AS YZEILE_UHN
			,YEAR(dt.date) AS YMJAHR_UHN
			,'305' AS BWART
			,w.sap_code AS WERKS
			,CASE WHEN dt.warehouse_id2 = 2 THEN 'UHN2' ELSE 'UHN1' END AS LGORT
			,dt.itemo_id AS MATNR
			,dt.qty AS ERFMG
			,'PC' AS ERFME
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN 'BATCH' ELSE 'BATCH' END AS CHARG
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN '31.12.9999' ELSE '31.12.9999' END AS VFDAT
			,'1601112' AS KOSTL -- cost center
			,1601 AS PRCTR
			-- ,CONCAT(DAY(dt.date),'.',MONTH(dt.date),'.',YEAR(dt.date)) AS BUDAT_MKPF
			-- ,dt.date AS BUDAT_MKPF
			,dt.date_approved AS BUDAT_MKPF
			,dt.jam AS CPUTM_MKPF
			,dt.createdby AS USNAM_MKPF
			,'01' AS YRETURN_IND
			,'IDR' AS WAERS
			,dt.hargao AS DMBTR
			-- ,'620209' AS SAKTO -- G/L Account
			FROM (
				SELECT 
				a.mutasi_id
				,a.id AS mutasi_d_id
				,b.warehouse_id2 
				,b.date
				,b.approved AS date_approved
				,b.createdby
				,DATE_FORMAT(b.created,'%H%i%s') as jam
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS itemo_id
				,CASE WHEN rp.item_id IS NOT NULL THEN a.harga ELSE a.harga END AS hargao
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*a.qty ELSE a.qty END AS qty
				,CASE WHEN rp.item_id IS NOT NULL THEN a.harga*rp.qty*a.qty ELSE a.qty*a.harga END AS total
				FROM mutasi_d a
				LEFT JOIN mutasi b ON a.mutasi_id = b.id
				LEFT JOIN manufaktur rp ON a.item_id = rp.manufaktur_id
				-- WHERE b.date BETWEEN '".$fromdate."' AND '".$todate."'
				WHERE b.approved BETWEEN '".$fromdate."' AND '".$todate."'
				AND (warehouse_id1 != 2 AND warehouse_id2 != 2)
				AND b.status = 'Y'
				/*
				AND a.mutasi_id NOT IN (
					SELECT 
					a.mutasi_id
					FROM mutasi_d a
					LEFT JOIN mutasi b ON a.mutasi_id = b.id
					LEFT JOIN manufaktur rp ON a.item_id = rp.manufaktur_id
					WHERE b.date BETWEEN '".$fromdate."' AND '".$todate."'
					-- WHERE b.approved BETWEEN '2019-07-01' AND '2019-07-31'
					AND (warehouse_id1 != 2 AND warehouse_id2 != 2)
					AND b.status = 'Y'
					GROUP BY a.mutasi_id
				)
				*/
			) AS dt 
			LEFT JOIN item i ON i.id = dt.itemo_id
			LEFT JOIN warehouse w ON w.id = dt.warehouse_id2
			-- where dt.mutasi_id in (1476)
		";
		$query=$this->db->query($cSql);
		if ($query){	
			 $x =1;
			 $currid = 0;
			 foreach($query->result_array() as $row){
				 // 11 sloc to sloc 
				$cRow=array();
				
				if($currid ==0) $currid  = $row['YMBLNR_UHN'];
				if($currid != $row['YMBLNR_UHN']){
					$currid  = $row['YMBLNR_UHN'];
					$x = 1;
				}
				
				$cRow['EKGRP']=$row['EKGRP'];
				$cRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$cRow['YZEILE_UHN']=$x;
				$cRow['YID_UHN']=$row['YZEILE_UHN'];
				$cRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$cRow['BWART']=$row['BWART'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['LGORT']=$row['LGORT'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ERFMG']=$row['ERFMG'];
				$cRow['ERFME']=$row['ERFME'];
				$cRow['CHARG']=$row['CHARG'];
				$cRow['VFDAT']=$row['VFDAT'];
				$cRow['KOSTL']=$row['KOSTL'];
				$cRow['PRCTR']=$row['PRCTR'];
				$cRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"d.m.Y");
				$cRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$cRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$cRow['YRETURN_IND']=$row['YRETURN_IND'];
				$cRow['WAERS']=$row['WAERS'];
				$cRow['DMBTR']=$row['DMBTR'];
				$tRow['EKGRP']=$row['EKGRP'];
				$tRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$tRow['YZEILE_UHN']=$x;
				$tRow['YID_UHN']=$row['YZEILE_UHN'];
				$tRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$tRow['BWART']=$row['BWART'];
				$tRow['WERKS']=$row['WERKS'];
				$tRow['LGORT']=$row['LGORT'];
				$tRow['MATNR']=$row['MATNR'];
				$tRow['ERFMG']=$row['ERFMG'];
				$tRow['ERFME']=$row['ERFME'];
				$tRow['CHARG']=$row['CHARG'];
				$tRow['VFDAT']=$row['VFDAT'];
				$tRow['KOSTL']=$row['KOSTL'];
				$tRow['PRCTR']=$row['PRCTR'];
				$tRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"Y-m-d");
				$tRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$tRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$tRow['YRETURN_IND']=$row['YRETURN_IND'];
				$tRow['WAERS']=$row['WAERS'];
				$tRow['DMBTR']=$row['DMBTR'];
				$tRow['STATUS']='Failed Send Data';
			
				$tRow['ACT'] = 'WHS To';

				$data=array(
					'STATUS' => 'Success Send Data'
				);

				$this->db->update('temp_materialncm',$data,array('YID_UHN'=>$row['YZEILE_UHN'], 'ACT'=>'WHS To'));

				$x++;
				$data[] = $cRow;
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return true;

	}

	public function get_data_whs_to_by_array_id($array_id){
		$data=array();

		$cSql="
			SELECT
			i.purchasing_group AS EKGRP
			,dt.mutasi_id AS YMBLNR_UHN
			,dt.mutasi_d_id AS YZEILE_UHN
			,YEAR(dt.date) AS YMJAHR_UHN
			,'305' AS BWART
			,w.sap_code AS WERKS
			,CASE WHEN dt.warehouse_id2 = 2 THEN 'UHN2' ELSE 'UHN1' END AS LGORT
			,dt.itemo_id AS MATNR
			,dt.qty AS ERFMG
			,'PC' AS ERFME
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN 'BATCH' ELSE 'BATCH' END AS CHARG
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN '31.12.9999' ELSE '31.12.9999' END AS VFDAT
			,'1601112' AS KOSTL -- cost center
			,1601 AS PRCTR
			-- ,CONCAT(DAY(dt.date),'.',MONTH(dt.date),'.',YEAR(dt.date)) AS BUDAT_MKPF
			-- ,dt.date AS BUDAT_MKPF
			,dt.date_approved AS BUDAT_MKPF
			,dt.jam AS CPUTM_MKPF
			,dt.createdby AS USNAM_MKPF
			,'01' AS YRETURN_IND
			,'IDR' AS WAERS
			,dt.hargao AS DMBTR
			-- ,'620209' AS SAKTO -- G/L Account
			FROM (
				SELECT 
				a.mutasi_id
				,a.id AS mutasi_d_id
				,b.warehouse_id2 
				,b.date
				,b.approved AS date_approved
				,b.createdby
				,DATE_FORMAT(b.created,'%H%i%s') as jam
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS itemo_id
				,CASE WHEN rp.item_id IS NOT NULL THEN a.harga ELSE a.harga END AS hargao
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*a.qty ELSE a.qty END AS qty
				,CASE WHEN rp.item_id IS NOT NULL THEN a.harga*rp.qty*a.qty ELSE a.qty*a.harga END AS total
				FROM mutasi_d a
				LEFT JOIN mutasi b ON a.mutasi_id = b.id
				LEFT JOIN manufaktur rp ON a.item_id = rp.manufaktur_id
				WHERE a.mutasi_id IN (".$array_id.")
				AND (warehouse_id1 != 2 AND warehouse_id2 != 2)
				AND b.status = 'Y'
			) AS dt 
			LEFT JOIN item i ON i.id = dt.itemo_id
			LEFT JOIN warehouse w ON w.id = dt.warehouse_id2
		";
		$query=$this->db->query($cSql);
		if ($query){	
			 $x =1;
			 $currid = 0;
			 foreach($query->result_array() as $row){
				 // 11 sloc to sloc 
				$cRow=array();
				
				if($currid ==0) $currid  = $row['YMBLNR_UHN'];
				if($currid != $row['YMBLNR_UHN']){
					$currid  = $row['YMBLNR_UHN'];
					$x = 1;
				}
				
				$cRow['EKGRP']=$row['EKGRP'];
				$cRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$cRow['YZEILE_UHN']=$x;
				$cRow['YID_UHN']=$row['YZEILE_UHN'];
				$cRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$cRow['BWART']=$row['BWART'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['LGORT']=$row['LGORT'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ERFMG']=$row['ERFMG'];
				$cRow['ERFME']=$row['ERFME'];
				$cRow['CHARG']=$row['CHARG'];
				$cRow['VFDAT']=$row['VFDAT'];
				$cRow['KOSTL']=$row['KOSTL'];
				$cRow['PRCTR']=$row['PRCTR'];
				$cRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"d.m.Y");
				$cRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$cRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$cRow['YRETURN_IND']=$row['YRETURN_IND'];
				$cRow['WAERS']=$row['WAERS'];
				$cRow['DMBTR']=$row['DMBTR'];
				$tRow['EKGRP']=$row['EKGRP'];
				$tRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$tRow['YZEILE_UHN']=$x;
				$tRow['YID_UHN']=$row['YZEILE_UHN'];
				$tRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$tRow['BWART']=$row['BWART'];
				$tRow['WERKS']=$row['WERKS'];
				$tRow['LGORT']=$row['LGORT'];
				$tRow['MATNR']=$row['MATNR'];
				$tRow['ERFMG']=$row['ERFMG'];
				$tRow['ERFME']=$row['ERFME'];
				$tRow['CHARG']=$row['CHARG'];
				$tRow['VFDAT']=$row['VFDAT'];
				$tRow['KOSTL']=$row['KOSTL'];
				$tRow['PRCTR']=$row['PRCTR'];
				$tRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"Y-m-d");
				$tRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$tRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$tRow['YRETURN_IND']=$row['YRETURN_IND'];
				$tRow['WAERS']=$row['WAERS'];
				$tRow['DMBTR']=$row['DMBTR'];
				$tRow['STATUS']='Failed Send Data';
			
				$tRow['ACT'] = 'WHS To';

				$x++;
				$data[] = $cRow;
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return $data;

	}

	public function update_data_whs_to_by_array_id($array_id){
		$data=array();

		$cSql="
			SELECT
			i.purchasing_group AS EKGRP
			,dt.mutasi_id AS YMBLNR_UHN
			,dt.mutasi_d_id AS YZEILE_UHN
			,YEAR(dt.date) AS YMJAHR_UHN
			,'305' AS BWART
			,w.sap_code AS WERKS
			,CASE WHEN dt.warehouse_id2 = 2 THEN 'UHN2' ELSE 'UHN1' END AS LGORT
			,dt.itemo_id AS MATNR
			,dt.qty AS ERFMG
			,'PC' AS ERFME
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN 'BATCH' ELSE 'BATCH' END AS CHARG
			,CASE WHEN i.material_group NOT IN ('UGIMMICK','UPROMAT','USTARTKIT','UTPACK') THEN '31.12.9999' ELSE '31.12.9999' END AS VFDAT
			,'1601112' AS KOSTL -- cost center
			,1601 AS PRCTR
			-- ,CONCAT(DAY(dt.date),'.',MONTH(dt.date),'.',YEAR(dt.date)) AS BUDAT_MKPF
			-- ,dt.date AS BUDAT_MKPF
			,dt.date_approved AS BUDAT_MKPF
			,dt.jam AS CPUTM_MKPF
			,dt.createdby AS USNAM_MKPF
			,'01' AS YRETURN_IND
			,'IDR' AS WAERS
			,dt.hargao AS DMBTR
			-- ,'620209' AS SAKTO -- G/L Account
			FROM (
				SELECT 
				a.mutasi_id
				,a.id AS mutasi_d_id
				,b.warehouse_id2 
				,b.date
				,b.approved AS date_approved
				,b.createdby
				,DATE_FORMAT(b.created,'%H%i%s') as jam
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE a.item_id END AS itemo_id
				,CASE WHEN rp.item_id IS NOT NULL THEN a.harga ELSE a.harga END AS hargao
				,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*a.qty ELSE a.qty END AS qty
				,CASE WHEN rp.item_id IS NOT NULL THEN a.harga*rp.qty*a.qty ELSE a.qty*a.harga END AS total
				FROM mutasi_d a
				LEFT JOIN mutasi b ON a.mutasi_id = b.id
				LEFT JOIN manufaktur rp ON a.item_id = rp.manufaktur_id
				WHERE a.mutasi_id IN (".$array_id.")
				AND (warehouse_id1 != 2 AND warehouse_id2 != 2)
				AND b.status = 'Y'
			) AS dt 
			LEFT JOIN item i ON i.id = dt.itemo_id
			LEFT JOIN warehouse w ON w.id = dt.warehouse_id2
		";
		$query=$this->db->query($cSql);
		if ($query){	
			 $x =1;
			 $currid = 0;
			 foreach($query->result_array() as $row){
				 // 11 sloc to sloc 
				$cRow=array();
				
				if($currid ==0) $currid  = $row['YMBLNR_UHN'];
				if($currid != $row['YMBLNR_UHN']){
					$currid  = $row['YMBLNR_UHN'];
					$x = 1;
				}
				
				$cRow['EKGRP']=$row['EKGRP'];
				$cRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$cRow['YZEILE_UHN']=$x;
				$cRow['YID_UHN']=$row['YZEILE_UHN'];
				$cRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$cRow['BWART']=$row['BWART'];
				$cRow['WERKS']=$row['WERKS'];
				$cRow['LGORT']=$row['LGORT'];
				$cRow['MATNR']=$row['MATNR'];
				$cRow['ERFMG']=$row['ERFMG'];
				$cRow['ERFME']=$row['ERFME'];
				$cRow['CHARG']=$row['CHARG'];
				$cRow['VFDAT']=$row['VFDAT'];
				$cRow['KOSTL']=$row['KOSTL'];
				$cRow['PRCTR']=$row['PRCTR'];
				$cRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"d.m.Y");
				$cRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$cRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$cRow['YRETURN_IND']=$row['YRETURN_IND'];
				$cRow['WAERS']=$row['WAERS'];
				$cRow['DMBTR']=$row['DMBTR'];
				$tRow['EKGRP']=$row['EKGRP'];
				$tRow['YMBLNR_UHN']=$row['YMBLNR_UHN'];
				$tRow['YZEILE_UHN']=$x;
				$tRow['YID_UHN']=$row['YZEILE_UHN'];
				$tRow['YMJAHR_UHN']=$row['YMJAHR_UHN'];
				$tRow['BWART']=$row['BWART'];
				$tRow['WERKS']=$row['WERKS'];
				$tRow['LGORT']=$row['LGORT'];
				$tRow['MATNR']=$row['MATNR'];
				$tRow['ERFMG']=$row['ERFMG'];
				$tRow['ERFME']=$row['ERFME'];
				$tRow['CHARG']=$row['CHARG'];
				$tRow['VFDAT']=$row['VFDAT'];
				$tRow['KOSTL']=$row['KOSTL'];
				$tRow['PRCTR']=$row['PRCTR'];
				$tRow['BUDAT_MKPF']= date_format(date_create($row['BUDAT_MKPF']),"Y-m-d");
				$tRow['CPUTM_MKPF'] = $row['CPUTM_MKPF'];
				$tRow['USNAM_MKPF']=$row['USNAM_MKPF'];
				$tRow['YRETURN_IND']=$row['YRETURN_IND'];
				$tRow['WAERS']=$row['WAERS'];
				$tRow['DMBTR']=$row['DMBTR'];
				$tRow['STATUS']='Failed Send Data';
			
				$tRow['ACT'] = 'WHS To';

				$data=array(
					'STATUS' => 'Success Send Data'
				);
					
				$this->db->update('temp_materialncm',$data,array('YID_UHN'=>$row['YZEILE_UHN'], 'ACT'=>'WHS To'));

				$x++;
				$data[] = $cRow;
			 }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return true;

	}

	public function delete_temp_ncm($data_arr){

		$this->db->where_in('YMBLNR_UHN', $data_arr);
		$this->db->delete('temp_materialncm');
		
		return true;
	}
 	
	public function test_json(){
		$data=array();
		
		/*
		[
			{
				"TCODE2_MKPF": "MIGO_TR",
				"MBLNR": 4901906158,
				"MJAHR": 2018,
				"XBLNR_MKPF": "whs to whs",
				"LINE_ID": 1,
				"BWART": 305,
				"SHKZG": "S",
				"WERKS": 1602,
				"LGORT": "UHN1",
				"MATNR": "BYI0017",
				"SGTXT": "Astaderm Set",
				"ERFMG": 2,
				"ERFME": "BOX",
				"CHARG": "CBFYC",
				"VFDAT": "30.06.2020",
				"WAERS": "IDR",
				"DMBTR": " -   ",
				"BUDAT_MKPF": "22.11.2018",
				"USNAM_MKP": "SIP_WHS13"
			}
		]
		*/
		
		$cRow=array();
		$cRow['TCODE2_MKPF']="MIGO_TR";
		$cRow['MBLNR']="4901906158";
		$cRow['MJAHR']="2018";
		$cRow['XBLNR_MKPF']="whs to whs";
		$cRow['LINE_ID']="1";
		$cRow['BWART']="305";
		$cRow['SHKZG']= "S";
		$cRow['WERKS']="1602";
		$cRow['LGORT']="UHN1";
		$cRow['MATNR']="BYI0017";
		$cRow['SGTXT']="Astaderm Set";
		$cRow['ERFMG']="2";
		$cRow['ERFME']="BOX";
		$cRow['CHARG']="CBFYC";
		$cRow['VFDAT']="30.06.2020";
		$cRow['WAERS']="IDR";
		$cRow['DMBTR']=" -   ";
		$cRow['BUDAT_MKPF']="22.11.2018";
		$cRow['USNAM_MKP']= "SIP_WHS13";
		$data[] = $cRow;
		
		return $data;
	}
 	
	
    
}
?>
