<?php
require APPPATH.'/libraries/REST_Controller.php';

class Mewallet extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | API view balance ewallet
    |--------------------------------------------------------------------------
    |
    | @author Budi Prayudi
	| @author Alfathin Hidayatulloh
	| @created 2020-02-10
	| @created 2019-02-20
    |
    */
    
    public function getBalanceEwalletDeposit($memberid,$fromdate,$todate){
		/* Untuk Stockiest */
        $data=array();
		$cSql = "
		SELECT  s.ewallet_stc_id AS doc_head
		,s.id 
		,m.nama AS name_cus
		, s.noref
		, s.description
		, s.created
		, DATE_FORMAT(s.created, '%Y-%m-%d') AS tgl
		, DATE_FORMAT(s.created, '%H:%i:%s') AS jam
		, s.member_id, 
		m.kota_id, acc.bank_id AS bank_id, k.warehouse_id, w.sap_code, 
		s.createdby, FORMAT(s.saldoawal, 0)AS fsaldoawal, FORMAT(s.kredit, 0)AS fkredit, 
		FORMAT(s.debet, 0)AS fdebet, FORMAT(s.saldoakhir, 0)AS fsaldoakhir
		FROM `ewallet_stc_d` `s`
		LEFT JOIN `member` `m` ON `m`.`id` = `s`.`member_id`
		LEFT JOIN `account` `acc` ON `acc`.`id` = `m`.`account_id`
		LEFT JOIN `kota` `k` ON `k`.`id` = `m`.`kota_id`
		LEFT JOIN `warehouse` `w` ON `w`.`id` = `k`.`warehouse_id`
		WHERE `s`.`member_id` <> 'company'
		AND s.description NOT IN('Request Order','RO From SC Payment')
		AND `s`.`created` >= '".$fromdate." 00:00:00' AND `s`.`created` <= '".$todate." 23:59:59'
		ORDER BY `s`.`id` ASC";
		
		$query=$this->db->query($cSql);
		if ($query){	
            foreach($query->result_array() as $row){
                $cat_cust='STOCKIST';
				$cat_doc='Error';
				
				$BankId = '';
				$cat_doc='Error';
				$pay_meth='TRANSFER';
				$cre_deb='CREDIT';

				$deposit_id = '-';
				$withdrawal_id = '-';
				
				if($row['description']=='Withdrawal'){
					$cekWithdrawal="SELECT withdrawal.remark, 
					withdrawal.remarkapp, withdrawal.flag,withdrawal.event_id  
					FROM withdrawal WHERE id='".$row['noref']."'
					AND status='approved' ";
					
					$cekWithdrawal="SELECT withdrawal.remark, 
					withdrawal.remarkapp, withdrawal.flag
					,upper(withdrawal.event_id) as event_id
					,case when upper(event_id) = 'WD1' then 'User Request via BCA'
						when upper(event_id) = 'WD2' then 'User Request via MANDIRI'
						when upper(event_id) = 'WD3' then 'Ongkos Kirim'
						when upper(event_id) = 'WD4' then 'Biaya Admin'
						when upper(event_id) = 'WD5' then 'Withdrawal Koreksi (Cash)'
						when upper(event_id) = 'WD6' then 'Withdrawal Koreksi (BCA)'
						when upper(event_id) = 'WD7' then 'Withdrawal Koreksi (MANDIRI)'
						end AS notewdw
					,case when upper(event_id) = 'WD1' then 'A4'
						when upper(event_id) = 'WD2' then 'A4'
						when upper(event_id) = 'WD3' then 'A5'
						when upper(event_id) = 'WD4' then 'A6'
						when upper(event_id) = 'WD5' then 'A3'
						when upper(event_id) = 'WD6' then 'A3'
						when upper(event_id) = 'WD7' then 'A3'
						end AS cat_doc
					,case when upper(event_id) = 'WD1' then 'BCA'
						when upper(event_id) = 'WD2' then 'MANDIRI'
						when upper(event_id) = 'WD3' then 'BCA'
						when upper(event_id) = 'WD4' then 'BCA'
						when upper(event_id) = 'WD5' then 'BCA'
						when upper(event_id) = 'WD6' then 'BCA'
						when upper(event_id) = 'WD7' then 'MANDIRI'
						end AS bank_id
					FROM withdrawal WHERE id='".$row['noref']."'
					AND flag = 'stc'
					-- AND status='approved' 
					";
					$qrWithdrawal=$this->db->query($cekWithdrawal);
					if ($qrWithdrawal){
						foreach($qrWithdrawal->result_array() as $rowWth){
							$cat_doc = $rowWth['cat_doc'];
							$BankId = $rowWth['bank_id'];
							
							if($rowWth['remarkapp'] == 'Auto Withdrawal Bonus'){
								$row['description'] = $rowWth['remarkapp'];
							}
							else if($rowWth['remarkapp'] == 'Auto Withdrawal Write-Off'){
								$row['description'] = $rowWth['remarkapp'];
								$cat_doc = 'A9';
							}
							else{
								$row['description'] = $rowWth['notewdw'];
							}
						}
					}else{
						$row['description']="Undefined";
					}

					$withdrawal_id = $row['noref'];

				}
				
				if ($row['description'] == 'Top Up STC') { $cat_doc='A1'; }
				else if (strpos($row['description'], 'Transfer') !== false) { $cat_doc='A2'; }
				else if (strpos($row['description'], 'Bonus') !== false) { $cat_doc='A7'; }
				else if ($row['description'] == 'Auto Withdrawal Bonus') { $cat_doc='A8'; }


				if ($row['description'] == 'Top Up STC') {
					$deposit_id = $row['noref'];
				}
				
				$pph21=0;
				$web_dmbtr=0;
				$web_dmbtr_cor=0;
				
				if (empty($row['fdebet'])){
					$cre_deb='CREDIT';
					$web_dmbtr= str_replace(",","",$row['fkredit']);
				}else{
					$cre_deb='DEBIT';
					$pph21=0;
					$web_dmbtr= str_replace(",","",$row['fdebet']);
				}
				
				if (($cat_doc=='A1')){
						// Bonus Title 999=pph21 
						$getBankId="SELECT * FROM deposit WHERE id = ".$row['noref'];
						$qrBankId=$this->db->query($getBankId);
						foreach($qrBankId->result_array() as $rowBankId){
							$BankId=$rowBankId['bank_id'];
							if($rowBankId['transfer']>0) $pay_meth='TRANSFER';
							else if($rowBankId['tunai']>0) $pay_meth='TRANSFER';
							else if($rowBankId['debit_card']>0) $pay_meth='DEBIT_CARD';
							else if($rowBankId['credit_card']>0) $pay_meth='CREDIT_CARD';
							
						}
						
						//tes, nanti harus di tambah bank id di SO admin
						if($BankId=='')$BankId='BCA';
				}
				
				$remark = '-';
				if ($cat_doc=='A1'){$remark = 'transaksi A1 - Top up';}
				else if ($cat_doc=='A2'){$remark = 'transaksi A2 - Transfer e-wallet';}
				else if ($cat_doc=='A3'){$remark = 'transaksi A3 - Withdrawal koreksi';}
				else if ($cat_doc=='A4'){$remark = 'transaksi A4 - User request';}
				else if ($cat_doc=='A5'){$remark = 'transaksi A5 - Ongkos Kirim';}
				else if ($cat_doc=='A6'){$remark = 'transaksi A6 - Biaya Admin';}
				else if ($cat_doc=='A7'){$remark = 'transaksi A7 - Bonus';}
				else if ($cat_doc=='A8'){$remark = 'transaksi A8 - Withdrawal bonus';}
				else if ($cat_doc=='A9'){$remark = 'transaksi A9 - withdrawal others';}
				
				if ($cat_doc<>"Error"){
					$cRow=array();
					$cRow['doc_head']=$row['doc_head'];
					$cRow['doc_det']=$row['id'];
					$cRow['cat_cust']=$cat_cust;
					$cRow['doc_date']=$row['tgl'];
					$cRow['doc_post']=$row['tgl'];
					$cRow['kunnr']=$row['member_id'];
					$cRow['name_cus']=$row['name_cus'];
					$cRow['deposit_id']=$deposit_id;
					$cRow['withdrawal_id']=$withdrawal_id;
					$cRow['cat_doc']=$cat_doc;
					$cRow['WERKS']=$row['sap_code'];
					$cRow['bank1']=$BankId;//$row['bank_id'];
					$cRow['pay_meth']=$pay_meth;
					$cRow['cre_deb']=$cre_deb;
					$cRow['PPh_21']=$pph21;
					$cRow['WEB_DMBTR_COR']=$web_dmbtr_cor;
					$cRow['web_dmbtr']=$web_dmbtr;
					$cRow['remark']=$cat_doc.' '.$row['description'];
					$cRow['web_CPUDT']=date("d.m.Y");
					$cRow['web_CPUTM']=date("H:i:s");

					$this->db->where('doc_det', $row['id']);
					$this->db->where('cat_cust','STOCKIST');
					$q = $this->db->get('temp_ewallet');
				
					if ( $q->num_rows() > 0 ) 
					{
						
					$this->db->where('doc_det', $row['id']);
					$this->db->where('cat_cust','STOCKIST');
					$this->db->delete('temp_ewallet');

					$this->db->insert('temp_ewallet', $cRow);

					} else {

						$this->db->insert('temp_ewallet', $cRow);

					}
				
					$data[] = $cRow;
				}
                
            }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return $data;
		
    }
	
    public function updateBalanceEwalletDeposit($memberid,$fromdate,$todate){
		/* Untuk Stockiest */
        $data=array();
		$cSql = "
		SELECT  s.ewallet_stc_id AS doc_head
		,s.id AS doc_det_id
		,m.nama AS name_cus
		, s.noref
		, s.description
		, s.created
		, DATE_FORMAT(s.created, '%d.%m.%Y') AS tgl
		, DATE_FORMAT(s.created, '%H:%i:%s') AS jam
		, s.member_id, 
		m.kota_id, acc.bank_id AS bank_id, k.warehouse_id, w.sap_code, 
		s.createdby, FORMAT(s.saldoawal, 0)AS fsaldoawal, FORMAT(s.kredit, 0)AS fkredit, 
		FORMAT(s.debet, 0)AS fdebet, FORMAT(s.saldoakhir, 0)AS fsaldoakhir
		FROM `ewallet_stc_d` `s`
		LEFT JOIN `member` `m` ON `m`.`id` = `s`.`member_id`
		LEFT JOIN `account` `acc` ON `acc`.`id` = `m`.`account_id`
		LEFT JOIN `kota` `k` ON `k`.`id` = `m`.`kota_id`
		LEFT JOIN `warehouse` `w` ON `w`.`id` = `k`.`warehouse_id`
		WHERE `s`.`member_id` <> 'company'
		AND s.description NOT IN('Request Order','RO From SC Payment')
		AND `s`.`created` >= '".$fromdate." 00:00:00' AND `s`.`created` <= '".$todate." 23:59:59'
		ORDER BY `s`.`id` ASC";
		
		$query=$this->db->query($cSql);
		if ($query){	
            foreach($query->result_array() as $row){

				$data=array(
					'status' => 'Success Send Data'
				);
					
				$this->db->update('temp_ewallet',$data,array('doc_det'=>$row['doc_det_id']));
                
			}

		}
		
		return true;
		
    }
	
    public function getBalanceEwallet($memberid,$fromdate,$todate){
    //public function getBalanceEwallet($memberid,$fromdate){
		/* Untuk Stockiest */
		$data=array();
		
		$cSql = "
		SELECT  s.ewallet_stc_id AS doc_head
			,s.id 
			,m.nama AS name_cus
			, s.noref
			, s.description
			, s.created
			, DATE_FORMAT(s.created, '%Y-%m-%d') AS tgl
			, DATE_FORMAT(s.created, '%H:%i:%s') AS jam
			, s.member_id, 
			m.kota_id, acc.bank_id AS bank_id, k.warehouse_id, w.sap_code, 
			s.createdby, FORMAT(s.saldoawal, 0)AS fsaldoawal, FORMAT(s.kredit, 0)AS fkredit, 
			FORMAT(s.debet, 0)AS fdebet, FORMAT(s.saldoakhir, 0)AS fsaldoakhir
		FROM `ewallet_stc_d` `s` 
		LEFT JOIN `member` `m` ON `m`.`id` = `s`.`member_id`
		LEFT JOIN `account` `acc` ON `acc`.`id` = `m`.`account_id`
		LEFT JOIN `kota` `k` ON `k`.`id` = `m`.`kota_id`
		LEFT JOIN `warehouse` `w` ON `w`.`id` = `k`.`warehouse_id`
		WHERE `s`.`member_id` <> 'company'
		AND s.description NOT IN('Request Order','RO From SC Payment')
		AND `s`.`created` >= '".$fromdate." 00:00:00' AND `s`.`created` <= '".$todate." 23:59:59'
		ORDER BY `s`.`id` ASC
		";
	
		$query=$this->db->query($cSql);
		if ($query){	
            foreach($query->result_array() as $row){
                $cat_cust='STOCKIST';
				$cat_doc='Error';
				
				$BankId = '';
				$cat_doc='Error';
				$pay_meth='TRANSFER';
				$cre_deb='CREDIT';
				
				if($row['description']=='Withdrawal'){
					$cekWithdrawal="SELECT withdrawal.remark, 
					withdrawal.remarkapp, withdrawal.flag,withdrawal.event_id  
					FROM withdrawal WHERE id='".$row['noref']."'
					AND status='approved' ";
					
					$cekWithdrawal="SELECT withdrawal.remark, 
					withdrawal.remarkapp, withdrawal.flag
					,upper(withdrawal.event_id) as event_id
					,case when upper(event_id) = 'WD1' then 'User Request via BCA'
						when upper(event_id) = 'WD2' then 'User Request via MANDIRI'
						when upper(event_id) = 'WD3' then 'Ongkos Kirim'
						when upper(event_id) = 'WD4' then 'Biaya Admin'
						when upper(event_id) = 'WD5' then 'Withdrawal Koreksi (Cash)'
						when upper(event_id) = 'WD6' then 'Withdrawal Koreksi (BCA)'
						when upper(event_id) = 'WD7' then 'Withdrawal Koreksi (MANDIRI)'
						end AS notewdw
					,case when upper(event_id) = 'WD1' then 'A4'
						when upper(event_id) = 'WD2' then 'A4'
						when upper(event_id) = 'WD3' then 'A5'
						when upper(event_id) = 'WD4' then 'A6'
						when upper(event_id) = 'WD5' then 'A3'
						when upper(event_id) = 'WD6' then 'A3'
						when upper(event_id) = 'WD7' then 'A3'
						end AS cat_doc
					,case when upper(event_id) = 'WD1' then 'BCA'
						when upper(event_id) = 'WD2' then 'MANDIRI'
						when upper(event_id) = 'WD3' then 'BCA'
						when upper(event_id) = 'WD4' then 'BCA'
						when upper(event_id) = 'WD5' then 'BCA'
						when upper(event_id) = 'WD6' then 'BCA'
						when upper(event_id) = 'WD7' then 'MANDIRI'
						end AS bank_id
					FROM withdrawal WHERE id='".$row['noref']."'
					AND flag = 'stc'
					-- AND status='approved' 
					";
					$qrWithdrawal=$this->db->query($cekWithdrawal);
					if ($qrWithdrawal){
						foreach($qrWithdrawal->result_array() as $rowWth){
							$cat_doc = $rowWth['cat_doc'];
							$BankId = $rowWth['bank_id'];
							
							if($rowWth['remarkapp'] == 'Auto Withdrawal Bonus'){
								$row['description'] = $rowWth['remarkapp'];
							}
							else if($rowWth['remarkapp'] == 'Auto Withdrawal Write-Off'){
								$row['description'] = $rowWth['remarkapp'];
								$cat_doc = 'A9';
							}
							else{
								$row['description'] = $rowWth['notewdw'];
							}
						}
					}else{
						$row['description']="Undefined";
					}
				}
				
				if ($row['description'] == 'Top Up STC') { $cat_doc='A1'; }
				else if (strpos($row['description'], 'Transfer') !== false) { $cat_doc='A2'; }
				else if (strpos($row['description'], 'Bonus') !== false) { $cat_doc='A7'; }
				else if ($row['description'] == 'Auto Withdrawal Bonus') { $cat_doc='A8'; }
				
				$pph21=0;
				$web_dmbtr=0;
				$web_dmbtr_cor=0;
				
				if (empty($row['fdebet'])){
					$cre_deb='CREDIT';
					$web_dmbtr= str_replace(",","",$row['fkredit']);
					/*
					if ($cat_doc='A7'){
						// Bonus Title 999=pph21 
						$bonPPH="SELECT bonus.periode, bonus.member_id, bonus.title, bonus.nominal FROM bonus WHERE bonus.title='999' AND bonus.periode 
						BETWEEN '".$fromdate."' AND '".$todate."' AND bonus.member_id='".$row['member_id']."'";
						$qrPph=$this->db->query($bonPPH);
						foreach($qrPph->result_array() as $rowPph){
							$pph21=$rowPph['nominal'];
						}
						//$pph21=10/100*$web_dmbtr; // Total dbmtr * 10%
					}
					*/
				}else{
					$cre_deb='DEBIT';
					$pph21=0;
					$web_dmbtr= str_replace(",","",$row['fdebet']);
				}
				
				if (($cat_doc=='A1')){
						// Bonus Title 999=pph21 
						$getBankId="SELECT * FROM deposit WHERE id = ".$row['noref'];
						$qrBankId=$this->db->query($getBankId);
						foreach($qrBankId->result_array() as $rowBankId){
							$BankId=$rowBankId['bank_id'];
							if($rowBankId['transfer']>0) $pay_meth='TRANSFER';
							else if($rowBankId['tunai']>0) $pay_meth='TRANSFER';
							else if($rowBankId['debit_card']>0) $pay_meth='DEBIT_CARD';
							else if($rowBankId['credit_card']>0) $pay_meth='CREDIT_CARD';
							
						}
						
						//tes, nanti harus di tambah bank id di SO admin
						if($BankId=='')$BankId='BCA';
				}
				
				$remark = '-';
				if ($cat_doc=='A1'){$remark = 'transaksi A1 - Top up';}
				else if ($cat_doc=='A2'){$remark = 'transaksi A2 - Transfer e-wallet';}
				else if ($cat_doc=='A3'){$remark = 'transaksi A3 - Withdrawal koreksi';}
				else if ($cat_doc=='A4'){$remark = 'transaksi A4 - User request';}
				else if ($cat_doc=='A5'){$remark = 'transaksi A5 - Ongkos Kirim';}
				else if ($cat_doc=='A6'){$remark = 'transaksi A6 - Biaya Admin';}
				else if ($cat_doc=='A7'){$remark = 'transaksi A7 - Bonus';}
				else if ($cat_doc=='A8'){$remark = 'transaksi A8 - Withdrawal bonus';}
				else if ($cat_doc=='A9'){$remark = 'transaksi A9 - withdrawal others';}
				
				if ($cat_doc<>"Error"){
					$cRow=array();
					$cRow['doc_head']=$row['doc_head'];
					$cRow['doc_det']=$row['id'];
					$cRow['noref'] = $row['noref'];
					$cRow['cat_cust']=$cat_cust;
					$cRow['doc_date']=$row['tgl'];
					$cRow['doc_post']=$row['tgl'];
					$cRow['kunnr']=$row['member_id'];
					$cRow['name_cus']=$row['name_cus'];
					$cRow['cat_doc']=$cat_doc;
					$cRow['WERKS']=$row['sap_code'];
					$cRow['bank1']=$BankId;//$row['bank_id'];
					$cRow['pay_meth']=$pay_meth;
					$cRow['cre_deb']=$cre_deb;
					$cRow['PPh_21']=$pph21;
					$cRow['WEB_DMBTR_COR']=$web_dmbtr_cor;
					$cRow['web_dmbtr']=$web_dmbtr;
					$cRow['remark']=$cat_doc.' '.$row['description'];
					$cRow['web_CPUDT']=date("d.m.Y");
					$cRow['web_CPUTM']=date("H:i:s");

					$data[] = $cRow;
				}
                
            }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return $data;
		
    }	
	
    //public function getBalanceEwallet_member($memberid,$fromdate){
    public function getBalanceEwallet_member($memberid,$fromdate,$todate){

		/* Untuk Member */
        $data=array();
		$cSql = "
		SELECT  s.ewallet_mem_id AS doc_head
			,s.id 
			,m.nama AS name_cus
			, s.noref
			, s.description
			, s.created
			, DATE_FORMAT(s.created, '%Y-%m-%d') AS tgl
			, DATE_FORMAT(LAST_DAY(s.created - INTERVAL 1 MONTH), '%d.%m.%Y') AS tglbackdate
			, DATE_FORMAT(LAST_DAY(s.created - INTERVAL 1 MONTH)+ INTERVAL 9 DAY, '%d.%m.%Y') AS tgla8
			, DATE_FORMAT(s.created, '%H:%i:%s') AS jam
			, s.member_id, 
			m.kota_id, acc.bank_id AS bank_id, k.warehouse_id, w.sap_code, 
			s.createdby, FORMAT(s.saldoawal, 0)AS fsaldoawal, FORMAT(s.kredit, 0) AS fkredit, 
			FORMAT(s.debet, 0)AS fdebet, FORMAT(s.saldoakhir, 0)AS fsaldoakhir
		FROM `ewallet_mem_d` `s` 
		LEFT JOIN `member` `m` ON `m`.`id` = `s`.`member_id`
		LEFT JOIN `account` `acc` ON `acc`.`id` = `m`.`account_id`
		LEFT JOIN `kota` `k` ON `k`.`id` = `m`.`kota_id`
		LEFT JOIN `warehouse` `w` ON `w`.`id` = `k`.`warehouse_id`
		WHERE `s`.`member_id` <> 'company'
		AND description NOT IN('SO STAFF', 'SO KIT', 'SO')
		AND `s`.`created` >= '".$fromdate." 00:00:00' AND `s`.`created` <= '".$todate." 23:59:59'
		ORDER BY `s`.`id` ASC
		";
		//echo $cSql;
		$query=$this->db->query($cSql);
		if ($query){	
            foreach($query->result_array() as $row){
                $cat_cust='MEMBER';
				//CEK Jika STAFF
				if ($row['member_id']=='STAFF'){
					$cat_cust='STAFF';
				}
				//default
				$BankId = '';
				$cat_doc='Error';
				$pay_meth='TRANSFER';
				$pph21=0;
				$web_dmbtr=0;
				$web_dmbtr_cor=0;

				$deposit_id = '-';
				$withdrawal_id = '-';
				
				if($row['description']=='Withdrawal'){
					$cekWithdrawal="SELECT withdrawal.remark, 
					withdrawal.remarkapp, withdrawal.flag
					,upper(withdrawal.event_id) as event_id
					,case when upper(event_id) = 'WD1' then 'User Request via BCA'
						when upper(event_id) = 'WD2' then 'User Request via MANDIRI'
						when upper(event_id) = 'WD3' then 'Ongkos Kirim'
						when upper(event_id) = 'WD4' then 'Biaya Admin'
						when upper(event_id) = 'WD5' then 'Withdrawal Koreksi (Cash)'
						when upper(event_id) = 'WD6' then 'Withdrawal Koreksi (BCA)'
						when upper(event_id) = 'WD7' then 'Withdrawal Koreksi (MANDIRI)'
						end AS notewdw
					,case when upper(event_id) = 'WD1' then 'A4'
						when upper(event_id) = 'WD2' then 'A4'
						when upper(event_id) = 'WD3' then 'A5'
						when upper(event_id) = 'WD4' then 'A6'
						when upper(event_id) = 'WD5' then 'A3'
						when upper(event_id) = 'WD6' then 'A3'
						when upper(event_id) = 'WD7' then 'A3'
						end AS cat_doc
					,case when upper(event_id) = 'WD1' then 'BCA'
						when upper(event_id) = 'WD2' then 'MANDIRI'
						when upper(event_id) = 'WD3' then 'BCA'
						when upper(event_id) = 'WD4' then 'BCA'
						when upper(event_id) = 'WD5' then 'BCA'
						when upper(event_id) = 'WD6' then 'BCA'
						when upper(event_id) = 'WD7' then 'MANDIRI'
						end AS bank_id
					,createdby
					FROM withdrawal WHERE id='".$row['noref']."'
					AND flag = 'mem'
					-- AND status='approved' 
					";
					$qrWithdrawal=$this->db->query($cekWithdrawal);
					if ($qrWithdrawal){
						foreach($qrWithdrawal->result_array() as $rowWth){
							$cat_doc = $rowWth['cat_doc'];
							$BankId = $rowWth['bank_id'];
							
							if($rowWth['remarkapp'] == 'Auto Withdrawal Bonus'){
								$row['description'] = $rowWth['remarkapp'];
							}

							else if($rowWth['remarkapp'] == 'Auto Withdrawal Write-Off'){
								$row['description'] = $rowWth['remarkapp'];
								$cat_doc = 'A9';
							}
							else{
								$row['description'] = $rowWth['notewdw'];
							}
							//$row['description']=$rowWth['remark'].'-'.$rowWth['remarkapp'];
							//$row['name_cus']=$row['name_cus'].''.substr_count($row['description'], 'Top Up');
						}
					}else{
						$cat_cust="Undefined";
					}
					
					//next harus diubah sp_withdrawal_auto noref nya malah ke withdrawal_list
					if($row['createdby'] == 'system' && $cat_doc != 'A9'){
							$row['description'] = 'Auto Withdrawal Bonus';
					}

					$withdrawal_id = $row['noref'];

				}

				if ($row['description'] == 'Top Up MEM') {
					$deposit_id = $row['noref'];
				}
				
				if(strpos($row['description'], ' ke (') !== false){
					//echo 'aaaaa';
					$cat_doc='A2';	
					$BankId = '';
					//echo $cat_doc;
					//echo $row['description'];
				}
				
				if(strpos($row['description'], ' ke Ewallet (') !== false){
					//echo 'aaaaa';
					$cat_doc='A2';	
					$BankId = '';
					//echo $cat_doc;
					//echo $row['description'];
				}


				// Test Logical Buat Menentukan Kategori.. 
				if ($row['description'] == 'Top Up MEM') { $cat_doc='A1'; }
				if (strpos($row['description'], 'Transfer') !== false) { $cat_doc='A2'; }
				if (substr($row['description'],0,5) == 'Bonus') { $cat_doc='A7'; }
				if ($row['description'] == 'Auto Withdrawal Bonus') { $cat_doc='A8'; }
				
				if (($cat_doc=='A1')){
						// Bonus Title 999=pph21 
						$getBankId="SELECT * FROM deposit WHERE id = ".$row['noref'];
						$qrBankId=$this->db->query($getBankId);
						foreach($qrBankId->result_array() as $rowBankId){
							$BankId=$rowBankId['bank_id'];
							if($rowBankId['transfer']>0) $pay_meth='TRANSFER';
							else if($rowBankId['tunai']>0) $pay_meth='TRANSFER';
							else if($rowBankId['debit_card']>0) $pay_meth='DEBIT_CARD';
							else if($rowBankId['credit_card']>0) $pay_meth='CREDIT_CARD';
							
						}
						
						//tes, nanti harus di tambah bank id di SO admin
						if($BankId=='')$BankId='BCA';
				}
				if (($cat_doc=='A8')){
					if($BankId=='')$BankId='BCA';
				}

				$doc_post = $row['tgl'];
				if (empty($row['fdebet'])){
					//$web_dmbtr= str_replace(",","",$row['fkredit']);
					
					$web_dmbtr= str_replace(",","",$row['fkredit']);
					$cre_deb='CREDIT';
					if ($cat_doc=='A7'){
						// Bonus Title 999=pph21 
						$bonPPH="SELECT bonus.periode, bonus.member_id, bonus.title, bonus.nominal 
								FROM bonus 
								WHERE bonus.title='999' 
								AND (bonus.periode = LAST_DAY('".$fromdate."' - INTERVAL 1 MONTH))
								AND bonus.member_id='".$row['member_id']."'";
						$qrPph=$this->db->query($bonPPH);
						foreach($qrPph->result_array() as $rowPph){
							$pph21=$rowPph['nominal']*-1;
						}
						$bonPotKor="SELECT bonus.periode, bonus.member_id, bonus.title, bonus.nominal 
								FROM bonus 
								WHERE bonus.title='994' 
								AND (bonus.periode = LAST_DAY('".$fromdate."' - INTERVAL 1 MONTH))
								AND bonus.member_id='".$row['member_id']."'";
						$qrPotKor=$this->db->query($bonPotKor);
						if($qrPotKor){
							foreach($qrPotKor->result_array() as $rowPotKor){
								$web_dmbtr_cor=$rowPotKor['nominal']*-1;
							}
						}
						//$pph21=10/100*$web_dmbtr; // Total dbmtr * 10%
						$web_dmbtr = $web_dmbtr + $pph21 + $web_dmbtr_cor;
						$doc_post = $row['tglbackdate'];
					}
					
				}else{
					$pph21 = 0;
					$web_dmbtr_cor = 0;
					$web_dmbtr = str_replace(",","",$row['fdebet']);
					$cre_deb = 'DEBIT';
				}
				
				$remark = '-';
				//echo $row['description']."<br>";
				if ($cat_doc=='A1'){$remark = 'transaksi A1 - Top up';}
				else if ($cat_doc=='A2'){$remark = 'transaksi A2 - Transfer e-wallet';}
				else if ($cat_doc=='A3'){$remark = 'transaksi A3 - Withdrawal koreksi';}
				else if ($cat_doc=='A4'){$remark = 'transaksi A4 - User request';}
				else if ($cat_doc=='A5'){$remark = 'transaksi A5 - Ongkos Kirim';}
				else if ($cat_doc=='A6'){$remark = 'transaksi A6 - Biaya Admin';}
				else if ($cat_doc=='A7'){$remark = 'transaksi A7 - Bonus';}
				else if ($cat_doc=='A8'){$remark = 'transaksi A8 - Withdrawal bonus';$doc_post = $row['tgla8'];}
				else if ($cat_doc=='A9'){$remark = 'transaksi A9 - withdrawal others';}

				$cRow=array();
				$cRow['doc_head']=$row['doc_head'];
				$cRow['doc_det']= $row['id'];
				$cRow['cat_cust']=$cat_cust;
				$cRow['doc_date']=$row['tgl'];
				$cRow['doc_post']=$doc_post;//$row['tgl'];
				$cRow['kunnr']=$row['member_id'];
				$cRow['name_cus']=$row['name_cus'];
				$cRow['deposit_id']=$deposit_id;
				$cRow['withdrawal_id']=$withdrawal_id;
				$cRow['cat_doc']=$cat_doc;
				$cRow['WERKS']=$row['sap_code'];
				$cRow['bank1']= $BankId; //$row['bank_id'];
				$cRow['pay_meth']=$pay_meth;//$row['id'];
				$cRow['cre_deb']=$cre_deb;
				$cRow['PPh_21']=$pph21;
				$cRow['WEB_DMBTR_COR']=$web_dmbtr_cor;
				$cRow['web_dmbtr']=$web_dmbtr;
				$cRow['remark']=$remark;//$cat_doc.' '.$row['description'];
				$cRow['web_CPUDT']=$row['tgl'];//date("d.m.Y");
				$cRow['web_CPUTM']=$row['jam'];//date("H:i:s");

				$this->db->where('doc_det', $row['id']);
				$this->db->where('cat_cust','MEMBER');
				$q = $this->db->get('temp_ewallet');
			 
				if ( $q->num_rows() > 0 ) 
				{
					
				   $this->db->where('doc_det', $row['id']);
				   $this->db->where('cat_cust','MEMBER');
				   $this->db->delete('temp_ewallet');

				   $this->db->insert('temp_ewallet', $cRow);

				} else {

					$this->db->insert('temp_ewallet', $cRow);

				}

				$data[] = $cRow;				
                
            }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
				
		return $data;
	}
	
    public function get_data_ewallet_mem_by_array_id($array_id){

		/* Untuk Member */
        $data=array();
		$cSql = "
		SELECT  s.ewallet_mem_id AS doc_head
			,s.id 
			,m.nama AS name_cus
			, s.noref
			, s.description
			, s.created
			, DATE_FORMAT(s.created, '%Y-%m-%d') AS tgl
			, DATE_FORMAT(LAST_DAY(s.created - INTERVAL 1 MONTH), '%d.%m.%Y') AS tglbackdate
			, DATE_FORMAT(LAST_DAY(s.created - INTERVAL 1 MONTH)+ INTERVAL 9 DAY, '%d.%m.%Y') AS tgla8
			, DATE_FORMAT(s.created, '%H:%i:%s') AS jam
			, s.member_id, 
			m.kota_id, acc.bank_id AS bank_id, k.warehouse_id, w.sap_code, 
			s.createdby, FORMAT(s.saldoawal, 0)AS fsaldoawal, FORMAT(s.kredit, 0) AS fkredit, 
			FORMAT(s.debet, 0)AS fdebet, FORMAT(s.saldoakhir, 0)AS fsaldoakhir
		FROM `ewallet_mem_d` `s` 
		LEFT JOIN `member` `m` ON `m`.`id` = `s`.`member_id`
		LEFT JOIN `account` `acc` ON `acc`.`id` = `m`.`account_id`
		LEFT JOIN `kota` `k` ON `k`.`id` = `m`.`kota_id`
		LEFT JOIN `warehouse` `w` ON `w`.`id` = `k`.`warehouse_id`
		WHERE `s`.`member_id` <> 'company'
		AND description NOT IN('SO STAFF', 'SO KIT', 'SO')
		AND s.ewallet_mem_id IN (".$array_id.")
		ORDER BY `s`.`id` ASC
		";
		//echo $cSql;

		$dateSql = "
		SELECT MIN(DATE_FORMAT(created, '%Y-%m-%d')) as fromdate FROM (
			Select s.ewallet_mem_id AS doc_head
					,s.id 
					,m.nama AS name_cus
					, s.noref
					, s.description
					, s.created
					, DATE_FORMAT(s.created, '%Y-%m-%d') AS tgl
					, DATE_FORMAT(LAST_DAY(s.created - INTERVAL 1 MONTH), '%d.%m.%Y') AS tglbackdate
					, DATE_FORMAT(LAST_DAY(s.created - INTERVAL 1 MONTH)+ INTERVAL 9 DAY, '%d.%m.%Y') AS tgla8
					, DATE_FORMAT(s.created, '%H:%i:%s') AS jam
					, s.member_id, 
					m.kota_id, acc.bank_id AS bank_id, k.warehouse_id, w.sap_code, 
					s.createdby, FORMAT(s.saldoawal, 0)AS fsaldoawal, FORMAT(s.kredit, 0) AS fkredit, 
					FORMAT(s.debet, 0)AS fdebet, FORMAT(s.saldoakhir, 0)AS fsaldoakhir
				FROM `ewallet_mem_d` `s` 
				LEFT JOIN `member` `m` ON `m`.`id` = `s`.`member_id`
				LEFT JOIN `account` `acc` ON `acc`.`id` = `m`.`account_id`
				LEFT JOIN `kota` `k` ON `k`.`id` = `m`.`kota_id`
				LEFT JOIN `warehouse` `w` ON `w`.`id` = `k`.`warehouse_id`
				WHERE `s`.`member_id` <> 'company'
				AND description NOT IN('SO STAFF', 'SO KIT', 'SO')
				AND s.ewallet_mem_id IN (".$array_id.")) as dp";
		
		$fromdate = '';
		$query_date=$this->db->query($dateSql);
		if ($query_date){
			$fromdate = $query_date->row('fromdate');
		}

		//var_dump($fromdate); die();


		$query=$this->db->query($cSql);
		if ($query){	
            foreach($query->result_array() as $row){
                $cat_cust='MEMBER';
				//CEK Jika STAFF
				if ($row['member_id']=='STAFF'){
					$cat_cust='STAFF';
				}
				//default
				$BankId = '';
				$cat_doc='Error';
				$pay_meth='TRANSFER';
				$pph21=0;
				$web_dmbtr=0;
				$web_dmbtr_cor=0;

				$deposit_id = '-';
				$withdrawal_id = '-';
				
				if($row['description']=='Withdrawal'){
					$cekWithdrawal="SELECT withdrawal.remark, 
					withdrawal.remarkapp, withdrawal.flag
					,upper(withdrawal.event_id) as event_id
					,case when upper(event_id) = 'WD1' then 'User Request via BCA'
						when upper(event_id) = 'WD2' then 'User Request via MANDIRI'
						when upper(event_id) = 'WD3' then 'Ongkos Kirim'
						when upper(event_id) = 'WD4' then 'Biaya Admin'
						when upper(event_id) = 'WD5' then 'Withdrawal Koreksi (Cash)'
						when upper(event_id) = 'WD6' then 'Withdrawal Koreksi (BCA)'
						when upper(event_id) = 'WD7' then 'Withdrawal Koreksi (MANDIRI)'
						end AS notewdw
					,case when upper(event_id) = 'WD1' then 'A4'
						when upper(event_id) = 'WD2' then 'A4'
						when upper(event_id) = 'WD3' then 'A5'
						when upper(event_id) = 'WD4' then 'A6'
						when upper(event_id) = 'WD5' then 'A3'
						when upper(event_id) = 'WD6' then 'A3'
						when upper(event_id) = 'WD7' then 'A3'
						end AS cat_doc
					,case when upper(event_id) = 'WD1' then 'BCA'
						when upper(event_id) = 'WD2' then 'MANDIRI'
						when upper(event_id) = 'WD3' then 'BCA'
						when upper(event_id) = 'WD4' then 'BCA'
						when upper(event_id) = 'WD5' then 'BCA'
						when upper(event_id) = 'WD6' then 'BCA'
						when upper(event_id) = 'WD7' then 'MANDIRI'
						end AS bank_id
					,createdby
					FROM withdrawal WHERE id='".$row['noref']."'
					AND flag = 'mem'
					-- AND status='approved' 
					";
					$qrWithdrawal=$this->db->query($cekWithdrawal);
					if ($qrWithdrawal){
						foreach($qrWithdrawal->result_array() as $rowWth){
							$cat_doc = $rowWth['cat_doc'];
							$BankId = $rowWth['bank_id'];
							
							if($rowWth['remarkapp'] == 'Auto Withdrawal Bonus'){
								$row['description'] = $rowWth['remarkapp'];
							}

							else if($rowWth['remarkapp'] == 'Auto Withdrawal Write-Off'){
								$row['description'] = $rowWth['remarkapp'];
								$cat_doc = 'A9';
							}
							else{
								$row['description'] = $rowWth['notewdw'];
							}
							//$row['description']=$rowWth['remark'].'-'.$rowWth['remarkapp'];
							//$row['name_cus']=$row['name_cus'].''.substr_count($row['description'], 'Top Up');
						}
					}else{
						$cat_cust="Undefined";
					}
					
					//next harus diubah sp_withdrawal_auto noref nya malah ke withdrawal_list
					if($row['createdby'] == 'system' && $cat_doc != 'A9'){
							$row['description'] = 'Auto Withdrawal Bonus';
					}

					$withdrawal_id = $row['noref'];

				}

				if ($row['description'] == 'Top Up MEM') {
					$deposit_id = $row['noref'];
				}
				
				if(strpos($row['description'], ' ke (') !== false){
					//echo 'aaaaa';
					$cat_doc='A2';	
					$BankId = '';
					//echo $cat_doc;
					//echo $row['description'];
				}
				
				if(strpos($row['description'], ' ke Ewallet (') !== false){
					//echo 'aaaaa';
					$cat_doc='A2';	
					$BankId = '';
					//echo $cat_doc;
					//echo $row['description'];
				}

				// Test Logical Buat Menentukan Kategori.. 
				if ($row['description'] == 'Top Up MEM') { $cat_doc='A1'; }
				if (strpos($row['description'], 'Transfer') !== false) { $cat_doc='A2'; }
				if (substr($row['description'],0,5) == 'Bonus') { $cat_doc='A7'; }
				if ($row['description'] == 'Auto Withdrawal Bonus') { $cat_doc='A8'; }
				
				if (($cat_doc=='A1')){
						// Bonus Title 999=pph21 
						$getBankId="SELECT * FROM deposit WHERE id = ".$row['noref'];
						$qrBankId=$this->db->query($getBankId);
						foreach($qrBankId->result_array() as $rowBankId){
							$BankId=$rowBankId['bank_id'];
							if($rowBankId['transfer']>0) $pay_meth='TRANSFER';
							else if($rowBankId['tunai']>0) $pay_meth='TRANSFER';
							else if($rowBankId['debit_card']>0) $pay_meth='DEBIT_CARD';
							else if($rowBankId['credit_card']>0) $pay_meth='CREDIT_CARD';
							
						}
						
						//tes, nanti harus di tambah bank id di SO admin
						if($BankId=='')$BankId='BCA';
				}
				if (($cat_doc=='A8')){
					if($BankId=='')$BankId='BCA';
				}

				$date = date('m-d-Y');
				$date1 = str_replace('-', '/', $date);
				$fromdate = date('Y-m-d',strtotime($date1 . "-1 days"));
				
				$doc_post = $row['tgl'];
				if (empty($row['fdebet'])){
					//$web_dmbtr= str_replace(",","",$row['fkredit']);
					
					$web_dmbtr= str_replace(",","",$row['fkredit']);
					$cre_deb='CREDIT';
					if ($cat_doc=='A7'){
						// Bonus Title 999=pph21 
						$bonPPH="SELECT bonus.periode, bonus.member_id, bonus.title, bonus.nominal 
								FROM bonus 
								WHERE bonus.title='999' 
								AND (bonus.periode = LAST_DAY('".$fromdate."' - INTERVAL 1 MONTH))
								AND bonus.member_id='".$row['member_id']."'";
						$qrPph=$this->db->query($bonPPH);
						foreach($qrPph->result_array() as $rowPph){
							$pph21=$rowPph['nominal']*-1;
						}
						$bonPotKor="SELECT bonus.periode, bonus.member_id, bonus.title, bonus.nominal 
								FROM bonus 
								WHERE bonus.title='994' 
								AND (bonus.periode = LAST_DAY('".$fromdate."' - INTERVAL 1 MONTH))
								AND bonus.member_id='".$row['member_id']."'";
						$qrPotKor=$this->db->query($bonPotKor);
						if($qrPotKor){
							foreach($qrPotKor->result_array() as $rowPotKor){
								$web_dmbtr_cor=$rowPotKor['nominal']*-1;
							}
						}
						//$pph21=10/100*$web_dmbtr; // Total dbmtr * 10%
						$web_dmbtr = $web_dmbtr + $pph21 + $web_dmbtr_cor;
						$doc_post = $row['tglbackdate'];
					}
					
				}else{
					$pph21 = 0;
					$web_dmbtr_cor = 0;
					$web_dmbtr = str_replace(",","",$row['fdebet']);
					$cre_deb = 'DEBIT';
				}
				
				$remark = '-';
				//echo $row['description']."<br>";
				if ($cat_doc=='A1'){$remark = 'transaksi A1 - Top up';}
				else if ($cat_doc=='A2'){$remark = 'transaksi A2 - Transfer e-wallet';}
				else if ($cat_doc=='A3'){$remark = 'transaksi A3 - Withdrawal koreksi';}
				else if ($cat_doc=='A4'){$remark = 'transaksi A4 - User request';}
				else if ($cat_doc=='A5'){$remark = 'transaksi A5 - Ongkos Kirim';}
				else if ($cat_doc=='A6'){$remark = 'transaksi A6 - Biaya Admin';}
				else if ($cat_doc=='A7'){$remark = 'transaksi A7 - Bonus';}
				else if ($cat_doc=='A8'){$remark = 'transaksi A8 - Withdrawal bonus';$doc_post = $row['tgla8'];}
				else if ($cat_doc=='A9'){$remark = 'transaksi A9 - withdrawal others';}

				$cRow=array();
				$cRow['doc_head']=$row['doc_head'];
				$cRow['doc_det']= $row['id'];
				$cRow['cat_cust']=$cat_cust;
				$cRow['doc_date']=$row['tgl'];
				$cRow['doc_post']=$doc_post;//$row['tgl'];
				$cRow['kunnr']=$row['member_id'];
				$cRow['name_cus']=$row['name_cus'];
				$cRow['deposit_id']=$deposit_id;
				$cRow['withdrawal_id']=$withdrawal_id;
				$cRow['cat_doc']=$cat_doc;
				$cRow['WERKS']=$row['sap_code'];
				$cRow['bank1']= $BankId; //$row['bank_id'];
				$cRow['pay_meth']=$pay_meth;//$row['id'];
				$cRow['cre_deb']=$cre_deb;
				$cRow['PPh_21']=$pph21;
				$cRow['WEB_DMBTR_COR']=$web_dmbtr_cor;
				$cRow['web_dmbtr']=$web_dmbtr;
				$cRow['remark']=$remark;//$cat_doc.' '.$row['description'];
				$cRow['web_CPUDT']=$row['tgl'];//date("d.m.Y");
				$cRow['web_CPUTM']=$row['jam'];//date("H:i:s");

				$this->db->where('doc_det', $row['id']);
				$this->db->where('cat_cust','MEMBER');
				$q = $this->db->get('temp_ewallet');
			 
				if ( $q->num_rows() > 0 ) 
				{
					
				   $this->db->where('doc_det', $row['id']);
				   $this->db->where('cat_cust','MEMBER');
				   $this->db->delete('temp_ewallet');

				   $this->db->insert('temp_ewallet', $cRow);

				} else {

					$this->db->insert('temp_ewallet', $cRow);

				}

				$data[] = $cRow;				
                
            }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
				
		return $data;
	}

    public function update_data_ewallet_mem_by_array_id($array_id){

		/* Untuk Member */
        $data=array();
		$cSql = "
<<<<<<< HEAD
		SELECT  s.ewallet_stc_id AS doc_head
		,s.id 
		,m.nama AS name_cus
		, s.noref
		, s.description
		, s.created
		, DATE_FORMAT(s.created, '%Y-%m-%d') AS tgl
		, DATE_FORMAT(s.created, '%H:%i:%s') AS jam
		, s.member_id, 
		m.kota_id, acc.bank_id AS bank_id, k.warehouse_id, w.sap_code, 
		s.createdby, FORMAT(s.saldoawal, 0)AS fsaldoawal, FORMAT(s.kredit, 0)AS fkredit, 
		FORMAT(s.debet, 0)AS fdebet, FORMAT(s.saldoakhir, 0)AS fsaldoakhir
		FROM `ewallet_stc_d` `s`
=======
		SELECT  s.ewallet_mem_id AS doc_head
			,s.id 
			,m.nama AS name_cus
			, s.noref
			, s.description
			, s.created
			, DATE_FORMAT(s.created, '%d.%m.%Y') AS tgl
			, DATE_FORMAT(LAST_DAY(s.created - INTERVAL 1 MONTH), '%d.%m.%Y') AS tglbackdate
			, DATE_FORMAT(LAST_DAY(s.created - INTERVAL 1 MONTH)+ INTERVAL 9 DAY, '%d.%m.%Y') AS tgla8
			, DATE_FORMAT(s.created, '%H:%i:%s') AS jam
			, s.member_id, 
			m.kota_id, acc.bank_id AS bank_id, k.warehouse_id, w.sap_code, 
			s.createdby, FORMAT(s.saldoawal, 0)AS fsaldoawal, FORMAT(s.kredit, 0) AS fkredit, 
			FORMAT(s.debet, 0)AS fdebet, FORMAT(s.saldoakhir, 0)AS fsaldoakhir
		FROM `ewallet_mem_d` `s` 
>>>>>>> master
		LEFT JOIN `member` `m` ON `m`.`id` = `s`.`member_id`
		LEFT JOIN `account` `acc` ON `acc`.`id` = `m`.`account_id`
		LEFT JOIN `kota` `k` ON `k`.`id` = `m`.`kota_id`
		LEFT JOIN `warehouse` `w` ON `w`.`id` = `k`.`warehouse_id`
		WHERE `s`.`member_id` <> 'company'
		AND description NOT IN('SO STAFF', 'SO KIT', 'SO')
		AND s.ewallet_mem_id IN (".$array_id.")
		ORDER BY `s`.`id` ASC
		";
		//echo $cSql;
		$query=$this->db->query($cSql);
		if ($query){	
            foreach($query->result_array() as $row){
                $cat_cust='MEMBER';
				//CEK Jika STAFF
				if ($row['member_id']=='STAFF'){
					$cat_cust='STAFF';
				}
				//default
				$BankId = '';
				$cat_doc='Error';
				$pay_meth='TRANSFER';
				$pph21=0;
				$web_dmbtr=0;
				$web_dmbtr_cor=0;

				$deposit_id = '-';
				$withdrawal_id = '-';
				
				if($row['description']=='Withdrawal'){
					$cekWithdrawal="SELECT withdrawal.remark, 
					withdrawal.remarkapp, withdrawal.flag
					,upper(withdrawal.event_id) as event_id
					,case when upper(event_id) = 'WD1' then 'User Request via BCA'
						when upper(event_id) = 'WD2' then 'User Request via MANDIRI'
						when upper(event_id) = 'WD3' then 'Ongkos Kirim'
						when upper(event_id) = 'WD4' then 'Biaya Admin'
						when upper(event_id) = 'WD5' then 'Withdrawal Koreksi (Cash)'
						when upper(event_id) = 'WD6' then 'Withdrawal Koreksi (BCA)'
						when upper(event_id) = 'WD7' then 'Withdrawal Koreksi (MANDIRI)'
						end AS notewdw
					,case when upper(event_id) = 'WD1' then 'A4'
						when upper(event_id) = 'WD2' then 'A4'
						when upper(event_id) = 'WD3' then 'A5'
						when upper(event_id) = 'WD4' then 'A6'
						when upper(event_id) = 'WD5' then 'A3'
						when upper(event_id) = 'WD6' then 'A3'
						when upper(event_id) = 'WD7' then 'A3'
						end AS cat_doc
					,case when upper(event_id) = 'WD1' then 'BCA'
						when upper(event_id) = 'WD2' then 'MANDIRI'
						when upper(event_id) = 'WD3' then 'BCA'
						when upper(event_id) = 'WD4' then 'BCA'
						when upper(event_id) = 'WD5' then 'BCA'
						when upper(event_id) = 'WD6' then 'BCA'
						when upper(event_id) = 'WD7' then 'MANDIRI'
						end AS bank_id
					,createdby
					FROM withdrawal WHERE id='".$row['noref']."'
					AND flag = 'mem'
					-- AND status='approved' 
					";
					$qrWithdrawal=$this->db->query($cekWithdrawal);
					if ($qrWithdrawal){
						foreach($qrWithdrawal->result_array() as $rowWth){
							$cat_doc = $rowWth['cat_doc'];
							$BankId = $rowWth['bank_id'];
							
							if($rowWth['remarkapp'] == 'Auto Withdrawal Bonus'){
								$row['description'] = $rowWth['remarkapp'];
							}

							else if($rowWth['remarkapp'] == 'Auto Withdrawal Write-Off'){
								$row['description'] = $rowWth['remarkapp'];
								$cat_doc = 'A9';
							}
							else{
								$row['description'] = $rowWth['notewdw'];
							}
							//$row['description']=$rowWth['remark'].'-'.$rowWth['remarkapp'];
							//$row['name_cus']=$row['name_cus'].''.substr_count($row['description'], 'Top Up');
						}
					}else{
						$cat_cust="Undefined";
					}
					
					//next harus diubah sp_withdrawal_auto noref nya malah ke withdrawal_list
					if($row['createdby'] == 'system' && $cat_doc != 'A9'){
							$row['description'] = 'Auto Withdrawal Bonus';
					}

					$withdrawal_id = $row['noref'];

				}

				if ($row['description'] == 'Top Up MEM') {
					$deposit_id = $row['noref'];
				}
				
				if(strpos($row['description'], ' ke (') !== false){
					//echo 'aaaaa';
					$cat_doc='A2';	
					$BankId = '';
					//echo $cat_doc;
					//echo $row['description'];
				}
				
				if(strpos($row['description'], ' ke Ewallet (') !== false){
					//echo 'aaaaa';
					$cat_doc='A2';	
					$BankId = '';
					//echo $cat_doc;
					//echo $row['description'];
				}

				// Test Logical Buat Menentukan Kategori.. 
				if ($row['description'] == 'Top Up MEM') { $cat_doc='A1'; }
				if (strpos($row['description'], 'Transfer') !== false) { $cat_doc='A2'; }
				if (substr($row['description'],0,5) == 'Bonus') { $cat_doc='A7'; }
				if ($row['description'] == 'Auto Withdrawal Bonus') { $cat_doc='A8'; }
				
				if (($cat_doc=='A1')){
						// Bonus Title 999=pph21 
						$getBankId="SELECT * FROM deposit WHERE id = ".$row['noref'];
						$qrBankId=$this->db->query($getBankId);
						foreach($qrBankId->result_array() as $rowBankId){
							$BankId=$rowBankId['bank_id'];
							if($rowBankId['transfer']>0) $pay_meth='TRANSFER';
							else if($rowBankId['tunai']>0) $pay_meth='TRANSFER';
							else if($rowBankId['debit_card']>0) $pay_meth='DEBIT_CARD';
							else if($rowBankId['credit_card']>0) $pay_meth='CREDIT_CARD';
							
						}
						
						//tes, nanti harus di tambah bank id di SO admin
						if($BankId=='')$BankId='BCA';
				}
				if (($cat_doc=='A8')){
					if($BankId=='')$BankId='BCA';
				}

				$date = date('m-d-Y');
				$date1 = str_replace('-', '/', $date);
				$fromdate = date('Y-m-d',strtotime($date1 . "-1 days"));

				$doc_post = $row['tgl'];
				if (empty($row['fdebet'])){
					//$web_dmbtr= str_replace(",","",$row['fkredit']);
					
					$web_dmbtr= str_replace(",","",$row['fkredit']);
					$cre_deb='CREDIT';
					if ($cat_doc=='A7'){
						// Bonus Title 999=pph21 
						$bonPPH="SELECT bonus.periode, bonus.member_id, bonus.title, bonus.nominal 
								FROM bonus 
								WHERE bonus.title='999' 
								AND (bonus.periode = LAST_DAY('".$fromdate."' - INTERVAL 1 MONTH))
								AND bonus.member_id='".$row['member_id']."'";
						$qrPph=$this->db->query($bonPPH);
						foreach($qrPph->result_array() as $rowPph){
							$pph21=$rowPph['nominal']*-1;
						}
						$bonPotKor="SELECT bonus.periode, bonus.member_id, bonus.title, bonus.nominal 
								FROM bonus 
								WHERE bonus.title='994' 
								AND (bonus.periode = LAST_DAY('".$fromdate."' - INTERVAL 1 MONTH))
								AND bonus.member_id='".$row['member_id']."'";
						$qrPotKor=$this->db->query($bonPotKor);
						if($qrPotKor){
							foreach($qrPotKor->result_array() as $rowPotKor){
								$web_dmbtr_cor=$rowPotKor['nominal']*-1;
							}
						}
						//$pph21=10/100*$web_dmbtr; // Total dbmtr * 10%
						$web_dmbtr = $web_dmbtr + $pph21 + $web_dmbtr_cor;
						$doc_post = $row['tglbackdate'];
					}
					
				}else{
					$pph21 = 0;
					$web_dmbtr_cor = 0;
					$web_dmbtr = str_replace(",","",$row['fdebet']);
					$cre_deb = 'DEBIT';
				}
				
				$remark = '-';
				//echo $row['description']."<br>";
				if ($cat_doc=='A1'){$remark = 'transaksi A1 - Top up';}
				else if ($cat_doc=='A2'){$remark = 'transaksi A2 - Transfer e-wallet';}
				else if ($cat_doc=='A3'){$remark = 'transaksi A3 - Withdrawal koreksi';}
				else if ($cat_doc=='A4'){$remark = 'transaksi A4 - User request';}
				else if ($cat_doc=='A5'){$remark = 'transaksi A5 - Ongkos Kirim';}
				else if ($cat_doc=='A6'){$remark = 'transaksi A6 - Biaya Admin';}
				else if ($cat_doc=='A7'){$remark = 'transaksi A7 - Bonus';}
				else if ($cat_doc=='A8'){$remark = 'transaksi A8 - Withdrawal bonus';$doc_post = $row['tgla8'];}
				else if ($cat_doc=='A9'){$remark = 'transaksi A9 - withdrawal others';}

				$cRow=array();
				$cRow['doc_head']=$row['doc_head'];
				$cRow['doc_det']= $row['id'];
				$cRow['cat_cust']=$cat_cust;
				$cRow['doc_date']=$row['tgl'];
				$cRow['doc_post']=$doc_post;//$row['tgl'];
				$cRow['kunnr']=$row['member_id'];
				$cRow['name_cus']=$row['name_cus'];
				$cRow['deposit_id']=$deposit_id;
				$cRow['withdrawal_id']=$withdrawal_id;
				$cRow['cat_doc']=$cat_doc;
				$cRow['WERKS']=$row['sap_code'];
				$cRow['bank1']= $BankId; //$row['bank_id'];
				$cRow['pay_meth']=$pay_meth;//$row['id'];
				$cRow['cre_deb']=$cre_deb;
				$cRow['PPh_21']=$pph21;
				$cRow['WEB_DMBTR_COR']=$web_dmbtr_cor;
				$cRow['web_dmbtr']=$web_dmbtr;
				$cRow['remark']=$remark;//$cat_doc.' '.$row['description'];
				$cRow['web_CPUDT']=$row['tgl'];//date("d.m.Y");
				$cRow['web_CPUTM']=$row['jam'];//date("H:i:s");

				$data=array(
					'status' => 'Success Send Data'
				);
					
				$this->db->update('temp_ewallet',$data,array('doc_det'=>$row['id'], 'cat_cust'=>'MEMBER'));

				$data[] = $cRow;				
                
            }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
				
		return true;
	}

    public function updateBalanceEwallet_member($memberid,$fromdate,$todate){

		/* Untuk Member */
        $data=array();
		$cSql = "
		SELECT  s.ewallet_mem_id AS doc_head
			,s.id AS doc_det_id
			,m.nama AS name_cus
			, s.noref
			, s.description
			, s.created
			, DATE_FORMAT(s.created, '%d.%m.%Y') AS tgl
			, DATE_FORMAT(LAST_DAY(s.created - INTERVAL 1 MONTH), '%d.%m.%Y') AS tglbackdate
			, DATE_FORMAT(LAST_DAY(s.created - INTERVAL 1 MONTH)+ INTERVAL 9 DAY, '%d.%m.%Y') AS tgla8
			, DATE_FORMAT(s.created, '%H:%i:%s') AS jam
			, s.member_id, 
			m.kota_id, acc.bank_id AS bank_id, k.warehouse_id, w.sap_code, 
			s.createdby, FORMAT(s.saldoawal, 0)AS fsaldoawal, FORMAT(s.kredit, 0) AS fkredit, 
			FORMAT(s.debet, 0)AS fdebet, FORMAT(s.saldoakhir, 0)AS fsaldoakhir
		FROM `ewallet_mem_d` `s` 
		LEFT JOIN `member` `m` ON `m`.`id` = `s`.`member_id`
		LEFT JOIN `account` `acc` ON `acc`.`id` = `m`.`account_id`
		LEFT JOIN `kota` `k` ON `k`.`id` = `m`.`kota_id`
		LEFT JOIN `warehouse` `w` ON `w`.`id` = `k`.`warehouse_id`
		WHERE `s`.`member_id` <> 'company'
		AND description NOT IN('SO STAFF', 'SO KIT', 'SO')
		AND `s`.`created` >= '".$fromdate." 00:00:00' AND `s`.`created` <= '".$todate." 23:59:59'
		ORDER BY `s`.`id` ASC
		";
		//echo $cSql;
		$query=$this->db->query($cSql);

		if ($query){	
			
            foreach($query->result_array() as $row){

				$data=array(
					'status' => 'Success Send Data'
				);
					
				$this->db->update('temp_ewallet',$data,array('doc_det'=>$row['doc_det_id']));
                
			}
			
		}
		
		return true;
	}

 	public function get_data_ewallet_stc_by_array_id($array_id){
		
		/* Untuk Stockiest */
        $data=array();
		$cSql = "
		SELECT  s.ewallet_stc_id AS doc_head
		,s.id 
		,m.nama AS name_cus
		, s.noref
		, s.description
		, s.created
		, DATE_FORMAT(s.created, '%d.%m.%Y') AS tgl
		, DATE_FORMAT(s.created, '%H:%i:%s') AS jam
		, s.member_id, 
		m.kota_id, acc.bank_id AS bank_id, k.warehouse_id, w.sap_code, 
		s.createdby, FORMAT(s.saldoawal, 0)AS fsaldoawal, FORMAT(s.kredit, 0)AS fkredit, 
		FORMAT(s.debet, 0)AS fdebet, FORMAT(s.saldoakhir, 0)AS fsaldoakhir
		FROM `ewallet_stc_d` `s`
		LEFT JOIN `member` `m` ON `m`.`id` = `s`.`member_id`
		LEFT JOIN `account` `acc` ON `acc`.`id` = `m`.`account_id`
		LEFT JOIN `kota` `k` ON `k`.`id` = `m`.`kota_id`
		LEFT JOIN `warehouse` `w` ON `w`.`id` = `k`.`warehouse_id`
		WHERE `s`.`member_id` <> 'company'
		AND s.description NOT IN('Request Order','RO From SC Payment')
		AND s.ewallet_stc_id IN (".$array_id.")
		ORDER BY `s`.`id` ASC";
		
		$query=$this->db->query($cSql);
		if ($query){	
            foreach($query->result_array() as $row){
                $cat_cust='STOCKIST';
				$cat_doc='Error';
				
				$BankId = '';
				$cat_doc='Error';
				$pay_meth='TRANSFER';
				$cre_deb='CREDIT';

				$deposit_id = '-';
				$withdrawal_id = '-';
				
				if($row['description']=='Withdrawal'){
					$cekWithdrawal="SELECT withdrawal.remark, 
					withdrawal.remarkapp, withdrawal.flag,withdrawal.event_id  
					FROM withdrawal WHERE id='".$row['noref']."'
					AND status='approved' ";
					
					$cekWithdrawal="SELECT withdrawal.remark, 
					withdrawal.remarkapp, withdrawal.flag
					,upper(withdrawal.event_id) as event_id
					,case when upper(event_id) = 'WD1' then 'User Request via BCA'
						when upper(event_id) = 'WD2' then 'User Request via MANDIRI'
						when upper(event_id) = 'WD3' then 'Ongkos Kirim'
						when upper(event_id) = 'WD4' then 'Biaya Admin'
						when upper(event_id) = 'WD5' then 'Withdrawal Koreksi (Cash)'
						when upper(event_id) = 'WD6' then 'Withdrawal Koreksi (BCA)'
						when upper(event_id) = 'WD7' then 'Withdrawal Koreksi (MANDIRI)'
						end AS notewdw
					,case when upper(event_id) = 'WD1' then 'A4'
						when upper(event_id) = 'WD2' then 'A4'
						when upper(event_id) = 'WD3' then 'A5'
						when upper(event_id) = 'WD4' then 'A6'
						when upper(event_id) = 'WD5' then 'A3'
						when upper(event_id) = 'WD6' then 'A3'
						when upper(event_id) = 'WD7' then 'A3'
						end AS cat_doc
					,case when upper(event_id) = 'WD1' then 'BCA'
						when upper(event_id) = 'WD2' then 'MANDIRI'
						when upper(event_id) = 'WD3' then 'BCA'
						when upper(event_id) = 'WD4' then 'BCA'
						when upper(event_id) = 'WD5' then 'BCA'
						when upper(event_id) = 'WD6' then 'BCA'
						when upper(event_id) = 'WD7' then 'MANDIRI'
						end AS bank_id
					FROM withdrawal WHERE id='".$row['noref']."'
					AND flag = 'stc'
					-- AND status='approved' 
					";
					$qrWithdrawal=$this->db->query($cekWithdrawal);
					if ($qrWithdrawal){
						foreach($qrWithdrawal->result_array() as $rowWth){
							$cat_doc = $rowWth['cat_doc'];
							$BankId = $rowWth['bank_id'];
							
							if($rowWth['remarkapp'] == 'Auto Withdrawal Bonus'){
								$row['description'] = $rowWth['remarkapp'];
							}
							else if($rowWth['remarkapp'] == 'Auto Withdrawal Write-Off'){
								$row['description'] = $rowWth['remarkapp'];
								$cat_doc = 'A9';
							}
							else{
								$row['description'] = $rowWth['notewdw'];
							}
						}
					}else{
						$row['description']="Undefined";
					}

					$withdrawal_id = $row['noref'];
				
				}
				
				if ($row['description'] == 'Top Up STC') { $cat_doc='A1'; }
				else if (strpos($row['description'], 'Transfer') !== false) { $cat_doc='A2'; }
				else if (strpos($row['description'], 'Bonus') !== false) { $cat_doc='A7'; }
				else if ($row['description'] == 'Auto Withdrawal Bonus') { $cat_doc='A8'; }


				if ($row['description'] == 'Top Up STC') {
					$deposit_id = $row['noref'];
				}
				
				$pph21=0;
				$web_dmbtr=0;
				$web_dmbtr_cor=0;
				
				if (empty($row['fdebet'])){
					$cre_deb='CREDIT';
					$web_dmbtr= str_replace(",","",$row['fkredit']);
				}else{
					$cre_deb='DEBIT';
					$pph21=0;
					$web_dmbtr= str_replace(",","",$row['fdebet']);
				}
				
				if (($cat_doc=='A1')){
						// Bonus Title 999=pph21 
						$getBankId="SELECT * FROM deposit WHERE id = ".$row['noref'];
						$qrBankId=$this->db->query($getBankId);
						foreach($qrBankId->result_array() as $rowBankId){
							$BankId=$rowBankId['bank_id'];
							if($rowBankId['transfer']>0) $pay_meth='TRANSFER';
							else if($rowBankId['tunai']>0) $pay_meth='TRANSFER';
							else if($rowBankId['debit_card']>0) $pay_meth='DEBIT_CARD';
							else if($rowBankId['credit_card']>0) $pay_meth='CREDIT_CARD';
							
						}
						
						//tes, nanti harus di tambah bank id di SO admin
						if($BankId=='')$BankId='BCA';
				}
				
				$remark = '-';
				if ($cat_doc=='A1'){$remark = 'transaksi A1 - Top up';}
				else if ($cat_doc=='A2'){$remark = 'transaksi A2 - Transfer e-wallet';}
				else if ($cat_doc=='A3'){$remark = 'transaksi A3 - Withdrawal koreksi';}
				else if ($cat_doc=='A4'){$remark = 'transaksi A4 - User request';}
				else if ($cat_doc=='A5'){$remark = 'transaksi A5 - Ongkos Kirim';}
				else if ($cat_doc=='A6'){$remark = 'transaksi A6 - Biaya Admin';}
				else if ($cat_doc=='A7'){$remark = 'transaksi A7 - Bonus';}
				else if ($cat_doc=='A8'){$remark = 'transaksi A8 - Withdrawal bonus';}
				else if ($cat_doc=='A9'){$remark = 'transaksi A9 - withdrawal others';}
				
				if ($cat_doc<>"Error"){
					$cRow=array();
					$cRow['doc_head']=$row['doc_head'];
					$cRow['doc_det']=$row['id'];
					$cRow['cat_cust']=$cat_cust;
					$cRow['doc_date']=$row['tgl'];
					$cRow['doc_post']=$row['tgl'];
					$cRow['kunnr']=$row['member_id'];
					$cRow['name_cus']=$row['name_cus'];
					$cRow['deposit_id']=$deposit_id;
					$cRow['withdrawal_id']=$withdrawal_id;
					$cRow['cat_doc']=$cat_doc;
					$cRow['WERKS']=$row['sap_code'];
					$cRow['bank1']=$BankId;//$row['bank_id'];
					$cRow['pay_meth']=$pay_meth;
					$cRow['cre_deb']=$cre_deb;
					$cRow['PPh_21']=$pph21;
					$cRow['WEB_DMBTR_COR']=$web_dmbtr_cor;
					$cRow['web_dmbtr']=$web_dmbtr;
					$cRow['remark']=$cat_doc.' '.$row['description'];
					$cRow['web_CPUDT']=date("d.m.Y");
					$cRow['web_CPUTM']=date("H:i:s");				
					
					$data[] = $cRow;

					$this->db->where('doc_det', $row['id']);
					$this->db->where('cat_cust','STOCKIST');
					$q = $this->db->get('temp_ewallet');
				
					if ( $q->num_rows() > 0 ) 
					{
						
					$this->db->where('doc_det', $row['id']);
					$this->db->where('cat_cust','STOCKIST');
					$this->db->delete('temp_ewallet');

					$this->db->insert('temp_ewallet', $cRow);

					} else {

						$this->db->insert('temp_ewallet', $cRow);

					}

				}
                
            }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return $data;

	}

 	public function update_data_ewallet_stc_by_array_id($array_id){
		
		/* Untuk Stockiest */
        $data=array();
		$cSql = "
		SELECT  s.ewallet_stc_id AS doc_head
		,s.id 
		,m.nama AS name_cus
		, s.noref
		, s.description
		, s.created
		, DATE_FORMAT(s.created, '%d.%m.%Y') AS tgl
		, DATE_FORMAT(s.created, '%H:%i:%s') AS jam
		, s.member_id, 
		m.kota_id, acc.bank_id AS bank_id, k.warehouse_id, w.sap_code, 
		s.createdby, FORMAT(s.saldoawal, 0)AS fsaldoawal, FORMAT(s.kredit, 0)AS fkredit, 
		FORMAT(s.debet, 0)AS fdebet, FORMAT(s.saldoakhir, 0)AS fsaldoakhir
		FROM `ewallet_stc_d` `s`
		LEFT JOIN `member` `m` ON `m`.`id` = `s`.`member_id`
		LEFT JOIN `account` `acc` ON `acc`.`id` = `m`.`account_id`
		LEFT JOIN `kota` `k` ON `k`.`id` = `m`.`kota_id`
		LEFT JOIN `warehouse` `w` ON `w`.`id` = `k`.`warehouse_id`
		WHERE `s`.`member_id` <> 'company'
		AND s.description NOT IN('Request Order','RO From SC Payment')
		AND s.ewallet_stc_id IN (".$array_id.")
		ORDER BY `s`.`id` ASC";
		
		$query=$this->db->query($cSql);
		if ($query){	
            foreach($query->result_array() as $row){
                $cat_cust='STOCKIST';
				$cat_doc='Error';
				
				$BankId = '';
				$cat_doc='Error';
				$pay_meth='TRANSFER';
				$cre_deb='CREDIT';

				$deposit_id = '-';
				$withdrawal_id = '-';
				
				if($row['description']=='Withdrawal'){
					$cekWithdrawal="SELECT withdrawal.remark, 
					withdrawal.remarkapp, withdrawal.flag,withdrawal.event_id  
					FROM withdrawal WHERE id='".$row['noref']."'
					AND status='approved' ";
					
					$cekWithdrawal="SELECT withdrawal.remark, 
					withdrawal.remarkapp, withdrawal.flag
					,upper(withdrawal.event_id) as event_id
					,case when upper(event_id) = 'WD1' then 'User Request via BCA'
						when upper(event_id) = 'WD2' then 'User Request via MANDIRI'
						when upper(event_id) = 'WD3' then 'Ongkos Kirim'
						when upper(event_id) = 'WD4' then 'Biaya Admin'
						when upper(event_id) = 'WD5' then 'Withdrawal Koreksi (Cash)'
						when upper(event_id) = 'WD6' then 'Withdrawal Koreksi (BCA)'
						when upper(event_id) = 'WD7' then 'Withdrawal Koreksi (MANDIRI)'
						end AS notewdw
					,case when upper(event_id) = 'WD1' then 'A4'
						when upper(event_id) = 'WD2' then 'A4'
						when upper(event_id) = 'WD3' then 'A5'
						when upper(event_id) = 'WD4' then 'A6'
						when upper(event_id) = 'WD5' then 'A3'
						when upper(event_id) = 'WD6' then 'A3'
						when upper(event_id) = 'WD7' then 'A3'
						end AS cat_doc
					,case when upper(event_id) = 'WD1' then 'BCA'
						when upper(event_id) = 'WD2' then 'MANDIRI'
						when upper(event_id) = 'WD3' then 'BCA'
						when upper(event_id) = 'WD4' then 'BCA'
						when upper(event_id) = 'WD5' then 'BCA'
						when upper(event_id) = 'WD6' then 'BCA'
						when upper(event_id) = 'WD7' then 'MANDIRI'
						end AS bank_id
					FROM withdrawal WHERE id='".$row['noref']."'
					AND flag = 'stc'
					-- AND status='approved' 
					";
					$qrWithdrawal=$this->db->query($cekWithdrawal);
					if ($qrWithdrawal){
						foreach($qrWithdrawal->result_array() as $rowWth){
							$cat_doc = $rowWth['cat_doc'];
							$BankId = $rowWth['bank_id'];
							
							if($rowWth['remarkapp'] == 'Auto Withdrawal Bonus'){
								$row['description'] = $rowWth['remarkapp'];
							}
							else if($rowWth['remarkapp'] == 'Auto Withdrawal Write-Off'){
								$row['description'] = $rowWth['remarkapp'];
								$cat_doc = 'A9';
							}
							else{
								$row['description'] = $rowWth['notewdw'];
							}
						}
					}else{
						$row['description']="Undefined";
					}

					$withdrawal_id = $row['noref'];
				
				}
				
				if ($row['description'] == 'Top Up STC') { $cat_doc='A1'; }
				else if (strpos($row['description'], 'Transfer') !== false) { $cat_doc='A2'; }
				else if (strpos($row['description'], 'Bonus') !== false) { $cat_doc='A7'; }
				else if ($row['description'] == 'Auto Withdrawal Bonus') { $cat_doc='A8'; }


				if ($row['description'] == 'Top Up STC') {
					$deposit_id = $row['noref'];
				}
				
				$pph21=0;
				$web_dmbtr=0;
				$web_dmbtr_cor=0;
				
				if (empty($row['fdebet'])){
					$cre_deb='CREDIT';
					$web_dmbtr= str_replace(",","",$row['fkredit']);
				}else{
					$cre_deb='DEBIT';
					$pph21=0;
					$web_dmbtr= str_replace(",","",$row['fdebet']);
				}
				
				if (($cat_doc=='A1')){
						// Bonus Title 999=pph21 
						$getBankId="SELECT * FROM deposit WHERE id = ".$row['noref'];
						$qrBankId=$this->db->query($getBankId);
						foreach($qrBankId->result_array() as $rowBankId){
							$BankId=$rowBankId['bank_id'];
							if($rowBankId['transfer']>0) $pay_meth='TRANSFER';
							else if($rowBankId['tunai']>0) $pay_meth='TRANSFER';
							else if($rowBankId['debit_card']>0) $pay_meth='DEBIT_CARD';
							else if($rowBankId['credit_card']>0) $pay_meth='CREDIT_CARD';
							
						}
						
						//tes, nanti harus di tambah bank id di SO admin
						if($BankId=='')$BankId='BCA';
				}
				
				$remark = '-';
				if ($cat_doc=='A1'){$remark = 'transaksi A1 - Top up';}
				else if ($cat_doc=='A2'){$remark = 'transaksi A2 - Transfer e-wallet';}
				else if ($cat_doc=='A3'){$remark = 'transaksi A3 - Withdrawal koreksi';}
				else if ($cat_doc=='A4'){$remark = 'transaksi A4 - User request';}
				else if ($cat_doc=='A5'){$remark = 'transaksi A5 - Ongkos Kirim';}
				else if ($cat_doc=='A6'){$remark = 'transaksi A6 - Biaya Admin';}
				else if ($cat_doc=='A7'){$remark = 'transaksi A7 - Bonus';}
				else if ($cat_doc=='A8'){$remark = 'transaksi A8 - Withdrawal bonus';}
				else if ($cat_doc=='A9'){$remark = 'transaksi A9 - withdrawal others';}
				
				if ($cat_doc<>"Error"){
					$cRow=array();
					$cRow['doc_head']=$row['doc_head'];
					$cRow['doc_det']=$row['id'];
					$cRow['cat_cust']=$cat_cust;
					$cRow['doc_date']=$row['tgl'];
					$cRow['doc_post']=$row['tgl'];
					$cRow['kunnr']=$row['member_id'];
					$cRow['name_cus']=$row['name_cus'];
					$cRow['deposit_id']=$deposit_id;
					$cRow['withdrawal_id']=$withdrawal_id;
					$cRow['cat_doc']=$cat_doc;
					$cRow['WERKS']=$row['sap_code'];
					$cRow['bank1']=$BankId;//$row['bank_id'];
					$cRow['pay_meth']=$pay_meth;
					$cRow['cre_deb']=$cre_deb;
					$cRow['PPh_21']=$pph21;
					$cRow['WEB_DMBTR_COR']=$web_dmbtr_cor;
					$cRow['web_dmbtr']=$web_dmbtr;
					$cRow['remark']=$cat_doc.' '.$row['description'];
					$cRow['web_CPUDT']=date("d.m.Y");
					$cRow['web_CPUTM']=date("H:i:s");
					
					$data=array(
						'status' => 'Success Send Data'
					);
						
					$this->db->update('temp_ewallet',$data,array('doc_det'=>$row['id'], 'cat_cust'=>'STOCKIST'));
				
				
					$data[] = $cRow;
				}
                
            }
		}else{
			$cRow=array();
			$cRow['id']='0';
			$cRow['data'][]='No Data';
			$data[]=$cRow;
		}
		
		return true;

	}

	public function delete_temp_ewallet($data_arr){

		$this->db->where_in('doc_head', $data_arr);
		$this->db->delete('temp_ewallet');
		
		return true;
	}	
	
	public function writeLogApi($kategori,$deskripsi){
		$data_log_api=array(
			'log_api_tgl' => date("Y-m-d H:i:s"),
			'log_api_ket' => $deskripsi,
			'log_api_kategori' => $kategori 
		);
		$this->db->insert('log_api',$data_log_api);
		
	}	
    
}
?>
